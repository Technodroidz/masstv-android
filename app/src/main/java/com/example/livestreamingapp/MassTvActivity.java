package com.example.livestreamingapp;

import android.content.Context;
import android.content.Intent;
import android.media.AudioManager;
import android.net.Uri;
import android.os.Bundle;
import android.os.Handler;
import android.util.Log;
import android.view.KeyEvent;
import android.widget.Toast;

import androidx.appcompat.app.AppCompatActivity;
import androidx.fragment.app.FragmentManager;

import com.example.livestreamingapp.home.ui.HomeDraweerStreamingActivity;
import com.example.livestreamingapp.login.MultipleLoginSelectionActivity;
import com.example.livestreamingapp.model.OnDemandVideo;
import com.example.livestreamingapp.model.ProfileData;
import com.example.livestreamingapp.model.VideoByIdResponse;
import com.example.livestreamingapp.utils.BottomSpinnerFragment;
import com.example.livestreamingapp.utils.Constants;
import com.example.livestreamingapp.utils.CustomProgressDialog;
import com.example.livestreamingapp.utils.SharedPreferenceHandler;
import com.example.livestreamingapp.video.VideoTrailerActivity;
import com.example.livestreamingapp.webservice.APIClient;
import com.example.livestreamingapp.webservice.APIInterface;
import com.google.android.gms.auth.api.signin.GoogleSignIn;
import com.google.android.gms.auth.api.signin.GoogleSignInAccount;
import com.google.android.gms.auth.api.signin.GoogleSignInClient;
import com.google.android.gms.auth.api.signin.GoogleSignInOptions;

import java.util.HashMap;
import java.util.Map;

import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;

public class MassTvActivity extends AppCompatActivity {

    private GoogleSignInClient mGoogleSignInClient;
    private String deepLink;
    private CustomProgressDialog customProgressDialog;
    private APIInterface apiInterface;
    private AudioManager amanager;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
       /* getWindow().setFlags(WindowManager.LayoutParams.FLAG_SECURE,
                WindowManager.LayoutParams.FLAG_SECURE);*/
        amanager = (AudioManager) getSystemService(Context.AUDIO_SERVICE);
        customProgressDialog = new CustomProgressDialog(MassTvActivity.this);
        customProgressDialog.setCancelable(false);
        customProgressDialog.setMessage("Please wait...");
        apiInterface = APIClient.getClient().create(APIInterface.class);

        GoogleSignInOptions gso = new GoogleSignInOptions.Builder(GoogleSignInOptions.DEFAULT_SIGN_IN)
                .requestEmail()
                .build();

        mGoogleSignInClient = GoogleSignIn.getClient(this, gso);
    }

    public void signOut() {
        try {
            mGoogleSignInClient.signOut();
            startActivity(new Intent(MassTvActivity.this, MultipleLoginSelectionActivity.class));
            finish();
        } catch (Exception exception) {
            exception.printStackTrace();
        }
    }


    public void loginUser() {
        try {
            // Check for existing Google Sign In account, if the user is already signed in
// the GoogleSignInAccount will be non-null.
            GoogleSignInAccount account = GoogleSignIn.getLastSignedInAccount(this);
            updateUI(account);
        } catch (Exception exception) {
            exception.printStackTrace();
            Toast.makeText(this,"error",Toast.LENGTH_SHORT);

        }

    }

    private void updateUI(GoogleSignInAccount account) {
        if (account != null && account.getEmail() != null) {

            deepLink = new SharedPreferenceHandler(MassTvActivity.this).getDeepLink();

            if (deepLink != null && deepLink.contains("%CE%BC") && deepLink.length() > 10) {
                String videoId = deepLink.split("%CE%BC")[1];
                String category = deepLink.split("%CE%BC")[2];
                openVideoUsingReferLink(videoId, category, account);
            } else {
                startActivity(new Intent(MassTvActivity.this, HomeDraweerStreamingActivity.class)
                        .setFlags(Intent.FLAG_ACTIVITY_NEW_TASK | Intent.FLAG_ACTIVITY_CLEAR_TASK));
                finish();
            }

        } else {

            deepLink = new SharedPreferenceHandler(MassTvActivity.this).getDeepLink();
            if (deepLink != null && deepLink.contains("%CE%BC") && deepLink.length() > 10) {
                String videoId = deepLink.toString().split("%CE%BC")[1];
                String category = deepLink.toString().split("%CE%BC")[2];
                openVideoUsingReferLink(videoId, category, account);
            } else {
                startActivity(new Intent(MassTvActivity.this, MultipleLoginSelectionActivity.class));
                finish();
            }


        }
    }

    private void openVideoUsingReferLink(String videoId, String category, GoogleSignInAccount account) {
        new SharedPreferenceHandler(MassTvActivity.this).saveDeepLink("null");
        customProgressDialog.show();
        Map<String, String> map = new HashMap<>();
        map.put("video_id", videoId);
        if (category.equalsIgnoreCase("Original")) {
            map.put("video_category", category);
        } else {
            map.put("video_category", category);
        }
        Call<VideoByIdResponse> call = apiInterface.getVideoById(map);
        call.enqueue(new Callback<VideoByIdResponse>() {
            @Override
            public void onResponse(Call<VideoByIdResponse> call, Response<VideoByIdResponse> response) {


                new Handler().postDelayed(new Runnable() {
                    @Override
                    public void run() {
                        customProgressDialog.dismiss();
                        OnDemandVideo onDemandVideo = response.body().getOnDemandVideo();
                        onDemandVideo.setSource(onDemandVideo.getSource_url());
                        onDemandVideo.setSrt_url(onDemandVideo.getSrt_url());
                        startActivity(new Intent(MassTvActivity.this,
                                VideoTrailerActivity.class).setFlags(Intent.FLAG_ACTIVITY_NEW_TASK | Intent.FLAG_ACTIVITY_CLEAR_TASK).
                                putExtra(Constants.SELECTED_ON_DEMAND_VIDEO, response.body().getOnDemandVideo()));
                    }
                }, 300);


            }

            @Override
            public void onFailure(Call<VideoByIdResponse> call, Throwable t) {

                customProgressDialog.dismiss();

                if (account != null && account.getEmail() != null) {
                    startActivity(new Intent(MassTvActivity.this, HomeDraweerStreamingActivity.class)
                            .setFlags(Intent.FLAG_ACTIVITY_NEW_TASK | Intent.FLAG_ACTIVITY_CLEAR_TASK));
                    finish();
                } else {
                    startActivity(new Intent(MassTvActivity.this, MultipleLoginSelectionActivity.class));
                    finish();
                }


            }
        });
    }


    public void removeSpinnerFragment() {
        FragmentManager fm = getSupportFragmentManager();
        fm.popBackStack(BottomSpinnerFragment.class.getName(), FragmentManager.POP_BACK_STACK_INCLUSIVE);
    }

    @Override
    public boolean onKeyDown(int keyCode, KeyEvent event) {

        if ((keyCode == KeyEvent.KEYCODE_VOLUME_DOWN)) {
            //Do something
          //  unMuteAudio();
            if(amanager!=null){
                int currentVolume = amanager.getStreamVolume(AudioManager.STREAM_MUSIC);
                int maxVolume = amanager.getStreamMaxVolume(AudioManager.STREAM_MUSIC);

                onVolumeButtonPress(currentVolume,maxVolume);
            }

        }if ((keyCode==KeyEvent.KEYCODE_VOLUME_UP)){
            if(amanager!=null){
                int currentVolume = amanager.getStreamVolume(AudioManager.STREAM_MUSIC);
                int maxVolume = amanager.getStreamMaxVolume(AudioManager.STREAM_MUSIC);
                onVolumeButtonPress(currentVolume,maxVolume);
            }
        }

        return super.onKeyDown(keyCode,event);


    }

    /**
     * Mute Audio
     */
    public void muteAudio() {

    }

    /**
     * Un Mute audio of video players
     */
    public  void onVolumeButtonPress(int currentVolume,int maxVolume){

    }
}