package com.example.livestreamingapp;

import android.app.Activity;
import android.app.Application;
import android.content.IntentFilter;
import android.view.WindowManager;

import androidx.localbroadcastmanager.content.LocalBroadcastManager;

import com.example.livestreamingapp.faq.ActivityTermCondition;
import com.example.livestreamingapp.firebase.LocalBroadCastReceiver;
import com.example.livestreamingapp.utils.ConnectivityReceiver;
import com.stripe.jetbrains.annotations.NotNull;
import com.stripe.stripeterminal.TerminalLifecycleObserver;

import java.util.TimeZone;

public class MassTvApplication extends Application {
    @NotNull
    private TerminalLifecycleObserver observer;
    private static MassTvApplication mInstance;
    private ConnectivityReceiver connectivityReceiver;
    private LocalBroadCastReceiver localBroadCastReceiver;


    @Override
    public void onCreate() {
        super.onCreate();
        mInstance = this;
        TimeZone.setDefault(TimeZone.getTimeZone("America/Los_Angeles"));

    }

    public static synchronized MassTvApplication getInstance() {
        return mInstance;
    }

    public void registerLocalReceiver(LocalBroadCastReceiver.localBroadCastListener localBroadCast){
        try {
            unregisterReceiver(localBroadCastReceiver);
        }catch (Exception e){
            e.printStackTrace();
        }
        localBroadCastReceiver = new LocalBroadCastReceiver(localBroadCast);
        IntentFilter filter = new IntentFilter();
        filter.addAction("com.intent.notification");
        LocalBroadcastManager.getInstance(this).registerReceiver(localBroadCastReceiver,filter);

    }

    public void unRegisteredLocalReceiver(){
        try {
            LocalBroadcastManager.getInstance(this).unregisterReceiver(localBroadCastReceiver);
        }catch (Exception e){
            e.printStackTrace();
        }
    }

    public void setConnectivityListener(ConnectivityReceiver.ConnectivityReceiverListener listener) {
        try {
            unregisterReceiver(connectivityReceiver);
        }catch (Exception e){

        }
        connectivityReceiver = new ConnectivityReceiver(listener);
        IntentFilter filter = new IntentFilter();
        filter.addAction("android.net.conn.CONNECTIVITY_CHANGE");
        registerReceiver(connectivityReceiver, filter);

        //ConnectivityReceiver.connectivityReceiverListener = listener;

    }

    public void setUnRegister() {
        //ConnectivityReceiver.connectivityReceiverListener = listener;
        unregisterReceiver(connectivityReceiver);

    }
}