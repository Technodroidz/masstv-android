package com.example.livestreamingapp;

import androidx.appcompat.app.AppCompatActivity;

import android.content.Intent;
import android.os.Bundle;

import com.example.livestreamingapp.home.ui.HomeDraweerStreamingActivity;
import com.example.livestreamingapp.login.LoginWithEmailActivity;
import com.example.livestreamingapp.login.SplashActivity;
import com.example.livestreamingapp.model.ProfileData;
import com.example.livestreamingapp.ondemand.OnDemandCategoryActivity;
import com.example.livestreamingapp.upload.UploadContentActivity;
import com.example.livestreamingapp.utils.SharedPreferenceHandler;

public class ShortCutActivity extends MassTvActivity {

    private ProfileData profileData;
    private Intent intentCreateContent;
    private Intent intentOnDemand;
    private Intent intentLive;
    private int value;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_short_cut);

        profileData = new SharedPreferenceHandler(ShortCutActivity.this).getProfileInfo();
        value = getIntent().getIntExtra("value",0);

        if(profileData==null){
            intentCreateContent = new Intent(Intent.ACTION_VIEW, null, this, LoginWithEmailActivity.class);
            intentOnDemand = new Intent(Intent.ACTION_VIEW, null, this, LoginWithEmailActivity.class);
            intentLive = new Intent(Intent.ACTION_VIEW, null, this, LoginWithEmailActivity.class);
        }else {
            intentCreateContent = new Intent(Intent.ACTION_VIEW, null, this, UploadContentActivity.class);
            intentOnDemand = new Intent(Intent.ACTION_VIEW, null, this, OnDemandCategoryActivity.class);
            intentLive = new Intent(Intent.ACTION_VIEW, null, this, HomeDraweerStreamingActivity.class);
        }

        switch (value){
            case 0:
                startActivity(intentCreateContent);
                finish();
                break;
            case 1:
                startActivity(intentOnDemand);
                finish();
                break;
            case 2:
                startActivity(intentLive);
                finish();
                break;
        }

    }
}