package com.example.livestreamingapp.contact;

import android.content.Context;
import android.content.DialogInterface;
import android.graphics.Color;
import android.os.Bundle;

import androidx.annotation.NonNull;
import androidx.appcompat.app.AlertDialog;
import androidx.fragment.app.Fragment;

import android.os.Handler;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.view.inputmethod.InputMethodManager;
import android.widget.Button;
import android.widget.EditText;
import android.widget.RelativeLayout;
import android.widget.TextView;

import com.example.livestreamingapp.R;
import com.example.livestreamingapp.model.LoginResponse;
import com.example.livestreamingapp.model.ProfileData;
import com.example.livestreamingapp.model.query.MyQuery;
import com.example.livestreamingapp.model.query.QueryResponseModel;
import com.example.livestreamingapp.model.query.QuerySubmitResponseModel;
import com.example.livestreamingapp.profile.EditProfileInfoActivity;
import com.example.livestreamingapp.utils.ConnectivityReceiver;
import com.example.livestreamingapp.utils.Constants;
import com.example.livestreamingapp.utils.SharedPreferenceHandler;
import com.example.livestreamingapp.webservice.APIClient;
import com.example.livestreamingapp.webservice.APIInterface;
import com.google.android.material.snackbar.Snackbar;
import com.google.gson.Gson;

import java.util.HashMap;
import java.util.Map;

import javax.annotation.Nullable;

import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;

/**
 * A simple {@link Fragment} subclass.
 * create an instance of this fragment.
 */
public class CreateQueryFragment extends Fragment implements View.OnClickListener {

    private EditText et_query;
    public APIInterface apiInterface;
    private View view;
    private RelativeLayout waitScreen;
    private Button btn_submit;
    private TextView tv_progress;

    public CreateQueryFragment() {
        // Required empty public constructor
    }


    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        // Inflate the layout for this fragment
         view = inflater.inflate(R.layout.fragment_create_query, container, false);
        initView(view);
        return view;
    }

    private void initView(View view) {
        waitScreen = view.findViewById(R.id.waitScreen);
        et_query = view.findViewById(R.id.et_query);
        tv_progress = view.findViewById(R.id.tv_progress);
        tv_progress.setText("Please wait...");
        apiInterface = APIClient.getClient().create(APIInterface.class);
         btn_submit = view.findViewById(R.id.btn_submit);
         btn_submit.setOnClickListener(this);



    }



    /**
     * @Rehan After api hit successfully we will get the LoginResponse object In LoginResponse if
     * status is 1 that means otp successfully send to to the entered email address
    if status is 0 it means something went wrong.
     */
    private void AttemptSubmitQuery(final String name) {

        hideKeyboard(et_query,getContext());

        waitScreen.setVisibility(View.VISIBLE);

        final ProfileData data = new SharedPreferenceHandler(getContext()).getProfileInfo();

        Map<String, String> credential = new HashMap<>();
        credential.put("query", name);
        credential.put("user_id",""+data.getId());
        // credential.put("profile_picture", null);
        Call<QuerySubmitResponseModel> editProfile = apiInterface.submitQuery(credential);

        // progressDialog.show();

        editProfile.enqueue(
                new Callback<QuerySubmitResponseModel>() {
                    @Override
                    public void onResponse(
                            Call<QuerySubmitResponseModel> call, Response<QuerySubmitResponseModel> response) {
                        // progressDialog.dismiss();
                       // waitScreen.setVisibility(View.GONE);

                        if (response.code() == 200) {
                            waitScreen.setVisibility(View.GONE);
                            if (response.body().getSuccess()) {
                                AlertDialog.Builder builder = new AlertDialog.Builder(getContext(), R.style.AlertDialogStyle)
                                        .setTitle("Success")
                                        .setCancelable(false)
                                        .setMessage("User Query Captured Successfully!");
                                builder.setPositiveButton("Ok", new DialogInterface.OnClickListener() {
                                    @Override
                                    public void onClick(DialogInterface dialog, int which) {
                                        et_query.setText("");

                                    }
                                });
                                builder.create().show();
                            } else {
                                new AlertDialog.Builder(getContext(),R.style.AlertDialogStyle)
                                        .setCancelable(false)
                                        .setMessage(getString(R.string.something_went_wrong))
                                        .setPositiveButton(
                                                "Ok",
                                                new DialogInterface.OnClickListener() {
                                                    @Override
                                                    public void onClick(
                                                            DialogInterface dialog, int which) {}
                                                })
                                        .show();
                            }


                        }
                    }

                    @Override
                    public void onFailure(Call<QuerySubmitResponseModel> call, Throwable t) {
                        try {
                            waitScreen.setVisibility(View.GONE);
                            new AlertDialog.Builder(getContext(),R.style.AlertDialogStyle)
                                    .setCancelable(false)
                                    .setMessage("" + t.getMessage())
                                    .setPositiveButton(
                                            "Ok",
                                            new DialogInterface.OnClickListener() {
                                                @Override
                                                public void onClick(
                                                        DialogInterface dialog, int which) {

                                                    try {
                                                        QueryPagerActivity.UPDATE = true;

                                                    }catch (Exception e){

                                                    }
                                                }
                                            })
                                    .show();
                        }catch (Exception e){

                        }

                    }
                });
    }

    @Override
    public void onClick(View v) {
        switch (v.getId()){
            case R.id.btn_submit:
                if(et_query!=null && et_query.length()>15){
                    if(checkConnection()){
                        AttemptSubmitQuery(et_query.getText().toString());
                    }else {
                        showSnack(checkConnection());
                    }  
                }else {
                    displayAlert("Validation Error","Query length must be at least 15 character!");

                }
                break;
        }
    }

    private void hideKeyboard(View view, Context context)
    {

        if (view != null) {

            // now assign the system
            // service to InputMethodManager
            InputMethodManager manager
                    = (InputMethodManager)
                    context.getSystemService(
                            Context.INPUT_METHOD_SERVICE);
            manager.hideSoftInputFromWindow(
                            view.getWindowToken(), 0);
        }
    }


    // Showing the status in Snackbar for intenet connectivity
    private void showSnack(boolean isConnected) {
        String message;
        int color;
        if (isConnected) {
            message = "Connection back Online!";
            color = Color.WHITE;
        } else {
            message = "Offline! Not connected to internet";
            color = Color.RED;


        }
        Snackbar snackbar = Snackbar
                .make(view.findViewById(R.id.parent), message, Snackbar.LENGTH_LONG);
        View sbView = snackbar.getView();
        TextView textView = (TextView) sbView.findViewById(R.id.snackbar_text);
        textView.setTextColor(color);
        snackbar.show();

    }


    /**
     * Check internet connection Manually
     */
    private boolean checkConnection() {
        return ConnectivityReceiver.isConnected();
    }

    private void displayAlert(@NonNull String title,
                              @Nullable String message) {
        AlertDialog.Builder builder = new AlertDialog.Builder(getContext(), R.style.AlertDialogStyle)
                .setTitle(title)
                .setMessage(message);
        builder.setPositiveButton("Ok", null);
        builder.create().show();
    }

    private void showAlert() {
        new AlertDialog.Builder(getContext(), R.style.AlertDialogStyle)
                .setCancelable(false)
                .setMessage("Work In Progress!")
                .setPositiveButton(
                        "Ok",
                        new DialogInterface.OnClickListener() {
                            @Override
                            public void onClick(
                                    DialogInterface dialog, int which) {
                            }
                        })
                .show();
    }

}