package com.example.livestreamingapp.contact;

import android.content.DialogInterface;
import android.graphics.Color;
import android.os.Bundle;
import android.os.Handler;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.RelativeLayout;
import android.widget.TextView;

import androidx.appcompat.app.AlertDialog;
import androidx.fragment.app.Fragment;
import androidx.recyclerview.widget.LinearLayoutManager;
import androidx.recyclerview.widget.RecyclerView;

import com.example.livestreamingapp.R;
import com.example.livestreamingapp.model.ProfileData;
import com.example.livestreamingapp.model.query.MyQuery;
import com.example.livestreamingapp.model.query.QueryResponseModel;
import com.example.livestreamingapp.utils.ConnectivityReceiver;
import com.example.livestreamingapp.utils.SharedPreferenceHandler;
import com.example.livestreamingapp.webservice.APIClient;
import com.example.livestreamingapp.webservice.APIInterface;
import com.google.android.material.snackbar.Snackbar;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;


/**
 * @Rehan_Ali
 * create an instance of this fragment.
 */
public class ListQueryFragment extends Fragment
        implements ConnectivityReceiver.ConnectivityReceiverListener{


    public APIInterface apiInterface;
    private View view;
    private RelativeLayout waitScreen;
    private RecyclerView rv_query;
    public MyQueryAdapter adapter;

    private boolean reload = true;
    private static Handler handler;
    private  static  Runnable runnable;
    public  List<MyQuery> queries;
    private TextView tv_progress;

    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);

    }

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        // Inflate the layout for this fragment
         view =  inflater.inflate(R.layout.fragment_list_query, container, false);
        initViews(view);
        return view;

    }

    private void initViews(View view) {
        waitScreen = view.findViewById(R.id.waitScreen);
        tv_progress = view.findViewById(R.id.tv_progress);
        tv_progress.setText("Please wait...");
        apiInterface = APIClient.getClient().create(APIInterface.class);
        rv_query = view.findViewById(R.id.rv_query);
        LinearLayoutManager  manager = new LinearLayoutManager(getContext());
        rv_query.setLayoutManager(manager);
         adapter = new MyQueryAdapter(getContext(),new ArrayList<>());
        rv_query.setAdapter(adapter);


        // search for new live video if live show is null
        handler = new Handler();
        runnable  = new Runnable() {
            @Override
            public void run() {
                if (QueryPagerActivity.UPDATE && waitScreen.getVisibility()!=View.VISIBLE) {
                    getQuery();
                    stopHandler();
                    QueryPagerActivity.UPDATE = false;
                }
            }
        };
      //  getQuery();

    }

    @Override
    public void onResume() {
        super.onResume();
        if(checkConnection()){
            getQuery();
        }else {
            showSnack(false);
        }

    }



    // Showing the status in Snackbar for intenet connectivity
    private void showSnack(boolean isConnected) {
        String message;
        int color;
        if (isConnected) {
            message = "Connection back Online!";
            color = Color.WHITE;
        } else {
            message = "Offline! Not connected to internet";
            color = Color.RED;


        }
        Snackbar snackbar = Snackbar
                .make(view.findViewById(R.id.parent), message, Snackbar.LENGTH_LONG);
        View sbView = snackbar.getView();
        TextView textView = (TextView) sbView.findViewById(R.id.snackbar_text);
        textView.setTextColor(color);
        snackbar.show();

    }


    /**
     * Check internet connection Manually
     */
    private boolean checkConnection() {
        return ConnectivityReceiver.isConnected();
    }


    private void getQuery() {

       reload = false;

       if(waitScreen!=null){
           waitScreen.setVisibility(View.VISIBLE);
       }

        final ProfileData data = new SharedPreferenceHandler(getContext()).getProfileInfo();

        Map<String, String> credential = new HashMap<>();
        credential.put("user_id",""+data.getId());
        // credential.put("profile_picture", null);
        Call<QueryResponseModel> editProfile = apiInterface.getQuery(credential);

        // progressDialog.show();

        editProfile.enqueue(
                new Callback<QueryResponseModel>() {
                    @Override
                    public void onResponse(
                            Call<QueryResponseModel> call, Response<QueryResponseModel> response) {
                        // progressDialog.dismiss();
                        if(waitScreen!=null){
                            waitScreen.setVisibility(View.GONE);
                        }

                        if (response.code() == 200) {

                            if (response.body().getSuccess()) {
                                if(queries!=null && queries.size()>0){
                                    queries.clear();
                                }
                                reload = true;
                                queries = new ArrayList<>();
                                queries.addAll(response.body().getData());

                                adapter = new MyQueryAdapter(getContext(),queries);
                                rv_query.setAdapter(adapter);
                                adapter.notifyDataSetChanged();

                            } else {
                                new AlertDialog.Builder(getContext(),R.style.AlertDialogStyle)
                                        .setCancelable(false)
                                        .setMessage(getString(R.string.something_went_wrong))
                                        .setPositiveButton(
                                                "Ok",
                                                new DialogInterface.OnClickListener() {
                                                    @Override
                                                    public void onClick(
                                                            DialogInterface dialog, int which) {}
                                                })
                                        .show();
                            }


                        }
                    }

                    @Override
                    public void onFailure(Call<QueryResponseModel> call, Throwable t) {
                        try {
                            if(waitScreen!=null){
                                waitScreen.setVisibility(View.GONE);
                            }
                            new AlertDialog.Builder(getContext(),R.style.AlertDialogStyle)
                                    .setCancelable(false)
                                    .setMessage("" + t.getMessage())
                                    .setPositiveButton(
                                            "Ok",
                                            new DialogInterface.OnClickListener() {
                                                @Override
                                                public void onClick(
                                                        DialogInterface dialog, int which) {}
                                            })
                                    .show();
                        }catch (Exception e){

                        }

                    }
                });
    }

    @Override
    public void onNetworkConnectionChanged(boolean isConnected) {
        if(isConnected && (queries==null || queries.size()==0)){
            getQuery();
        }
    }

    @Override
    public void onPause() {
        super.onPause();
        stopHandler();
    }



    /**
     * Remove the Controller hide handler if not necessary
     */
    public void stopHandler() {
        try {
            handler.removeCallbacks(runnable);
        } catch (Exception e) {

        }
    }

    /**
     * start the handler so that if we play any media and donot do any interaction
     * then i will hide after 3 second we can set any amount of time
     */
    public  void startHandler() {
        getQuery();
        adapter = new MyQueryAdapter(getContext(),queries);
        rv_query.setAdapter(adapter);
        adapter.notifyDataSetChanged();

    }

    @Override
    public void setUserVisibleHint(boolean isVisibleToUser) {
        super.setUserVisibleHint(isVisibleToUser);
        if(isVisibleToUser){
            getQuery();
        }
    }
}