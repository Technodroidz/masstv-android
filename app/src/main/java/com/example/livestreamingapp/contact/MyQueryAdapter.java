package com.example.livestreamingapp.contact;

import android.content.Context;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.TextView;

import androidx.annotation.NonNull;
import androidx.recyclerview.widget.RecyclerView;

import com.example.livestreamingapp.R;
import com.example.livestreamingapp.model.query.MyQuery;

import java.util.List;

public class MyQueryAdapter extends RecyclerView.Adapter<MyQueryAdapter.QueryHolder> {
    private final List<MyQuery> queryList;
    private final Context context;

    public MyQueryAdapter(Context context, List<MyQuery> queryList){
        this.context =context;
        this.queryList = queryList;
    }
    @NonNull
    @Override
    public QueryHolder onCreateViewHolder(@NonNull ViewGroup parent, int viewType) {
        View view = LayoutInflater.from(context).inflate(R.layout.item_list_query,parent,false);
        return new QueryHolder(view);
    }

    @Override
    public void onBindViewHolder(@NonNull QueryHolder holder, int position) {

        holder.tv_query.setText(""+queryList.get(position).getQuery());
        if(queryList.get(position).getResponse()==null|| queryList.get(position).getResponse().length()==0){
            holder.tv_response.setText("No response yet");
        }else {
            holder.tv_response.setText(""+queryList.get(position).getResponse());
        }

    }

    @Override
    public int getItemCount() {
        return queryList.size();
    }

    public class QueryHolder extends RecyclerView.ViewHolder{
        TextView tv_query;
        TextView tv_response;
        public QueryHolder(@NonNull View itemView) {
            super(itemView);
            tv_query = itemView.findViewById(R.id.tv_query);
            tv_response = itemView.findViewById(R.id.tv_response);
        }
    }
}
