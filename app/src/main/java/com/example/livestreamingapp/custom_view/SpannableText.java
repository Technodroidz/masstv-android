package com.example.livestreamingapp.custom_view;

import android.content.Context;
import android.content.DialogInterface;
import android.content.Intent;
import android.graphics.Color;
import android.net.Uri;
import android.text.Spannable;
import android.text.SpannableString;
import android.text.Spanned;
import android.text.TextPaint;
import android.text.method.LinkMovementMethod;
import android.text.style.ClickableSpan;
import android.text.style.ForegroundColorSpan;
import android.view.View;
import android.widget.TextView;

import androidx.annotation.NonNull;
import androidx.appcompat.app.AlertDialog;

import com.example.livestreamingapp.R;
import com.example.livestreamingapp.utils.FileUtils;

public class SpannableText {
    public static void setMessageWithClickableLink(Context context, TextView textView, int start1, int end1,int start2,int end2) {
        //The text and the URL
        String content = textView.getText().toString();
        //Clickable Span will help us to make clickable a text

        ClickableSpan clickableSpan1 = new ClickableSpan() {
            @Override
            public void onClick(@NonNull View widget) {
                // show term and condition
                showTermOfServiceAlert(context);
            }

            @Override
            public void updateDrawState(@NonNull TextPaint ds) {
                super.updateDrawState(ds);
            }
        };

        String url = "https://masstv.app/policy";
        Intent intentBrowser = new Intent(Intent.ACTION_VIEW);
        Uri u = Uri.parse(url);

        ClickableSpan clickableSpan2 = new ClickableSpan() {
            @Override
            public void onClick(@NonNull View widget) {
               // new FileUtils().showPdf(context);
               // FileUtils.copyFile(context,"policy");
                /// TODO: 12-11-2021
                intentBrowser.setData(u);
                try {
                    context.startActivity(intentBrowser);
                }catch (Exception e){

                }

            }

            @Override
            public void updateDrawState(@NonNull TextPaint ds) {
                super.updateDrawState(ds);
            }
        };

        //SpannableString will be created with the full content and
        // the clickable content all together
        SpannableString spannableString = new SpannableString(content);
        //only the word 'link' is clickable
        spannableString.setSpan(clickableSpan1, start1, end1, Spanned.SPAN_EXCLUSIVE_EXCLUSIVE);
        spannableString.setSpan(clickableSpan2, start2, end2, Spanned.SPAN_EXCLUSIVE_EXCLUSIVE);
        spannableString.setSpan(new ForegroundColorSpan(context.getResources().getColor(R.color.green)), start1, end1, Spannable.SPAN_EXCLUSIVE_EXCLUSIVE);
        spannableString.setSpan(new ForegroundColorSpan(context.getResources().getColor(R.color.green)), start2,end2, Spannable.SPAN_EXCLUSIVE_EXCLUSIVE);

        //The following is to set the new text in the TextView
        //no styles for an already clicked link
        textView.setText(spannableString);
        textView.setMovementMethod(LinkMovementMethod.getInstance());
        textView.setHighlightColor(Color.TRANSPARENT);
    }

    /**
     * @Rehan Term of service and privacy policy alert
     */
    private static void showTermOfServiceAlert(Context context) {
        new AlertDialog.Builder(context, R.style.AlertDialogStyle).setCancelable(false)
                .setTitle("Welcome to MASS TV")
                .setMessage(
                        "1. DESCRIPTION OF SERVICE AND ACCEPTANCE OF TERMS OF USE \n" +
                                "\n" +
                                "Mass TV (\"we\" or \"us\") provides an online video service offering a selection of shows, movies, clips, originals and other content (collectively, the \"Content\"). Our video service, the Content, our player for viewing the Content (the \"Video Player\") and any other features, tools, applications, materials, or other services offered from time to time by Mass TV in connection with its business, however accessed, are referred to collectively as the \"Mass TV Services.\" \n" +
                                "\n" +
                                "The Content is available for permissible viewing on or through the following (collectively, the \"Properties\"): \n" +
                                "\n" +
                                "Mass TV iOS APP \n" +
                                "\n" +
                                "Mass TV on Apple TV \n" +
                                "\n" +
                                "Mass TV Android APP \n" +
                                "\n" +
                                "Mass TV on Android TV \n" +
                                "\n" +
                                "Mass TV on Mass TV Website \n" +
                                "\n" +
                                "Mass TV authorized applications features or devices. \n" +
                                "\n" +
                                "Use of the Mass TV Services (including access to the Content) on the Properties is subject to compliance with these Terms of Use (\"Terms of Use\" or \"Terms\") and any end user license agreement that might accompany a Mass TV application, feature or device. \n" +
                                "\n" +
                                "It is our pleasure to provide the Mass TV Services for your personal enjoyment and entertainment in accordance with these Terms of Use. To access and enjoy the Mass TV Services, you must agree to, and follow, the terms and conditions set forth in these Terms of Use. By visiting the Mass TV Site or using any of the Mass TV Services (including accessing any Content) you are agreeing to these Terms. Please take a moment to carefully read through these Terms. It is our goal to provide you with a first-class user experience, so if you have any questions or comments about these Terms, please contact us at: admin@masstv.app \n" +
                                "\n" +
                                " \n" +
                                "\n" +
                                " \n" +
                                "\n" +
                                "2. CHANGES TO THE TERMS OF USE BY Mass TV \n" +
                                "\n" +
                                "Mass TV may amend these Terms of Use at any time by posting the amended Terms of Use on the Mass TV Site at https://masstv.app/terms/. If we make a material amendment to these Terms of Use, we will notify you by posting notice of the amendment on the Mass TV website. Any material amendment to these Terms of Use shall be effective automatically 30 days after it is initially posted or, for users who register or otherwise provide opt-in consent during this 30-day period, at the time of registration or consent, as applicable. \n" +
                                "\n" +
                                " \n" +
                                "\n" +
                                "3. ACCESS AND USE OF THE MASS TV.COM SERVICES \n" +
                                "\n" +
                                "Age Limitations. If you are under the age of 13, then you are not permitted to register with Mass TV or use any feature or other part of the Mass TV Services that requires registration. In order to subscribe to Mass TV, you must be at least 18 years of age. Please note for those under 18 years of age, if your parent or guardian subscribes to Mass TV and authorizes you to access his or her Mass TV account then you will not be able to access all the Content offered as part of the Mass TV service if we have been informed during the registration process for the account that the primary user of such account will be under 18 years of age. Parents and guardians and users under the age of 18 must review these Terms with each other to make sure that each understands and agrees with them. You represent that you are at least 13 years of age if you are registering an account, and, if you are a Mass TV user, that you are at least 18 years of age or you have received consent from your parent or guardian to access his or her Mass TV account. \n" +
                                "\n" +
                                "Your License. Mass TV is pleased to grant you a non-exclusive limited license to use the Mass TV Services on the Properties, including accessing and viewing the Content on a streaming-only basis through the Video Player, for personal, non-commercial purposes as set forth in these Terms. \n" +
                                "\n" +
                                "The Content. You may only access and view the Content personally and for a non-commercial purpose in compliance with these Terms. You may not either directly or using any device, software, internet site, web-based service, or other means remove, alter, bypass, avoid, interfere with, or circumvent any copyright, trademark, or other proprietary notices marked on the Content or any digital rights management mechanism, device, or other content protection or access control measure associated with the Content including geo-filtering mechanisms. You may not either directly or through the use of any device, software, internet site, web-based service, or other means copy, download, stream capture, reproduce, duplicate, archive, distribute, upload, publish, modify, translate, broadcast, perform, display, sell, transmit or retransmit the Content unless expressly permitted by Mass TV in writing. You may not incorporate the Content into, or stream or retransmit the Content via, any hardware or software application or make it available via frames or in-line links unless expressly permitted by Mass TV in writing. Furthermore, you may not create, recreate, distribute or advertise an index of any significant portion of the Content unless authorized by Mass TV. You may not build a business utilizing the Content, whether or not for profit. The Content covered by these restrictions includes without limitation any text, graphics, layout, interface, logos, photographs, audio and video materials, and stills. In addition, you are strictly prohibited from creating derivative works or materials that otherwise are derived from or based on in any way the Content, including montages, mash-ups and similar videos, wallpaper, desktop themes, greeting cards, and merchandise, unless it is expressly permitted by Mass TV in writing. This prohibition applies even if you intend to give away the derivative materials free of charge. \n" +
                                "\n" +
                                "The Video Player. You may not modify, enhance, remove, interfere with, or otherwise alter in any way any portion of the Video Player, its underlying technology, any digital rights management mechanism, device, or other content protection or access control measure incorporated into the Video Player. This restriction includes, without limitation, disabling, reverse engineering, modifying, interfering with or otherwise circumventing the Video Player in any manner that enables users to view the Content without: (i) displaying visibly both the Video Player and all surrounding elements (including the graphical user interface, any advertising, copyright notices, and trademarks) of the webpage where the Video Player is located; and (ii) having full access to all functionality of the Video Player, including, without limitation, all video quality and display functionality and all interactive, elective, or click-through advertising functionality. \n" +
                                "\n" +
                                "Embedding a Video Using the Video Player. Where Mass TV has incorporated an embed option in connection with Content on the Mass TV Services, you may embed videos using the Video Player, provided you do not embed the Video Player on any website or other location that (i) contains or hosts content that is unlawful, infringing, pornographic, obscene, defamatory, libelous, threatening, harassing, vulgar, indecent, profane, hateful, racially or ethnically offensive, encourages criminal conduct, gives rise to civil liability, violates any law, rule, or regulation, infringes any right of any third party including intellectual property rights, or is otherwise inappropriate or objectionable to Mass TV (in Mass TV's sole discretion), or (ii) links to infringing or unauthorized content (collectively, \"Unsuitable Material\"). You may not embed the Video Player into any hardware or software application, even for non-commercial purposes. Mass TV reserves the right to prevent embedding to any website or other location that Mass TV finds inappropriate or objectionable (as determined by Mass TV in its sole discretion). \n" +
                                "\n" +
                                "Ownership. You agree that Mass TV owns and retains all rights to the Mass TV Services. You further agree that the Content you access and view as part of the Mass TV Services is owned or controlled by Mass TV and Mass TV's licensors. The Mass TV Services and the Content are protected by copyright, trademark, and other intellectual property laws. \n" +
                                "\n" +
                                "Your Responsibilities. In order for us to keep the Mass TV Services safe and available for everyone to use, we all have to follow the same rules of the road. You and other users must use the Mass TV Services for lawful, non-commercial, and appropriate purposes only. Your commitment to this principle is critical. You agree to observe the Mass TV Services, Content, Video Player and embedding restrictions detailed above, and further agree that you will not access the Mass TV Site or use the Mass TV Services in a way that: \n" +
                                "\n" +
                                "violates the rights of others, including patent, trademark, trade secret, copyright, privacy, publicity, or other proprietary rights; \n" +
                                "\n" +
                                "uses technology or other means to access, index, frame or link to the Mass TV Services (including the Content) that is not authorized by Mass TV (including by removing, disabling, bypassing, or circumventing any content protection or access control mechanisms intended to prevent the unauthorized download, stream capture, linking, framing, reproduction, access to, or distribution of the Mass TV Services); \n" +
                                "\n" +
                                "involves accessing the Mass TV Services (including the Content)through any automated means, including \"robots,\" \"spiders,\" or \"offline readers\" (other than by individually performed searches on publicly accessible search engines for the sole purpose of, and solely to the extent necessary for, creating publicly available search indices - but not caches or archives - of the Mass TV Services and excluding those search engines or indices that host, promote, or link primarily to infringing or unauthorized content); \n" +
                                "\n" +
                                "introduces viruses or any other computer code, files, or programs that interrupt, destroy, or limit the functionality of any computer software or hardware or telecommunications equipment; \n" +
                                "\n" +
                                "damages, disables, overburdens, impairs, or gains unauthorized access to the Mass TV Services, including Mass TV's servers, computer network, or user accounts; \n" +
                                "\n" +
                                "removes, modifies, disables, blocks, obscures or otherwise impairs any advertising in connection with the Mass TV Services (including the Content); \n" +
                                "\n" +
                                "uses the Mass TV Services to advertise or promote services that are not expressly approved in advance in writing by Mass TV; \n" +
                                "\n" +
                                "collects personally identifiable information in violation of Mass TV's Privacy Policy; \n" +
                                "\n" +
                                "encourages conduct that would constitute a criminal offense or give rise to civil liability; \n" +
                                "\n" +
                                "violates these Terms or any guidelines or policies posted by Mass TV; \n" +
                                "\n" +
                                "interferes with any other party's use and enjoyment of the Mass TV Services; or \n" +
                                "\n" +
                                "attempts to do any of the foregoing. \n" +
                                "\n" +
                                "If Mass TV determines in its sole discretion that you are violating any of these Terms, we may (i) notify you, and (ii) use technical measures to block or restrict your access or use of the Mass TV Services. In either case, you agree to immediately stop accessing or using in any way (or attempting to access or use) the Mass TV Services, and you agree not to circumvent, avoid, or bypass such restrictions, or otherwise restore or attempt to restore such access or use. \n" +
                                "\n" +
                                "No Spam/Unsolicited Communications. We know how annoying and upsetting it can be to receive unwanted email or instant messages from people you do not know. Therefore, no one may use the Mass TV Services to harvest information about users for the purpose of sending, or to facilitate or encourage the sending of, unsolicited bulk or other communications. You understand that we may take any technical remedies to prevent spam or unsolicited bulk or other communications from entering, utilizing, or remaining within our computer or communications networks. If you Post (as defined below in Section 7) or otherwise send spam, advertising, or other unsolicited communications of any kind through the Mass TV Services, you acknowledge that you will have caused substantial harm to Mass TV and that the amount of such harm would be extremely difficult to measure. As a reasonable estimation of such harm, you agree to pay Mass TV $50 for each such unsolicited communication you send through the Mass TV Services. \n" +
                                "\n" +
                                "Downloads. In order to participate in certain Mass TV Services or access certain Content, you may be notified that it is necessary to download software or other materials or agree to additional terms and conditions. Unless otherwise provided by these additional terms and conditions, they are hereby incorporated into these Terms. \n" +
                                "\n" +
                                "Internet Access Charges. You are responsible for any costs you incur to access the internet. \n" +
                                "\n" +
                                "Customer Service. If we can be of help to you, please do not hesitate to contact us via the contact box at the bottom of every page. It would be our pleasure to serve you. \n" +
                                "\n" +
                                " \n" +
                                "\n" +
                                "4. ACCOUNTS AND REGISTRATION \n" +
                                "\n" +
                                "We may from time to time offer various features that require registration or the creation of an account with Mass TV. If at any time you choose to register or create an account with us, the additional terms and conditions set forth below also will apply. \n" +
                                "\n" +
                                "All registration information you submit must be accurate and updated. Please keep your password confidential. You will not have to reveal it to any Mass TV representative. You are responsible for all use on your account, including unauthorized use by any third party, so please be very careful to guard the security of your password. Please notify us as soon as you know of, or suspect any unauthorized use of, your account. Please also make sure to notify us if your registration information changes, in case we need to contact you. \n" +
                                "\n" +
                                "We reserve the right to immediately terminate or restrict your account or your use of the Mass TV Services or access to Content at any time, without notice or liability, if Mass TV determines in its sole discretion that you have breached these Terms of Use, violated any law, rule, or regulation, engaged in other inappropriate conduct, or for any other business reason. We also reserve the right to terminate your account or your use of the Mass TV Services or access to Content if such use places an undue burden on our networks or servers. Of course, we would prefer to avoid such termination; therefore, we may use technology to limit activities, such as the number of calls to the Mass TV servers being made or the volume of User Material (as defined below in Section 7) being Posted, and you agree to respect these limitations and not take any steps to circumvent, avoid, or bypass them. \n" +
                                "\n" +
                                " \n" +
                                "\n" +
                                "5. COLLECTION AND USE OF PERSONAL INFORMATION \n" +
                                "\n" +
                                "The Privacy Policy is incorporated by reference and made part of these Terms of Use. Thus, by agreeing to these Terms of Use, you agree that your presence on the Mass TV Site and use of the Mass TV Services on any of the Properties are governed by the Mass TV Privacy Policy in effect at the time of your use. We do not collect your sell or share your personal information with other parties or affiliates. We only collect this information for ad targeting and providing our users with a better user experience when accessing Mass TV services. Also, available is our privacy policy at www. masstv.app/privacy.  \n" +
                                "\n" +
                                " \n" +
                                "\n" +
                                " \n" +
                                "\n" +
                                "6. USER REVIEWS, COMMENTS, AND OTHER MATERIAL \n" +
                                "\n" +
                                "Your Posts. As part of the Mass TV Services, users may have an opportunity to publish, transmit, submit, or otherwise post (collectively, \"Post\") reviews, comments, articles, or other materials (collectively, \"User Material\"). In order to keep the Mass TV Services enjoyable for all of our users, you must adhere to the rules below. \n" +
                                "\n" +
                                "Please choose carefully the User Material that you Post. Please limit yourself to User Material directly relevant to the Mass TV Services. Moreover, you must not Post User Material that: (i) contains Unsuitable Material (as defined above in Section 3); or (ii) improperly claims the identity of another person. Please note that we use your first and last name as your user ID and therefore your first and last name will appear to the public each time you Post. We advise that you do not, and you should also be careful if you decide to, Post additional personally identifiable information, such as your email address, telephone number, or street address. \n" +
                                "\n" +
                                "You must be, or have first obtained permission from, the rightful owner of any User Material you Post. By submitting User Material, you represent and warrant that you own the User Material or otherwise have the right to grant Mass TV the license provided below. You also represent and warrant that the Posting of your User Material does not violate any right of any party, including privacy rights, publicity rights, and intellectual property rights. In addition, you agree to pay for all royalties, fees, and other payments owed to any party by reason of your Posting User Material. Mass TV will remove all User Material if we are properly notified that such User Material infringes on another person's rights. You acknowledge that Mass TV does not guarantee any confidentiality with respect to any User Material. \n" +
                                "\n" +
                                "By Posting User Material, you are not forfeiting any ownership rights in such material to Mass TV. After Posting your User Material, you continue to retain all of the same ownership rights you had prior to Posting. By Posting your User Material, you grant Mass TV a limited license to use, display, reproduce, distribute, modify, delete from, add to, prepare derivative works of, publicly perform, and publish such User Material through the Mass TV Services worldwide, including on or through any Property, in perpetuity, in any media formats and any media channels now known or hereinafter created. The license you grant to Mass TV is non-exclusive (meaning you are not prohibited by us from licensing your User Material to anyone else in addition to Mass TV), fully-paid, royalty-free (meaning that Mass TV is not required to pay you for the use of your User Material), and sublicensable (so that Mass TV is able to use its affiliates, subcontractors, and other partners, such as internet content delivery networks, to provide the Mass TV Services). By Posting your User Material, you also hereby grant each user of the Mass TV Services a non-exclusive, limited license to access your User Material, and to use, display, reproduce, distribute, and perform such User Material as permitted through the functionality of the Mass TV Services and under these Terms of Use. \n" +
                                "\n" +
                                "Third Party Posts. Despite these restrictions, please be aware that some material provided by users may be objectionable, unlawful, inaccurate, or inappropriate. Mass TV does not endorse any User Material, and User Material that is Posted does not reflect the opinions or policies of Mass TV. We reserve the right, but have no obligation, to monitor User Material and to restrict or remove User Material that we determine, in our sole discretion, is inappropriate or for any other business reason. In no event does Mass TV assume any responsibility or liability whatsoever for any User Material, and you agree to waive any legal or equitable rights or remedies you may have against Mass TV with respect to such User Material. You can help us tremendously by notifying us of any inappropriate User Material you find. If a \"report\" feature through the Mass TV Services is not available for a specific instance of inappropriate User Material, please email admin@masstv.app (subject line: \"Inappropriate User Material\"). \n" +
                                "\n" +
                                " \n" +
                                "\n" +
                                "7. LINKED DESTINATIONS AND ADVERTISING \n" +
                                "\n" +
                                "Third Party Destinations. If we provide links or pointers to other websites or destinations, you should not infer or assume that Mass TV operates, controls, or is otherwise connected with these other websites or destinations. When you click on a link within the Mass TV Services, we will not warn you that you have left the Mass TV Services and are subject to the terms and conditions (including privacy policies) of another website or destination. In some cases, it may be less obvious than others that you have left the Mass TV Services and reached another website or destination. Please be careful to read the terms of use and privacy policy of any other website or destination before you provide any confidential information or engage in any transactions. You should not rely on these Terms to govern your use of another website or destination. \n" +
                                "\n" +
                                "Mass TV is not responsible for the content or practices of any website or destination other than the Mass TV Site, even if it links to the Mass TV Site and even if the website or destination is operated by a company affiliated or otherwise connected with Mass TV. By using the Mass TV Services, you acknowledge and agree that Mass TV is not responsible or liable to you for any content or other materials hosted and served from any website or destination other than the Mass TV Site. \n" +
                                "\n" +
                                "Advertisements. Mass TV takes no responsibility for advertisements or any third party material Posted on any of the Properties, nor does it take any responsibility for the products or services provided by advertisers. Any dealings you have with advertisers found while using the Mass TV Services are between you and the advertiser, and you agree that Mass TV is not liable for any loss or claim that you may have against an advertiser. \n" +
                                "\n" +
                                " \n" +
                                "\n" +
                                "8. TRADEMARKS \n" +
                                "\n" +
                                "Mass TV, the Mass TV logo, https://masstv.app/ and other Mass TV marks, graphics, logos, scripts, and sounds are trademarks of Mass TV. None of the Mass TV trademarks may be copied, downloaded, or otherwise exploited. This has been filed with USPTO as in pending approval.  \n" +
                                "\n" +
                                " \n" +
                                "\n" +
                                "9. UNSOLICITED SUBMISSIONS \n" +
                                "\n" +
                                "It is Mass TV's policy not to accept unsolicited submissions, including scripts, story lines, articles, fan fiction, characters, drawings, information, suggestions, ideas, or concepts. Mass TV's policy is to delete any such submission without reading it. Therefore, any similarity between an unsolicited submission and any elements in any Mass TV creative work, including a film, series, story, title, or concept, would be purely coincidental. \n" +
                                "\n" +
                                " \n" +
                                "\n" +
                                "10. DISCLAIMER OF WARRANTIES, LIMITATION OF LIABILITY AND INDEMNITY \n" +
                                "\n" +
                                "WHILE WE DO OUR BEST TO PROVIDE THE OPTIMAL PERFORMANCE OF THE MASS TV SERVICES, YOU AGREE THAT USE OF THE MASS TV SERVICES IS AT YOUR OWN RISK. THE MASS TV SERVICES, INCLUDING THE MASS TV SITE AND THE OTHER PROPERTIES, THE CONTENT, THE VIDEO PLAYER, USER MATERIAL, AND ANY OTHER MATERIALS CONTAINED ON OR PROVIDED THROUGH THE PROPERTIES, ARE PROVIDED \"AS IS\" AND, TO THE FULLEST EXTENT PERMITTED BY LAW, ARE PROVIDED WITHOUT WARRANTIES OF ANY KIND, EITHER EXPRESS OR IMPLIED. WITHOUT LIMITING THE FOREGOING, MASS TV DOES NOT MAKE ANY WARRANTIES OF FITNESS FOR A PARTICULAR PURPOSE, TITLE, MERCHANTABILITY, COMPLETENESS, AVAILABILITY, SECURITY, COMPATIBILITY OR NONINFRINGEMENT; OR THAT THE MASS TV SERVICES WILL BE UNINTERRUPTED, FREE OF VIRUSES AND OTHER HARMFUL COMPONENTS, ACCURATE, ERROR FREE, OR RELIABLE. \n" +
                                "\n" +
                                "IN NO EVENT SHALL MASS TV OR ITS AFFILIATES, SUCCESSORS, AND ASSIGNS, AND EACH OF THEIR RESPECTIVE INVESTORS, DIRECTORS, OFFICERS, EMPLOYEES, AGENTS, AND SUPPLIERS (INCLUDING DISTRIBUTORS AND CONTENT LICENSORS) (COLLECTIVELY, THE \"MASS TV PARTIES\"), BE LIABLE FOR ANY DIRECT, INDIRECT, PUNITIVE, INCIDENTAL, SPECIAL, CONSEQUENTIAL, OR OTHER DAMAGES, INCLUDING LOSS OF PROFITS, ARISING OUT OF OR IN ANY WAY RELATED TO THE USE OF THE MASS TV SERVICES (INCLUDING ANY INFORMATION, PRODUCTS, OR SERVICES ADVERTISED IN, OBTAINED ON, OR PROVIDED THROUGH THE PROPERTIES), WHETHER BASED IN CONTRACT, TORT, STRICT LIABILITY, OR OTHER THEORY, EVEN IF THE MASS TV PARTIES HAVE BEEN ADVISED OF THE POSSIBILITY OF DAMAGES. CERTAIN STATE LAWS DO NOT ALLOW LIMITATIONS ON IMPLIED WARRANTIES OR THE EXCLUSION OR LIMITATION OF CERTAIN DAMAGES. IF THESE LAWS APPLY TO YOU, SOME OR ALL OF THE ABOVE DISCLAIMERS, EXCLUSIONS, OR LIMITATIONS MAY NOT APPLY TO YOU. IN NO EVENT SHALL OUR TOTAL LIABILITY TO YOU FOR ALL DAMAGES, LOSSES AND CAUSES OF ACTION WHETHER IN CONTRACT, TORT (INCLUDING NEGLIGENCE) OR OTHERWISE EXCEED THE AMOUNT PAID BY YOU TO US, IF ANY, FOR ACCESSING OR PARTICIPATING IN ANY ACTIVITY RELATED TO USE OF THE MASS TV SERVICE OR $50 (WHICHEVER IS LESS). \n" +
                                "\n" +
                                "YOU AGREE TO DEFEND, INDEMNIFY, AND HOLD HARMLESS THE MASS TV PARTIES FROM AND AGAINST ANY AND ALL LIABILITIES, CLAIMS, DAMAGES, EXPENSES (INCLUDING REASONABLE ATTORNEYS' FEES AND COSTS), AND OTHER LOSSES ARISING OUT OF OR IN ANY WAY RELATED TO YOUR BREACH OR ALLEGED BREACH OF THESE TERMS OR YOUR USE OF THE MASS TV SERVICES (INCLUDING YOUR USE OF THE CONTENT). MASS TV RESERVES THE RIGHT, AT OUR OWN EXPENSE, TO EMPLOY SEPARATE COUNSEL AND ASSUME THE EXCLUSIVE DEFENSE AND CONTROL OF ANY MATTER OTHERWISE SUBJECT TO INDEMNIFICATION BY YOU. \n" +
                                "\n" +
                                " \n" +
                                "\n" +
                                "11. NOTICE AND PROCEDURE FOR CLAIMS OF COPYRIGHT INFRINGEMENT \n" +
                                "\n" +
                                "Mass TV respects the intellectual property rights of others and takes claims of copyright and trademark infringement very seriously. We license the works displayed on our website from artists and other rights holders to ensure our compliance with applicable law. If you believe any materials accessible on or from this site infringe your intellectual property, you may request removal of those materials (or access to them) by submitting at report here. \n" +
                                "\n" +
                                " \n" +
                                "\n" +
                                "12. INFORMATION SHARING \n\n" +
                                "We may share information that we collect about you to provide the Mass TV Services, including:\n" +
                                "\n" +
                                "with social networking services with your consent;\n" +
                                "with other business partners with your consent;\n" +
                                "in aggregated or other non-personally identifiable form; or\n" +
                                "when necessary to protect or defend the rights of Mass TV or our users, and when required by law or public authorities.\n\n" +
                                "13. CONTACT INFORMATION \n" +
                                "\n" +
                                "If you have any questions or concerns related to this Privacy Policy, you may send a regular mail to: \n" +
                                "\n" +
                                "ATTN: Mass TV \n" +
                                "6020 Ocean Blvd \n" +
                                "Long Beach, CA \n" +
                                "\n" +
                                "United States \n" +
                                "\n" +
                                " ").setPositiveButton("Ok", new DialogInterface.OnClickListener() {
            @Override
            public void onClick(DialogInterface dialog, int which) {

            }
        }).show();
    }
}
