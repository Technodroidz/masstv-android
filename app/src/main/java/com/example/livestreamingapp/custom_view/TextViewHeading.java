package com.example.livestreamingapp.custom_view;

import android.annotation.SuppressLint;
import android.content.Context;
import android.graphics.Typeface;
import android.util.AttributeSet;
import android.widget.TextView;

@SuppressLint("AppCompatCustomView")
public class TextViewHeading extends TextView {

    public TextViewHeading(Context context) {
        super(context);
        Typeface face = Typeface.createFromAsset(context.getAssets(), "robotobold.ttf");
        this.setTypeface(face);
    }

    public TextViewHeading(Context context, AttributeSet attrs) {
        super(context, attrs);
        Typeface face = Typeface.createFromAsset(context.getAssets(), "robotobold.ttf");
        this.setTypeface(face);
    }

    public TextViewHeading(Context context, AttributeSet attrs, int defStyle) {
        super(context, attrs, defStyle);
        Typeface face = Typeface.createFromAsset(context.getAssets(), "robotobold.ttf");
        this.setTypeface(face);
    }
}
