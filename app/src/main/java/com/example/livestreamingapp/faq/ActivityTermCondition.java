package com.example.livestreamingapp.faq;

import androidx.annotation.NonNull;
import androidx.appcompat.app.AppCompatActivity;
import androidx.appcompat.widget.Toolbar;
import androidx.core.widget.NestedScrollView;
import androidx.recyclerview.widget.LinearLayoutManager;
import androidx.recyclerview.widget.RecyclerView;

import android.app.Activity;
import android.app.SearchManager;
import android.content.Context;
import android.os.Bundle;
import android.os.Handler;
import android.util.Log;
import android.view.Menu;
import android.view.MenuInflater;
import android.view.MenuItem;
import android.view.View;
import android.view.animation.Animation;
import android.webkit.WebView;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.SearchView;

import com.example.livestreamingapp.MassTvActivity;
import com.example.livestreamingapp.R;
import com.example.livestreamingapp.model.query.MyQuery;
import com.example.livestreamingapp.utils.MyCustomAnimation;
import com.google.android.gms.cast.framework.CastButtonFactory;
import com.google.android.gms.cast.framework.CastContext;
import com.google.android.material.resources.TextAppearance;
import com.google.gson.Gson;

import java.util.ArrayList;

public class ActivityTermCondition extends MassTvActivity implements View.OnClickListener {

    private Toolbar mToolbar;
    private String title;
    private LinearLayout ln_content;
    private RecyclerView rv_search;
    private RecyclerView rv_general;
    private RecyclerView rv_trouble;
    private RecyclerView rv_management;
    private RecyclerView rv_submission;

    private FaqAdapter searchAdapter;
    private FaqAdapter faqGeneral;
    private FaqAdapter faqTrouble;
    private FaqAdapter faqManagement;
    private FaqAdapter faqSubmission;
    private ArrayList<MyQuery> searchList;
    private ArrayList<MyQuery> generalList;
    private ArrayList<MyQuery> troubleList;
    private ArrayList<MyQuery> manageList;
    private ArrayList<MyQuery> submissionList;

    private LinearLayout ln_general;
    private LinearLayout ln_trouble;
    private LinearLayout ln_management;
    private LinearLayout ln_submission;

    private int heightGeneral;
    private int heightTrouble;
    private int heightManagement;
    private int heightSubmission;
    private int openPosition = -2;

    private ImageView iv_general;
    private ImageView iv_trouble;
    private ImageView iv_management;
    private ImageView iv_submission;
    private NestedScrollView nestedScrollView;
    private MenuItem mediaRouteMenuItem;
    private CastContext mCastContext;

    @Override
    public boolean onCreateOptionsMenu(Menu menu) {
        // Inflate the options menu from XML
        MenuInflater inflater = getMenuInflater();
        inflater.inflate(R.menu.menu_search, menu);

        mediaRouteMenuItem = CastButtonFactory.setUpMediaRouteButton(getApplicationContext(), menu,
                R.id.media_route_menu_item);

        // Get the SearchView and set the searchable configuration
        SearchManager searchManager = (SearchManager) getSystemService(Context.SEARCH_SERVICE);
        SearchView searchView = (SearchView) menu.findItem(R.id.menu_search).getActionView();
        searchView.setSearchableInfo(searchManager.getSearchableInfo(getComponentName()));
        //searchView.setIconifiedByDefault(false); // Do not iconify the widget; expand it by default
        searchView.setOnQueryTextListener(new SearchView.OnQueryTextListener() {
            @Override
            public boolean onQueryTextSubmit(String query) {
                return false;
            }

            @Override
            public boolean onQueryTextChange(String newText) {
                if(newText.length()>0){
                    // hide faqlist
                    ln_content.setVisibility(View.GONE);
                    rv_search.setVisibility(View.VISIBLE);
                    searchList.clear();
                    searchList = new ArrayList<>();
                    searchList =searchFaqList(newText.toLowerCase());
                    searchAdapter = new FaqAdapter(ActivityTermCondition.this,searchList);
                    rv_search.setAdapter(searchAdapter);
                    searchAdapter.notifyDataSetChanged();
                }else {
                   // hide search
                    ln_content.setVisibility(View.VISIBLE);
                    rv_search.setVisibility(View.GONE);
                    searchAdapter = new FaqAdapter(ActivityTermCondition.this,searchList);
                    rv_search.setAdapter(searchAdapter);
                    searchAdapter.notifyDataSetChanged();
                }

                return false;
            }
        });


        return true;
    }

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_term_condition);
        title = getIntent().getStringExtra("title");
        setupActionBar();
        initViews();
        initializeList();
        setAdapters();
        setupListener();
        mCastContext = CastContext.getSharedInstance(this);
    }

    private void setAdapters() {
        // general faq list
        LinearLayoutManager searchManager = new LinearLayoutManager(this);
        rv_search.setLayoutManager(searchManager);
        searchAdapter = new FaqAdapter(this, searchList);
        rv_search.setAdapter(searchAdapter);

 // general faq list
        LinearLayoutManager generalManager = new LinearLayoutManager(this);
        rv_general.setLayoutManager(generalManager);
        faqGeneral = new FaqAdapter(this, generalList);
        rv_general.setAdapter(faqGeneral);

        // trouble faq list
        LinearLayoutManager trouble = new LinearLayoutManager(this);
        rv_trouble.setLayoutManager(trouble);
        faqTrouble = new FaqAdapter(this, troubleList);
        rv_trouble.setAdapter(faqTrouble);

        // management
        LinearLayoutManager manager = new LinearLayoutManager(this);
        rv_management.setLayoutManager(manager);
        faqManagement = new FaqAdapter(this, manageList);
        rv_management.setAdapter(faqManagement);

        // submission
        LinearLayoutManager submission = new LinearLayoutManager(this);
        rv_submission.setLayoutManager(submission);
        faqSubmission = new FaqAdapter(this, submissionList);
        rv_submission.setAdapter(faqSubmission);
    }

    private void setupListener() {
        ln_general.setOnClickListener(this);
        ln_trouble.setOnClickListener(this);
        ln_management.setOnClickListener(this);
        ln_submission.setOnClickListener(this);
    }

    private void initializeList() {

        MyQuery general1 = new MyQuery();
        general1.setQuery("Q1. What is MASS TV?");
        general1.setResponse("MASS TV is the streaming home for entertainment. MASS TV is an app-based Tech Company designed for NRIs (Non-Resident Indians) to submit content for us to publish on our app. \n" +
                "\n" +
                "  \n" +
                "\n" +
                "MASS TV brings you the best stories in the world, all in one place. \n" +
                "\n" +
                "The home for movies, shows, shorts, and originals. \n" +
                "\n" +
                "Exclusive originals with new feature films, shows, short-form content, and documentaries. \n" +
                "\n" +
                "Access to thousands of the movies and shows you love \n" +
                "\n" +
                "Personalized recommendations \n" +
                "\n" +
                "High-quality viewing experience \n" +
                "\n" +
                "What devices and platforms are supported? \n" +
                "\n" +
                "MASS TV supports mobile devices, web browsers including: \n" +
                "\n" +
                "Web Browsers (via streaming) \n" +
                "www.masstv.app \n" +
                "\n" +
                "Mobile Devices and Tablets (via free downloaded app) \n" +
                "\n" +
                "Android Phone and tablets \n" +
                "\n" +
                "Apple iPhones and iPads ");
        MyQuery general2 = new MyQuery();
        general2.setQuery("Q2. Does it provide original content? ");
        general2.setResponse("Yes! MASS TV provides original contents to you and that is one of our top priority.");

        MyQuery general3 = new MyQuery();
        general3.setQuery("Q3. How to sign-up for MASS TV? ");
        general3.setResponse("You can sign up for MASSTV in a number of ways. Select the device you’re using, and we’ll tell you how to do it:  \n" +
                "\n" +
                " \n" +
                "\n" +
                "Web Browser \n" +
                "\n" +
                "In your web browser, go to www.masstv.app  \n" +
                "\n" +
                "Select SIGN UP \n" +
                "\n" +
                "Enter your email address \n" +
                "\n" +
                "Enter a password \n" +
                "\n" +
                "START WATCHING \n" +
                "\n" +
                " \n" +
                "\n" +
                "Mobile Device \n" +
                "\n" +
                "On your mobile device \n" +
                "\n" +
                "Go to the Apple App Store or Google Play App store< \n" +
                "\n" +
                "Download the MASS TV app \n" +
                "\n" +
                "Open the MASS TV \n" +
                "\n" +
                "Select SIGN UP \n" +
                "\n" +
                "Enter your email address \n" +
                "\n" +
                "Enter a password \n" +
                "\n" +
                "START WATCHING \n" +
                "\n" +
                " \n" +
                "\n" +
                " \n" +
                "\n" +
                "Apple TV \n" +
                "\n" +
                "On your Apple TV device \n" +
                "\n" +
                "Go to the Apple App Store \n" +
                "\n" +
                "Download the MASS TV app \n" +
                "\n" +
                "Open the MASS TV app \n" +
                "\n" +
                "Select SIGN UP  \n" +
                "\n" +
                "Enter your email address \n" +
                "\n" +
                "Enter a password \n" +
                "\n" +
                "START WATCHING \n" +
                "\n" +
                " \n" +
                "\n" +
                "Android TV \n" +
                "\n" +
                "On your Android TV device \n" +
                "\n" +
                "Go to the App Store \n" +
                "\n" +
                "Download the MASS TV app \n" +
                "\n" +
                "Open the MASS TV app \n" +
                "\n" +
                "Select SIGN UP  \n" +
                "\n" +
                "Enter your email address \n" +
                "\n" +
                "Enter a password \n" +
                "\n" +
                "START WATCHING ");
        MyQuery general4 = new MyQuery();
        general4.setQuery("Q4. Does it provide original content? ");
        general4.setResponse("Yes! MASS TV provides original contents to you and that is one of our top priority.");


        MyQuery general5 = new MyQuery();
        general5.setQuery("Q5. Where can I set up and manage profiles? ");
        general5.setResponse("Currently we are supporting only single profile. We are working towards providing the following features on Profile \n" +
                "\n" +
                "Set up a new profile \n" +
                "\n" +
                "Manage profiles \n" +
                "\n" +
                "Add a profile \n" +
                "\n" +
                "Create a Kids’ Profile ");


        MyQuery general6 = new MyQuery();
        general6.setQuery("Q6. Does MASS TV support multiple profiles? ");
        general6.setResponse("We are working towards the support for multiple profiles as we want everyone in your household to have their own experience. ");

        MyQuery general7 = new MyQuery();
        general7.setQuery("Q7. Is MASS TV free?");
        general7.setResponse("Of course! Our number one goal is to keep this app free so you can enjoy our content anytime. ");

        MyQuery general8 = new MyQuery();
        general8.setQuery("Q8. Can I set up MASS TV on multiple devices? What is the limit? ");
        general8.setResponse("Yes, you can set up MASS TV on multiple devices. The limit is up to 4 devices. ");

        generalList.add(0, general1);
        generalList.add(1, general2);
        generalList.add(2, general3);
        generalList.add(3, general4);
        generalList.add(4, general5);
        generalList.add(5, general6);
        generalList.add(6, general7);
        generalList.add(7, general8);


        // add trouble shoot
        MyQuery trouble1 = new MyQuery();
        trouble1.setQuery("Q9. Why am I asked to login? ");
        trouble1.setResponse("You are being asked to login so that we can get your contact information to reach out to you regarding payments, time slots for your content, etc. ");

        MyQuery trouble2 = new MyQuery();
        trouble2.setQuery("Q10. I am having login issues? ");
        trouble2.setResponse("Not to worry. We can help. This article will help you log in to the following devices and platforms:  \n" +
                "\n" +
                " \n" +
                "\n" +
                "Web Browser \n" +
                "\n" +
                "Mobile Device \n" +
                "\n" +
                "Apple TV \n" +
                "\n" +
                " \n" +
                "\n" +
                "Web Browser \n" +
                "\n" +
                "If you are using your browser, please follow these steps to reset your password. \n" +
                "\n" +
                " \n" +
                "\n" +
                "In your web browser, go to www.masstv.app \n" +
                "\n" +
                "Select LOG IN (It is in the upper right-hand corner.) \n" +
                "\n" +
                "Enter your email address \n" +
                "\n" +
                "Enter your password \n" +
                "\n" +
                "If you have forgotten your password, please follow these steps to reset your password \n" +
                "\n" +
                "Select LOG IN \n" +
                "\n" +
                "  \n" +
                "\n" +
                "Mobile Device (for iOS and Android) \n" +
                "\n" +
                "If you are using your phone, please follow these steps to reset your password. \n" +
                "\n" +
                " \n" +
                "\n" +
                "On your mobile device, open the MASS TV app  \n" +
                "\n" +
                "If you do not have the app already, download it from App Store or Google Play \n" +
                "\n" +
                "Select LOG IN (It is at the bottom of the screen.) \n" +
                "\n" +
                "Enter your email address \n" +
                "\n" +
                "Enter your password \n" +
                "\n" +
                "If you have forgotten your password, please follow these steps to reset your password. \n" +
                "\n" +
                "SELECT LOG IN \n" +
                "\n" +
                "  \n" +
                "\n" +
                " \n" +
                "\n" +
                "Apple TV \n" +
                "\n" +
                "On your Apple TV, open the MASS TV app.  \n" +
                "\n" +
                "If you do not have the app, you can download it from the Apple TV app store \n" +
                "\n" +
                "Select LOG IN \n" +
                "\n" +
                "Enter your email address \n" +
                "\n" +
                "Enter your password \n" +
                "\n" +
                "If you have forgotten your password, please follow these steps to reset your password. \n" +
                "\n" +
                "Select LOG IN");
        MyQuery trouble3 = new MyQuery();
        trouble3.setQuery("Q11. Why is my video not playing?");
        trouble3.setResponse("If you are having issues with your video not playing try out the following steps. \n" +
                "\n" +
                "Check your Network connection. \n" +
                "\n" +
                "A slow Internet connection could be the problem. Check your location and the number of devices connected to your network. Both factors can slow things down. \n" +
                "\n" +
                "A weak Internet connection can also cause issues. Check the strength on your device; if it is low, try moving closer to your Wi-Fi router. \n" +
                "\n" +
                "Restart your device and start the app again. \n" +
                "\n" +
                "If you are accessing from a computer, make sure to clear your computer’s cache and cookies and reload the page. \n" +
                "\n" +
                "Close other windows that are open as it may help to make the video stream faster. \n" +
                "\n" +
                "Check to make sure your device is supported \n" +
                "\n" +
                "Update your device \n" +
                "\n" +
                "Try on another device \n" +
                "\n" +
                "Other possible causes \n" +
                "\n" +
                "For the best experience, we recommend a high-speed Internet connection. To enjoy the highest-quality MASS TV experience, all components, including HDMI cables, should support high-bandwidth digital content. External displays should support HDCP 2.2 for 4K Ultra HD and HDR content. Older audio and video components can have an impact on quality or possibly prevent playback entirely. MASS TV may not be available in your Country/Region yet. Read this article to find out where the service is currently available. \n" +
                "\n" +
                "  ");

        MyQuery trouble4 = new MyQuery();
        trouble4.setQuery("Q12. What is MASS TV’s recommended Internet speed?");
        trouble4.setResponse("Our recommended Internet speed is about 3Mbps for standard streaming and 5Mbps for HD streaming. If you are expecting Ultra HD, the recommended speed is about 25Mbps if you want to avoid chances of buffering. ");

        MyQuery trouble5 = new MyQuery();
        trouble5.setQuery("Q13. What quality videos are supported? ");
        trouble5.setResponse("We support videos that are HD (1080p or more) and Ultra HD 4K (2160p) resolution. ");


        MyQuery trouble6 = new MyQuery();
        trouble6.setQuery("Q14. How about audio quality? ");
        trouble6.setResponse("Ans : MASS TV offers a growing library of content in 5.1 Surround Sound and Dolby Atmos. To enjoy 5.1 surround sound or Dolby Atmos, you will need an audio system compatible with Dolby Digital Plus (for 5.1) and/or Dolby Atmos. \n" +
                "\n" +
                " \n" +
                "\n" +
                "Check to see what formats are supported  \n" +
                "\n" +
                " \n" +
                "\n" +
                "It is easy. You will find the supported audio formats in the DETAILS tab of every title. \n" +
                "\n" +
                " \n" +
                "\n" +
                "For your convenience, MASS TV will automatically use the highest-quality audio your system can support. \n" +
                "\n" +
                " \n" +
                "\n" +
                "To enjoy the highest-quality MASS TV experience, all components should support high-bandwidth digital content, including HDMI cables. Consult with your device manufacturer for specific details on the capabilities of your device(s).  \n" +
                "\n" +
                " \n" +
                "\n" +
                "Note: Older audio and video components can have an impact on quality and possibly prevent playback entirely. \n" +
                "\n" +
                " \n" +
                "\n" +
                "Have you checked your connection speed? Your connection speed can also affect your audio quality \n" +
                "\n" +
                " ");

        troubleList.add(0, trouble1);
        troubleList.add(1, trouble2);
        troubleList.add(2, trouble3);
        troubleList.add(3, trouble4);
        troubleList.add(4, trouble5);
        troubleList.add(5, trouble6);


        MyQuery management1 = new MyQuery();
        management1.setQuery("Q15. How do I reset my password? ");
        management1.setResponse("To reset your password, please follow these steps: \n" +
                "\n" +
                " \n" +
                "\n" +
                "1. On the MASS TV welcome page, select LOG IN \n" +
                "\n" +
                "1. Enter your email address \n" +
                "\n" +
                "3. Select CONTINUE \n" +
                "\n" +
                "4. Select FORGOT PASSWORD? \n" +
                "\n" +
                "5. Check your email \n" +
                "\n" +
                " You will receive a 6-digit passcode code \n" +
                "\n" +
                "  It may take up to 15 min. \n" +
                "\n" +
                "If you do not receive a code, check your spam folder. \n" +
                "\n" +
                "If it is not there, select RESEND to send the code again. \n" +
                "\n" +
                "If you still have issues, please contact Customer Service. \n" +
                "\n" +
                "You can reach us 24/7 via chat, phone, or social media. \n" +
                "\n" +
                "6. Enter your 6-digit verification code \n" +
                "\n" +
                "7. Enter new password \n" +
                "\n" +
                "8. Select CONTINUE  ");

        MyQuery management2 = new MyQuery();
        management2.setQuery("Q16. How do I establish a payment link? ");
        management2.setResponse("TBD");

        MyQuery management3 = new MyQuery();
        management3.setQuery("Q.17 How do I update my phone number?  ");
        management3.setResponse("It is easy. Let us show you how. \n" +
                "\n" +
                " \n" +
                "\n" +
                "First, please select your device: \n" +
                "\n" +
                " \n" +
                "\n" +
                "Web Browser \n" +
                "\n" +
                "Mobile Device \n" +
                "\n" +
                "Apple TV  \n" +
                "\n" +
                " \n" +
                "\n" +
                "Web Browser \n" +
                "\n" +
                "On a computer, log into the masstv.app \n" +
                "\n" +
                "Hover over your Profile. (It’s in the top-right corner.) \n" +
                "\n" +
                "Select ACCOUNT \n" +
                "\n" +
                "Select CHANGE Phone number \n" +
                "\n" +
                "Enter your new Phone number \n" +
                "\n" +
                "Select SAVE \n" +
                "\n" +
                "You will receive an email to your email address confirming the change. \n" +
                "\n" +
                " \n" +
                "\n" +
                "Mobile Device \n" +
                "\n" +
                "On your mobile device, open the MASS TV App \n" +
                "\n" +
                "Log in \n" +
                "\n" +
                "Tap your profile icon.  \n" +
                "\n" +
                "Tap ACCOUNT \n" +
                "\n" +
                "Tap CHANGE Phone number \n" +
                "\n" +
                "Enter your new Phone number \n" +
                "\n" +
                "Tap SAVE \n" +
                "\n" +
                "You will receive an email to your email address confirming the change." +
                "\n \n " +
                "Apple TV \n" +
                "\n" +
                "On your Apple TV, open the MASS TV App \n" +
                "\n" +
                "Log in \n" +
                "\n" +
                "Select the Settings icon. \n" +
                "\n" +
                "Select ACCOUNT \n" +
                "\n" +
                "Select CHANGE Phone number \n" +
                "\n" +
                "Enter your new Phone number \n" +
                "\n" +
                "Select SAVE \n" +
                "\n" +
                "You will receive an email to your email address confirming the change. \n" +
                "\n" +
                " \n" +
                "\n" +
                "How do I change my address? \n" +
                "\n" +
                "It is pretty easy. Let us show you how. \n" +
                "\n" +
                " \n" +
                "\n" +
                "First, please select your device: \n" +
                "\n" +
                " \n" +
                "\n" +
                "Web Browser \n" +
                "\n" +
                "Mobile Device \n" +
                "\n" +
                "Apple TV  \n" +
                "\n" +
                " \n" +
                "\n" +
                "Web Browser \n" +
                "\n" +
                "On a computer, log into the masstv.app \n" +
                "\n" +
                "Hover over your Profile. (It is in the top-right corner.) \n" +
                "\n" +
                "Select ACCOUNT \n" +
                "\n" +
                "Select CHANGE Address \n" +
                "\n" +
                "Enter your new Address \n" +
                "\n" +
                "Select SAVE \n" +
                "\n" +
                "You will receive an email to your new email address confirming the change. \n" +
                "\n" +
                " \n" +
                "\n" +
                "Mobile Device \n" +
                "\n" +
                "On your mobile device, open the MASS TV App \n" +
                "\n" +
                "Log in \n" +
                "\n" +
                "Tap your profile icon.  \n" +
                "\n" +
                "Tap ACCOUNT \n" +
                "\n" +
                "Tap CHANGE Address \n" +
                "\n" +
                "Enter your new Address \n" +
                "\n" +
                "Tap SAVE \n" +
                "\n" +
                "You will receive an email to your new email address confirming the change. \n" +
                "\n" +
                " \n" +
                "\n" +
                "Apple TV \n" +
                "\n" +
                "On your Apple TV, open the MASS TV App \n" +
                "\n" +
                "Log in \n" +
                "\n" +
                "Select the Settings icon. \n" +
                "\n" +
                "Select ACCOUNT \n" +
                "\n" +
                "Select CHANGE Address \n" +
                "\n" +
                "Enter your new Address \n" +
                "\n" +
                "Select SAVE \n" +
                "\n" +
                "You will receive an email to your new email address confirming the change. \n" +
                "\n" +
                " ");

        MyQuery management4 = new MyQuery();
        management4.setQuery("Q.18 How do I sign-up for a MASS TV Newsletter? ");
        management4.setResponse("Since you Sign up with MASS TV using your email address, you will be sent MASS TV Newsletter automatically.");


        manageList.add(0, management1);
        manageList.add(1, management2);
        manageList.add(2, management3);
        manageList.add(3, management4);

        MyQuery submission1 = new MyQuery();
        submission1.setQuery("Q19. Who can submit content? ");
        submission1.setResponse("Any NRI (Non-Resident Indian) who is willing to showcase their unseen talents to the world can submit their content here. ");

        MyQuery submission2 = new MyQuery();
        submission2.setQuery("Q20. What type of content can be submitted?  ");
        submission2.setResponse("All kinds of content can be submitted if it is PG-13 appropriate and is submitted in a good quality. ");

        MyQuery submission3 = new MyQuery();
        submission3.setQuery("Q21. Does someone review our content before publishing? ");
        submission3.setResponse("Yes! We have a team who reviews your content to make sure it fits the requirements before publishing. ");

        MyQuery submission4 = new MyQuery();
        submission4.setQuery("Q22. Once our video is approved, how will the payment be approved? ");
        submission4.setResponse("We will be collecting your contact information and email address while you submit your content. If approved, we will send you your payment through PayPal based on the email address provided.  ");

        MyQuery submission5 = new MyQuery();
        submission5.setQuery("Q23. How long will it take before I am paid? ");
        submission5.setResponse(" You should receive your payment within 2-3 business days but allow us 1 week at the most before contacting us. \n" +
                "\n" +
                " ");

        MyQuery submission6 = new MyQuery();
        submission6.setQuery("Q24. What type of quality content can be submitted?");
        submission6.setResponse(" In order to increase your chances of us accepting your content, we recommend you send the best quality you have. Typically, HD (1080p) is one of the bare minimum qualities we are expecting.");

        submissionList.add(0, submission1);
        submissionList.add(1, submission2);
        submissionList.add(2, submission3);
        submissionList.add(3, submission4);
        submissionList.add(4, submission5);
        submissionList.add(5, submission6);


    }

    /**
     * @Rehan Visible view with animation
     */
    public void setVisiblity(View view, final ImageView iv_Arrow, int duration, LinearLayout linearLayout) {


        if (view.getVisibility() == View.VISIBLE) {
            //  view.setVisibility(View.GONE);
            openPosition = -2;
            iv_Arrow.setImageResource(R.drawable.ic_drop_down);
            final MyCustomAnimation a = new MyCustomAnimation(view, duration, MyCustomAnimation.COLLAPSE);
            switch (iv_Arrow.getId()) {
                case R.id.iv_general:
                    heightGeneral = a.getHeight();
                    //  iv_filled1.setVisibility(isFilled("1")?View.VISIBLE:View.GONE);

                    break;
                case R.id.iv_trouble:
                    heightTrouble = a.getHeight();
                    //  iv_filled2.setVisibility(isFilled("2")?View.VISIBLE:View.GONE);
                    break;
                case R.id.iv_management:
                    heightManagement = a.getHeight();
                    //  iv_filled3.setVisibility(isFilled("3")?View.VISIBLE:View.GONE);
                    break;
                case R.id.iv_submission:
                    heightSubmission = a.getHeight();
                    break;
            }
            view.startAnimation(a);

        } else {


            iv_Arrow.setImageResource(R.drawable.ic_drop_up);
            MyCustomAnimation a = new MyCustomAnimation(view, duration, MyCustomAnimation.EXPAND);
            switch (iv_Arrow.getId()) {
                case R.id.iv_general:

                    a.setHeight(heightGeneral);

                    break;
                case R.id.iv_trouble:
                    a.setHeight(heightTrouble);

                    break;
                case R.id.iv_management:
                    a.setHeight(heightManagement);
                    break;
                case R.id.iv_submission:
                    a.setHeight(heightSubmission);

                    break;

            }
            view.startAnimation(a);
        }
    }

    /**
     * Detect last open positon and hide if other view tap
     *
     * @param position
     */
    private void hideWithAnimation(int position) {
        if (openPosition != position && openPosition != -2) {

            switch (openPosition) {
                case 0:
                    hideVisibility(rv_general, iv_general, 300, position);
                    break;
                case 1:
                    hideVisibility(rv_trouble, iv_trouble, 300, position);
                    break;
                case 2:
                    hideVisibility(rv_management, iv_management, 300, position);
                    break;
                case 3:
                    hideVisibility(rv_submission, iv_submission, 300, position);
                    break;

            }

        } else {
            openPosition = position;
            switch (position) {
                case 0:

                    setVisiblity(rv_general, iv_general, 300, ln_general);
                    break;
                case 1:
                    setVisiblity(rv_trouble, iv_trouble, 300, ln_trouble);
                    break;
                case 2:
                    setVisiblity(rv_management, iv_management, 300, ln_management);
                    break;
                case 3:
                    setVisiblity(rv_submission, iv_submission, 300, ln_submission);
                    break;
            }
        }
    }


    /**
     * @Rehan Hide view with animation
     */
    private void hideVisibility(RecyclerView view, ImageView iv_Arrow, int duration, int position) {
        iv_Arrow.setImageResource(R.drawable.ic_drop_down);
        final MyCustomAnimation a = new MyCustomAnimation(view, duration, MyCustomAnimation.COLLAPSE);
        switch (iv_Arrow.getId()) {
            case R.id.iv_general:
                heightGeneral = a.getHeight();
                //  iv_filled1.setVisibility(isFilled("1")?View.VISIBLE:View.GONE);

                break;
            case R.id.iv_trouble:
                heightTrouble = a.getHeight();
                //  iv_filled2.setVisibility(isFilled("2")?View.VISIBLE:View.GONE);
                break;
            case R.id.iv_management:
                heightManagement = a.getHeight();
                //  iv_filled3.setVisibility(isFilled("3")?View.VISIBLE:View.GONE);
                break;
            case R.id.iv_submission:
                heightSubmission = a.getHeight();
                break;
        }
        a.setAnimationListener(new Animation.AnimationListener() {
            @Override
            public void onAnimationStart(Animation animation) {

            }

            @Override
            public void onAnimationEnd(Animation animation) {
                runOnUiThread(new Runnable() {
                    @Override
                    public void run() {
                        int scrollTo;

                        switch (position) {
                            case 0:
                                /*scrollTo = ((View) ln_general.getParent().getParent()).getTop()+ln_general.getTop();
                                nestedScrollView.scrollTo(0,scrollTo);*/
                                setVisiblity(rv_general, iv_general, 300, ln_general);
                                break;
                            case 1:
                              /*  scrollTo = ((View) ln_trouble.getParent().getParent()).getTop()+ln_trouble.getTop();
                                nestedScrollView.scrollTo(0,scrollTo);*/
                                setVisiblity(rv_trouble, iv_trouble, 300, ln_trouble);
                                break;
                            case 2:
                              /*  scrollTo = ((View) ln_management.getParent().getParent()).getTop()+ln_management.getTop();
                                nestedScrollView.scrollTo(0,scrollTo);
                             */
                                setVisiblity(rv_management, iv_management, 300, ln_management);
                                break;
                            case 3:
                              /*  scrollTo = ((View) ln_submission.getParent().getParent()).getTop()+ln_submission.getTop();
                                nestedScrollView.scrollTo(0,scrollTo);
                         */
                                setVisiblity(rv_submission, iv_submission, 300, ln_submission);
                                break;
                        }

                    }
                });

                openPosition = position;
            }

            @Override
            public void onAnimationRepeat(Animation animation) {

            }
        });

        view.startAnimation(a);


    }

    /**
     * @Rehan initialize all views here
     */
    private void initViews() {

        nestedScrollView = findViewById(R.id.nestedScrollView);

        ln_content = findViewById(R.id.ln_content);
        rv_search = findViewById(R.id.rv_search);
        rv_general = findViewById(R.id.rv_general);
        rv_trouble = findViewById(R.id.rv_trouble);
        rv_management = findViewById(R.id.rv_management);
        rv_submission = findViewById(R.id.rv_submission);

        ln_general = findViewById(R.id.ln_general);
        ln_trouble = findViewById(R.id.ln_trouble);
        ln_management = findViewById(R.id.ln_management);
        ln_submission = findViewById(R.id.ln_submission);

        iv_general = findViewById(R.id.iv_general);
        iv_trouble = findViewById(R.id.iv_trouble);
        iv_management = findViewById(R.id.iv_management);
        iv_submission = findViewById(R.id.iv_submission);

        searchList = new ArrayList<>();
        generalList = new ArrayList<>();
        troubleList = new ArrayList<>();
        manageList = new ArrayList<>();
        submissionList = new ArrayList<>();

    }

    private void setupActionBar() {
        mToolbar = (Toolbar) findViewById(R.id.toolbar);
        mToolbar.setTitle("FAQ");
        setSupportActionBar(mToolbar);

        if (getSupportActionBar() != null) {
            getSupportActionBar().setDisplayHomeAsUpEnabled(true);
        }
    }

    @Override
    public boolean onOptionsItemSelected(@NonNull MenuItem item) {
        switch (item.getItemId()) {
            case android.R.id.home:
                // todo: goto back activity from here
                finish();
                return true;

            default:
                return super.onOptionsItemSelected(item);
        }

    }

    public ArrayList<MyQuery> searchFaqList(String value) {

        ArrayList<MyQuery> searchList = new ArrayList<>();

        for (int i = 0; i < generalList.size(); i++) {
            MyQuery query = generalList.get(i);
            if (query.getQuery().toLowerCase().contains(value) || query.getResponse().toLowerCase().equalsIgnoreCase(value)) {
                searchList.add(query);
            }
        }

        for (int i = 0; i < troubleList.size(); i++) {
            MyQuery query = troubleList.get(i);
            if (query.getQuery().toLowerCase().contains(value) || query.getResponse().toLowerCase().equalsIgnoreCase(value)) {
                searchList.add(query);
            }
        }

        for (int i = 0; i < manageList.size(); i++) {
            MyQuery query = manageList.get(i);
            if (query.getQuery().toLowerCase().contains(value) || query.getResponse().toLowerCase().equalsIgnoreCase(value)) {
                searchList.add(query);
            }
        }

        for (int i = 0; i < submissionList.size(); i++) {
            MyQuery query = submissionList.get(i);
            if (query.getQuery().toLowerCase().contains(value) || query.getResponse().toLowerCase().equalsIgnoreCase(value)) {
                searchList.add(query);
            }
        }

        return searchList;


    }

    @Override
    public void onClick(View v) {
        switch (v.getId()) {
            case R.id.ln_general:
                hideWithAnimation(0);

                break;
            case R.id.ln_trouble:
                hideWithAnimation(1);

                // setVisiblity(rv_trouble, iv_trouble, 300,ln_trouble);
                break;
            case R.id.ln_management:
                hideWithAnimation(2);
                //  setVisiblity(rv_management, iv_management, 300,ln_management);

                break;
            case R.id.ln_submission:
                hideWithAnimation(3);

                // setVisiblity(rv_submission, iv_submission, 300,ln_submission);
                break;
        }
    }
}