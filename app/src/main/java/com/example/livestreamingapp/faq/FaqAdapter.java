package com.example.livestreamingapp.faq;

import android.content.Context;
import android.text.Layout;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.TextView;

import androidx.annotation.NonNull;
import androidx.recyclerview.widget.RecyclerView;

import com.example.livestreamingapp.R;
import com.example.livestreamingapp.model.query.MyQuery;
import com.example.livestreamingapp.model.query.QueryResponseModel;

import java.util.List;

public class FaqAdapter extends RecyclerView.Adapter<FaqAdapter.Faqholder> {
    private Context context;
    List<MyQuery> faqList;

    public FaqAdapter(Context context,List<MyQuery> faqList){
        this.context = context;
        this.faqList = faqList;
    }
    @NonNull
    @Override
    public Faqholder onCreateViewHolder(@NonNull ViewGroup parent, int viewType) {
        View view = LayoutInflater.from(context).inflate(R.layout.list_item_faq,parent,false);
        return new Faqholder(view);
    }

    @Override
    public void onBindViewHolder(@NonNull Faqholder holder, int position) {
        int pos = position+1;
        MyQuery query = faqList.get(position);
        holder.tv_question .setText(query.getQuery());
        holder.tv_answer .setText("Ans : "+query.getResponse());
    }

    @Override
    public int getItemCount() {
        return faqList.size();
    }

    public class Faqholder  extends  RecyclerView.ViewHolder{
        private TextView tv_question;
        private TextView tv_answer;
        public Faqholder(@NonNull View itemView) {
            super(itemView);
            tv_question = itemView.findViewById(R.id.tv_question);
            tv_answer = itemView.findViewById(R.id.tv_answer);
        }
    }
}
