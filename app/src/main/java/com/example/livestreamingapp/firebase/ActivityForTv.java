package com.example.livestreamingapp.firebase;

import android.content.Intent;
import android.os.Bundle;
import android.os.Handler;
import android.view.WindowManager;

import com.example.livestreamingapp.MassTvActivity;
import com.example.livestreamingapp.R;
import com.example.livestreamingapp.login.MultipleLoginSelectionActivity;

public class ActivityForTv extends MassTvActivity {

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_splash);
        getWindow().addFlags(WindowManager.LayoutParams.FLAG_FULLSCREEN);


        new Handler().postDelayed(new Runnable() {
           @Override
           public void run() {
               startActivity(new Intent(ActivityForTv.this, MultipleLoginSelectionActivity.class));
               finish();
           }
       },1000);


    }
}