package com.example.livestreamingapp.firebase;

public class DeepLink {

    public static String createWebLink(String category,String id){
        //https://masstv.app/playwebseries/523
        //https://masstv.app/?video%CE%BC" + onDemandVideo.getId() + "%CE%BC" + onDemandVideo.getCategory()
        if(!category.equalsIgnoreCase("Original")){
            return "https://masstv.app/playwebseries/" +
                    id +
                    "/?video%CE%BC" + id + "%CE%BC" +category;
        }else {
            return "https://masstv.app/playoriginals/" +
                    id +
                    "/?video%CE%BC" + id + "%CE%BC" +category;
        }
    }

}
