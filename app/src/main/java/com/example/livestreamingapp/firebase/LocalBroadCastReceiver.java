package com.example.livestreamingapp.firebase;

import android.content.BroadcastReceiver;
import android.content.Context;
import android.content.Intent;

import com.example.livestreamingapp.home.ui.HomeDraweerStreamingActivity;

public class LocalBroadCastReceiver extends BroadcastReceiver {

    private  localBroadCastListener listener;


    public LocalBroadCastReceiver(localBroadCastListener listener){
        this.listener = listener;
    }

    @Override
    public void onReceive(Context context, Intent intent) {

        if(listener!=null){
            try {
                HomeDraweerStreamingActivity.isResume = true;
            }catch (Exception e){

            }

            listener.onGetBroadCast(intent);
        }

    }

    public interface localBroadCastListener{
        public void onGetBroadCast(Intent intent);
    }
}
