package com.example.livestreamingapp.firebase;

import android.app.NotificationChannel;
import android.app.NotificationManager;
import android.app.PendingIntent;
import android.content.Context;
import android.content.Intent;
import android.content.IntentFilter;
import android.graphics.Bitmap;
import android.graphics.BitmapFactory;
import android.media.RingtoneManager;
import android.net.Uri;
import android.os.AsyncTask;
import android.os.Build;
import android.util.Log;

import androidx.annotation.NonNull;
import androidx.core.app.NotificationCompat;
import androidx.core.app.NotificationManagerCompat;
import androidx.localbroadcastmanager.content.LocalBroadcastManager;

import com.example.livestreamingapp.R;
import com.example.livestreamingapp.home.ui.HomeDraweerStreamingActivity;
import com.example.livestreamingapp.model.ProfileData;
import com.example.livestreamingapp.notification.NotificationActivity;
import com.example.livestreamingapp.utils.NotificationMessage;
import com.example.livestreamingapp.utils.SharedPreferenceHandler;
import com.example.livestreamingapp.webservice.FcmService;
import com.google.firebase.messaging.FirebaseMessagingService;
import com.google.firebase.messaging.RemoteMessage;
import com.google.gson.Gson;

import java.io.IOException;
import java.io.InputStream;
import java.net.HttpURLConnection;
import java.net.MalformedURLException;
import java.net.URL;
import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Calendar;
import java.util.Date;
import java.util.List;
import java.util.TimeZone;
import java.util.concurrent.TimeUnit;

public class MyFirebaseMessagingService extends FirebaseMessagingService {

    private static final String NOTIFICATION_CHANNEL = "notification_mass_tv";
    SharedPreferenceHandler sharedPreferenceHandler;
    private LocalBroadCastReceiver localBroadCastReceiver;
    private PendingIntent pendingIntent;
    private Intent intent;
    private NotificationCompat.Builder builder;
    private NotificationManagerCompat notificationManager;

    @Override
    public void onMessageReceived(RemoteMessage remoteMessage) {


        try {
            Log.e("check msg "," "+new Gson().toJson(remoteMessage));
            sharedPreferenceHandler = new SharedPreferenceHandler(this);
            int count = sharedPreferenceHandler.getUnReadCount()+1;
            sharedPreferenceHandler.setUnReadCount(count);
            List<NotificationMessage> list = sharedPreferenceHandler.getAllMessage();
            NotificationMessage notificationMessage = new NotificationMessage();
            notificationMessage.setTitle(remoteMessage.getNotification().getTitle());
            notificationMessage.setTime(timeInNotificationFormat(new Date()));
            notificationMessage.setThumb(String.valueOf(remoteMessage.getNotification().getImageUrl()));
            notificationMessage.setMessage(remoteMessage.getNotification().getBody());
            Log.e("check msg "," after"+new Gson().toJson(notificationMessage));
            if(list==null){
                list = new ArrayList<>();
            }
            list.add(0,notificationMessage);
            sharedPreferenceHandler.saveAllNotification(list);
            LocalBroadcastManager.getInstance(this).sendBroadcast(new Intent("com.intent.notification"));
            createNotificationChannel(notificationMessage);
        }catch (Exception e){

        }

      //  sendNotification(remoteMessage);


    }

    public String timeInNotificationFormat(Date dateTime){
        SimpleDateFormat inputFormat = new SimpleDateFormat("yyyy-MM-dd'T'HH:mm:ss.SSS'Z'");
        inputFormat.setTimeZone(TimeZone.getDefault());
        return inputFormat.format(dateTime);

    }



    @Override
    public void onNewToken(@NonNull String s) {
        super.onNewToken(s);
        new SharedPreferenceHandler(this).saveFcmToken(s);
     /*   ProfileData data = new SharedPreferenceHandler(this).getProfileInfo();
        new FcmService(this).uploadFcmToken(s,""+data.getId());
*/
    }



    private void sendNotification(RemoteMessage intentMessage) {
        Intent intent = new Intent(this, NotificationActivity.class);
        intent.putExtra("title",intentMessage.getData().get("title"));

        intent.putExtra("time",System.currentTimeMillis());
        intent.putExtra("message",""+intentMessage.getData().get("message"));

        intent.addFlags(Intent.FLAG_ACTIVITY_NEW_TASK | Intent.FLAG_ACTIVITY_SINGLE_TOP | Intent.FLAG_ACTIVITY_CLEAR_TOP);
        int uniqueInt = (int) (System.currentTimeMillis() & 0xff);
        PendingIntent pendingIntent = PendingIntent.getActivity(getApplicationContext(), uniqueInt, intent,
                PendingIntent.FLAG_UPDATE_CURRENT);

        Uri defaultSoundUri= RingtoneManager.getDefaultUri(RingtoneManager.TYPE_NOTIFICATION);


        NotificationCompat.Builder notificationBuilder = new NotificationCompat.Builder(this, "channel_id")
                .setContentTitle(intentMessage.getData().get("title"))
                .setContentText(intentMessage.getData().get("body"))
                .setPriority(NotificationCompat.PRIORITY_HIGH)
                .setSound(defaultSoundUri)
                .setStyle(new NotificationCompat.BigTextStyle())
                .setSound(RingtoneManager.getDefaultUri(RingtoneManager.TYPE_NOTIFICATION))
                .setSmallIcon(R.mipmap.ic_launcher)
                .setContentIntent(pendingIntent)
                .setAutoCancel(true);

        NotificationManager notificationManager =
                (NotificationManager) getSystemService(Context.NOTIFICATION_SERVICE);

        notificationManager.notify(0, notificationBuilder.build());
    }


    private void createNotificationChannel(NotificationMessage notificationMessage) {
        // Create the NotificationChannel, but only on API 26+ because
        // the NotificationChannel class is new and not in the support library
         notificationManager = NotificationManagerCompat.from(this);
        if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.O) {
            CharSequence name = getString(R.string.channel_name);
            String description = getString(R.string.channel_description);
            int importance = NotificationManager.IMPORTANCE_DEFAULT;
            NotificationChannel channel = new NotificationChannel(NOTIFICATION_CHANNEL, name, importance);
            channel.setDescription(description);
            // Register the channel with the system; you can't change the importance
            // or other notification behaviors after this

            notificationManager.createNotificationChannel(channel);

        }

         intent = new Intent(this, NotificationActivity.class);
         pendingIntent = PendingIntent.getActivity(this, 0, intent, 0);


        if(notificationMessage.getThumb()!=null && notificationMessage.getThumb().length()>0){
           new sendPictureNotification(getApplicationContext(),notificationMessage).execute();
        }else {
            builder = new NotificationCompat.Builder(this, NOTIFICATION_CHANNEL)
                    .setSmallIcon(R.mipmap.ic_launcher)
                    .setContentTitle(notificationMessage.getTitle())
                    .setContentText(notificationMessage.getMessage())
                    .setPriority(NotificationCompat.PRIORITY_DEFAULT)

                    // Set the intent that will fire when the user taps the notification
                    .setContentIntent(pendingIntent)
                    .setAutoCancel(true);
            notificationManager.notify(0, builder.build());
        }




    }

    private class sendPictureNotification extends AsyncTask<String, Void, Bitmap> {

        Context ctx;
        String message;
        NotificationMessage notificationMessage;

        public sendPictureNotification(Context context,NotificationMessage notificationMessage) {
            super();
            this.ctx = context;
            this.notificationMessage = notificationMessage;
        }

        @Override
        protected Bitmap doInBackground(String... params) {

            InputStream in;
            try {

                URL url = new URL(notificationMessage.getThumb());
                HttpURLConnection connection = (HttpURLConnection)url.openConnection();
                connection.setDoInput(true);
                connection.connect();
                in = connection.getInputStream();
                Bitmap myBitmap = BitmapFactory.decodeStream(in);
                return myBitmap;
            } catch (MalformedURLException e) {
                e.printStackTrace();
            } catch (IOException e) {
                e.printStackTrace();
            }
            return null;
        }

        @Override
        protected void onPostExecute(Bitmap result) {

            super.onPostExecute(result);
            try {
                builder = new NotificationCompat.Builder(ctx, NOTIFICATION_CHANNEL)
                        .setSmallIcon(R.mipmap.ic_launcher)
                        .setContentTitle(notificationMessage.getTitle())
                        .setContentText(notificationMessage.getMessage())
                        .setStyle(new NotificationCompat.BigPictureStyle()
                                .bigPicture(result))/*Notification with Image*/
                        .setPriority(NotificationCompat.PRIORITY_DEFAULT)
                        // Set the intent that will fire when the user taps the notification
                        .setContentIntent(pendingIntent)
                        .setAutoCancel(true);

                notificationManager.notify(0, builder.build());

            } catch (Exception e) {
                e.printStackTrace();
            }
        }
    }
}
