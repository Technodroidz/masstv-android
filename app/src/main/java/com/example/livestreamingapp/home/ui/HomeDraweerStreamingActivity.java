package com.example.livestreamingapp.home.ui;

import android.app.Dialog;
import android.content.Context;
import android.content.DialogInterface;
import android.content.Intent;
import android.content.pm.ActivityInfo;
import android.content.res.Configuration;
import android.graphics.Color;
import android.graphics.Point;
import android.graphics.drawable.ColorDrawable;
import android.graphics.drawable.InsetDrawable;
import android.hardware.Sensor;
import android.hardware.SensorManager;
import android.net.Uri;
import android.os.AsyncTask;
import android.os.Bundle;
import android.os.Handler;
import android.support.v4.media.session.PlaybackStateCompat;
import android.util.Log;
import android.view.Gravity;
import android.view.Menu;
import android.view.MenuItem;
import android.view.MotionEvent;
import android.view.OrientationEventListener;
import android.view.View;
import android.view.Window;
import android.view.WindowManager;
import android.widget.Button;
import android.widget.EditText;
import android.widget.FrameLayout;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.ProgressBar;
import android.widget.SeekBar;
import android.widget.TextView;

import androidx.annotation.NonNull;
import androidx.appcompat.app.ActionBarDrawerToggle;
import androidx.appcompat.app.AlertDialog;
import androidx.appcompat.widget.AppCompatSeekBar;
import androidx.appcompat.widget.Toolbar;
import androidx.core.view.MenuItemCompat;
import androidx.drawerlayout.widget.DrawerLayout;
import androidx.recyclerview.widget.LinearLayoutManager;
import androidx.recyclerview.widget.RecyclerView;
import androidx.viewpager.widget.ViewPager;

import com.example.livestreamingapp.MassTvActivity;
import com.example.livestreamingapp.MassTvApplication;
import com.example.livestreamingapp.MediaItem;
import com.example.livestreamingapp.R;
import com.example.livestreamingapp.contact.QueryPagerActivity;
import com.example.livestreamingapp.expandedcontrols.ExpandedControlsActivity;
import com.example.livestreamingapp.faq.ActivityTermCondition;
import com.example.livestreamingapp.firebase.LocalBroadCastReceiver;
import com.example.livestreamingapp.home.ui.adapter.AdapterVideoList;
import com.example.livestreamingapp.home.ui.adapter.EndlessPagerAdapter;
import com.example.livestreamingapp.home.ui.adapter.ViewPagerAdapter;
import com.example.livestreamingapp.model.CountryResponse;
import com.example.livestreamingapp.model.LiveShow;
import com.example.livestreamingapp.model.LiveShowData;
import com.example.livestreamingapp.model.LoginResponse;
import com.example.livestreamingapp.model.MySubscription;
import com.example.livestreamingapp.model.OnDemandCategory;
import com.example.livestreamingapp.model.OnDemandVideo;
import com.example.livestreamingapp.model.ProfileData;
import com.example.livestreamingapp.model.VideoByIdResponse;
import com.example.livestreamingapp.model.homescreen.CategoryModel;
import com.example.livestreamingapp.model.homescreen.HomeScreenData;
import com.example.livestreamingapp.model.homescreen.PromotionBanner;
import com.example.livestreamingapp.model.webseries.ContentDetail;
import com.example.livestreamingapp.model.webseries.ContentInfo;
import com.example.livestreamingapp.notification.NotificationActivity;
import com.example.livestreamingapp.ondemand.AdapterHomeVideoCategory;
import com.example.livestreamingapp.ondemand.AdapterViewClickListener;
import com.example.livestreamingapp.ondemand.OnDemandCategoryActivity;
import com.example.livestreamingapp.ondemand.VideoListActivity;
import com.example.livestreamingapp.profile.ProfileActivity;
import com.example.livestreamingapp.subscription.SubscriptionSelectorActivity;
import com.example.livestreamingapp.upload.UploadContentActivity;
import com.example.livestreamingapp.utils.BottomNavigationHelper;
import com.example.livestreamingapp.utils.CompleteProfile;
import com.example.livestreamingapp.utils.ConnectivityReceiver;
import com.example.livestreamingapp.utils.Constants;
import com.example.livestreamingapp.utils.CustomProgressDialog;
import com.example.livestreamingapp.utils.DateCalculation;
import com.example.livestreamingapp.utils.SharedPreferenceHandler;
import com.example.livestreamingapp.utils.Utils;
import com.example.livestreamingapp.video.VideoDetailActivity;
import com.example.livestreamingapp.webseries.WebSeriesDetailActivity;
import com.example.livestreamingapp.webservice.APIClient;
import com.example.livestreamingapp.webservice.APIInterface;
import com.example.livestreamingapp.webservice.FcmService;
import com.example.livestreamingapp.webservice.GenericResponseClass;
import com.example.livestreamingapp.webservice.GetOnDemandVideoList;
import com.example.livestreamingapp.webservice.SubscriptionService;
import com.example.livestreamingapp.webservice.WebServiceConstant;
import com.facebook.shimmer.ShimmerFrameLayout;
import com.google.android.exoplayer2.C;
import com.google.android.exoplayer2.DefaultLoadControl;
import com.google.android.exoplayer2.ExoPlaybackException;
import com.google.android.exoplayer2.ExoPlayerFactory;
import com.google.android.exoplayer2.Format;
import com.google.android.exoplayer2.PlaybackParameters;
import com.google.android.exoplayer2.SimpleExoPlayer;
import com.google.android.exoplayer2.Timeline;
import com.google.android.exoplayer2.extractor.DefaultExtractorsFactory;
import com.google.android.exoplayer2.extractor.ExtractorsFactory;
import com.google.android.exoplayer2.source.ExtractorMediaSource;
import com.google.android.exoplayer2.source.MediaSource;
import com.google.android.exoplayer2.source.MergingMediaSource;
import com.google.android.exoplayer2.source.SingleSampleMediaSource;
import com.google.android.exoplayer2.source.TrackGroupArray;
import com.google.android.exoplayer2.trackselection.DefaultTrackSelector;
import com.google.android.exoplayer2.trackselection.TrackSelectionArray;
import com.google.android.exoplayer2.ui.AspectRatioFrameLayout;
import com.google.android.exoplayer2.ui.PlayerView;
import com.google.android.exoplayer2.upstream.BandwidthMeter;
import com.google.android.exoplayer2.upstream.DataSource;
import com.google.android.exoplayer2.upstream.DefaultBandwidthMeter;
import com.google.android.exoplayer2.upstream.DefaultDataSourceFactory;
import com.google.android.exoplayer2.util.MimeTypes;
import com.google.android.exoplayer2.util.Util;
import com.google.android.gms.cast.MediaInfo;
import com.google.android.gms.cast.MediaLoadRequestData;
import com.google.android.gms.cast.MediaMetadata;
import com.google.android.gms.cast.framework.CastButtonFactory;
import com.google.android.gms.cast.framework.CastContext;
import com.google.android.gms.cast.framework.CastSession;
import com.google.android.gms.cast.framework.CastState;
import com.google.android.gms.cast.framework.CastStateListener;
import com.google.android.gms.cast.framework.IntroductoryOverlay;
import com.google.android.gms.cast.framework.SessionManagerListener;
import com.google.android.gms.cast.framework.media.RemoteMediaClient;
import com.google.android.material.appbar.AppBarLayout;
import com.google.android.material.navigation.NavigationView;
import com.google.android.material.snackbar.Snackbar;
import com.google.gson.Gson;

import java.io.BufferedReader;
import java.io.File;
import java.io.InputStreamReader;
import java.net.HttpURLConnection;
import java.net.URL;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import java.util.Timer;
import java.util.TimerTask;

import javax.annotation.Nullable;

import okhttp3.ResponseBody;
import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;

import static com.example.livestreamingapp.utils.Constants.CATEGORY_TYPE;

public class HomeDraweerStreamingActivity extends MassTvActivity implements
        View.OnClickListener, SimpleExoPlayer.EventListener, AdapterViewClickListener,
        NavigationView.OnNavigationItemSelectedListener,
        GetOnDemandVideoList.onDemandVideoListListener,
        ConnectivityReceiver.ConnectivityReceiverListener,
        LocalBroadCastReceiver.localBroadCastListener,
        CompleteProfile.profileAlertListener {

    public static boolean isCaptionOff;

    public static final int DEFAULT_MIN_BUFFER_MS = 50000;
    public static final int DEFAULT_MAX_BUFFER_MS = 50000;
    public static final int DEFAULT_BUFFER_FOR_PLAYBACK_MS = 5000;
    public static final int DEFAULT_BUFFER_FOR_PLAYBACK_AFTER_REBUFFER_MS = 4000;


    private RecyclerView rv_on_demand;
    private RecyclerView rv_video_category;

    private RecyclerView rv_live_streaming;
    private TextView tv_label_streaming;

    private static final String TAG = HomeDraweerStreamingActivity.class.getName();
    private SimpleExoPlayer player;
    private PlayerView exoplayerView_portrait;
    private ImageView iv_play_pause;
    FrameLayout fl_controller_portrait;
    FrameLayout fl_controller_landscape;
    private int playbackPosition = 0;
    AppCompatSeekBar mSeekbar;

    private Handler handler;
    private Runnable runnable;
    boolean isVideoPlaying;
    private PlaybackState mPlaybackState;
    private Timer mSeekbarTimer;
    private Timer mControllersTimer;
    private PlaybackLocation mLocation;
    private Handler mHandler;
    private CastSession mCastSession;
    private SessionManagerListener<CastSession> mSessionManagerListener;
    private MediaItem mSelectedMedia;
    private CastContext mCastContext;

    private ImageView iv_full_screen_toggle;
    private AppBarLayout appBarLayout;
    private LinearLayout lin_content;
    private ImageView iv_full_screen_toggle_landscape;
    private ImageView iv_play_pause_landscape;
    private SeekBar mSeekbar_landscape;
    private AdapterVideoList adapterLiveStreaming;
    private AdapterVideoList adapterVideoOnDemand;
    private AdapterVideoList adapterCategoryLists;
    private DrawerLayout drawer;
    private CastStateListener mCastStateListener;
    private IntroductoryOverlay mIntroductoryOverlay;
    private MenuItem mediaRouteMenuItem;

    Handler userInteractionHandler;
    Runnable userInteractionRunnable;

    ProfileData profileInfo;
    private View headerView;
    private TextView nav_header_title;
    private TextView nav_header_subtitle;
    private APIInterface apiInterface;
    private LiveShowData liveShow;
    private TextView tv_live_subtitle;

    private TextView tv_live_subtitle_portrait;
    private TextView
            tv_video_type_protrait;

    private PlayerView exoplayerView_landscape;
    private boolean shouldRetry;
    private ProgressBar progress_portrait_live;
    private ProgressBar progress_landscape_live;
    //  private AdapterVideoCategory adapterVideoCategory;
    private AdapterHomeVideoCategory adapterVideoCategory;
    private SharedPreferenceHandler sharedPreferenceHandler;
    private MySubscription mySubscription;
    private Handler liveVideoSerchHandler;
    private Runnable liveVideoSearchRunnable;
    private ShimmerFrameLayout shimmer_view_container;
    private boolean isBuffering;
    private TextView tv_letter_name;
    private TextView notification_count;

    public static boolean isAliveActivity;
    public static boolean isScreenVisible;
    private boolean isPlayingAd;
    public boolean isStartVideo;
    public static boolean isResume;
    private TextView tv_video_type;
    private Runnable newLiveVideoRunnable;
    Handler newLiveVideoHandler;
    private int last10Second;
    private LinearLayout ln_desc_port;

    private ImageView iv_caption_portrait;
    private ImageView iv_caption_landscape;

    private TextView tv_time_portrait;
    private TextView tv_time_lanscape;
    private String totalVideoTime;
    private boolean isLandscape;

    private SensorManager mSensorManager;
    private Sensor mRotationSensor;
    private LiveShowData analyticsVideo;

    private static final int SENSOR_DELAY = 500 * 1000; // 500ms
    private static final int FROM_RADS_TO_DEGS = -57;
    private OrientationEventListener myOrientationEventListener;
    private boolean PORTRAIT_MODE;
    private BottomNavigationHelper bottomNavigationHelper;
    private boolean isCompleteProfileOpen;
    private CustomProgressDialog customProgressDialog;
    private boolean secondTime;

    public static long lastDuration = 0;
    public static String lastVideo = null;
    private ImageView iv_banner;
    private ViewPager viewPagerBanner;
    private Handler bannerHandler;
    private Runnable bannerRunnable;
    private List<PromotionBanner> promotionBanners;
    private ViewPagerAdapter mViewPagerAdapter;
    private int bannerPosition;
    private EndlessPagerAdapter endlessPagerAdapter;
    private View decorView;
    private boolean isFirstTimeVolume;
    private long startTime;
    private String IPaddress;
    private int videoPosition;


    public boolean isTablet() {
        return (getResources().getConfiguration().screenLayout
                & Configuration.SCREENLAYOUT_SIZE_MASK)
                >= Configuration.SCREENLAYOUT_SIZE_LARGE;

    }

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_home_drawer);
        Toolbar toolbar = findViewById(R.id.toolbar);
        customProgressDialog = new CustomProgressDialog(HomeDraweerStreamingActivity.this);
        new GetIpAsyncTask().execute();
        toolbar.setTitle("");
        setSupportActionBar(toolbar);
        decorView = getWindow().getDecorView();
        isAliveActivity = true;
        bottomNavigationHelper = new BottomNavigationHelper(this);
        sharedPreferenceHandler = new SharedPreferenceHandler(this);
        profileInfo = sharedPreferenceHandler.getProfileInfo();

        drawer = findViewById(R.id.drawer_layout);
        int lockMode =
                DrawerLayout.LOCK_MODE_LOCKED_CLOSED;

        ActionBarDrawerToggle toggle = new ActionBarDrawerToggle(
                this, drawer, toolbar, R.string.navigation_drawer_open, R.string.navigation_drawer_close);
        drawer.addDrawerListener(toggle);
        toggle.syncState();
        drawer.setDrawerLockMode(lockMode);
        if (getSupportActionBar() != null) {
            getSupportActionBar().setHomeButtonEnabled(false);
            getSupportActionBar().setDisplayHomeAsUpEnabled(false);
            getSupportActionBar().setHomeAsUpIndicator(R.drawable.ic_hameburger);
        }

        NavigationView navigationView = (NavigationView) findViewById(R.id.nav_view);
        navigationView.setItemIconPadding(0);
        navigationView.setNavigationItemSelectedListener(this);
        headerView = navigationView.getHeaderView(0);
        nav_header_title = headerView.findViewById(R.id.nav_title);
        nav_header_subtitle = headerView.findViewById(R.id.nav_subtitle);
        tv_letter_name = headerView.findViewById(R.id.tv_letter_name);

        notification_count = (TextView) MenuItemCompat.getActionView(navigationView.getMenu().
                findItem(R.id.nav_notification));
        notification_count.setGravity(Gravity.CENTER);
        notification_count.setTextColor(Color.RED);
        // set profile info in Navigation Drawer
       // updateNavigationHeader();
        mCastStateListener = new CastStateListener() {
            @Override
            public void onCastStateChanged(int newState) {
                if (newState != CastState.NO_DEVICES_AVAILABLE) {
                    showIntroductoryOverlay();
                }
            }
        };
        setupCastListener();
        mCastContext = CastContext.getSharedInstance(this);
        mCastSession = mCastContext.getSessionManager().getCurrentCastSession();
        // this mediaItem is set static for for now it is used to play on cast devices
        mSelectedMedia = new MediaItem();
       /* mSelectedMedia.setUrl(mp4Url);
        mSelectedMedia.setTitle("Rated 18+ | KICKBOXER RETALIATION");
        mSelectedMedia.setSubTitle("Rated 18+");*/

        apiInterface = APIClient.getClient().create(APIInterface.class);

        mHandler = new Handler();
        bannerHandler = new Handler();
        initViews();
        // search for new live video if live show is null
        liveVideoSerchHandler = new Handler();
        liveVideoSearchRunnable = new Runnable() {
            @Override
            public void run() {
                if (liveShow == null && isScreenVisible) {
                    showLiveShow();
                }
            }
        };
        showLiveShow();


        File directory = new File(getCacheDir(), "media/");
        if (!directory.exists()) {
            try {
                directory.mkdir();
            } catch (Exception e) {

            }
        }

        decorView.setOnSystemUiVisibilityChangeListener(new View.OnSystemUiVisibilityChangeListener() {
            @Override
            public void onSystemUiVisibilityChange(int visibility) {
              /*  if ((visibility & View.SYSTEM_UI_FLAG_FULLSCREEN) == 0) {

                    // TODO: The system bars are visible. Make any desired
                    // adjustments to your UI, such as showing the action bar or
                    // other navigational controls.
                } else {
                    // TODO: The system bars are NOT visible. Make any desired
                    // adjustments to your UI, such as hiding the action bar or
                    // other navigational controls.
                }*/
                startHandler();
            }
        });

        decorView.setOnSystemUiVisibilityChangeListener(new View.OnSystemUiVisibilityChangeListener() {
            @Override
            public void onSystemUiVisibilityChange(int visibility) {
              /*  if ((visibility & View.SYSTEM_UI_FLAG_FULLSCREEN) == 0) {

                    // TODO: The system bars are visible. Make any desired
                    // adjustments to your UI, such as showing the action bar or
                    // other navigational controls.
                } else {
                    // TODO: The system bars are NOT visible. Make any desired
                    // adjustments to your UI, such as hiding the action bar or
                    // other navigational controls.
                }*/

                startHandler();
            }
        });

       /* String deepLink = new SharedPreferenceHandler(HomeDraweerStreamingActivity.this).getDeepLink();
        Log.e("check deep link","HOme "+deepLink);
        if (deepLink!=null &&  deepLink.contains("%CE%BC") && deepLink.length()>10) {
            String videoId = deepLink.toString().split("%CE%BC")[1];
            String category = deepLink.toString().split("%CE%BC")[2];
            openVideoUsingReferLink(videoId,category);
        }*/

    }

    /**
     * open video perform task
     *
     * @param videoId
     */
    private void openVideoUsingReferLink(String videoId, String category) {
        new SharedPreferenceHandler(HomeDraweerStreamingActivity.this).saveDeepLink("null");
        customProgressDialog.show();
        Map<String, String> map = new HashMap<>();
        map.put("video_id", videoId);
        if (category.equalsIgnoreCase("Original")) {
            map.put("video_category", category);
        } else {
            map.put("video_category", category);
        }
        Call<VideoByIdResponse> call = apiInterface.getVideoById(map);
        call.enqueue(new Callback<VideoByIdResponse>() {
            @Override
            public void onResponse(Call<VideoByIdResponse> call, Response<VideoByIdResponse> response) {


                new Handler().postDelayed(new Runnable() {
                    @Override
                    public void run() {
                        customProgressDialog.dismiss();
                        startActivity(new Intent(HomeDraweerStreamingActivity.this,
                                VideoDetailActivity.class).
                                putExtra(Constants.SELECTED_ON_DEMAND_VIDEO, response.body().getOnDemandVideo()));
                    }
                }, 300);


            }

            @Override
            public void onFailure(Call<VideoByIdResponse> call, Throwable t) {

                customProgressDialog.dismiss();

            }
        });
    }

    /**
     * @param onDemandVideo
     * @Rehan Ali
     * Send Analytics Report
     */
    private void sendAnaLyticsReport(OnDemandVideo onDemandVideo) {



        long endTime = System.currentTimeMillis();

        String minute =  DateCalculation.getTimeDiffinMinute(startTime, endTime, player.getDuration());

        if(!minute.equalsIgnoreCase("0") &&  !minute.equalsIgnoreCase("0.0")){
            Map<String, String> map = new HashMap<>();
            map.put("user_id", "" + new SharedPreferenceHandler(HomeDraweerStreamingActivity.this).getProfileInfo().getId());
            map.put("video_id", "" + onDemandVideo.getId());
            map.put("video_category", "" + onDemandVideo.getCategory());
            map.put("video_start_date", "" + DateCalculation.getDateInDD_MM_YY(startTime));
            map.put("video_start_time", "" + DateCalculation.getTimeInHHmmss(startTime));
            map.put("video_end_time", "" + DateCalculation.getTimeInHHmmss(endTime));
            map.put("total_watched_duration", "" + DateCalculation.getTimeDiffinMinute(startTime, endTime, player.getDuration()));
            map.put("from_country", IPaddress);

            startTime = 0;

            Call<ResponseBody> call = apiInterface.sendUserAnalyticsData(map);
            call.enqueue(new Callback<ResponseBody>() {
                @Override
                public void onResponse(Call<ResponseBody> call, Response<ResponseBody> response) {
                    // startTime = 0;

                }

                @Override
                public void onFailure(Call<ResponseBody> call, Throwable t) {

                }
            });
        }



        //  startTime = 0;
    }


    /**
     * perform background operation of getting current country name
     *
     * @param url
     * @return
     */
    public String findJSONFromUrl(String url) {
        String result = "";
        try {
            URL urls = new URL(url);
            HttpURLConnection conn = (HttpURLConnection) urls.openConnection();
            conn.setReadTimeout(15000); //milliseconds
            conn.setConnectTimeout(15000); // milliseconds
            conn.setRequestMethod("GET");

            conn.connect();

            if (conn.getResponseCode() == HttpURLConnection.HTTP_OK) {

                BufferedReader reader = new BufferedReader(new InputStreamReader(
                        conn.getInputStream(), "iso-8859-1"), 8);
                StringBuilder sb = new StringBuilder();
                String line = null;
                while ((line = reader.readLine()) != null) {
                    sb.append(line + "\n");
                }
                result = sb.toString();
            } else {

                return "error";
            }


        } catch (Exception e) {
            // System.out.println("exception in jsonparser class ........");
            e.printStackTrace();
            return "error";
        }

        return result;
    } // method ends


    public class GetIpAsyncTask extends AsyncTask<String, String, String> {


        @Override
        protected String doInBackground(String... strings) {
            return findJSONFromUrl("http://ip-api.com/json");
        }

        public GetIpAsyncTask() {

        }

        @Override
        protected void onPreExecute() {
            super.onPreExecute();
        }

        @Override
        protected void onPostExecute(String s) {
            super.onPostExecute(s);

            if (s.equalsIgnoreCase("error")) {

            } else {
                try {
                    IPaddress = new Gson().fromJson(s, CountryResponse.class).getCountry();
                } catch (Exception e) {

                }


            }

        }
    }


    /**
     * Here we will call the method for getting onDemand video list
     * and set the list to ui
     */
    private void FetchOnDemandVideoList() {
        new GetOnDemandVideoList(this, this);
    }

    public void completeProfileAlert() {
        isCompleteProfileOpen = true;
        View layout = getLayoutInflater().inflate(R.layout.layout_register,
                null);
        final Dialog dialog = setDialog(HomeDraweerStreamingActivity.this, layout);
        final EditText et_fname = (EditText) layout.findViewById(R.id.et_firstname);
        final EditText et_lname = (EditText) layout.findViewById(R.id.et_lastname);
        final EditText et_phone_number = (EditText) layout.findViewById(R.id.et_phone_number);
        final EditText et_email = (EditText) layout.findViewById(R.id.et_email);
        et_email.setText("" + profileInfo.getEmail());
        final ImageView iv_close = (ImageView) layout.findViewById(R.id.iv_close);
        Button btnOk = (Button) layout.findViewById(R.id.btnOk);
        iv_close.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                isCompleteProfileOpen = false;
                resumeMediaPlayer();
                dialog.dismiss();
            }
        });
        dialog.setCancelable(false);
        dialog.getWindow().setLayout(WindowManager.LayoutParams.MATCH_PARENT, WindowManager.LayoutParams.WRAP_CONTENT);
        btnOk.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                boolean valid = true;
                if (et_fname.getText() != null && et_fname.getText().toString().isEmpty()) {
                    valid = false;
                    et_fname.requestFocus();
                    et_fname.setError("First Name is Mandatory!");
                } else if (et_phone_number.getText() != null && et_phone_number.getText().toString().isEmpty()) {
                    valid = false;
                    et_phone_number.setError("Field can't be left blank");
                } else if (et_phone_number.getText() != null && !et_phone_number.getText().toString().isEmpty()) {
                    if (et_phone_number.getText().toString().length() < 10) {
                        valid = false;
                        et_phone_number.setError("Please enter valid phone number!");
                    }
                }
                if (valid) {
                    customProgressDialog.show();
                    final ProfileData data = new SharedPreferenceHandler(HomeDraweerStreamingActivity.this).getProfileInfo();
                    Map<String, String> credential = new HashMap<>();
                    String name = et_fname.getText().toString();
                    if (et_lname.getText() != null && !et_lname.getText().toString().isEmpty()) {
                        name = name + " " + et_lname.getText().toString();
                    }
                    credential.put("name", name);
                    credential.put("phone_number", et_phone_number.getText().toString());
                    if (data.getCity() != null) {
                        credential.put("country", data.getCountry());
                        credential.put("state", data.getState());
                        credential.put("city", data.getCity());
                    } else {
                        credential.put("country", "");
                        credential.put("state", "");
                        credential.put("city", "");
                    }
                    credential.put("user_id", "" + data.getId());
                    //   Log.e("check profile ","json "+new Gson().toJson(credential));
                    // credential.put("profile_picture", null);
                    Call<LoginResponse> editProfile = apiInterface.editProfile(credential);

                    // progressDialog.show();

                    editProfile.enqueue(
                            new Callback<LoginResponse>() {
                                @Override
                                public void onResponse(
                                        Call<LoginResponse> call, Response<LoginResponse> response) {
                                    // progressDialog.dismiss();


                                    customProgressDialog.dismiss();
                                    if (response.code() == 200) {

                                        isCompleteProfileOpen = false;
                                        resumeMediaPlayer();
                                        dialog.cancel();

                                        //  Log.e("Check ","res success "+new Gson().toJson(response.body()));

                                        if (response.body().getStatus()) {
                                            // dialog.dismiss();

                                            data.setUserName(et_fname.getText().toString() + " " + et_lname.getText().toString());

                                            new SharedPreferenceHandler(HomeDraweerStreamingActivity.this)
                                                    .setPreference(Constants.PROFILE_INFO, new Gson().toJson(data));
                                            finish();

                                        } else {
                                            new AlertDialog.Builder(HomeDraweerStreamingActivity.this, R.style.AlertDialogStyle)
                                                    .setCancelable(false)
                                                    .setMessage(getString(R.string.something_went_wrong))
                                                    .setPositiveButton(
                                                            "Ok",
                                                            new DialogInterface.OnClickListener() {
                                                                @Override
                                                                public void onClick(
                                                                        DialogInterface dialog, int which) {
                                                                }
                                                            })
                                                    .show();
                                        }


                                    }
                                }

                                @Override
                                public void onFailure(Call<LoginResponse> call, Throwable t) {
                                    customProgressDialog.dismiss();
                                    try {

                                        new AlertDialog.Builder(HomeDraweerStreamingActivity.this, R.style.AlertDialogStyle)
                                                .setCancelable(false)
                                                .setMessage("" + t.getMessage())
                                                .setPositiveButton(
                                                        "Ok",
                                                        new DialogInterface.OnClickListener() {
                                                            @Override
                                                            public void onClick(
                                                                    DialogInterface dialog, int which) {
                                                            }
                                                        })
                                                .show();
                                    } catch (Exception e) {

                                    }

                                }
                            });
                }

            }
        });
        dialog.show();

    }

    public static Dialog setDialog(Context context, View view) {
        Dialog dialog_post_type = new Dialog(context);
        dialog_post_type.requestWindowFeature(Window.FEATURE_NO_TITLE);
        dialog_post_type.getWindow().setBackgroundDrawable(new ColorDrawable(Color.TRANSPARENT));
        dialog_post_type.setCancelable(true);
        dialog_post_type.setContentView(view);
        return dialog_post_type;
    }

    /**
     * Get Live Show
     */
    private void showLiveShow() {
        shouldRetry = true;

        final Call<LiveShow> liveShowCall = apiInterface.getLiveVideo();
        liveShowCall.enqueue(new Callback<LiveShow>() {
            @Override
            public void onResponse(Call<LiveShow> call, Response<LiveShow> response) {
                shouldRetry = false;

                if (response.code() == 200 && response.body().getSuccess() && liveShow == null) {

                    isPlayingAd = false;
                    LiveShow liveShowRes = (LiveShow) response.body();
                    liveShow = liveShowRes.getData();
                    ln_desc_port.setVisibility(View.VISIBLE);
                    analyticsVideo = liveShow;

                    if (liveShow.getVideo_type().equalsIgnoreCase("ad_video")) {
                        if (liveShow.getNextVideoDetail() != null && liveShow.getNextVideoDetail().size() > 0) {
                            analyticsVideo = new SharedPreferenceHandler(HomeDraweerStreamingActivity.this).getAdVideo();

                            if (analyticsVideo!=null) {
                                videoPosition = getCurrentVideoPosition(liveShow.getNextVideoDetail(),analyticsVideo);
                                updatePlayerWithNewVideo(analyticsVideo);
                            } else {
                                videoPosition = 0;
                                analyticsVideo = liveShow.getNextVideoDetail().get(0);
                                updatePlayerWithNewVideo(analyticsVideo);
                            }
                        } else {
                            hideBuffering();
                            startLiveVideoHandler();
                        }
                    } else {
                        updatePlayerWithNewVideo(analyticsVideo);
                    }


                } else {
                    hideBuffering();
                    startLiveVideoHandler();
                }
            }

            @Override
            public void onFailure(Call<LiveShow> call, Throwable t) {
                isStartVideo = false;
                hideBuffering();
                shouldRetry = false;
                ln_desc_port.setVisibility(View.GONE);
                startLiveVideoHandler();
            }
        });

    }

    private int getCurrentVideoPosition(List<LiveShowData> nextVideoDetail, LiveShowData analyticsVideo) {
        for(int i =0;i<nextVideoDetail.size();i++){
            if(nextVideoDetail.get(i).getId().equals(analyticsVideo.getId())){
                return i;
            }
        }
        return 0;
    }

    /**
     * Start handler for getting live video in every 6 seconds if live video data is null
     */
    private void startLiveVideoHandler() {
        if (liveShow == null) {
            try {
                liveVideoSerchHandler.removeCallbacks(liveVideoSearchRunnable);
            } catch (Exception e) {

            }
            if (liveVideoSerchHandler != null && liveShow == null && !isCompleteProfileOpen) {
                liveVideoSerchHandler.postDelayed(liveVideoSearchRunnable, 6 * 1000);
            }
        }

    }

    private void stopLiveVideoHandler() {
        try {
            liveVideoSerchHandler.removeCallbacks(liveVideoSearchRunnable);
        } catch (Exception e) {

        }
    }


    private void showBuffering() {

        isBuffering = true;
        // mPlaybackState = PlaybackState.BUFFERING;
        if (checkLandscapeOrientation()) {
            progress_landscape_live.setVisibility(View.VISIBLE);
            progress_portrait_live.setVisibility(View.GONE);
        } else {
            progress_portrait_live.setVisibility(View.VISIBLE);
            progress_landscape_live.setVisibility(View.GONE);
        }


        /*if (checkLandscapeOrientation()) {
            fl_controller_landscape.setVisibility(View.VISIBLE);
            fl_controller_portrait.setVisibility(View.GONE);
        } else {
            fl_controller_portrait.setVisibility(View.VISIBLE);
            fl_controller_landscape.setVisibility(View.GONE);
        }*/
    }

    private void hideBuffering() {
        isBuffering = false;
        //isStartVideo = true;
        progress_landscape_live.setVisibility(View.GONE);
        progress_portrait_live.setVisibility(View.GONE);

    }


    /**
     * in this method we will the navigation Header content
     * like user name, phone number and image
     */
    private void updateNavigationHeader() {

        profileInfo = new SharedPreferenceHandler(HomeDraweerStreamingActivity.this).getProfileInfo();
        nav_header_title.setText("User Name");

        if(profileInfo!=null){
             if (profileInfo.getEmail()!=null && profileInfo.getEmail().length() > 19) {
            String subString = profileInfo.getEmail().substring(0, 15);
            nav_header_subtitle.setText("" + subString + "...");

        } else {
            nav_header_subtitle.setText("" + profileInfo.getEmail());

        }

        if (profileInfo.getUserName() != null) {
            nav_header_title.setText(profileInfo.getUserName());
        }
        if (profileInfo.getPhoneNumber() != null) {
            nav_header_subtitle.setText(profileInfo.getPhoneNumber());
        }


        tv_letter_name.setText("" + nav_header_title.getText().toString().charAt(0));
        }



    }

    // show google cast overlay animation when first time activity open
    private void showIntroductoryOverlay() {
        if (mIntroductoryOverlay != null) {
            mIntroductoryOverlay.remove();
        }
        if ((mediaRouteMenuItem != null) && mediaRouteMenuItem.isVisible()) {
            new Handler().post(new Runnable() {
                @Override
                public void run() {
                    mIntroductoryOverlay = new IntroductoryOverlay.Builder(
                            HomeDraweerStreamingActivity.this, mediaRouteMenuItem)
                            .setTitleText("Introducing Cast")
                            .setSingleTime()
                            .setOnOverlayDismissedListener(
                                    new IntroductoryOverlay.OnOverlayDismissedListener() {
                                        @Override
                                        public void onOverlayDismissed() {
                                            mIntroductoryOverlay = null;
                                        }
                                    })
                            .build();
                    mIntroductoryOverlay.show();
                }
            });
        }
    }

    /**
     * Initialize all the views here
     */
    public void initViews() {

        promotionBanners = new ArrayList<>();

        iv_banner = findViewById(R.id.iv_banner);
        viewPagerBanner = findViewById(R.id.viewPagerBanner);

        //initalize viewpager
        // Initializing the ViewPagerAdapter
        //   viewPagerBanner.setOnPageChangeListener(new CircularViewPagerHandler(viewPagerBanner));
        mViewPagerAdapter = new ViewPagerAdapter(HomeDraweerStreamingActivity.this, promotionBanners, new ViewPagerAdapter.BannerListener() {
            @Override
            public void onBannerListener(PromotionBanner promotionBanner) {
                Intent intentBrowser = new Intent(Intent.ACTION_VIEW);
                Uri u = Uri.parse(promotionBanner.getWebsiteUrl());
                intentBrowser.setData(u);
                startActivity(intentBrowser);
            }
        });


        // Adding the Adapter to the ViewPager
        viewPagerBanner.setAdapter(mViewPagerAdapter);

        iv_caption_landscape = findViewById(R.id.iv_caption_landscape);
        iv_caption_portrait = findViewById(R.id.iv_caption_portrait);

        tv_time_portrait = findViewById(R.id.tv_time_portrait);
        tv_time_lanscape = findViewById(R.id.tv_time_landscape);

        rv_live_streaming = (RecyclerView) findViewById(R.id.rv_live_streaming);
        rv_on_demand = (RecyclerView) findViewById(R.id.rv_on_demand);
        rv_video_category = (RecyclerView) findViewById(R.id.rv_video_category);

        shimmer_view_container = findViewById(R.id.shimmer_view_container);

        progress_portrait_live = (ProgressBar) findViewById(R.id.progress_portrait_live);
        progress_landscape_live = (ProgressBar) findViewById(R.id.progress_landscape_live);

        appBarLayout = findViewById(R.id.appBar);
        lin_content = findViewById(R.id.lin_content);

        tv_video_type = findViewById(R.id.tv_video_type);

        fl_controller_portrait = (FrameLayout) findViewById(R.id.fl_controller_portrait);
        fl_controller_landscape = (FrameLayout) findViewById(R.id.controllers2);

        iv_play_pause = (ImageView) findViewById(R.id.iv_play_pause);
        iv_play_pause_landscape = (ImageView) findViewById(R.id.iv_play_pause_landscape);

        tv_live_subtitle = (TextView) findViewById(R.id.tv_live_subtitle);

        tv_video_type_protrait = (TextView) findViewById(R.id.tv_video_type_portrait);
        tv_live_subtitle_portrait = (TextView) findViewById(R.id.tv_live_subtitle_portrait);
        ln_desc_port = (LinearLayout) findViewById(R.id.ln_desc_port);


        iv_full_screen_toggle = (ImageView) findViewById(R.id.iv_full_screen_toggle);
        iv_full_screen_toggle_landscape = (ImageView) findViewById(R.id.iv_full_screen_toggle_landscape);

        exoplayerView_portrait = findViewById(R.id.exoplayerView);
        exoplayerView_landscape = findViewById(R.id.exoplayerView_landscape);
        exoplayerView_portrait.setResizeMode(AspectRatioFrameLayout.RESIZE_MODE_FIT);
        exoplayerView_landscape.setResizeMode(AspectRatioFrameLayout.RESIZE_MODE_FIT);

        mSeekbar = findViewById(R.id.seekbar);
        mSeekbar_landscape = findViewById(R.id.seekbar_landscape);
        //  mSeekbar.setProgressDrawable(getResources().getDrawable(R.drawable.shape_seekbar));

        handler = new Handler();

        iv_play_pause.setVisibility(View.VISIBLE);
        iv_play_pause.setOnClickListener(this);

        iv_caption_portrait.setOnClickListener(this);
        iv_caption_landscape.setOnClickListener(this);

        iv_play_pause_landscape.setOnClickListener(this);

        iv_full_screen_toggle.setOnClickListener(this);
        iv_full_screen_toggle_landscape.setOnClickListener(this);

        fl_controller_portrait.setOnClickListener(this);
        fl_controller_landscape.setOnClickListener(this);


        exoplayerView_portrait.setOnTouchListener(new View.OnTouchListener() {
            @Override
            public boolean onTouch(View v, MotionEvent event) {
                if (checkLandscapeOrientation()) {
                    fl_controller_landscape.setVisibility(View.VISIBLE);
                } else {
                    fl_controller_portrait.setVisibility(View.VISIBLE);
                }
                startHandler();

                if (mPlaybackState == PlaybackState.PLAYING) {
                    if (checkLandscapeOrientation()) {
                        hideSystemUi();
                    }
                }

                return false;
            }
        });

        exoplayerView_landscape.setOnTouchListener(new View.OnTouchListener() {
            @Override
            public boolean onTouch(View v, MotionEvent event) {
                if (checkLandscapeOrientation()) {
                    fl_controller_landscape.setVisibility(View.VISIBLE);
                } else {
                    fl_controller_portrait.setVisibility(View.VISIBLE);
                }
                startHandler();

                if (mPlaybackState == PlaybackState.PLAYING) {
                    if (checkLandscapeOrientation()) {
                        hideSystemUi();
                    }
                }

                return false;
            }
        });


        // hide control after 2 second
        userInteractionHandler = new Handler();
        userInteractionRunnable = new Runnable() {
            @Override
            public void run() {
                if (mPlaybackState == PlaybackState.PLAYING && isStartVideo) {
                    if (checkLandscapeOrientation()) {
                        fl_controller_landscape.setVisibility(View.INVISIBLE);
                    } else {
                        fl_controller_portrait.setVisibility(View.INVISIBLE);
                    }
                } else {
                    startHandler();
                }

                if (checkLandscapeOrientation()) {
                    // getWindow().getDecorView().setSystemUiVisibility(View.SYSTEM_UI_FLAG_HIDE_NAVIGATION);
                    hideSystemUi();

                }

            }
        };

        // banner change every 2 second
        bannerRunnable = new Runnable() {
            @Override
            public void run() {
                if (promotionBanners != null && promotionBanners.size() > 1) {
                  /*  bannerPosition = viewPagerBanner.getCurrentItem()==promotionBanners.size()-1?--bannerPosition:++bannerPosition;
                    viewPagerBanner.setCurrentItem(bannerPosition);
                    startBannerHandler();*/
                    bannerPosition = viewPagerBanner.getCurrentItem();
                    viewPagerBanner.setCurrentItem(++bannerPosition, true);
                    startBannerHandler();
                }
            }
        };

        rv_video_category = (RecyclerView) findViewById(R.id.rv_video_category);
        LinearLayoutManager categoryLayoutManager = new LinearLayoutManager(HomeDraweerStreamingActivity.this);
        rv_video_category.setLayoutManager(categoryLayoutManager);
        adapterVideoCategory = new AdapterHomeVideoCategory(HomeDraweerStreamingActivity.this,
                new ArrayList<CategoryModel>(), HomeDraweerStreamingActivity.this);
        rv_video_category.setAdapter(adapterVideoCategory);

        shimmer_view_container.startShimmer();

        if (checkConnection()) {
            secondTime = false;
            fetchVideoList();
        } else {
            secondTime = true;//showSnack(checkConnection());
        }

        /**
         * User subscription check logic
         * since in this build there is no payment action so we will hide this logic
         */
       /* mySubscription = sharedPreferenceHandler.getMySubscriptionDetail();
        if (mySubscription == null || mySubscription.getRemainingDays() == null ||
                mySubscription.getRemainingDays().equalsIgnoreCase("0") ||
                !mySubscription.getLastRefreshDate().
                        equalsIgnoreCase(DateCalculation.getCurrentDateInDDMMYY())) {
            if (checkConnection()) {
                getMySubscriptionDetail(null);

            }

        }*/

        // disable dragging of seekbar
        mSeekbar.setOnTouchListener(new View.OnTouchListener() {
            @Override
            public boolean onTouch(View view, MotionEvent motionEvent) {
                return true;
            }
        });

        mSeekbar_landscape.setOnTouchListener(new View.OnTouchListener() {
            @Override
            public boolean onTouch(View v, MotionEvent event) {
                return true;
            }
        });
    }
  /*  private void fetchVideoList() {

        new GetVideoCategoryList(this, new GetVideoCategoryList.onDemandCategoryListListener() {
            @Override
            public void onFetchedCategoryList(List<OnDemandCategory> categoryList) {

                try {

                    adapterVideoCategory = new AdapterHomeVideoCategory(HomeDraweerStreamingActivity.this,
                            categoryList, HomeDraweerStreamingActivity.this);
                    rv_video_category.setAdapter(adapterVideoCategory);
                    shimmer_view_container.hideShimmer();
                    shimmer_view_container.setVisibility(View.GONE);
                    rv_video_category.setVisibility(View.VISIBLE);


                    if (categoryList.size() == 0 && adapterVideoCategory != null && adapterVideoCategory.getItemCount() == 0 && isScreenVisible) {
                        iv_banner.setVisibility(View.GONE);
                        fetchVideoList();
                    } else {

                        startBannerHandler();
                    }
                } catch (Exception e) {

                }

            }

            @Override
            public void onFetchedMasterCategoryList(List<OnDemandMasterCategory> videoData) {

            }

            @Override
            public void onCategoryFetchingError(String error) {
                shimmer_view_container.hideShimmer();
                shimmer_view_container.setVisibility(View.GONE);
                rv_video_category.setVisibility(View.VISIBLE);
                displayAlert("Error", error);
            }
        }).GetCategoryList();
    }*/

    private void fetchVideoList() {

        new GenericResponseClass(HomeDraweerStreamingActivity.this, new GenericResponseClass.GenericApiListener() {
            @Override
            public void onSuccess(HomeScreenData homeScreenData) {

                shimmer_view_container.hideShimmer();
                shimmer_view_container.setVisibility(View.GONE);

                try {

                    if (homeScreenData != null) {

                        if (homeScreenData.getCategory_list() != null && homeScreenData.getCategory_list().size() > 0) {
                            adapterVideoCategory = new AdapterHomeVideoCategory(HomeDraweerStreamingActivity.this,
                                    homeScreenData.getCategory_list(), HomeDraweerStreamingActivity.this);
                            rv_video_category.setAdapter(adapterVideoCategory);
                            shimmer_view_container.hideShimmer();
                            shimmer_view_container.setVisibility(View.GONE);
                            rv_video_category.setVisibility(View.VISIBLE);


                        } else {
                            if (homeScreenData.getCategory_list() == null && isScreenVisible) {
                                iv_banner.setVisibility(View.GONE);
                                fetchVideoList();
                            }
                        }

                        if (homeScreenData.getPromotion_list() != null && homeScreenData.getPromotion_list().size() > 0) {

                            promotionBanners.clear();
                            promotionBanners = new ArrayList<>();
                            promotionBanners.addAll(homeScreenData.getPromotion_list());

                            if (promotionBanners.size() > 0) {
                                viewPagerBanner.setVisibility(View.VISIBLE);
                                mViewPagerAdapter.addBannerList(homeScreenData.getPromotion_list());
                                mViewPagerAdapter.notifyDataSetChanged();
                                endlessPagerAdapter = new EndlessPagerAdapter(mViewPagerAdapter, viewPagerBanner);
                                viewPagerBanner.setAdapter(endlessPagerAdapter);
                                startBannerHandler();
                            } else {
                                viewPagerBanner.setVisibility(View.GONE);
                            }


                        }

                    }

                } catch (Exception e) {
                    if (adapterVideoCategory != null && adapterVideoCategory.getItemCount() == 0) {
                        fetchVideoList();
                    }

                }
            }

            @Override
            public void onError(String error) {
                if (adapterVideoCategory != null && adapterVideoCategory.getItemCount() == 0 && isScreenVisible) {
                    fetchVideoList();
                }

            }
        }).FetchAllVideoData();
    }

    private void startBannerHandler() {
        stopBannerHandler();
        try {
            if (bannerHandler != null) {
                bannerHandler.postDelayed(bannerRunnable, 5000);
            }
        } catch (Exception e) {

        }
    }

    private void stopBannerHandler() {
        try {
            bannerHandler.removeCallbacks(bannerRunnable);
        } catch (Exception e) {

        }
    }

    private void displayAlert(@NonNull String title,
                              @Nullable String message) {
        AlertDialog.Builder builder = new AlertDialog.Builder(this, R.style.AlertDialogStyle)
                .setTitle(title)
                .setMessage(message);
        builder.setPositiveButton("Ok", null);
        builder.create().show();
    }

    @Override
    public void onVideoSelected(int position, OnDemandVideo onDemandVideo) {
        if (position == -1) {
            startActivity(new Intent(this,
                    VideoListActivity.class).putExtra(Constants.CATEGORY_TYPE, onDemandVideo.getCategory()));
        } else {

            /**
             * UnComment this logic when user subsction is required to play video
             */
          /*  if (mySubscription != null && Integer.parseInt(mySubscription.getRemainingDays()) != 0) {
                if (!mySubscription.getLastRefreshDate().equalsIgnoreCase(DateCalculation.getCurrentDateInDDMMYY())) {
                    if (sharedPreferenceHandler.getPrefernceBoolean(SharedPreferenceHandler.SUBSCRIBED)) {
                        if (checkConnection()) {
                            getMySubscriptionDetail(null);

                        }
                    }
                } else {
                    startActivity(new Intent(HomeDraweerStreamingActivity.this,
                            VideoDetailActivity.class).
                            putExtra(Constants.SELECTED_ON_DEMAND_VIDEO, video));
                }

            } else {
                showSubscriptionAlert();
            }*/

            /**
             * Comment this one when subsction required to play video
             */
            if (onDemandVideo.getCategory().equalsIgnoreCase("Web Series")) {

                com.example.livestreamingapp.model.webseries.ContentInfo contentInfo = new ContentInfo();
                contentInfo.setSubtitle(onDemandVideo.getSubtitle());
                contentInfo.setTitle(String.valueOf(onDemandVideo.getTitle()));
                contentInfo.setBannerImage(onDemandVideo.getThumb());
                contentInfo.setThumb(onDemandVideo.getThumb());
                contentInfo.setAvgRate(onDemandVideo.getAvg_rate());
                contentInfo.setIsWatchLater(onDemandVideo.isWatchList());
                contentInfo.setIsRated(onDemandVideo.isRated());
                contentInfo.setTotalUserRated(Integer.parseInt(onDemandVideo.getTotal_user_rated()));
                contentInfo.setCategory(onDemandVideo.getCategory());
                contentInfo.setId(onDemandVideo.getId());

                ContentDetail content_detail = new ContentDetail();
                content_detail.setContentInfo(contentInfo);

                startActivity(new Intent(HomeDraweerStreamingActivity.this,
                        WebSeriesDetailActivity.class).
                        putExtra(Constants.SELECTED_ON_DEMAND_VIDEO, content_detail));

            } else {
                startActivity(new Intent(HomeDraweerStreamingActivity.this,
                        VideoDetailActivity.class).
                        putExtra(Constants.SELECTED_ON_DEMAND_VIDEO, onDemandVideo));

            }

        }
    }

    @Override
    public void onCategorySelected(int position, OnDemandCategory onDemandCategory) {

    }

    @Override
    public void onWatchLater(int i, OnDemandVideo onDemandVideo) {

    }

    /**
     * Update Subscription detail
     *
     * @param videoToOpen
     */
    private void getMySubscriptionDetail(final OnDemandVideo videoToOpen) {

        new SubscriptionService(HomeDraweerStreamingActivity.this).
                getMySubscriptionInfo(new SubscriptionService.onSubscriptionInfoListener() {
                    @Override
                    public void onSubscriptionInfo(MySubscription subscriptionInfo) {
                        if (subscriptionInfo != null) {
                            new SharedPreferenceHandler(HomeDraweerStreamingActivity.this).saveSubscription(subscriptionInfo);
                            mySubscription = sharedPreferenceHandler.getMySubscriptionDetail();

                            if (videoToOpen != null && !subscriptionInfo.getRemainingDays().equalsIgnoreCase("0")) {
                                startActivity(new Intent(HomeDraweerStreamingActivity.this,
                                        VideoDetailActivity.class).
                                        putExtra(Constants.SELECTED_ON_DEMAND_VIDEO, videoToOpen));
                            }
                        } else {
                            new SharedPreferenceHandler(HomeDraweerStreamingActivity.this).saveSubscription(null);
                            mySubscription = sharedPreferenceHandler.getMySubscriptionDetail();

                        }
                    }

                    @Override
                    public void onFailure(String error) {

                       /* new SharedPreferenceHandler(HomeDraweerStreamingActivity.this).saveSubscription(null);
                        mySubscription = sharedPreferenceHandler.getMySubscriptionDetail();*/


                    }
                });
    }

    public void showSubscriptionAlert() {
        // Create an alert builder
        AlertDialog.Builder builder
                = new AlertDialog.Builder(this);
        // set the custom layout
        final View customLayout
                = getLayoutInflater()
                .inflate(
                        R.layout.layout_alert_subscribe,
                        null);
        builder.setView(customLayout);

        Button btn_subscribe = customLayout.findViewById(R.id.btn_subscribe);

        final AlertDialog dialog
                = builder.create();
        dialog.getWindow().setBackgroundDrawableResource(android.R.color.transparent);
        ColorDrawable back = new ColorDrawable(Color.TRANSPARENT);
        InsetDrawable inset = new InsetDrawable(back, 40, 0, 40, 0);
        if (dialog.getWindow() != null) {
            dialog.getWindow().setBackgroundDrawable(inset);

        }
        btn_subscribe.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                dialog.dismiss();
                startActivity(new Intent(HomeDraweerStreamingActivity.this, SubscriptionSelectorActivity.class));
            }
        });

        // create and show
        // the alert dialog

        dialog.show();
    }

    @Override
    public boolean onNavigationItemSelected(@NonNull MenuItem item) {
        switch (item.getItemId()) {
            case R.id.nav_home:
                //    startActivity(new Intent(HomeDraweerStreamingActivity.this, VideoDetailActivity.class));
                //  item.setChecked(true);
                drawer.close();
                break;

            case R.id.nav_live:
                startActivity(new Intent(HomeDraweerStreamingActivity.this, VideoDetailActivity.class));
                //   item.setChecked(true);
                drawer.close();
                break;

            case R.id.nav_faq:
                startActivity(new Intent(HomeDraweerStreamingActivity.this, ActivityTermCondition.class)
                        .putExtra("title", "FAQ LIST"));
                //   item.setChecked(true);"
                drawer.close();
                break;

            case R.id.nav_on_demand:

                startActivity(new Intent(HomeDraweerStreamingActivity.this, OnDemandCategoryActivity.class));

                break;


            case R.id.nav_subscription:
                startActivity(new Intent(HomeDraweerStreamingActivity.this, SubscriptionSelectorActivity.class));

                break;


            case R.id.nav_upload_content:
               /* if (profileInfo.getPhoneNumber() == null) {
                   startActivity(new Intent(HomeDraweerStreamingActivity.this, AddPhoneNumberActivity.class));
                    //startActivity(new Intent(HomeDraweerStreamingActivity.this, UploadContentActivity.class));

                } else {
                    startActivity(new Intent(HomeDraweerStreamingActivity.this, UploadContentActivity.class));

                }*/
                startActivity(new Intent(HomeDraweerStreamingActivity.this, UploadContentActivity.class));


                break;

            case R.id.nav_watch_later:
                startActivity(new Intent(HomeDraweerStreamingActivity.this, VideoListActivity.class).
                        putExtra("WatchLater", true).putExtra(CATEGORY_TYPE, "Watch Later"));
                //   item.setChecked(true);
                drawer.close();
                break;


            case R.id.nav_profile:
                startActivity(new Intent(HomeDraweerStreamingActivity.this, ProfileActivity.class));

                break;

            case R.id.nav_contact:
                startActivity(new Intent(HomeDraweerStreamingActivity.this, QueryPagerActivity.class));
                break;

            case R.id.nav_notification:
                startActivity(new Intent(HomeDraweerStreamingActivity.this, NotificationActivity.class));
                break;


            case R.id.nav_logout:
             /*   sharedPreferenceHandler.saveFcmToken(null);
                sharedPreferenceHandler.setProfileData(null);
                sharedPreferenceHandler.setUnReadCount(0);
                sharedPreferenceHandler.saveSubscription(null);
                sharedPreferenceHandler.saveAllNotification(null);
                startActivity(new Intent(HomeDraweerStreamingActivity.this, MultipleLoginSelectionActivity.class));*/
                finish();
                try {
                    finish();
                } catch (Exception e) {

                }
                break;
        }
        return false;
    }

    /**
     * This will give the on demand video list.
     *
     * @param videoData
     */
    @Override
    public void onDemandFetchedVideoList(List<OnDemandVideo> videoData) {

    }

    /**
     * this will show the error while fetching on demand video list
     * here we can do some action about error
     *
     * @param error
     */
    @Override
    public void onDemandFetchingError(String error) {

    }

    @Override
    public void onNetworkConnectionChanged(boolean isConnected) {
        if (liveShow == null && isConnected && isScreenVisible) {
            showLiveShow();
        } else if (liveShow != null && isConnected) {
            if (isScreenVisible) {
                resumeMediaPlayer();
            }

        }
        if (isConnected && adapterVideoCategory != null && adapterVideoCategory.getItemCount() == 0 && secondTime) {
            fetchVideoList();
        }
        if (!isConnected) {
            secondTime = true;
            showSnack(isConnected);
        }

    }

    @Override
    public void onGetBroadCast(Intent intent) {
        runOnUiThread(new Runnable() {
            @Override
            public void run() {
                updateNotificationCount();
                //  getMySubscriptionDetail(null);
                //  new SnackBar(HomeDraweerStreamingActivity.this).showPositiveSnack("Notification Received!");
            }
        });

    }

    @Override
    protected void onStop() {
        super.onStop();

        if (analyticsVideo != null && startTime != 0) {
            sendAnaLyticsReport(getOnDemandVideoDetail(analyticsVideo));
            startTime = 0;
        }


        saveCurrentAdVideo();


       /* if(player!=null && liveShow!=null && liveShow.getVideo_type()!=null
                && liveShow.getVideo_type().equalsIgnoreCase("ad_video")){
            lastDuration = player.getCurrentPosition();
            lastVideo = liveShow.getSource();
        }*/

        try {
            stopLiveVideoHandler();
        } catch (Exception e) {

        }

      /*  if(player!=null){
            try {
                player.setPlayWhenReady(false);
                relasePlayer();
            }catch (Exception e){

            }
F
        }

        try {
            player.setPlayWhenReady(false);
            player.stop();

        }catch (Exception e){

        }*/

        isScreenVisible = false;
        //isAliveActivity = false;


    }

    private void saveCurrentAdVideo() {
        if( liveShow!=null && liveShow.getVideo_type()!=null
                && liveShow.getVideo_type().equalsIgnoreCase("ad_video") && analyticsVideo!=null && player!=null && analyticsVideo.getSource()!=null){
            long time = player.getCurrentPosition();
            Log.e("check","duration "+time);
            if(time>0){
                new SharedPreferenceHandler(HomeDraweerStreamingActivity.this).saveCurrentAdVideoPosition(videoPosition);
                analyticsVideo.setPlayBackTime(time);
                sharedPreferenceHandler.saveCurrentAdVideo(analyticsVideo);
            }
        }
    }


    private void updateNotificationCount() {
        runOnUiThread(new Runnable() {
            @Override
            public void run() {
                int count = sharedPreferenceHandler.getUnReadCount();
                bottomNavigationHelper.setNotification(count);
               /* if (count > 0) {
                    notification_count.setText("" + count);
                    notification_count.setVisibility(View.VISIBLE);
                } else {
                    notification_count.setVisibility(View.GONE);
                }*/
            }
        });
    }

    @Override
    public void isAlertShowing(boolean isShowing) {


        if (liveShow != null) {
            if (isShowing) {
                isCompleteProfileOpen = true;
                //play(mSeekbar.getProgress() * 1000);
                mPlaybackState = PlaybackState.PAUSED;
                pauseMediaPlayer();
                mPlaybackState = PlaybackState.PAUSED;
            } else {
                isCompleteProfileOpen = false;
                resumeMediaPlayer();
                mPlaybackState = PlaybackState.PLAYING;
            }
        } else {
            if (!isShowing) {
                isCompleteProfileOpen = false;
                startLiveVideoHandler();
            } else {
                isCompleteProfileOpen = true;
                stopLiveVideoHandler();
            }

        }
    }


    /**
     * indicates whether we are doing a local or a remote playback
     */
    public enum PlaybackLocation {
        LOCAL,
        REMOTE
    }

    /**
     * List of various states that we can be in
     */
    public enum PlaybackState {
        PLAYING, PAUSED, BUFFERING, IDLE, END
    }

    /**
     * the position where we want to play video
     * shouldRetry is there to check if i already tried to play media
     *
     * @param position
     */
    private void play(int position) {
        if ((player == null && !shouldRetry) || mPlaybackState == PlaybackState.END) {
            showLiveShow();
        } else {
            mPlaybackState = PlaybackState.PLAYING;
            startControllersTimer();
            switch (mLocation) {
                case LOCAL:
                    player.seekTo(position);
                    playMedia();

                    break;
                case REMOTE:
                    mPlaybackState = PlaybackState.BUFFERING;
                    updatePlayButton(mPlaybackState);
                    mCastSession.getRemoteMediaClient().seek(position);
                    //  mSeekbar.setVisibility(View.GONE);
                    fl_controller_landscape.setVisibility(View.GONE);
                    fl_controller_portrait.setVisibility(View.GONE);
                    break;
                default:
                    break;
            }
            restartTrickplayTimer();
        }

    }

    private void updatePlayButton(PlaybackState state) {
        Log.d(TAG, "Controls: PlayBackState: " + state);
        boolean isConnected = (mCastSession != null)
                && (mCastSession.isConnected() || mCastSession.isConnecting());

        if (!isConnected) {
            switch (state) {
                case PLAYING:
                    //  hideBuffering();

                    startHandler();
                    iv_play_pause.setImageDrawable(getResources().getDrawable(R.drawable.ic_pause_video_24));
                    iv_play_pause_landscape.setImageDrawable(getResources().getDrawable(R.drawable.ic_pause_video_36));

                    // playMedia();
                    break;
                case IDLE:
                    // pauseMediaPlayer();
                    iv_play_pause.setImageDrawable(getResources().getDrawable(R.drawable.ic_play_video));
                    iv_play_pause_landscape.setImageDrawable(getResources().getDrawable(R.drawable.ic_play_video_36));

                    if (checkLandscapeOrientation()) {

                        fl_controller_landscape.setVisibility(View.VISIBLE);
                        fl_controller_portrait.setVisibility(View.GONE);
                    } else {
                        fl_controller_portrait.setVisibility(View.VISIBLE);
                        fl_controller_landscape.setVisibility(View.GONE);
                    }
                    break;
                case PAUSED:
                    if (checkLandscapeOrientation()) {
                        fl_controller_landscape.setVisibility(View.VISIBLE);
                        fl_controller_portrait.setVisibility(View.GONE);
                    } else {
                        fl_controller_portrait.setVisibility(View.VISIBLE);
                        fl_controller_landscape.setVisibility(View.GONE);
                    }
                    iv_play_pause.setImageDrawable(getResources().getDrawable(R.drawable.ic_play_video));
                    iv_play_pause_landscape.setImageDrawable(getResources().getDrawable(R.drawable.ic_play_video_36));
                    break;
                case BUFFERING:
                    showBuffering();
                    break;
                default:
                    break;
            }
        }

    }


    private void stopTrickplayTimer() {
        Log.d(TAG, "Stopped TrickPlay Timer");

        if (mSeekbarTimer != null) {
            mSeekbarTimer.cancel();
        }
    }

    private void restartTrickplayTimer() {
        stopTrickplayTimer();
        mSeekbarTimer = new Timer();
        mSeekbarTimer.scheduleAtFixedRate(new UpdateSeekbarTask(), 100, 1000);
        Log.d(TAG, "Restarted TrickPlay Timer");

    }

    private class UpdateSeekbarTask extends TimerTask {
        @Override
        public void run() {
            mHandler.post(new Runnable() {

                @Override
                public void run() {
                    if (mLocation == PlaybackLocation.LOCAL) {
                        //  int currentPos = (int) player.getCurrentPosition() / 1000;

                        if (player != null) {

                            try {
                                long position = player.getCurrentPosition();
                                int currentPos = (int) position / 1000;

                                if (currentPos > 1) {
                                    isStartVideo = true;
                                }

                                updateSeekbar(currentPos, (int) player.getDuration() / 1000);

                            } catch (Exception e) {

                            }

                        }

                    }
                }
            });
        }
    }

    private void checkAdEnd() {
        if (liveShow != null &&
                liveShow.getVideo_type().equalsIgnoreCase("ad_video")) {


            checkLiveVideo();
        } else if (liveShow != null && liveShow.getSchedule_date() != null && liveShow.getSchedule_time() != null) {
            //todo
            boolean isAdTimerOver = DateCalculation.isLiveTimeOver(liveShow.getSchedule_date(),
                    liveShow.getDuration());
            if (isAdTimerOver) {
                showLiveShow();
            }
        }
    }

    private void checkLiveVideo() {
        final Call<LiveShow> liveShowCall = apiInterface.checkLiveVideo();
        liveShowCall.enqueue(new Callback<LiveShow>() {
            @Override
            public void onResponse(Call<LiveShow> call, Response<LiveShow> response) {
                if (response != null && response.code() == 200 && response.body() != null &&
                        response.body().getData() != null && response.body().getData().getSchedule_time() != null) {
                    saveCurrentAdVideo();
                    lastDuration = 0;
                    mSeekbar.setProgress(0);
                    progress_landscape_live.setProgress(0);
                    progress_portrait_live.setProgress(0);
                    try {
                        pauseMediaPlayer();
                        relasePlayer();
                    } catch (Exception e) {

                    }
                    liveShow = null;
                    //showBuffering();
                    showLiveShow();
                }
            }

            @Override
            public void onFailure(Call<LiveShow> call, Throwable t) {

            }
        });

    }


    private void updateSeekbar(int position, int duration) {

        mSeekbar.setProgress(position);
        mSeekbar.setMax(duration);


        mSeekbar_landscape.setProgress(position);
        mSeekbar_landscape.setMax(duration);
        if (totalVideoTime == null) {
            totalVideoTime = "00:00";
        }

        String time = DateCalculation.ConvertMillisToHHMMSS(player.getCurrentPosition()) + " / " + totalVideoTime;
        tv_time_lanscape.setText("" + time);
        tv_time_portrait.setText("" + time);

      /*  if (mPlaybackState == PlaybackState.PLAYING) {
            hideBuffering();
        }*/

        last10Second = last10Second + 1;
        if (last10Second > 3) {
            last10Second = 0;
            checkAdEnd();
        }

    }

    private void stopControllersTimer() {
        if (mControllersTimer != null) {
            mControllersTimer.cancel();
        }
    }

    private void startControllersTimer() {
        if (mControllersTimer != null) {
            mControllersTimer.cancel();
        }
        if (mLocation == PlaybackLocation.REMOTE) {
            return;
        }
        mControllersTimer = new Timer();
        mControllersTimer.schedule(new HideControllersTask(), 5000);
    }

    private class HideControllersTask extends TimerTask {

        @Override
        public void run() {
            mHandler.post(new Runnable() {
                @Override
                public void run() {
              /*      updateControllersVisibility(false);
                    mControllersVisible = false;*/
                }
            });

        }
    }

    /**
     * @Rehan Stop Live Video Handler too
     * if we are playing locally, we needp to stop the playback of
     * video (if user is not watching, pause it!)
     */
    @Override
    protected void onPause() {
        super.onPause();
        isScreenVisible = false;
        isStartVideo = false;

        Log.e("check","on Pause Called");




        try {
            pauseMediaPlayer();
            stopLiveVideoHandler();
        } catch (Exception e) {

        }

        try {
            MassTvApplication.getInstance().setUnRegister();
            MassTvApplication.getInstance().unRegisteredLocalReceiver();

        } catch (Exception e) {

        }

        Log.d(TAG, "onPause() was called");
        if (mLocation == PlaybackLocation.LOCAL) {
            stopLiveVideoHandler();
            try {
                if (mSeekbarTimer != null) {
                    mSeekbarTimer.cancel();
                    mSeekbarTimer = null;
                }
                if (mControllersTimer != null) {
                    mControllersTimer.cancel();
                }
            } catch (Exception e) {

            }

            // since we are playing locally, we need to stop the playback of
            // video (if user is not watching, pause it!)
            pauseMediaPlayer();
            mPlaybackState = PlaybackState.PAUSED;
            updatePlayButton(PlaybackState.PAUSED);
        }

        if (drawer != null) {
            drawer.close();
        }

        try {
            stopHandler();
        } catch (Exception e) {

        }
        mCastContext.getSessionManager().removeSessionManagerListener(
                mSessionManagerListener, CastSession.class);
    }


    /**
     * handle playback according to type
     * REMOTE means user is casting video
     * LOCAL means video is playing on the current device
     */

    @Override
    protected void onResume() {
        isScreenVisible = true;
        new CompleteProfile(HomeDraweerStreamingActivity.this, this);

        if (isResume) {
            isResume = false;
            fetchVideoList();
        } else {
            //  showSnack(false);
        }

        updateNotificationCount();
        MassTvApplication.getInstance().setConnectivityListener(this);
        try {
            MassTvApplication.getInstance().registerLocalReceiver(this);
        } catch (Exception e) {

        }

        //   mySubscription = sharedPreferenceHandler.getMySubscriptionDetail();
        // updateNavigationHeader();
        try {
            mCastContext.addCastStateListener(mCastStateListener);
            mCastContext.getSessionManager().addSessionManagerListener(
                    mSessionManagerListener, CastSession.class);
        } catch (Exception e) {


        }

        if (liveShow == null) {
            startLiveVideoHandler();
        } else if (!isEmptySlot()
                && getVideoPostionToCurrentTime() == -1) {
            liveShow = null;
            startLiveVideoHandler();

        } else {

            if (mCastSession != null && mCastSession.isConnected()) {
                updatePlaybackLocation(PlaybackLocation.REMOTE);
            } else {
                updatePlaybackLocation(PlaybackLocation.LOCAL);
                mPlaybackState = PlaybackState.PLAYING;
                updatePlayButton(mPlaybackState);
                resumeMediaPlayer();
                //  player.setPlayWhenReady(true);
               /* if(player!=null && lastVideo!=null && liveShow!=null && liveShow.getVideo_type()!=null && liveShow.getVideo_type().equalsIgnoreCase("ad_video")){
                    if(liveShow.getSource()!=null && liveShow.getSource().toString().equalsIgnoreCase(lastVideo) && lastDuration>0){
                        player.seekTo(lastDuration);
                    }
                }*/
            }
        }

        startBannerHandler();
        super.onResume();
        uploadFcmToken();
        if (player != null) {
            player.setPlayWhenReady(true);
        }


    }

    /**
     * Pause media player and change icon to indicate it is pause
     * Handle both landscape and portrait mode
     * mControllers handle seek bar. in this  case we stop seek bar
     */
    private void pauseMediaPlayer() {
        try {
            if (player != null) {
                if (mControllersTimer != null) {
                    mControllersTimer.cancel();
                }
                if (checkLandscapeOrientation()) {

                    fl_controller_landscape.setVisibility(View.VISIBLE);
                    fl_controller_portrait.setVisibility(View.GONE);
                } else {
                    fl_controller_portrait.setVisibility(View.VISIBLE);
                    fl_controller_landscape.setVisibility(View.GONE);

                }
                iv_play_pause.setImageDrawable(getResources().getDrawable(R.drawable.ic_play_video));
                iv_play_pause_landscape.setImageDrawable(getResources().getDrawable(R.drawable.ic_play_video_36));
                mPlaybackState = PlaybackState.PAUSED;
                player.setPlayWhenReady(false);
            }
        } catch (Exception e) {
            e.printStackTrace();
        }

    }

    /**
     * Initialize live video player
     *
     * @param liveShow
     */
    private void initializePlayer(LiveShowData liveShow) {

// Create a default TrackSelector
        BandwidthMeter bandwidthMeter = new DefaultBandwidthMeter();
  /*     TrackSelection videoTrackSelectionFactory =
                new AdaptiveTrackSelection.Factory(bandwidthMeter);
        TrackSelector trackSelector =
                new DefaultTrackSelector(videoTrackSelectionFactory);*/

        DefaultLoadControl loadControl = new DefaultLoadControl();

//Initialize the player
        player = ExoPlayerFactory.newSimpleInstance(this, new DefaultTrackSelector(), loadControl);

        if (!isFirstTimeVolume) {
            player.setVolume(0f);
        }


//Initialize simpleExoPlayerView
        if (checkLandscapeOrientation()) {
            exoplayerView_landscape.setPlayer(player);
        } else {
            exoplayerView_portrait.setPlayer(player);
        }

        exoplayerView_portrait.setUseController(false);
        exoplayerView_landscape.setUseController(false);

// Produces DataSource instances through which media data is loaded.
        DataSource.Factory dataSourceFactory =
                new DefaultDataSourceFactory(this, Util.getUserAgent(this, "Video Player"));

// Produces Extractor instances for parsing the media data.
        ExtractorsFactory extractorsFactory = new DefaultExtractorsFactory();

// This is the MediaSource representing the media to be played.

        MediaSource videoSource;
        if (isPlayingAd) {
            //   RawResourceDataSource rawDataSource = new RawResourceDataSource(this);
// open the /raw resource file
            videoSource = new ExtractorMediaSource(Uri.parse(liveShow.getSource_ad()), dataSourceFactory,
                    extractorsFactory, null, null);
            player.prepare(videoSource);

        } else {
            // Uri videoUri = Uri.parse(this.liveShow.getSource());
            /*if (liveShow.getVideo_type().equalsIgnoreCase("ad_video")) {
                videoUri = Uri.parse(liveShow.getSource());
            }*/
            Uri videoUri;
            MergingMediaSource mediaSource;
            if (liveShow.getSource().startsWith("http")|| liveShow.getSource().contains("https")) {

                videoUri = Uri.parse(Utils.fixedSpacesUrl(liveShow.getSource()));
             /*   videoSource = new ExtractorMediaSource(Uri.parse(liveShow.getSource()),
                        new CacheDataSourceFactory(this, 50 * 1024 * 1024, 50 * 1024 * 1024), extractorsFactory, null, null);
        */        videoSource = new ExtractorMediaSource(videoUri,
                        dataSourceFactory,
                        new DefaultExtractorsFactory(), null, null);


            } else {
                videoUri = Uri.parse(WebServiceConstant.VIDEO_URL+Utils.fixedSpacesUrl(liveShow.getSource()));
                videoSource = new ExtractorMediaSource(videoUri,
                        dataSourceFactory,
                        new DefaultExtractorsFactory(), null, null);

            }

            Log.e("check video uri"," "+videoUri.toString());
        /*    videoSource = new ExtractorMediaSource(videoUri,
                    dataSourceFactory, extractorsFactory, null, null);
*/

        /*    videoSource = new ExtractorMediaSource(Uri.parse(liveShow.getSource()),
                    new CacheDataSourceFactory(this, 50 * 1024 * 1024, 50 * 1024 * 1024), extractorsFactory, null, null);
*/


            if (liveShow.getSrt() != null && !liveShow.getSrt().isEmpty() && liveShow.getSrt().contains(".srt")
            ) {
                Uri srtUri = Uri.parse(liveShow.getSrt());

                if (liveShow.getSrt().startsWith("http")) {
                    srtUri = Uri.parse(liveShow.getSrt());

                } else {
                    srtUri = Uri.parse(WebServiceConstant.SUBTITLE_URL + liveShow.getSrt());

                }
/*                Format textFormat = Format.createTextSampleFormat(null, MimeTypes.APPLICATION_SUBRIP,
                        null, Format.NO_VALUE, Format.NO_VALUE, "en", null, Format.OFFSET_SAMPLE_RELATIVE);*/
                Format textFormat = Format.createTextSampleFormat(null,MimeTypes.APPLICATION_SUBRIP,Format.NO_VALUE,"en",Format.NO_VALUE,Format.OFFSET_SAMPLE_RELATIVE, null);
                MediaSource textMediaSource = new SingleSampleMediaSource.Factory(dataSourceFactory)
                        .createMediaSource(srtUri, textFormat, C.TIME_UNSET);
                mediaSource = new MergingMediaSource(videoSource, textMediaSource);
                player.prepare(mediaSource);
            } else {
                iv_caption_portrait.setVisibility(View.GONE);
                iv_caption_landscape.setVisibility(View.GONE);
                player.prepare(videoSource);
            }


        }
        player.addListener(this);

        // exoplayerView_portrait.setResizeMode(AspectRatioFrameLayout.RESIZE_MODE_FILL);
        exoplayerView_landscape.setResizeMode(AspectRatioFrameLayout.RESIZE_MODE_FILL);
        player.setVideoScalingMode(C.VIDEO_SCALING_MODE_SCALE_TO_FIT_WITH_CROPPING);

        startTime = System.currentTimeMillis();

        if(this.liveShow.getVideo_type().equalsIgnoreCase("ad_video") && player!=null){
            if(liveShow.getPlayBackTime()>0){
                player.seekTo(liveShow.getPlayBackTime());
            }
        }

    }


    @Override
    public void onTimelineChanged(Timeline timeline, Object manifest, int reason) {

    }

    @Override
    public void onTracksChanged(TrackGroupArray trackGroups, TrackSelectionArray trackSelections) {

    }

    @Override
    public void onLoadingChanged(boolean isLoading) {

        if (liveShow != null) {
            if (isLoading && liveShow != null) {
                showBuffering();
            } else {
                mPlaybackState = PlaybackState.PLAYING;
                hideBuffering();
            }
        }

    }

    @Override
    public void onPlayerStateChanged(boolean playWhenReady, int playbackState) {


        if (playbackState == android.media.session.PlaybackState.STATE_PLAYING) {
            hideBuffering();
            totalVideoTime = DateCalculation.ConvertMillisToHHMMSS(player.getDuration());

            if (startTime == 0) {
                startTime = System.currentTimeMillis();
            }

            if (!isCompleteProfileOpen) {
                mPlaybackState = PlaybackState.PLAYING;
                //   isStartVideo = true;
                // hideBuffering();
                totalVideoTime = DateCalculation.ConvertMillisToHHMMSS(player.getDuration());
                String time = "00:00 | " + totalVideoTime;
                tv_time_lanscape.setText("" + time);
                tv_time_portrait.setText("" + time);
            } else {
                mPlaybackState = PlaybackState.PAUSED;
                pauseMediaPlayer();
                mPlaybackState = PlaybackState.PAUSED;
                // hideBuffering();
            }

            //  play(mSeekbar.getProgress());
        } else if (playbackState == android.media.session.PlaybackState.STATE_BUFFERING) {
            //  mPlaybackState = PlaybackState.BUFFERING;
            showBuffering();
        } else if (playbackState == android.media.session.PlaybackState.STATE_CONNECTING) {
            //  mPlaybackState = PlaybackState.IDLE;
        } else if (playbackState == PlaybackStateCompat.STATE_PAUSED) {
            mPlaybackState = PlaybackState.PAUSED;
            hideBuffering();
        } else if (playbackState == 4) {
            lastDuration = 0;
          /*  progress_landscape_live.setProgress(0);
            progress_portrait_live.setProgress(0);
            mPlaybackState = PlaybackState.END;
            pauseMediaPlayer();
            isStartVideo = false;*/
            mSeekbar.setProgress(0);
            progress_landscape_live.setProgress(0);
            progress_portrait_live.setProgress(0);
            isPlayingAd = false;
            if (isPlayingAd) {
                showBuffering();
                pauseMediaPlayer();
                isPlayingAd = false;
                resumeLiveVideo();
            } else {
                if (liveShow != null && !liveShow.getVideo_type().equalsIgnoreCase("ad_video")) {
                    fetchVideoList();
                }
                //  showBuffering();
                try {
                    pauseMediaPlayer();
                    relasePlayer();
                } catch (Exception e) {

                }


                if (liveShow != null && liveShow.getVideo_type().equalsIgnoreCase("ad_video")) {
                    playNextVideo(liveShow.getNextVideoDetail());
                } else {
                    if (analyticsVideo != null && startTime != 0) {
                        sendAnaLyticsReport(getOnDemandVideoDetail(analyticsVideo));
                        startTime = 0;
                    }
                    liveShow = null;
                    showLiveShow();
                }


            }


        }
    }

    private OnDemandVideo getOnDemandVideoDetail(LiveShowData liveShow) {

        OnDemandVideo onDemandVideo = new OnDemandVideo();
        onDemandVideo.setSource(liveShow.getSource());
        onDemandVideo.setId(liveShow.getId());
        onDemandVideo.setCategory(liveShow.getCategory());
        return onDemandVideo;
    }

    private void resumeLiveVideo() {
        if (liveShow != null) {
            // showBuffering();
            stopLiveVideoHandler();
            if (isPlayingAd) {
                mSelectedMedia.setTitle("" + liveShow.getTitle());
                mSelectedMedia.setSubTitle("" + liveShow.getSubtitle());
                mSelectedMedia.setUrl(liveShow.getSource());
                if (liveShow.getThumb() != null) {
                    mSelectedMedia.addImage(liveShow.getThumb());
                }
                tv_live_subtitle.setText(liveShow.getTitle());
            } else {
                mSelectedMedia.setTitle("" + liveShow.getTitle());
                mSelectedMedia.setSubTitle("" + liveShow.getSubtitle());
                mSelectedMedia.setUrl(liveShow.getSource());
                if (liveShow.getThumb() != null) {
                    mSelectedMedia.addImage(liveShow.getThumb());
                }
                tv_live_subtitle.setText(liveShow.getTitle());
            }

            if (player != null) {

                try {
                    player.setPlayWhenReady(false);
                    player.stop();
                    player.release();
                } catch (Exception e) {

                }

            }

            initializePlayer(liveShow);

            if (mCastSession != null && mCastSession.isConnected()) {
                updatePlaybackLocation(PlaybackLocation.REMOTE);
            } else {
                updatePlaybackLocation(PlaybackLocation.LOCAL);
            }
            resumeMediaPlayer();
            mPlaybackState = PlaybackState.PLAYING;
            updatePlayButton(mPlaybackState);
        } else {
            startLiveVideoHandler();
        }
    }

    private void relasePlayer() {
        try {
            player.stop();
            player.release();
        } catch (Exception e) {

        }

        mSeekbar_landscape.setProgress(0);
        mSeekbar.setProgress(0);
        // liveShow = null;
        try {
            stopTrickplayTimer();
            stopControllersTimer();
            stopHandler();
        } catch (Exception e) {
            e.printStackTrace();
        }

    }

    @Override
    public void onRepeatModeChanged(int repeatMode) {

    }

    @Override
    public void onShuffleModeEnabledChanged(boolean shuffleModeEnabled) {

    }

    @Override
    public void onPlayerError(ExoPlaybackException error) {

        try {
            // Utils.showErrorDialog(this, "Please Check your internet connection!");
            mPlaybackState = PlaybackState.IDLE;
            if (liveShow != null && isEmptySlot() &&
                    liveShow.getNextVideoDetail() != null &&
                    liveShow.getNextVideoDetail().size() > 0) {
                playNextVideo(liveShow.getNextVideoDetail());
            } else {
                sharedPreferenceHandler.saveCurrentAdVideo(null);
                showLiveShow();
            }
            //  pauseMediaPlayer();
            // pauseMediaPlayer();
        } catch (Exception e) {

        }


    }

    @Override
    public void onPositionDiscontinuity(int reason) {

    }

    @Override
    public void onPlaybackParametersChanged(PlaybackParameters playbackParameters) {
        if (player != null) {


        }
        //    progressBar.setProgress((int)player.getCurrentPosition()/1000);

    }

    @Override
    public void onSeekProcessed() {

        //   progressBar.setProgress((int)player.getCurrentPosition()/1000);
    }


    /**
     * Play media player and change icon to indicate it is play
     * Handle both landscape and portrait mode
     */
    private void playMedia() {
        iv_play_pause.setImageDrawable(getResources().getDrawable(R.drawable.ic_pause_video_24));
        iv_play_pause_landscape.setImageDrawable(getResources().getDrawable(R.drawable.ic_pause_video_36));

        if (player != null) {
            getWindow().addFlags(WindowManager.LayoutParams.FLAG_KEEP_SCREEN_ON);
            mPlaybackState = HomeDraweerStreamingActivity.PlaybackState.PLAYING;
            player.setPlayWhenReady(true);
            startHandler();
        }

    }

    // listener for casting
    private void setupCastListener() {
        mSessionManagerListener = new SessionManagerListener<CastSession>() {

            @Override
            public void onSessionEnded(CastSession session, int error) {
                onApplicationDisconnected();
            }

            @Override
            public void onSessionResumed(CastSession session, boolean wasSuspended) {
                onApplicationConnected(session);
            }

            @Override
            public void onSessionResumeFailed(CastSession session, int error) {
                onApplicationDisconnected();
            }

            @Override
            public void onSessionStarted(CastSession session, String sessionId) {
                onApplicationConnected(session);
            }

            @Override
            public void onSessionStartFailed(CastSession session, int error) {
                onApplicationDisconnected();
            }

            @Override
            public void onSessionStarting(CastSession session) {
            }

            @Override
            public void onSessionEnding(CastSession session) {
            }

            @Override
            public void onSessionResuming(CastSession session, String sessionId) {
            }

            @Override
            public void onSessionSuspended(CastSession session, int reason) {
            }

            private void onApplicationConnected(CastSession castSession) {
                mCastSession = castSession;
                if (null != mSelectedMedia) {
                    mPlaybackState = PlaybackState.PLAYING;
                    mLocation = PlaybackLocation.REMOTE;
                    if (isPlayingAd && isEmptySlot()) {
                        playMedia();
                        if (mSeekbar != null) {
                            if (liveShow != null && liveShow.getSchedule_time() != null) {
                                loadRemoteMedia(mSeekbar.getProgress(), true);
                            }
                        } else {
                            if (liveShow != null && liveShow.getSchedule_time() != null) {
                                loadRemoteMedia(0, true);
                            }

                        }
                    } else {
                        playMedia();
                        if (liveShow != null && liveShow.getSchedule_time() != null) {
                            loadRemoteMedia((int) getVideoPostionToCurrentTime() / 1000, true);

                        }
                    }
                    return;
                }
                updatePlayButton(mPlaybackState);
                invalidateOptionsMenu();
            }

            private void onApplicationDisconnected() {
                updatePlaybackLocation(HomeDraweerStreamingActivity.PlaybackLocation.LOCAL);
                mPlaybackState = HomeDraweerStreamingActivity.PlaybackState.IDLE;
                mLocation = HomeDraweerStreamingActivity.PlaybackLocation.LOCAL;
                updatePlayButton(mPlaybackState);
                invalidateOptionsMenu();
            }
        };
    }

    private void updatePlaybackLocation(PlaybackLocation location) {
        mLocation = location;
        if (location == HomeDraweerStreamingActivity.PlaybackLocation.LOCAL) {
            if (mPlaybackState == HomeDraweerStreamingActivity.PlaybackState.PLAYING
                    || mPlaybackState == HomeDraweerStreamingActivity.PlaybackState.BUFFERING) {
                // setCoverArtStatus(null);
                startControllersTimer();
            } else {
                stopControllersTimer();
                //  setCoverArtStatus(mSelectedMedia.getImage(0));
            }
        } else {
            stopControllersTimer();
            // setCoverArtStatus(mSelectedMedia.getImage(0));
            fl_controller_landscape.setVisibility(View.GONE);
            fl_controller_portrait.setVisibility(View.GONE);

        }
    }

    private MediaInfo buildMediaInfo() {
        MediaMetadata movieMetadata = new MediaMetadata(MediaMetadata.MEDIA_TYPE_MOVIE);

        movieMetadata.putString(MediaMetadata.KEY_SUBTITLE, "Bunny");
        movieMetadata.putString(MediaMetadata.KEY_TITLE, mSelectedMedia.getTitle());
       /* movieMetadata.addImage(new WebImage(Uri.parse(mSelectedMedia.getImage(0))));
        movieMetadata.addImage(new WebImage(Uri.parse(mSelectedMedia.getImage(1))));
*/
        //  mSelectedMedia = new MediaItem();
      /*  if(liveShow!=null){
            mSelectedMedia.setTitle("" + liveShow.getTitle());
            mSelectedMedia.setSubTitle("" + liveShow.getSubtitle());
            mSelectedMedia.setUrl(liveShow.getSource());
            if (liveShow.getThumb() != null) {
                mSelectedMedia.addImage(liveShow.getThumb());
            }
            tv_live_subtitle.setText(liveShow.getTitle());
            return new MediaInfo.Builder(mSelectedMedia.getUrl())
                    .setStreamType(MediaInfo.STREAM_TYPE_BUFFERED)
                    .setContentType("videos/mp4")
                    .setMetadata(movieMetadata)
                    .setStreamDuration(mSelectedMedia.getDuration() * 1000)
                    .build();
        }*/

        if (liveShow != null && liveShow.getSchedule_time() != null) {
            mSelectedMedia.setSubTitle("");
            mSelectedMedia.setSubTitle("");
            mSelectedMedia.setDuration(0);
            mSelectedMedia.setUrl("");
        }


        return new MediaInfo.Builder(mSelectedMedia.getUrl())
                .setStreamType(MediaInfo.STREAM_TYPE_BUFFERED)
                .setContentType("videos/mp4")
                .setMetadata(movieMetadata)
                .setStreamDuration(mSelectedMedia.getDuration() * 1000)
                .build();


    }

    private void loadRemoteMedia(int position, boolean autoPlay) {
        if (mCastSession == null) {
            return;
        }
        final RemoteMediaClient remoteMediaClient = mCastSession.getRemoteMediaClient();
        if (remoteMediaClient == null) {
            return;
        }
        remoteMediaClient.registerCallback(new RemoteMediaClient.Callback() {
            @Override
            public void onStatusUpdated() {
                Intent intent = new Intent(HomeDraweerStreamingActivity.this, ExpandedControlsActivity.class);
                startActivity(intent);
                remoteMediaClient.unregisterCallback(this);
            }
        });
        remoteMediaClient.load(new MediaLoadRequestData.Builder()
                .setMediaInfo(buildMediaInfo())
                .setAutoplay(autoPlay)
                .setCurrentTime(position).build());
    }

    /**
     * Hide / show caption
     */
    private void setCaptionVisibility() {
        if (HomeDraweerStreamingActivity.isCaptionOff) {
            exoplayerView_portrait.getSubtitleView().setVisibility(View.GONE);
            exoplayerView_landscape.getSubtitleView().setVisibility(View.GONE);
            iv_caption_landscape.setImageResource(R.drawable.ic_caption_off);
            iv_caption_portrait.setImageResource(R.drawable.ic_caption_off);
        } else {
            exoplayerView_portrait.getSubtitleView().setVisibility(View.VISIBLE);
            exoplayerView_landscape.getSubtitleView().setVisibility(View.VISIBLE);
            iv_caption_landscape.setImageResource(R.drawable.ic_caption_on);
            iv_caption_portrait.setImageResource(R.drawable.ic_caption_on);
        }
    }

    @Override
    public void onClick(View v) {
        switch (v.getId()) {
            case R.id.iv_back:
                finish();
                break;

            case R.id.iv_caption_landscape:
                //todo load caption
                startHandler();
                HomeDraweerStreamingActivity.isCaptionOff = !HomeDraweerStreamingActivity.isCaptionOff;
                setCaptionVisibility();
                break;

            case R.id.iv_caption_portrait:
                //todo load caption
                startHandler();
                HomeDraweerStreamingActivity.isCaptionOff = !HomeDraweerStreamingActivity.isCaptionOff;
                setCaptionVisibility();
                /*simpleExoPlayerView_portrait.getSubtitleView().setStyle(new CaptionStyleCompat(Color.TRANSPARENT,
                        Color.TRANSPARENT, Color.TRANSPARENT, CaptionStyleCompat.EDGE_TYPE_NONE, Color.TRANSPARENT,
                        Typeface.DEFAULT_BOLD));*/

                break;

            case R.id.iv_play_pause:

                if (liveShow != null) {
                    if (mPlaybackState == PlaybackState.PLAYING) {
                        //play(mSeekbar.getProgress() * 1000);
                        mPlaybackState = PlaybackState.PAUSED;
                        pauseMediaPlayer();
                        mPlaybackState = PlaybackState.PAUSED;

                    } else {
                        try {
                            resumeMediaPlayer();
                            mPlaybackState = PlaybackState.PLAYING;

                        } catch (Exception e) {

                        }

                    }
                } else {
                    startLiveVideoHandler();
                }

                break;

            case R.id.iv_play_pause_landscape:
                if (liveShow != null) {
                    if (mPlaybackState == PlaybackState.PLAYING) {
                        mPlaybackState = PlaybackState.PAUSED;
                        pauseMediaPlayer();
                    } else {
                        resumeMediaPlayer();
                    }
                } else {
                    startLiveVideoHandler();
                }

                break;

            case R.id.fl_controller_portrait:

                if (mPlaybackState == PlaybackState.PLAYING) {
                    fl_controller_portrait.setVisibility(View.INVISIBLE);

                   /* if(checkLandscapeOrientation()){
                        // getWindow().getDecorView().setSystemUiVisibility(View.SYSTEM_UI_FLAG_HIDE_NAVIGATION);
                        hideSystemUi();

                    }
*/
                } else {
                    fl_controller_portrait.setVisibility(View.VISIBLE);
                }
                break;

            case R.id.controllers2:

                if (mPlaybackState == PlaybackState.PLAYING) {
                    fl_controller_landscape.setVisibility(View.INVISIBLE);

                  /*  if(checkLandscapeOrientation()){
                        // getWindow().getDecorView().setSystemUiVisibility(View.SYSTEM_UI_FLAG_HIDE_NAVIGATION);
                        hideSystemUi();

                    }*/

                } else {
                    fl_controller_landscape.setVisibility(View.VISIBLE);
                }
                break;

            case R.id.iv_full_screen_toggle:
                if (checkLandscapeOrientation()) {
                    changeOrientationToLandscape(false);

                } else {
                    changeOrientationToLandscape(true);

                }

            case R.id.iv_full_screen_toggle_landscape:
                if (checkLandscapeOrientation()) {
                    changeOrientationToLandscape(false);
                    //   changeOrientationToLandscape(false);
                } else {
                    changeOrientationToLandscape(true);

                }


        }
    }

    private void resumeMediaPlayer() {

        // showBuffering();
        if (liveShow != null) {
            mPlaybackState = PlaybackState.PLAYING;
            long milis = 0;
            if (!isEmptySlot()) {
                milis = getVideoPostionToCurrentTime();
                if (milis == -1) {
                    liveShow = null;
                    //  mPlaybackState = PlaybackState.BUFFERING;
                    showLiveShow();
                    return;
                }
            }
            startControllersTimer();
            switch (mLocation) {
                case LOCAL:

                    if (isPlayingAd || isEmptySlot()) {
                        // player.seekTo(mSeekbar.getProgress() * 1000);
                    } else {
                        player.seekTo(milis);
                    }
                    playMedia();

                    break;
                case REMOTE:
                    mPlaybackState = PlaybackState.BUFFERING;
                    updatePlayButton(mPlaybackState);
                    if (isPlayingAd || isEmptySlot()) {
                        mCastSession.getRemoteMediaClient().seek(mSeekbar.getProgress() * 1000);
                    } else {
                        mCastSession.getRemoteMediaClient().seek(milis);
                    }
                    //  mSeekbar.setVisibility(View.GONE);
                    fl_controller_landscape.setVisibility(View.GONE);
                    fl_controller_portrait.setVisibility(View.GONE);
                    break;
                default:
                    break;
            }
            restartTrickplayTimer();
        } else {
            showLiveShow();
        }


    }

    private boolean isEmptySlot() {
        return liveShow != null && liveShow.getVideo_type().equalsIgnoreCase("ad_video");
    }

    private long getVideoPostionToCurrentTime() {
        if (liveShow != null && liveShow.getSchedule_time() != null) {
            return DateCalculation.getTotalDurationInMillis(liveShow.getSchedule_time(), liveShow.getDuration(), liveShow.getSchedule_date());
        }
        return -1;
    }

    /**
     * Change device orientation manually
     *
     * @param shouldLandscape
     */
    public void changeOrientationToLandscape(Boolean shouldLandscape) {
        if (shouldLandscape) {
            setRequestedOrientation(ActivityInfo.SCREEN_ORIENTATION_USER_LANDSCAPE);
            //   setRequestedOrientation(ActivityInfo.SCREEN_ORIENTATION_FULL_SENSOR);
            setRequestedOrientation(ActivityInfo.SCREEN_ORIENTATION_SENSOR_LANDSCAPE);
        } else {
            setRequestedOrientation(ActivityInfo.SCREEN_ORIENTATION_USER_PORTRAIT);

        }

    }

    /**
     * Checks the Orientation
     * And returns true if Landscape else false
     */
    public boolean checkLandscapeOrientation() {
        int orientation = getResources().getConfiguration().orientation;
        return orientation == Configuration.ORIENTATION_LANDSCAPE;
    }

    @Override
    public void setRequestedOrientation(int requestedOrientation) {
        super.setRequestedOrientation(requestedOrientation);

    }

    /**
     * This method is called when orientation is changed
     *
     * @param newConfig
     */
    @Override
    public void onConfigurationChanged(Configuration newConfig) {
        super.onConfigurationChanged(newConfig);
        getSupportActionBar().show();

        if (newConfig.orientation == Configuration.ORIENTATION_LANDSCAPE) {

            isLandscape = true;
       /*     if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.R) {
                final WindowInsetsController insetsController = getWindow().getInsetsController();
                if (insetsController != null) {
                    insetsController.hide(WindowInsets.Type.statusBars());
                    insetsController.hide(WindowInsets.Type.navigationBars());
                    insetsController.hide(WindowInsets.Type.displayCutout());
                    insetsController.hide(WindowInsets.Type.tappableElement());
                }
            } else {
                getWindow().clearFlags(WindowManager.LayoutParams.FLAG_FORCE_NOT_FULLSCREEN);
                getWindow().setFlags(WindowManager.LayoutParams.FLAG_FULLSCREEN,
                        WindowManager.LayoutParams.FLAG_FULLSCREEN);
                getWindow().getDecorView().setSystemUiVisibility(View.SYSTEM_UI_FLAG_LOW_PROFILE);



            }*/
            hideSystemUi();
            progress_landscape_live.setVisibility(View.GONE);
            updateMetadata(false);


        } else {
            progress_landscape_live.setVisibility(View.GONE);
            getWindow().setFlags(WindowManager.LayoutParams.FLAG_FORCE_NOT_FULLSCREEN,
                    WindowManager.LayoutParams.FLAG_FORCE_NOT_FULLSCREEN);
            getWindow().clearFlags(
                    WindowManager.LayoutParams.FLAG_FULLSCREEN);
            getWindow().getDecorView().setSystemUiVisibility(View.SYSTEM_UI_FLAG_VISIBLE);

        /*
            getWindow().setFlags(WindowManager.LayoutParams.FLAG_FORCE_NOT_FULLSCREEN,
                    WindowManager.LayoutParams.FLAG_FORCE_NOT_FULLSCREEN);
            getWindow().clearFlags(
                    WindowManager.LayoutParams.FLAG_FULLSCREEN);
            getWindow().getDecorView().setSystemUiVisibility(View.SYSTEM_UI_FLAG_VISIBLE);
        */
            updateMetadata(true);
            //  mContainer.setBackgroundColor(getResources().getColor(R.color.white));
        }
    }

    /**
     * Handle all the views according to device orientation
     *
     * @param isLandscape if true then it is landscape then we are going to hide everything
     *                    except video player and controller
     *                    if false then show other views
     */
    private void updateMetadata(boolean isLandscape) {
        Point displaySize;
        if (!isLandscape) {
            progress_portrait_live.setVisibility(View.GONE);
            fl_controller_landscape.setVisibility(View.VISIBLE);
            fl_controller_portrait.setVisibility(View.GONE);
            appBarLayout.setVisibility(View.GONE);
            lin_content.setVisibility(View.GONE);
            hideBottomNavigation();
            switchPlayerView(player, exoplayerView_portrait, exoplayerView_landscape);
        } else {
            progress_landscape_live.setVisibility(View.GONE);
            fl_controller_landscape.setVisibility(View.GONE);
            fl_controller_portrait.setVisibility(View.VISIBLE);
            appBarLayout.setVisibility(View.VISIBLE);
            lin_content.setVisibility(View.VISIBLE);
            fl_controller_portrait.setVisibility(View.VISIBLE);
            showBottomNavigation();
            switchPlayerView(player, exoplayerView_landscape, exoplayerView_portrait);
        }
    }

    @Override
    public boolean onOptionsItemSelected(@NonNull MenuItem item) {
        if (item.getItemId() == R.id.action_search) {
            startActivity(new Intent(HomeDraweerStreamingActivity.this, SearchActivity.class));
            // Toast.makeText(this, "Comming Soon!", Toast.LENGTH_SHORT).show();
        }
        return super.onOptionsItemSelected(item);

    }

    @Override
    public boolean onCreateOptionsMenu(Menu menu) {
        // Inflate the menu; this adds items to the action bar if it is present.
        getMenuInflater().inflate(R.menu.home_drawer, menu);
        mediaRouteMenuItem = CastButtonFactory.setUpMediaRouteButton(getApplicationContext(), menu,
                R.id.media_route_menu_item);
        return true;
    }


    /**
     * Stop all the handler so that it will not cause any exception if user navigate to other activity
     */
    @Override
    protected void onDestroy() {
        Log.d(TAG, "onDestroy() is called");
        try {
            relasePlayer();
            stopBannerHandler();
           /* stopControllersTimer();
            stopTrickplayTimer();*/
            //   stopHandler();

        } catch (Exception e) {

        }
         isAliveActivity = false;
        super.onDestroy();
    }


    /**
     * If we are in Landscape mode and we Back pressed then it will open in portrait mode
     * if we are in portrait mode and Back pressed then activity will finish
     */
    @Override
    public void onBackPressed() {

        if (checkLandscapeOrientation()) {
            changeOrientationToLandscape(false);
        } else {
            super.onBackPressed();
        }
    }


    // hide play pause control of live  if no user interaction for some time
    @Override
    public void onUserInteraction() {
        // TODO Auto-generated method stub
        super.onUserInteraction();
        // stopHandler();
        //  startHandler();
    }

    /**
     * Remove the Controller hide handler if not necessary
     */
    public void stopHandler() {
        try {
            userInteractionHandler.removeCallbacks(userInteractionRunnable);
        } catch (Exception e) {

        }
    }

    /**
     * start the handler so that if we play any media and donot do any interaction
     * then i will hide after 3 second we can set any amount of time
     */
    public void startHandler() {
        try {
            userInteractionHandler.removeCallbacks(userInteractionRunnable);
        } catch (Exception e) {

        }
        if (userInteractionHandler != null) {
            userInteractionHandler.postDelayed(userInteractionRunnable, 6 * 1000);

        }

    }

    private void switchPlayerView(SimpleExoPlayer player, PlayerView oldPlayerView, PlayerView newPlayerView) {
        oldPlayerView.setVisibility(View.GONE);
        newPlayerView.setVisibility(View.VISIBLE);
        PlayerView.switchTargetView(player, oldPlayerView, newPlayerView);
        if (mPlaybackState == PlaybackState.PLAYING) {
            playMedia();
        } else {
            resumeMediaPlayer();
        }

        if (shouldRetry) {
            showBuffering();
        }
    }

    /**
     * Check internet connection Manually
     */
    private boolean checkConnection() {
        return ConnectivityReceiver.isConnected();
    }

    // Showing the status in Snackbar for intenet connectivity
    private void showSnack(boolean isConnected) {
        String message;
        int color;
        if (isConnected) {
            message = "Connection back Online!";
            color = Color.WHITE;
        } else {
            message = "Offline! Not connected to internet";
            color = Color.RED;


        }
        Snackbar snackbar = Snackbar
                .make(findViewById(R.id.parent), message, Snackbar.LENGTH_LONG);
        View sbView = snackbar.getView();
        TextView textView = (TextView) sbView.findViewById(R.id.snackbar_text);
        textView.setTextColor(color);
        if (!isConnected) {
            snackbar.show();
        }


    }

    public static void clearVideoCache(Context context) {
        try {
            File dir = new File(context.getCacheDir(), "media/");
            deleteDir(dir);
        } catch (Exception e) {
            e.printStackTrace();
        }
    }

    public static boolean deleteDir(File dir) {
        if (dir != null && dir.isDirectory()) {
            String[] children = dir.list();
            for (String child : children) {
                boolean success = deleteDir(new File(dir, child));
                if (!success) {
                    return false;
                }
            }
            return dir.delete();
        } else if (dir != null && dir.isFile()) {
            return dir.delete();
        } else {
            return false;
        }
    }


    public void uploadFcmToken() {
        profileInfo = sharedPreferenceHandler.getProfileInfo();
        String token = sharedPreferenceHandler.getFcmToken();
        if (token != null) {
            new FcmService(HomeDraweerStreamingActivity.this).uploadFcmToken("" + profileInfo.getId(), token);
        }
    }


    private void hideSystemUi() {


        getWindow().clearFlags(WindowManager.LayoutParams.FLAG_FORCE_NOT_FULLSCREEN);
        getWindow().setFlags(WindowManager.LayoutParams.FLAG_FULLSCREEN,
                WindowManager.LayoutParams.FLAG_FULLSCREEN);
        decorView.setSystemUiVisibility(
                View.SYSTEM_UI_FLAG_IMMERSIVE
                        // Set the content to appear under the system bars so that the
                        // content doesn't resize when the system bars hide and show.
                        | View.SYSTEM_UI_FLAG_LAYOUT_STABLE
                        | View.SYSTEM_UI_FLAG_LAYOUT_HIDE_NAVIGATION
                        | View.SYSTEM_UI_FLAG_LAYOUT_FULLSCREEN
                        // Hide the nav bar and status bar
                        | View.SYSTEM_UI_FLAG_HIDE_NAVIGATION
                        | View.STATUS_BAR_HIDDEN
                        | View.SYSTEM_UI_FLAG_FULLSCREEN);
    }

    /**
     * Hide bottom navigation
     */
    private void hideBottomNavigation() {


    }

    /**
     * SHow Bottom navigation
     */
    private void showBottomNavigation() {

    }

    @Override
    public void onVolumeButtonPress(int currentVolume, int maxVolume) {
        if (player != null && !isFirstTimeVolume) {
            isFirstTimeVolume = true;
            float volume = ((currentVolume * 100) / maxVolume);
            volume = (float) (volume * 0.01);
            player.setVolume(volume);
        }

    }

    public void updatePlayerWithNewVideo(LiveShowData liveShow) {
        isPlayingAd = false;
        analyticsVideo = liveShow;


        ln_desc_port.setVisibility(View.VISIBLE);

        if (liveShow != null && isScreenVisible) {
            // showBuffering();
            stopLiveVideoHandler();
            mSelectedMedia.setTitle("" + liveShow.getTitle());
            mSelectedMedia.setSubTitle("" + liveShow.getSubtitle());
            if (liveShow.getSource().contains("http")) {
                mSelectedMedia.setUrl(liveShow.getSource());
            } else {
                mSelectedMedia.setUrl(WebServiceConstant.VIDEO_URL + liveShow.getSource());

            }
            if (liveShow.getThumb() != null) {
                mSelectedMedia.addImage(liveShow.getThumb());
            }
            tv_live_subtitle.setText(liveShow.getTitle());
            tv_video_type_protrait.setVisibility(View.VISIBLE);
            tv_video_type.setVisibility(View.VISIBLE);
            tv_live_subtitle_portrait.setText(liveShow.getTitle());

            if (isEmptySlot()) {
                tv_video_type.setText("Ad");
                tv_video_type_protrait.setText("Ad");
                tv_video_type.setVisibility(View.GONE);
                analyticsVideo = liveShow;
                tv_video_type_protrait.setVisibility(View.GONE);
            } else {
                tv_video_type.setVisibility(View.VISIBLE);
                tv_video_type.setText("LIVE");
                analyticsVideo = liveShow;
                tv_video_type_protrait.setText("LIVE");
                tv_video_type_protrait.setVisibility(View.GONE);
            }

            if (player != null) {

                try {
                    player.setPlayWhenReady(false);
                    player.stop();
                    player.release();
                } catch (Exception e) {

                }

            }

            if (liveShow.getSrt() != null && liveShow.getSrt().contains(".srt")) {
                iv_caption_portrait.setVisibility(View.VISIBLE);
                iv_caption_landscape.setVisibility(View.VISIBLE);
            } else {
                iv_caption_portrait.setVisibility(View.GONE);
                iv_caption_landscape.setVisibility(View.GONE);
            }
            initializePlayer(liveShow);
            if (mCastSession != null && mCastSession.isConnected()) {
                updatePlaybackLocation(PlaybackLocation.REMOTE);
            } else {
                updatePlaybackLocation(PlaybackLocation.LOCAL);
            }
            resumeMediaPlayer();
            mPlaybackState = PlaybackState.PLAYING;
            updatePlayButton(mPlaybackState);

        }

    }

    public void playNextVideo(List<LiveShowData> videosList) {

        videoPosition = videoPosition + 1;
        if (videosList.size() > videoPosition) {
            if (analyticsVideo != null && startTime != 0) {
                sendAnaLyticsReport(getOnDemandVideoDetail(analyticsVideo));
                startTime = 0;
            }
            analyticsVideo = videosList.get(videoPosition);
            new SharedPreferenceHandler(HomeDraweerStreamingActivity.this).saveCurrentAdVideoPosition(videoPosition);
            updatePlayerWithNewVideo(videosList.get(videoPosition));
        } else {
            videoPosition = 0;
            new SharedPreferenceHandler(this).saveCurrentAdVideo(null);
            liveShow = null;
            videoPosition = 0;
            showLiveShow();
        }

    }

}