package com.example.livestreamingapp.home.ui;

import androidx.appcompat.app.AppCompatActivity;
import androidx.recyclerview.widget.GridLayoutManager;
import androidx.recyclerview.widget.LinearLayoutManager;
import androidx.recyclerview.widget.RecyclerView;

import android.content.Intent;
import android.os.Bundle;
import android.text.Editable;
import android.text.TextWatcher;
import android.util.Log;
import android.view.KeyEvent;
import android.view.Menu;
import android.view.MenuItem;
import android.view.View;
import android.view.inputmethod.EditorInfo;
import android.widget.EditText;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.TextView;
import android.widget.Toast;

import com.example.livestreamingapp.MassTvActivity;
import com.example.livestreamingapp.MassTvApplication;
import com.example.livestreamingapp.R;
import com.example.livestreamingapp.home.ui.adapter.AdapterCategory;
import com.example.livestreamingapp.home.ui.adapter.AdapterSearchHint;
import com.example.livestreamingapp.model.CategoryData;
import com.example.livestreamingapp.model.OnDemandCategory;
import com.example.livestreamingapp.model.OnDemandVideo;
import com.example.livestreamingapp.model.mastercategory.OnDemandMasterCategory;
import com.example.livestreamingapp.ondemand.AdapterViewClickListener;
import com.example.livestreamingapp.ondemand.VideoListActivity;
import com.example.livestreamingapp.utils.ConnectivityReceiver;
import com.example.livestreamingapp.utils.Constants;
import com.example.livestreamingapp.utils.SharedPreferenceHandler;
import com.example.livestreamingapp.utils.SpacesItemDecoration;
import com.example.livestreamingapp.webservice.GetVideoCategoryList;
import com.google.android.gms.cast.framework.CastButtonFactory;
import com.google.android.gms.cast.framework.CastContext;

import java.util.ArrayList;
import java.util.List;

public class SearchActivity extends MassTvActivity implements TextWatcher, AdapterViewClickListener,
        ConnectivityReceiver.ConnectivityReceiverListener, View.OnClickListener {

    private EditText et_search;
    private AdapterSearchHint adapterSearchHint;
    private AdapterCategory adapterCategory;
    private RecyclerView rv_search_result;
    private RecyclerView rv_search_category;
    private List<OnDemandMasterCategory> hintList;
    private int INDEX_SIZE;
    private MenuItem mediaRouteMenuItem;
    private CastContext mCastContext;
    private ImageView iv_back;
    private ArrayList<OnDemandCategory> categoryList;
    private LinearLayout lin_category;
    private SpacesItemDecoration spacesItemDecoration;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_search);
        initViews();

    }

    @Override
    protected void onResume() {
        super.onResume();
        MassTvApplication.getInstance().setConnectivityListener(this);

    }

    @Override
    protected void onPause() {
        super.onPause();
        try {
            MassTvApplication.getInstance().setUnRegister();
        }catch (Exception e){

        }
    }

    private void initViews() {
        et_search = findViewById(R.id.et_search);
        lin_category = findViewById(R.id.lin_cat);
        et_search.addTextChangedListener(this);
        rv_search_result = findViewById(R.id.rv_search_result);
        rv_search_category = findViewById(R.id.rv_category_search);
        iv_back = findViewById(R.id.iv_back);
        iv_back.setOnClickListener(this);

        spacesItemDecoration = new SpacesItemDecoration(0,0,0,24);

        hintList = new ArrayList<>();
        LinearLayoutManager layoutManager = new LinearLayoutManager(this);
        rv_search_result.setLayoutManager(layoutManager);
        adapterSearchHint = new AdapterSearchHint(this,hintList,this,"");
        rv_search_result.setAdapter(adapterSearchHint);

        categoryList = new ArrayList<>();
        GridLayoutManager layoutManager2 = new GridLayoutManager(this,2);
        rv_search_category.setLayoutManager(layoutManager2);
        rv_search_category.addItemDecoration(spacesItemDecoration);
        adapterCategory = new AdapterCategory(this,categoryList,this,"");
        rv_search_category.setAdapter(adapterCategory);

        et_search.setOnEditorActionListener(new TextView.OnEditorActionListener() {
            @Override
            public boolean onEditorAction(TextView v, int actionId, KeyEvent event) {
                if ((actionId== EditorInfo.IME_ACTION_SEARCH
                        || actionId == EditorInfo.IME_ACTION_DONE
                        || actionId == KeyEvent.ACTION_DOWN)
                        || actionId == KeyEvent.KEYCODE_ENTER){
                    if(et_search.getText().length()>0){
                        Intent intent = new Intent(SearchActivity.this,SearchVideoListActivity.class);
                        intent.putExtra(Constants.CATEGORY_TYPE,et_search.getText().toString());
                        startActivity(intent);
                    }else {
                        Toast.makeText(SearchActivity.this, "Field can't be empty", Toast.LENGTH_SHORT).show();
                    }

                    return true;
                }
                return false;
            }
        });

        mCastContext = CastContext.getSharedInstance(this);

        if(checkConnection()){
            getCategoryList();
        }



    }


    /**
     * Check internet connection Manually
     */
    private boolean checkConnection() {
        return ConnectivityReceiver.isConnected();
    }


    private void getCategoryList() {

        new GetVideoCategoryList(this, new GetVideoCategoryList.onDemandCategoryListListener() {

            @Override
            public void onFetchedCategoryList(CategoryData videoData) {
                categoryList.clear();
                categoryList = new ArrayList<>();
                categoryList.addAll(videoData.getCategory_list());
                adapterCategory.addData(categoryList,"search");
                adapterCategory.notifyDataSetChanged();

                if(et_search.getText().toString().isEmpty()){
                    lin_category.setVisibility(View.VISIBLE);
                }
            }

            @Override
            public void onFetchedMasterCategoryList(List<OnDemandMasterCategory> videoData) {

            }

            @Override
            public void onCategoryFetchingError(String error) {


            }
        }).GetCategoryList();

    }

    @Override
    public void beforeTextChanged(CharSequence s, int start, int count, int after) {

    }

    @Override
    public void onTextChanged(CharSequence s, int start, int before, int count) {

        INDEX_SIZE = 0;
        if(s.toString().isEmpty()||s.toString().replaceAll(" ","").toString().isEmpty()){
            rv_search_result.setVisibility(View.GONE);
            lin_category.setVisibility(View.VISIBLE);
        }else {
            rv_search_result.setVisibility(View.VISIBLE);
            lin_category.setVisibility(View.GONE);
        }
        getSearchHint(et_search.getText().toString(),0);



    }

    @Override
    public void afterTextChanged(Editable s) {

    }

    @Override
    public void onVideoSelected(int position, OnDemandVideo onDemandVideo) {

    }

    @Override
    public void onCategorySelected(int position, OnDemandCategory onDemandCategory) {
        String search = et_search.getText().toString();
      Intent intent ;

      if(onDemandCategory.getName()!=null && !onDemandCategory.getName().isEmpty()){
          intent = new Intent(this, VideoListActivity.class);
      }else {
          intent = new Intent(this,SearchVideoListActivity.class);
      }


        if(!search.isEmpty()){
            if(onDemandCategory.getTitle()!=null &&
                    onDemandCategory.getTitle().toLowerCase().contains(search.toLowerCase())){
                intent.putExtra(Constants.CATEGORY_TYPE,onDemandCategory.getTitle());
            }else {
                intent.putExtra(Constants.CATEGORY_TYPE,onDemandCategory.getTitle());
            }
        }else {
            intent.putExtra(Constants.CATEGORY_TYPE,onDemandCategory.getTitle());
        }

      //  intent.putExtra(Constants.CATEGORY_TYPE,onDemandCategory.getTitle());



        startActivity(intent);

    }

    @Override
    public void onWatchLater(int i, OnDemandVideo onDemandVideo) {

    }

    @Override
    public void onNetworkConnectionChanged(boolean isConnected) {

        if(isConnected &&  et_search.getText().toString().length()>0){
            getSearchHint(et_search.getText().toString(),INDEX_SIZE);
        }
        if(isConnected && categoryList==null || categoryList.size()==0){
            getCategoryList();
        }
    }

    public void getSearchHint(String search,int index){
        new GetVideoCategoryList(this, new GetVideoCategoryList.onDemandCategoryListListener() {


            @Override
            public void onFetchedCategoryList(CategoryData videoData) {



            }

            @Override
            public void onFetchedMasterCategoryList(List<OnDemandMasterCategory> categories) {
                hintList.clear();
                hintList = new ArrayList<>();
                hintList.addAll(categories);
                adapterSearchHint = new AdapterSearchHint(SearchActivity.this,
                        hintList, SearchActivity.this,search);
                rv_search_result.setAdapter(adapterSearchHint);
                adapterSearchHint.notifyDataSetChanged();
            }

            @Override
            public void onCategoryFetchingError(String error) {


            }
        }).GetSearchHint(search,index);
    }


    @Override
    public boolean onCreateOptionsMenu(Menu menu) {
        // Inflate the menu; this adds items to the action bar if it is present.
        getMenuInflater().inflate(R.menu.menu_cast, menu);
        mediaRouteMenuItem = CastButtonFactory.setUpMediaRouteButton(getApplicationContext(), menu,
                R.id.media_route_menu_item);
        return true;
    }

    @Override
    public void onClick(View v) {
        switch (v.getId()){
            case R.id.iv_back:
                onBackPressed();
                break;
        }
    }
}