package com.example.livestreamingapp.home.ui;

import android.content.Intent;
import android.content.res.Configuration;
import android.graphics.Color;
import android.graphics.drawable.ColorDrawable;
import android.graphics.drawable.InsetDrawable;
import android.os.Bundle;
import android.util.Log;
import android.view.Menu;
import android.view.MenuItem;
import android.view.View;
import android.widget.Button;
import android.widget.TextView;

import androidx.annotation.NonNull;
import androidx.appcompat.app.AlertDialog;
import androidx.appcompat.app.AppCompatActivity;
import androidx.appcompat.widget.Toolbar;
import androidx.recyclerview.widget.GridLayoutManager;
import androidx.recyclerview.widget.RecyclerView;

import com.example.livestreamingapp.MassTvActivity;
import com.example.livestreamingapp.MassTvApplication;
import com.example.livestreamingapp.R;
import com.example.livestreamingapp.model.MySubscription;
import com.example.livestreamingapp.model.OnDemandCategory;
import com.example.livestreamingapp.model.OnDemandVideo;
import com.example.livestreamingapp.model.ProfileData;
import com.example.livestreamingapp.model.webseries.ContentDetail;
import com.example.livestreamingapp.model.webseries.ContentInfo;
import com.example.livestreamingapp.ondemand.AdapterGridVideoList;
import com.example.livestreamingapp.ondemand.AdapterViewClickListener;
import com.example.livestreamingapp.ondemand.VideoListActivity;
import com.example.livestreamingapp.subscription.SubscriptionSelectorActivity;
import com.example.livestreamingapp.utils.ConnectivityReceiver;
import com.example.livestreamingapp.utils.Constants;
import com.example.livestreamingapp.utils.SharedPreferenceHandler;
import com.example.livestreamingapp.utils.SpacesItemDecoration;
import com.example.livestreamingapp.video.VideoDetailActivity;
import com.example.livestreamingapp.webseries.WebSeriesDetailActivity;
import com.example.livestreamingapp.webservice.GetOnDemandVideoList;
import com.example.livestreamingapp.webservice.SubscriptionService;
import com.facebook.shimmer.ShimmerFrameLayout;
import com.google.android.gms.cast.framework.CastButtonFactory;
import com.google.android.gms.cast.framework.CastContext;
import com.google.android.material.snackbar.Snackbar;
import com.google.gson.Gson;

import java.util.ArrayList;
import java.util.List;

public class SearchVideoListActivity extends MassTvActivity implements View.OnClickListener,
        AdapterViewClickListener, ConnectivityReceiver.ConnectivityReceiverListener {

    Toolbar mToolbar;
    private RecyclerView rv_video_list;
    private CastContext mCastContext;
    private MenuItem mediaRouteMenuItem;
    private TextView tv_toolbar_title;
    private AdapterGridVideoList adapterVideoOnDemand;
    private ShimmerFrameLayout shimmer_view_container;

    private MySubscription mySubscription;
    private SharedPreferenceHandler sharedPreferenceHandler;
    
    private int INDEX_SIZE = 0;
    private  List<OnDemandVideo> videosList;
    private String category;
    TextView tv_not_found;
    private String searchQuery = "";
    private ProfileData profileData;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_on_demand_video_list);
        setSupportActionBar(mToolbar);
        tv_toolbar_title = findViewById(R.id.tv_toolbar_title);
        tv_not_found = findViewById(R.id.tv_not_found);
        searchQuery = getIntent().getStringExtra(Constants.CATEGORY_TYPE);
       /* if (searchQuery != null) {
            tv_toolbar_title.setText(searchQuery);
        }*/
        setupActionBar(searchQuery);
        initViews();

        mCastContext = CastContext.getSharedInstance(this);

    }

    private void setupActionBar(String title) {
        mToolbar = (Toolbar) findViewById(R.id.toolbar);
      mToolbar.setTitle("");
        tv_toolbar_title.setText(""+title);
        setSupportActionBar(mToolbar);

        if(getSupportActionBar()!=null){
            getSupportActionBar().setDisplayHomeAsUpEnabled(true);
        }

    }

    @Override
    public boolean onOptionsItemSelected(@NonNull MenuItem item) {

        switch (item.getItemId()) {
            case android.R.id.home:
                // todo: goto back activity from here
                finish();
                return true;

            default:
                return super.onOptionsItemSelected(item);
        }
    }

    @Override
    protected void onResume() {
        super.onResume();
        MassTvApplication.getInstance().setConnectivityListener(this);
    }

    @Override
    protected void onPause() {
        super.onPause();
        try {
            MassTvApplication.getInstance().setUnRegister();
        }catch (Exception e){

        }
    }

    public boolean isTablet() {
      /*  return (getResources().getConfiguration().screenLayout
                & Configuration.SCREENLAYOUT_SIZE_MASK)
                >= Configuration.SCREENLAYOUT_SIZE_LARGE;*/

        return getResources().getBoolean(R.bool.isTablet);

    }


    public void initViews() {

        profileData = new SharedPreferenceHandler(this).getProfileInfo();
        videosList = new ArrayList<>();
        rv_video_list = (RecyclerView) findViewById(R.id.rv_video_list);
        shimmer_view_container = findViewById(R.id.shimmer_view_container);
        // set adapterVideoOnDemand Adapter for recyclerView
        GridLayoutManager layoutManagerDemand;
        if(isTablet()){
            layoutManagerDemand = new GridLayoutManager(this, 3);
        }else {
            layoutManagerDemand = new GridLayoutManager(this, 2);
        }

        adapterVideoOnDemand = new AdapterGridVideoList(this, new ArrayList<OnDemandVideo>(), this);
        rv_video_list.setLayoutManager(layoutManagerDemand);
        rv_video_list.addItemDecoration(new SpacesItemDecoration(16));
        rv_video_list.setAdapter(adapterVideoOnDemand);
        shimmer_view_container.startShimmer();


        rv_video_list.addOnScrollListener(new RecyclerView.OnScrollListener() {
            @Override
            public void onScrolled(@NonNull RecyclerView recyclerView, int dx, int dy) {
                super.onScrolled(recyclerView, dx, dy);
                int lastItem = layoutManagerDemand.findLastVisibleItemPosition();
                if(INDEX_SIZE-1==lastItem && INDEX_SIZE%15==0){
                    if(checkConnection()){
                        getVideoList(INDEX_SIZE);
                    }else {
                        showSnack(false);
                    }
                }
            }
        });

      /*  rv_video_list.addOnScrollListener(new RecyclerView.OnScrollListener() {
            @Override
            public void onScrolled(@NonNull RecyclerView recyclerView, int dx, int dy) {
                super.onScrolled(recyclerView, dx, dy);
                int lastItem = layoutManagerDemand.findLastVisibleItemPosition();
                if(INDEX_SIZE-1==lastItem && INDEX_SIZE%15==0){
                    if(checkConnection()){
                        getVideoList(INDEX_SIZE);
                    }else {
                        showSnack(false);
                    }
                }
            }
        });*/


        if(checkConnection() && videosList.size()==0){
           // getVideoList(INDEX_SIZE);
        }else {
            showSnack(checkConnection());
        }


        sharedPreferenceHandler = new SharedPreferenceHandler(this);
       // mySubscription = sharedPreferenceHandler.getMySubscriptionDetail();
    }

    private void getVideoList(int index_size) {
        new GetOnDemandVideoList(this, new GetOnDemandVideoList.onDemandVideoListListener() {
            @Override
            public void onDemandFetchedVideoList(List<OnDemandVideo> videoData) {

                hideShimmer();


                if (videoData.size() > 0) {

                    if (INDEX_SIZE == 0) {

                        videosList.clear();
                        videosList = new ArrayList<>();

                        videosList.addAll(videoData);
                        adapterVideoOnDemand = new AdapterGridVideoList(SearchVideoListActivity.this,
                                videosList, SearchVideoListActivity.this);
                        rv_video_list.setAdapter(adapterVideoOnDemand);
                        INDEX_SIZE = videosList.size();


                    } else {
                        videosList.addAll(videoData);
                        adapterVideoOnDemand.addData(videosList);
                        adapterVideoOnDemand.notifyItemInserted(INDEX_SIZE);
                        INDEX_SIZE = videosList.size();
                    }
                } else {
                    INDEX_SIZE = videosList.size() + 1;
                }

/*

                if(INDEX_SIZE == 0){
                    videosList.clear();
                    videosList = new ArrayList<>();
                    videosList.addAll(videoData);
                    adapterVideoOnDemand = new AdapterGridVideoList(SearchVideoListActivity.this,
                            videosList, SearchVideoListActivity.this);
                    rv_video_list.setAdapter(adapterVideoOnDemand);
                    INDEX_SIZE = videosList.size();
                    if(INDEX_SIZE==0){
                        tv_not_found.setVisibility(View.VISIBLE);
                    }else {
                        tv_not_found.setVisibility(View.GONE);
                    }
                }else {
                    videosList.addAll(videoData);
                    adapterVideoOnDemand.addData(videosList);
                    adapterVideoOnDemand.notifyItemInserted(INDEX_SIZE);
                    INDEX_SIZE = videosList.size();
                }
*/


            }

            @Override
            public void onDemandFetchingError(String error) {
                hideShimmer();
            }
        }).GetSearchVideoList(searchQuery,INDEX_SIZE,profileData.getId());
    }

    private void hideShimmer() {

        shimmer_view_container.hideShimmer();
        shimmer_view_container.setVisibility(View.GONE);
        rv_video_list.setVisibility(View.VISIBLE);

    }

    @Override
    public void onVideoSelected(int position, OnDemandVideo onDemandVideo) {

        /**
         * this below logic for checking user has subscription or not
         *  uncomment this logic when subscription is required
         */
       /* if (mySubscription != null && Integer.parseInt(mySubscription.getRemainingDays()) != 0) {
            if (!mySubscription.getLastRefreshDate().equalsIgnoreCase(DateCalculation.getCurrentDateInDDMMYY())) {
                if (sharedPreferenceHandler.getPrefernceBoolean(SharedPreferenceHandler.SUBSCRIBED)) {
                    if(checkConnection()){
                        getMySubscriptionDetail(null);

                    }
                }
            } else {
                startActivity(new Intent(SearchVideoListActivity.this,
                        VideoDetailActivity.class).
                        putExtra(Constants.SELECTED_ON_DEMAND_VIDEO, onDemandVideo));
            }

        } else {
            showSubscriptionAlert();
        }*/

        // direct play video don't check subscription

        if(onDemandVideo.getCategory().equalsIgnoreCase("Web Series")){

            com.example.livestreamingapp.model.webseries.ContentInfo contentInfo = new ContentInfo();
            contentInfo.setSubtitle(onDemandVideo.getSubtitle());
            contentInfo.setTitle(String.valueOf(onDemandVideo.getTitle()));
            contentInfo.setBannerImage(onDemandVideo.getThumb());
            contentInfo.setThumb(onDemandVideo.getThumb());
            contentInfo.setAvgRate(onDemandVideo.getAvg_rate());
            contentInfo.setIsWatchLater(onDemandVideo.isWatchList());
            contentInfo.setIsRated(onDemandVideo.isRated());
            contentInfo.setTotalUserRated(Integer.parseInt(onDemandVideo.getTotal_user_rated()));
            contentInfo.setCategory(onDemandVideo.getCategory());
            contentInfo.setId(onDemandVideo.getId());

            ContentDetail content_detail = new ContentDetail();
            content_detail.setContentInfo(contentInfo);

            startActivity(new Intent(SearchVideoListActivity.this,
                    WebSeriesDetailActivity.class).
                    putExtra(Constants.SELECTED_ON_DEMAND_VIDEO, content_detail));

        }else {
            startActivity(new Intent(SearchVideoListActivity.this,
                    VideoDetailActivity.class).
                    putExtra(Constants.SELECTED_ON_DEMAND_VIDEO, onDemandVideo));

        }



    }

    @Override
    public void onCategorySelected(int position, OnDemandCategory onDemandCategory) {

    }

    @Override
    public void onWatchLater(int i, OnDemandVideo onDemandVideo) {
       /* videosList.clear();
        videosList =new ArrayList<>();
        videosList = adapterVideoOnDemand.getVideoList();
        adapterVideoOnDemand = new AdapterGridVideoList(this,videosList,this);
        adapterVideoOnDemand.notifyDataSetChanged();*/
    }

    /**
     * Update Subscription detail
     *
     * @param videoToOpen
     */
    /**
     * Update Subscription detail
     *
     * @param videoToOpen
     */
    private void getMySubscriptionDetail(final OnDemandVideo videoToOpen) {

        new SubscriptionService(SearchVideoListActivity.this).
                getMySubscriptionInfo(new SubscriptionService.onSubscriptionInfoListener() {
                    @Override
                    public void onSubscriptionInfo(MySubscription subscriptionInfo) {
                        if (subscriptionInfo != null) {
                            new SharedPreferenceHandler(SearchVideoListActivity.this).saveSubscription(subscriptionInfo);
                            mySubscription = sharedPreferenceHandler.getMySubscriptionDetail();

                            if (videoToOpen != null && !subscriptionInfo.getRemainingDays().equalsIgnoreCase("0")) {
                                startActivity(new Intent(SearchVideoListActivity.this,
                                        VideoDetailActivity.class).
                                        putExtra(Constants.SELECTED_ON_DEMAND_VIDEO, videoToOpen));
                            }
                        } else {
                            new SharedPreferenceHandler(SearchVideoListActivity.this).saveSubscription(null);
                            mySubscription = sharedPreferenceHandler.getMySubscriptionDetail();

                        }
                    }

                    @Override
                    public void onFailure(String error) {

                       /* new SharedPreferenceHandler(HomeDraweerStreamingActivity.this).saveSubscription(null);
                        mySubscription = sharedPreferenceHandler.getMySubscriptionDetail();*/


                    }
                });
    }


    public void showSubscriptionAlert() {
        // Create an alert builder
        AlertDialog.Builder builder
                = new AlertDialog.Builder(this);
        // set the custom layout
        final View customLayout
                = getLayoutInflater()
                .inflate(
                        R.layout.layout_alert_subscribe,
                        null);
        builder.setView(customLayout);

        Button btn_subscribe = customLayout.findViewById(R.id.btn_subscribe);

        final AlertDialog dialog
                = builder.create();

        dialog.getWindow().setBackgroundDrawableResource(android.R.color.transparent);


        ColorDrawable back = new ColorDrawable(Color.TRANSPARENT);
        InsetDrawable inset = new InsetDrawable(back, 40, 0, 40, 0);
        if (dialog.getWindow() != null) {
            dialog.getWindow().setBackgroundDrawable(inset);

        }

        btn_subscribe.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                dialog.dismiss();
                startActivity(new Intent(SearchVideoListActivity.this, SubscriptionSelectorActivity.class));
            }
        });

        // create and show
        // the alert dialog

        dialog.show();
    }

    @Override
    public void onClick(View v) {
        switch (v.getId()) {
            case R.id.iv_back:
                finish();
                break;
        }
    }


    @Override
    public boolean onCreateOptionsMenu(Menu menu) {
        // Inflate the menu; this adds items to the action bar if it is present.
        getMenuInflater().inflate(R.menu.menu_cast, menu);
        mediaRouteMenuItem = CastButtonFactory.setUpMediaRouteButton(getApplicationContext(), menu,
                R.id.media_route_menu_item);
        return true;
    }

    @Override
    public void onNetworkConnectionChanged(boolean isConnected) {
        if((videosList==null||videosList.size()==0) && isConnected){
            getVideoList(INDEX_SIZE);
        }else {
            showSnack(isConnected);
        }
    }

    /**
     * Check internet connection Manually
     */
    private boolean checkConnection() {
        return ConnectivityReceiver.isConnected();
    }

    // Showing the status in Snackbar for intenet connectivity
    private void showSnack(boolean isConnected) {
        String message;
        int color;
        if (isConnected) {
            message = "Connection back Online!";
            color = Color.WHITE;
        } else {
            message = "Offline! Not connected to internet";
            color = Color.RED;


        }
        Snackbar snackbar = Snackbar
                .make(findViewById(R.id.parent), message, Snackbar.LENGTH_LONG);
        View sbView = snackbar.getView();
        TextView textView = (TextView) sbView.findViewById(R.id.snackbar_text);
        textView.setTextColor(color);
        snackbar.show();

    }



}