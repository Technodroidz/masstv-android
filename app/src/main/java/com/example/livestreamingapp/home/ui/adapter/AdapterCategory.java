package com.example.livestreamingapp.home.ui.adapter;

import android.content.Context;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.TextView;

import androidx.annotation.NonNull;
import androidx.recyclerview.widget.RecyclerView;

import com.example.livestreamingapp.R;
import com.example.livestreamingapp.model.OnDemandCategory;
import com.example.livestreamingapp.model.OnDemandVideo;
import com.example.livestreamingapp.ondemand.AdapterViewClickListener;

import java.util.List;

public class AdapterCategory extends RecyclerView.Adapter<AdapterCategory.CategoryHeadingHolder>
{

    private  List<OnDemandCategory> categoryList;
    private Context context;
    AdapterViewClickListener lister;
    private String search;

    public AdapterCategory(Context context, List<OnDemandCategory> categoryList ,
                           AdapterViewClickListener listener, String search){
        this.context = context;
        this.categoryList = categoryList;
        this.lister = listener;
        this.search = search;
    }

    public void addData(List<OnDemandCategory> categoryList,String search){
        if(this.categoryList.size()>0){
            this.categoryList.clear();
        }
        this.categoryList = categoryList;
        this.search = search;
    }

    @NonNull
    @Override
    public CategoryHeadingHolder onCreateViewHolder(@NonNull ViewGroup parent, int viewType) {
        View view = LayoutInflater.from(context).inflate(R.layout._item_list_search_category,parent,false);
        return new CategoryHeadingHolder(view);
    }

    @Override
    public void onBindViewHolder(@NonNull final CategoryHeadingHolder holder, int position) {
        OnDemandCategory onDemandCategory = categoryList.get(position);
        holder.tv_search.setText(onDemandCategory.getName());

        holder.itemView.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                onDemandCategory.setTitle(onDemandCategory.getName());
                onDemandCategory.setSubtitle(onDemandCategory.getName());
                lister.onCategorySelected(position,onDemandCategory);
            }
        });

    }

    @Override
    public int getItemCount() {
        return categoryList.size();
    }



    public  class CategoryHeadingHolder extends RecyclerView.ViewHolder
            implements AdapterViewClickListener {
        final private TextView tv_search;

        public CategoryHeadingHolder(@NonNull View itemView) {
            super(itemView);
            tv_search = itemView.findViewById(R.id.tv_search);

        }

        @Override
        public void onVideoSelected(int position,OnDemandVideo video) {

            lister.onVideoSelected(position,video);

        }

        @Override
        public void onCategorySelected(int position, OnDemandCategory onDemandCategory) {

        }

        @Override
        public void onWatchLater(int i, OnDemandVideo onDemandVideo) {

        }
    }
}
