package com.example.livestreamingapp.home.ui.adapter;

import android.content.Context;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.TextView;
import androidx.annotation.NonNull;
import androidx.recyclerview.widget.RecyclerView;
import com.example.livestreamingapp.R;
import com.example.livestreamingapp.model.OnDemandCategory;
import com.example.livestreamingapp.model.OnDemandVideo;
import com.example.livestreamingapp.model.mastercategory.OnDemandMasterCategory;
import com.example.livestreamingapp.ondemand.AdapterViewClickListener;

import java.util.List;

public class AdapterSearchHint extends RecyclerView.Adapter<AdapterSearchHint.CategoryHeadingHolder>
{

    private  List<OnDemandMasterCategory> categoryList;
    private Context context;
    AdapterViewClickListener lister;
    private String search;

    public AdapterSearchHint(Context context, List<OnDemandMasterCategory> categoryList ,
                             AdapterViewClickListener listener,String search){
        this.context = context;
        this.categoryList = categoryList;
        this.lister = listener;
        this.search = search;
    }

    public void addData(List<OnDemandMasterCategory> categoryList,String search){
        this.categoryList = categoryList;
        this.search = search;
    }

    @NonNull
    @Override
    public CategoryHeadingHolder onCreateViewHolder(@NonNull ViewGroup parent, int viewType) {
        View view = LayoutInflater.from(context).inflate(R.layout._item_list_search,parent,false);
        return new CategoryHeadingHolder(view);
    }

    @Override
    public void onBindViewHolder(@NonNull final CategoryHeadingHolder holder, int position) {
        OnDemandMasterCategory onDemandCategory = categoryList.get(position);
        if(!search.isEmpty()){
            if(categoryList.get(position).getTitle()!=null &&
                    categoryList.get(position).getTitle().toLowerCase().contains(this.search.toLowerCase())){
                holder.tv_search.setText(""+onDemandCategory.getTitle());
            }else {
                holder.tv_search.setText(""+onDemandCategory.getTitle());
            }
        }else {
            holder.tv_search.setText(""+onDemandCategory.getTitle());
        }


        holder.itemView.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                OnDemandCategory category = new OnDemandCategory();
                category.setTitle(onDemandCategory.getTitle());
                category.setSubtitle(onDemandCategory.getSubtitle());
                lister.onCategorySelected(position,category);
            }
        });

    }

    @Override
    public int getItemCount() {
        return categoryList.size();
    }



    public  class CategoryHeadingHolder extends RecyclerView.ViewHolder
    implements AdapterViewClickListener {
        final private TextView tv_search;

        public CategoryHeadingHolder(@NonNull View itemView) {
            super(itemView);
            tv_search = itemView.findViewById(R.id.tv_search);

        }

        @Override
        public void onVideoSelected(int position,OnDemandVideo video) {

            lister.onVideoSelected(position,video);

        }

        @Override
        public void onCategorySelected(int position, OnDemandCategory onDemandCategory) {

        }

        @Override
        public void onWatchLater(int i, OnDemandVideo onDemandVideo) {

        }
    }
}
