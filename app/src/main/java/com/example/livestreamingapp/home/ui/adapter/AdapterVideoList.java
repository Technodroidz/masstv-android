package com.example.livestreamingapp.home.ui.adapter;

import android.content.Context;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Button;
import android.widget.ImageButton;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.Switch;
import android.widget.TextView;

import androidx.annotation.NonNull;
import androidx.recyclerview.widget.RecyclerView;

import com.bumptech.glide.Glide;
import com.bumptech.glide.request.RequestOptions;
import com.example.livestreamingapp.R;
import com.example.livestreamingapp.model.OnDemandVideo;
import com.example.livestreamingapp.model.WatchLaterResponse;
import com.example.livestreamingapp.ondemand.AdapterViewClickListener;
import com.example.livestreamingapp.utils.Constants;
import com.example.livestreamingapp.utils.CustomProgressDialog;
import com.example.livestreamingapp.utils.SnackBar;
import com.example.livestreamingapp.utils.Utils;
import com.example.livestreamingapp.video.VideoDetailActivity;
import com.example.livestreamingapp.webservice.WatchLaterService;

import java.util.List;

public class AdapterVideoList extends RecyclerView.Adapter<AdapterVideoList.VideoListHolder> {

    private Context context;
    private List<OnDemandVideo> videoList;
    AdapterViewClickListener listener;
    private CustomProgressDialog pDialog;
    private OnDemandVideo onDemandVideo;

    public AdapterVideoList(Context context, List<OnDemandVideo> videoList, AdapterViewClickListener listener) {
        this.context = context;
        this.videoList = videoList;
        this.listener = listener;
    }

    private void setNowPlaying(OnDemandVideo onDemandVideo) {
        this.onDemandVideo = onDemandVideo;
    }

    protected void showProgrss() {
        if (pDialog == null) {
            // pDialog = new ProgressDialog(this);
            pDialog = new CustomProgressDialog(context);
            pDialog.setCancelable(false);
        }
        if (!pDialog.isShowing()) {
            // Show progressbar
            pDialog.show();
        }
    }

    public void changeVideoList(List<OnDemandVideo> videoList) {
        this.videoList = videoList;
    }

    protected void dismisProgrss() {
        if (pDialog != null && pDialog.isShowing()) {
            pDialog.dismiss();
        }
    }

    public List<OnDemandVideo> getVideoList() {
        return this.videoList;
    }

    @NonNull
    @Override
    public VideoListHolder onCreateViewHolder(@NonNull ViewGroup parent, int viewType) {
        View view = LayoutInflater.from(context).inflate(R.layout._item_list_video, parent, false);

        return new VideoListHolder(view);
    }

    @Override
    public void onBindViewHolder(@NonNull VideoListHolder holder, int position) {


        holder.ib_play_later.setVisibility(View.VISIBLE);
        holder.lin_more_videos.setVisibility(View.GONE);
        // holder.iv_now_playing.setVisibility(View.GONE);

        if (context instanceof VideoDetailActivity) {
            holder.ib_play_later.setVisibility(View.GONE);
        }

        if (position < videoList.size()) {
            //   holder.tv_rating.setText(""+ Utils.getDecimalOnePoint(videoList.get(position).getAvg_rate()));
            holder.tv_rating.setText("" + Utils.getDecimalOnePoint(videoList.get(position).getAvg_rate()));
            holder.tv_title.setText("" + videoList.get(position).getTitle());
            if (videoList.get(position).isWatchList()) {
                holder.ib_play_later.setImageResource(R.drawable.ic_watched_list);
            } else {
                holder.ib_play_later.setImageResource(R.drawable.ic_watch_later);
            }
            OnDemandVideo video = videoList.get(position);

            RequestOptions requestOptions = new RequestOptions();
            requestOptions.placeholder(R.drawable.placeholder_black);
            requestOptions.error(R.drawable.placeholder_black);
            try {
                Glide.with(context).setDefaultRequestOptions(requestOptions).load(video.getThumb()).into(holder.iv_image);
            } catch (Exception e) {
                e.printStackTrace();
            }

        } else {
            if (videoList.size() > 8) {
                holder.lin_more_videos.setVisibility(View.VISIBLE);
            } else {
                holder.itemView.setVisibility(View.GONE);
            }
            //  holder.iv_image.setImageDrawable(context.getResources().getDrawable(R.drawable.upload_background));
        }

    }

    @Override
    public int getItemCount() {
        return videoList.size() + 1;
    }

    public class VideoListHolder extends RecyclerView.ViewHolder {
        private ImageView iv_now_playing;
        private ImageView ib_play_later;
        private ImageView iv_image;
        private LinearLayout lin_more_videos;
        private TextView tv_rating;
        private TextView tv_title;

        public VideoListHolder(@NonNull View itemView) {
            super(itemView);
            iv_now_playing = (ImageView) itemView.findViewById(R.id.iv_now_playing);
            ib_play_later = (ImageView) itemView.findViewById(R.id.ib_play_later);
            iv_image = (ImageView) itemView.findViewById(R.id.iv_image);
            lin_more_videos = (LinearLayout) itemView.findViewById(R.id.lin_more_videos);
            tv_rating = (TextView) itemView.findViewById(R.id.tv_rating);
            tv_title = (TextView) itemView.findViewById(R.id.tv_title);

            ib_play_later.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View v) {
                    showProgrss();
                    new WatchLaterService(context, new WatchLaterService.OnWatchLaterResponse() {
                        @Override
                        public void onResponse(WatchLaterResponse response) {
                            dismisProgrss();
                            new SnackBar(context).showPositiveSnack("" + response.getMessage());
                            videoList.get(getLayoutPosition()).setWatchList(!videoList.get(getLayoutPosition()).isWatchList());
                            listener.onWatchLater(getLayoutPosition(), videoList.get(getLayoutPosition()));

                        }

                        @Override
                        public void onError(String error) {
                            dismisProgrss();
                        }
                    }).addToWatchListVideo(videoList.get(getLayoutPosition()).getId(), videoList.get(getLayoutPosition()).isWatchList() ? "remove" : "add",videoList.get(getLayoutPosition()).getCategory());
                }
            });

            itemView.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View v) {
                    if (getAdapterPosition() == videoList.size()) {
                        listener.onVideoSelected(-1, null);

                    } else {
                        listener.onVideoSelected(getLayoutPosition(), videoList.get(getAdapterPosition()));

                    }
                }
            });

        }
    }

    public int getSize() {
        return videoList.size();
    }

    @Override
    public int getItemViewType(int position) {
        return position;
    }
}
