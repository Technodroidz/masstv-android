package com.example.livestreamingapp.home.ui.adapter;

import android.content.Context;
import android.content.Intent;
import android.net.Uri;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;

import androidx.annotation.NonNull;
import androidx.recyclerview.widget.RecyclerView;

import com.example.livestreamingapp.R;
import com.example.livestreamingapp.video.VideoDetailActivity;
import com.google.android.exoplayer2.C;
import com.google.android.exoplayer2.ExoPlaybackException;
import com.google.android.exoplayer2.ExoPlayer;
import com.google.android.exoplayer2.ExoPlayerFactory;
import com.google.android.exoplayer2.PlaybackParameters;
import com.google.android.exoplayer2.Player;
import com.google.android.exoplayer2.SimpleExoPlayer;
import com.google.android.exoplayer2.Timeline;
import com.google.android.exoplayer2.extractor.DefaultExtractorsFactory;
import com.google.android.exoplayer2.extractor.ExtractorsFactory;
import com.google.android.exoplayer2.source.ExtractorMediaSource;
import com.google.android.exoplayer2.source.MediaSource;
import com.google.android.exoplayer2.source.TrackGroupArray;
import com.google.android.exoplayer2.trackselection.DefaultTrackSelector;
import com.google.android.exoplayer2.trackselection.TrackSelectionArray;
import com.google.android.exoplayer2.ui.AspectRatioFrameLayout;

import com.google.android.exoplayer2.ui.PlayerView;
import com.google.android.exoplayer2.upstream.BandwidthMeter;
import com.google.android.exoplayer2.upstream.DataSource;
import com.google.android.exoplayer2.upstream.DefaultBandwidthMeter;
import com.google.android.exoplayer2.upstream.DefaultDataSourceFactory;
import com.google.android.exoplayer2.util.Util;

import java.util.List;

public class DashBoardImageAdapter extends RecyclerView.Adapter<DashBoardImageAdapter.ImageSlider1_ViewHolder> {

    private List<String> items;
    private Context context;
    int playingPosition = -1;

    public DashBoardImageAdapter(List<String> items,Context context) {
        this.items = items;
        this.context = context;
    }

    public void setItems(List<String> items) {
        this.items = items;
    }


    @NonNull
    @Override
    public ImageSlider1_ViewHolder onCreateViewHolder(@NonNull ViewGroup parent, int viewType) {
        return new ImageSlider1_ViewHolder(LayoutInflater.from(context).inflate(R.layout.sliding_adapter, parent, false));
    }

    @Override
    public void onBindViewHolder(@NonNull ImageSlider1_ViewHolder holder, int position) {

      //  holder.iv_image.setImageDrawable(context.getResources().getDrawable(R.drawable.demo_video_image2));
        initializePlayer(holder.player,holder.exoplayerView);
        holder.player.setPlayWhenReady(false);
        if(playingPosition==position){
            holder.player.setPlayWhenReady(true);
        }


    }

    public void setPlayVideoPosition(int playingPosition){
        this.playingPosition = playingPosition;
    }

    @Override
    public int getItemCount() {
        return 8;
    }

    class ImageSlider1_ViewHolder extends RecyclerView.ViewHolder {

        private PlayerView exoplayerView;
        private SimpleExoPlayer player;

        ImageSlider1_ViewHolder(@NonNull View itemView) {
            super(itemView);
            exoplayerView = itemView.findViewById(R.id.exoplayerView);

            itemView.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View v) {

                        context.startActivity(new Intent(context, VideoDetailActivity.class));
                    }
            });
        }


    }

    private void initializePlayer(SimpleExoPlayer player, PlayerView simpleExoPlayerView){
// Create a default TrackSelector
        BandwidthMeter bandwidthMeter = new DefaultBandwidthMeter();
        /*TrackSelection.Factory videoTrackSelectionFactory =
                new AdaptiveTrackSelection.Factory(bandwidthMeter);
        TrackSelector trackSelector =
                new DefaultTrackSelector(videoTrackSelectionFactory);
*/
//Initialize the player
        player = ExoPlayerFactory.newSimpleInstance(context, new DefaultTrackSelector());


//Initialize simpleExoPlayerView
        simpleExoPlayerView.setPlayer(player);

        simpleExoPlayerView.setUseController(false);

// Produces DataSource instances through which media data is loaded.
        DataSource.Factory dataSourceFactory =
                new DefaultDataSourceFactory(context, Util.getUserAgent(context, "Video Player"));

// Produces Extractor instances for parsing the media data.
        ExtractorsFactory extractorsFactory = new DefaultExtractorsFactory();

// This is the MediaSource representing the media to be played.
         String mp4Url = "http://commondatastorage.googleapis.com/gtv-videos-bucket/sample/BigBuckBunny.mp4";

        Uri videoUri = Uri.parse(mp4Url);
        MediaSource videoSource = new ExtractorMediaSource(videoUri,
                dataSourceFactory, extractorsFactory, null, null);

// Prepare the player with the source.
        player.prepare(videoSource);

        player.addListener(new Player.EventListener(){

            @Override
            public void onTimelineChanged(Timeline timeline, Object manifest, int reason) {

            }

            @Override
            public void onTracksChanged(TrackGroupArray trackGroups, TrackSelectionArray trackSelections) {

            }

            @Override
            public void onLoadingChanged(boolean isLoading) {

            }

            @Override
            public void onPlayerStateChanged(boolean playWhenReady, int playbackState) {

            }

            @Override
            public void onRepeatModeChanged(int repeatMode) {

            }

            @Override
            public void onShuffleModeEnabledChanged(boolean shuffleModeEnabled) {

            }

            @Override
            public void onPlayerError(ExoPlaybackException error) {

            }

            @Override
            public void onPositionDiscontinuity(int reason) {

            }

            @Override
            public void onPlaybackParametersChanged(PlaybackParameters playbackParameters) {

            }

            @Override
            public void onSeekProcessed() {

            }
        });

        simpleExoPlayerView.setResizeMode(AspectRatioFrameLayout.RESIZE_MODE_FILL);
        player.setVideoScalingMode(C.VIDEO_SCALING_MODE_SCALE_TO_FIT_WITH_CROPPING);




    }

}