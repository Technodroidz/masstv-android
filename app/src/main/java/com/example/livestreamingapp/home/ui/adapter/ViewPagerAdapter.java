package com.example.livestreamingapp.home.ui.adapter;

import android.content.Context;
import android.net.sip.SipSession;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.LinearLayout;

import androidx.annotation.NonNull;
import androidx.viewpager.widget.PagerAdapter;

import com.bumptech.glide.Glide;
import com.bumptech.glide.request.RequestOptions;
import com.example.livestreamingapp.R;
import com.example.livestreamingapp.home.ui.HomeDraweerStreamingActivity;
import com.example.livestreamingapp.model.homescreen.PromotionBanner;

import java.util.ArrayList;
import java.util.List;
import java.util.Objects;

public class ViewPagerAdapter extends PagerAdapter {

    public interface BannerListener {
        public void onBannerListener(PromotionBanner promotionBanner);
    }

    // Context object 
    Context context;

    private BannerListener bannerListener;

    // Array of images
    List<PromotionBanner> images;

    // Layout Inflater
    LayoutInflater mLayoutInflater;

    // Viewpager Constructor 
    public ViewPagerAdapter(Context context, List<PromotionBanner> images, BannerListener bannerListener) {
        this.context = context;
        this.images = images;
        this.bannerListener = bannerListener;
        mLayoutInflater = (LayoutInflater) context.getSystemService(Context.LAYOUT_INFLATER_SERVICE);
    }

    public void addBannerList(List<PromotionBanner> list) {
        this.images.clear();
        this.images = new ArrayList<>();
        this.images.addAll(list);
    }

    @Override
    public int getCount() {
        // return the number of images
        return images.size();
    }

    @Override
    public boolean isViewFromObject(@NonNull View view, @NonNull Object object) {
        return view == ((LinearLayout) object);
    }

    @NonNull
    @Override
    public Object instantiateItem(@NonNull ViewGroup container, final int position) {
        // inflating the item.xml 
        View itemView = mLayoutInflater.inflate(R.layout.item, container, false);

        // referencing the image view from the item.xml file 
        ImageView imageView = (ImageView) itemView.findViewById(R.id.iv_banner);

        // setting the image in the imageView
        RequestOptions requestOptions = new RequestOptions();
        requestOptions.placeholder(R.drawable.place_holder_banner);
        requestOptions.error(R.drawable.place_holder_banner);

        Glide.with(context)
                .applyDefaultRequestOptions(requestOptions)
                .load(images.get(position).getBannerImage()).into(imageView);

        imageView.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                bannerListener.onBannerListener(images.get(position));
            }
        });

        container.addView(itemView, 0);
        return itemView;
    }

    @Override
    public void destroyItem(ViewGroup container, int position, Object object) {

        container.removeView((LinearLayout) object);
    }
}