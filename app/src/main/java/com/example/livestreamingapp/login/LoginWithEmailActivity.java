package com.example.livestreamingapp.login;

import android.app.ProgressDialog;
import android.content.Context;
import android.content.DialogInterface;
import android.content.Intent;
import android.net.ConnectivityManager;
import android.net.NetworkInfo;
import android.net.wifi.WifiManager;
import android.os.AsyncTask;
import android.os.Bundle;
import android.text.format.Formatter;
import android.util.Log;
import android.view.View;
import android.widget.Button;
import android.widget.EditText;

import androidx.appcompat.app.AlertDialog;
import androidx.appcompat.app.AppCompatActivity;
import androidx.appcompat.widget.Toolbar;

import com.example.livestreamingapp.MassTvActivity;
import com.example.livestreamingapp.R;
import com.example.livestreamingapp.model.APIError;
import com.example.livestreamingapp.model.LoginResponse;
import com.example.livestreamingapp.profile.AccountActivity;
import com.example.livestreamingapp.upload.AddPhoneNumberActivity;
import com.example.livestreamingapp.utils.ConnectivityReceiver;
import com.example.livestreamingapp.utils.Constants;
import com.example.livestreamingapp.utils.CustomProgressDialog;
import com.example.livestreamingapp.utils.ErrorUtils;
import com.example.livestreamingapp.utils.SharedPreferenceHandler;
import com.example.livestreamingapp.utils.SnackBar;
import com.example.livestreamingapp.webservice.APIClient;
import com.example.livestreamingapp.webservice.APIInterface;
import com.google.firebase.crashlytics.internal.network.HttpResponse;
import com.google.gson.Gson;

import org.bouncycastle.util.IPAddress;

import java.io.BufferedReader;
import java.io.InputStreamReader;
import java.net.HttpURLConnection;
import java.net.InetAddress;
import java.net.NetworkInterface;
import java.net.URL;
import java.util.Collections;
import java.util.Enumeration;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;

public class LoginWithEmailActivity extends MassTvActivity implements View.OnClickListener {

    private Toolbar mToolbar;
    private EditText et_email;
    private Button btn_proceed;
    String EMAIL_REGEX = "^[\\w-_\\.+]*[\\w-_\\.]\\@([\\w]+\\.)+[\\w]+[\\w]$";
    private APIInterface apiInterface;
    private CustomProgressDialog progressDialog;
    String IPaddress;
    Boolean IPValue;


    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.layout_login_with_mobile);
        setupActionBar();
        initViews();
    }

    public class GetIpAsyncTask extends AsyncTask<String, String, String> {
        @Override
        protected String doInBackground(String... strings) {
            return findJSONFromUrl("https://api.ipify.org/");
        }

        @Override
        protected void onPreExecute() {
            super.onPreExecute();
            progressDialog.show();
        }

        @Override
        protected void onPostExecute(String s) {
            super.onPostExecute(s);

            if (s.equalsIgnoreCase("error")) {
                progressDialog.dismiss();
                new SnackBar(LoginWithEmailActivity.this).showErrorSnack("Please check your internet connection!");
            } else {
                IPaddress = s;
                attemptLoginOrCreateAccount(et_email.getText().toString());
            }

        }
    }
    // Create Http connection And find Json

    public String findJSONFromUrl(String url) {
        String result = "";
        try {
            URL urls = new URL(url);
            HttpURLConnection conn = (HttpURLConnection) urls.openConnection();
            conn.setReadTimeout(15000); //milliseconds
            conn.setConnectTimeout(15000); // milliseconds
            conn.setRequestMethod("GET");

            conn.connect();

            if (conn.getResponseCode() == HttpURLConnection.HTTP_OK) {

                BufferedReader reader = new BufferedReader(new InputStreamReader(
                        conn.getInputStream(), "iso-8859-1"), 8);
                StringBuilder sb = new StringBuilder();
                String line = null;
                while ((line = reader.readLine()) != null) {
                    sb.append(line + "\n");
                }
                result = sb.toString();
            } else {

                return "error";
            }


        } catch (Exception e) {
            // System.out.println("exception in jsonparser class ........");
            e.printStackTrace();
            return "error";
        }

        return result;
    } // method ends


    private void initViews() {
        et_email = findViewById(R.id.et_email);
        btn_proceed = findViewById(R.id.btn_proceed);
        btn_proceed.setOnClickListener(this);
        new SharedPreferenceHandler(this).saveSubscription(null);

        progressDialog = new CustomProgressDialog(LoginWithEmailActivity.this);
        progressDialog.setCancelable(false);

        apiInterface = APIClient.getClient().create(APIInterface.class);
        // NetwordDetect();
    }

    //Check the internet connection.
    private void NetwordDetect() {

        boolean WIFI = false;

        boolean MOBILE = false;

        ConnectivityManager CM = (ConnectivityManager) getSystemService(Context.CONNECTIVITY_SERVICE);

        NetworkInfo[] networkInfo = CM.getAllNetworkInfo();

        for (NetworkInfo netInfo : networkInfo) {

            if (netInfo.getTypeName().equalsIgnoreCase("WIFI"))

                if (netInfo.isConnected())

                    WIFI = true;

            if (netInfo.getTypeName().equalsIgnoreCase("MOBILE"))

                if (netInfo.isConnected())

                    MOBILE = true;
        }
        if (WIFI) {
            IPaddress = GetDeviceipWiFiData();
            //textview.setText(IPaddress);

        }

        if (MOBILE) {
            IPaddress = GetDeviceipMobileData();
            //   textview.setText(IPaddress);
        }
    }

    public String GetDeviceipMobileData() {
        try {
            for (Enumeration<NetworkInterface> en = NetworkInterface.getNetworkInterfaces();
                 en.hasMoreElements(); ) {
                NetworkInterface networkinterface = en.nextElement();
                for (Enumeration<InetAddress> enumIpAddr = networkinterface.getInetAddresses(); enumIpAddr.hasMoreElements(); ) {
                    InetAddress inetAddress = enumIpAddr.nextElement();
                    if (!inetAddress.isLoopbackAddress()) {
                        return inetAddress.getAddress().toString();
                    }
                }
            }
        } catch (Exception ex) {

        }
        return null;
    }


    public String GetDeviceipWiFiData() {

        WifiManager wm = (WifiManager) getApplicationContext().getSystemService(WIFI_SERVICE);

        @SuppressWarnings("deprecation")

        String ip = Formatter.formatIpAddress(wm.getConnectionInfo().getIpAddress());

        return ip;

    }


    private void setupActionBar() {
        mToolbar = (Toolbar) findViewById(R.id.toolbar);
        mToolbar.setTitle("");
        setSupportActionBar(mToolbar);
    }


    /**
     * Check internet connection Manually
     */
    private boolean checkConnection() {
        return ConnectivityReceiver.isConnected();
    }

    @Override
    public void onClick(View v) {
        switch (v.getId()) {
            case R.id.btn_proceed:
                if (isValidEmail(et_email.getText().toString())) {

                    if (checkConnection()) {
                        new GetIpAsyncTask().execute();
                    } else {
                        new SnackBar(LoginWithEmailActivity.this).showErrorSnack("Please check your internet connection!");
                    }
                } else {
                    et_email.requestFocus();
                    et_email.setError("invalid email address!");
                }

                break;
        }
    }

    /**
     * @Rehan After api hit successfully we will get the LoginResponse object In LoginResponse if
     * status is 1 that means otp successfully send to to the entered email address
     * if status is 0 it means something went wrong.
     */
    private void attemptLoginOrCreateAccount(String email) {

        Map<String, String> credential = new HashMap<>();
        credential.put(getString(R.string.email), email);
        credential.put(getString(R.string.ip_address), IPaddress);
        Call<LoginResponse> loginApiCall = apiInterface.login(credential);


        loginApiCall.enqueue(
                new Callback<LoginResponse>() {
                    @Override
                    public void onResponse(
                            Call<LoginResponse> call, Response<LoginResponse> response) {
                        progressDialog.dismiss();

                        if (response.code() == 200) {

                            if (response.body().getStatus()) {


                                startActivity(new Intent(LoginWithEmailActivity.this, OtpVerificationActivity.class).
                                        putExtra(Constants.EMAIL_ID, et_email.getText().toString()));

                            } else {
                                new AlertDialog.Builder(LoginWithEmailActivity.this, R.style.AlertDialogStyle)
                                        .setCancelable(false)
                                        .setMessage(getString(R.string.something_went_wrong))
                                        .setPositiveButton(
                                                "Ok",
                                                new DialogInterface.OnClickListener() {
                                                    @Override
                                                    public void onClick(
                                                            DialogInterface dialog, int which) {
                                                    }
                                                })
                                        .show();
                            }


                        } else {
                            try {
                                APIError error = ErrorUtils.parseError(response);
                                String errorMessage = error.getData().getError();

                                if (errorMessage == null) {
                                    errorMessage = "Something went wrong";
                                }

                                new AlertDialog.Builder(LoginWithEmailActivity.this, R.style.AlertDialogStyle)
                                        .setCancelable(false)
                                        .setMessage(errorMessage)
                                        .setPositiveButton(
                                                "Ok",
                                                new DialogInterface.OnClickListener() {
                                                    @Override
                                                    public void onClick(
                                                            DialogInterface dialog, int which) {
                                                    }
                                                })
                                        .show();
                            } catch (Exception e) {

                            }


                        }
                    }

                    @Override
                    public void onFailure(Call<LoginResponse> call, Throwable t) {
                        progressDialog.dismiss();
                        try {
                            new AlertDialog.Builder(LoginWithEmailActivity.this, R.style.AlertDialogStyle)
                                    .setCancelable(false)
                                    .setMessage("" + t.getMessage())
                                    .setPositiveButton(
                                            "Ok",
                                            new DialogInterface.OnClickListener() {
                                                @Override
                                                public void onClick(
                                                        DialogInterface dialog, int which) {
                                                }
                                            })
                                    .show();
                        }catch (Exception e){

                        }

                    }
                });
    }

    public boolean isValidEmail(String email) {
        return email.matches(EMAIL_REGEX);
    }

}
