package com.example.livestreamingapp.login;

import android.accounts.Account;
import android.content.DialogInterface;
import android.content.Intent;
import android.net.Uri;
import android.os.AsyncTask;
import android.os.Bundle;
import android.os.Handler;
import android.util.Log;
import android.view.MenuItem;
import android.view.View;
import android.widget.Button;
import android.widget.TextView;
import android.widget.Toast;

import androidx.annotation.NonNull;
import androidx.appcompat.app.AlertDialog;
import androidx.appcompat.app.AppCompatActivity;
import androidx.appcompat.widget.Toolbar;

import com.example.livestreamingapp.MassTvActivity;
import com.example.livestreamingapp.R;
import com.example.livestreamingapp.custom_view.SpannableText;
import com.example.livestreamingapp.home.ui.HomeDraweerStreamingActivity;
import com.example.livestreamingapp.model.APIError;
import com.example.livestreamingapp.model.LoginResponse;
import com.example.livestreamingapp.utils.Constants;
import com.example.livestreamingapp.utils.CustomProgressDialog;
import com.example.livestreamingapp.utils.ErrorUtils;
import com.example.livestreamingapp.utils.SharedPreferenceHandler;
import com.example.livestreamingapp.utils.SnackBar;
import com.example.livestreamingapp.webservice.APIClient;
import com.example.livestreamingapp.webservice.APIInterface;
import com.google.android.gms.auth.api.signin.GoogleSignIn;
import com.google.android.gms.auth.api.signin.GoogleSignInAccount;
import com.google.android.gms.auth.api.signin.GoogleSignInClient;
import com.google.android.gms.auth.api.signin.GoogleSignInOptions;
import com.google.android.gms.cast.framework.CastContext;
import com.google.android.gms.cast.framework.CastStateListener;
import com.google.android.gms.cast.framework.IntroductoryOverlay;
import com.google.android.gms.common.api.ApiException;
import com.google.android.gms.tasks.OnCompleteListener;
import com.google.android.gms.tasks.Task;
import com.google.firebase.messaging.FirebaseMessaging;
import com.google.firebase.messaging.FirebaseMessagingService;
import com.google.gson.Gson;

import java.io.BufferedReader;
import java.io.InputStreamReader;
import java.net.HttpURLConnection;
import java.net.URL;
import java.util.HashMap;
import java.util.Map;

import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;

public class MultipleLoginSelectionActivity extends MassTvActivity implements View.OnClickListener {

    private static final int RC_SIGN_IN = 444;
    private static final String TAG = MultipleLoginSelectionActivity.class.getName();
    private MenuItem mediaRouteMenuItem;
    private CastContext mCastContext;
    private Toolbar mToolbar;
    private CastStateListener mCastStateListener;
    private IntroductoryOverlay mIntroductoryOverlay;
    private GoogleSignInClient mGoogleSignInClient;
    private CustomProgressDialog progressDialog;
    private String IPaddress;
    private APIInterface apiInterface;
    private Button btn_login_google;
    private TextView tv_privacy_policy;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.layout_multiple_login_chooser);
        setupActionBar();
        tv_privacy_policy = findViewById(R.id.tv_privacy_policy);
        btn_login_google = findViewById(R.id.btn_login_google);
        btn_login_google.setOnClickListener(this);
        apiInterface = APIClient.getClient().create(APIInterface.class);
        progressDialog = new CustomProgressDialog(this);
        progressDialog.setCancelable(false);

        GoogleSignInOptions gso = new GoogleSignInOptions.Builder(GoogleSignInOptions.DEFAULT_SIGN_IN)
                .requestEmail()
                .build();
        mGoogleSignInClient = GoogleSignIn.getClient(this, gso);

        try {

            tv_privacy_policy.setText("By sign up, I agree to MassTv's Terms of Service and Privacy Policy");
            if(tv_privacy_policy.getText().toString()!=null && tv_privacy_policy.getText().toString().length()>60){
                SpannableText.setMessageWithClickableLink(this,tv_privacy_policy,32,48,53,tv_privacy_policy.getText().toString().length());
            }
        }catch (Exception e){

        }

    }


    public void attemptSignIn() {
        progressDialog.show();
        Intent signInIntent = mGoogleSignInClient.getSignInIntent();
        startActivityForResult(signInIntent, RC_SIGN_IN);
    }


    private void setupActionBar() {
        mToolbar = (Toolbar) findViewById(R.id.toolbar);
        mToolbar.setTitle("");
        setSupportActionBar(mToolbar);
    }

    @Override
    public void onActivityResult(int requestCode, int resultCode, Intent data) {
        super.onActivityResult(requestCode, resultCode, data);

        // Result returned from launching the Intent from GoogleSignInClient.getSignInIntent(...);
        if (requestCode == RC_SIGN_IN) {
            // The Task returned from this call is always completed, no need to attach
            // a listener.
            progressDialog.dismiss();
            Task<GoogleSignInAccount> task = GoogleSignIn.getSignedInAccountFromIntent(data);
            handleSignInResult(task);
        }
    }

    /**
     * @param completedTask
     * @Rehan handle Response from google login
     */
    private void handleSignInResult(Task<GoogleSignInAccount> completedTask) {
        try {
            GoogleSignInAccount account = completedTask.getResult(ApiException.class);

            // Signed in successfully, show authenticated UI.

            if(account==null || account.getEmail()==null || account.getEmail().length()==0){
                logOut();
                new AlertDialog.Builder(MultipleLoginSelectionActivity.this,R.style.AlertDialogStyle).setCancelable(false)
                        .setMessage("Google Login Failed! Please try again")
                        .setPositiveButton("Ok", new DialogInterface.OnClickListener() {
                            @Override
                            public void onClick(DialogInterface dialog, int which) {

                            }
                        }).show();
            }else {
                updateUI(account);
            }

        } catch (ApiException e) {
            // The ApiException status code indicates the detailed failure reason.
            // Please refer to the GoogleSignInStatusCodes class reference for more information.
            Log.w(TAG, "signInResult:failed code=" + e.getStatusCode());
          //  updateUI(null);
        }
    }

    private void updateUI(GoogleSignInAccount account) {
      /*  Log.e("live ",""+new Gson().toJson(account));
        Log.e(TAG, "check google id" +
                "3" +
                " " + new Gson().toJson(account.getId()));*/
        // Log.e(TAG, "check google email " + new Gson().toJson(account.getEmail()));
        new GetIpAsyncTask(account).execute();
    }

    public class GetIpAsyncTask extends AsyncTask<String, String, String> {
        GoogleSignInAccount account;

        @Override
        protected String doInBackground(String... strings) {
            return findJSONFromUrl("https://api.ipify.org/");
        }

        public GetIpAsyncTask(GoogleSignInAccount account) {
            this.account = account;
        }

        @Override
        protected void onPreExecute() {
            super.onPreExecute();
            progressDialog.show();
            btn_login_google.setEnabled(false);
        }

        @Override
        protected void onPostExecute(String s) {
            super.onPostExecute(s);

            if (s.equalsIgnoreCase("error")) {
                progressDialog.dismiss();
                btn_login_google.setEnabled(true);
                Toast.makeText(MultipleLoginSelectionActivity.this, "Please check your internet connection!", Toast.LENGTH_SHORT).show();
              //  new SnackBar(MultipleLoginSelectionActivity.this).showErrorSnack("Please check your internet connection!");
            } else {
                IPaddress = s;
                attemptLoginOrCreateAccount(account.getEmail(), account.getId());
            }

        }
    }

    /**
     * @Rehan After api hit successfully we will get the LoginResponse object In LoginResponse if
     * status is 1 that means otp successfully send to to the entered email address
     * if status is 0 it means something went wrong.
     */
    private void attemptLoginOrCreateAccount(String email, String id) {

        progressDialog.show();

        Map<String, String> credential = new HashMap<>();
        credential.put(getString(R.string.email), email);
        credential.put(getString(R.string.google_id), id);
        credential.put(getString(R.string.ip_address), IPaddress);
        credential.put(getString(R.string.device_type), "android");
        Call<LoginResponse> loginApiCall = apiInterface.loginGoogle(credential);


        loginApiCall.enqueue(
                new Callback<LoginResponse>() {
                    @Override
                    public void onResponse(
                            Call<LoginResponse> call, Response<LoginResponse> response) {
                        progressDialog.dismiss();

                        if (response.code() == 200) {



                            if (response.body().getStatus()) {
                                btn_login_google.setEnabled(false);
                                Log.e("check detail", "" + response.body().getUser().getEmail());
                                new SharedPreferenceHandler(MultipleLoginSelectionActivity.this).setProfileData(response.body().getUser());

                                new SharedPreferenceHandler(MultipleLoginSelectionActivity.this)
                                        .setPreference(Constants.PROFILE_INFO, new Gson().toJson(response.body().getUser()));

                                try {
                                    initializeFireBaseToken();
                                } catch (Exception e) {

                                }
                                startActivity(new Intent(MultipleLoginSelectionActivity.this, HomeDraweerStreamingActivity.class)
                                        .setFlags(Intent.FLAG_ACTIVITY_NEW_TASK | Intent.FLAG_ACTIVITY_CLEAR_TASK));


                            } else {
                                btn_login_google.setEnabled(true);
                                new AlertDialog.Builder(MultipleLoginSelectionActivity.this, R.style.AlertDialogStyle)
                                        .setCancelable(false)
                                        .setMessage(getString(R.string.something_went_wrong))
                                        .setPositiveButton(
                                                "Ok",
                                                new DialogInterface.OnClickListener() {
                                                    @Override
                                                    public void onClick(
                                                            DialogInterface dialog, int which) {
                                                    }
                                                })
                                        .show();
                            }


                        } else {
                            btn_login_google.setEnabled(true);
                            try {
                                APIError error = ErrorUtils.parseError(response);
                                String errorMessage = error.getData().getError();

                                if (errorMessage == null) {
                                    errorMessage = "Something went wrong";
                                }

                                new AlertDialog.Builder(MultipleLoginSelectionActivity.this, R.style.AlertDialogStyle)
                                        .setCancelable(false)
                                        .setMessage(errorMessage)
                                        .setPositiveButton(
                                                "Ok",
                                                new DialogInterface.OnClickListener() {
                                                    @Override
                                                    public void onClick(
                                                            DialogInterface dialog, int which) {
                                                    }
                                                })
                                        .show();
                            } catch (Exception e) {

                            }


                        }
                    }

                    @Override
                    public void onFailure(Call<LoginResponse> call, Throwable t) {
                        progressDialog.dismiss();
                        new AlertDialog.Builder(MultipleLoginSelectionActivity.this, R.style.AlertDialogStyle)
                                .setCancelable(false)
                                .setMessage("" + t.getMessage())
                                .setPositiveButton(
                                        "Ok",
                                        new DialogInterface.OnClickListener() {
                                            @Override
                                            public void onClick(
                                                    DialogInterface dialog, int which) {
                                            }
                                        })
                                .show();
                    }
                });
    }

    private void initializeFireBaseToken() {


        FirebaseMessaging.getInstance().getToken()
                .addOnCompleteListener(new OnCompleteListener<String>() {
                    @Override
                    public void onComplete(@NonNull Task<String> task) {
                        if (!task.isSuccessful()) {
                            Log.w("FCM ", "Fetching FCM registration token failed", task.getException());
                            return;
                        }
                        // Get new FCM registration token
                        String token = task.getResult();
                        new SharedPreferenceHandler(MultipleLoginSelectionActivity.this).saveFcmToken(token);

                    }

                });
    }

    // Create Http connection And find Json

    public String findJSONFromUrl(String url) {
        String result = "";
        try {
            URL urls = new URL(url);
            HttpURLConnection conn = (HttpURLConnection) urls.openConnection();
            conn.setReadTimeout(15000); //milliseconds
            conn.setConnectTimeout(15000); // milliseconds
            conn.setRequestMethod("GET");

            conn.connect();

            if (conn.getResponseCode() == HttpURLConnection.HTTP_OK) {

                BufferedReader reader = new BufferedReader(new InputStreamReader(
                        conn.getInputStream(), "iso-8859-1"), 8);
                StringBuilder sb = new StringBuilder();
                String line = null;
                while ((line = reader.readLine()) != null) {
                    sb.append(line + "\n");
                }
                result = sb.toString();
            } else {

                return "error";
            }


        } catch (Exception e) {
            // System.out.println("exception in jsonparser class ........");
            e.printStackTrace();
            return "error";
        }

        return result;
    } // method ends



    @Override
    public void onClick(View v) {
        switch (v.getId()) {
            case R.id.btn_email:
                //  startActivity(new Intent(this, MultipleLoginSelectionActivity.class));
                logOut();
                attemptSignIn();
                break;
            case R.id.btn_mobile_no:
                // startActivity(new Intent(this, MultipleLoginSelectionActivity.class));
                logOut();
                attemptSignIn();
                break;

            case R.id.btn_login_google:
                //  startActivity(new Intent(this, MultipleLoginSelectionActivity.class));
                logOut();
                attemptSignIn();
                break;
        }
    }

    public void logOut(){
        try {
            mGoogleSignInClient.signOut();

            try {
                GoogleSignInOptions gso = new GoogleSignInOptions.Builder(GoogleSignInOptions.DEFAULT_SIGN_IN)
                        .requestEmail()
                        .build();

                mGoogleSignInClient = GoogleSignIn.getClient(this, gso);
            }catch (Exception e){
                e.printStackTrace();
            }

        }catch (Exception exception){
            exception.printStackTrace();
        }
    }

    /*   @Override
       public boolean onCreateOptionsMenu(Menu menu) {
           // Inflate the menu; this adds items to the action bar if it is present.
           getMenuInflater().inflate(R.menu.menu_cast, menu);
           mediaRouteMenuItem = CastButtonFactory.setUpMediaRouteButton(getApplicationContext(), menu,
                   R.id.media_route_menu_item);
           return true;
       }
   */
    // show google cast overlay animation when first time activity open
    private void showIntroductoryOverlay() {
        if (mIntroductoryOverlay != null) {
            mIntroductoryOverlay.remove();
        }
        if ((mediaRouteMenuItem != null) && mediaRouteMenuItem.isVisible()) {
            new Handler().post(new Runnable() {
                @Override
                public void run() {
                    mIntroductoryOverlay = new IntroductoryOverlay.Builder(
                            MultipleLoginSelectionActivity.this, mediaRouteMenuItem)
                            .setTitleText("Introducing Cast")
                            .setSingleTime()
                            .setOnOverlayDismissedListener(
                                    new IntroductoryOverlay.OnOverlayDismissedListener() {
                                        @Override
                                        public void onOverlayDismissed() {
                                            mIntroductoryOverlay = null;
                                        }
                                    })
                            .build();
                    mIntroductoryOverlay.show();
                }
            });
        }
    }

    @Override
    protected void onPause() {
        //  mCastContext.removeCastStateListener(mCastStateListener);
        super.onPause();
    }

    @Override
    protected void onResume() {
        //  mCastContext.addCastStateListener(mCastStateListener);
        super.onResume();
    }


}
