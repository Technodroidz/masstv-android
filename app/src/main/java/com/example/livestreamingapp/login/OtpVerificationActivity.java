package com.example.livestreamingapp.login;

import android.app.ProgressDialog;
import android.content.Context;
import android.content.DialogInterface;
import android.content.Intent;
import android.os.Bundle;
import android.text.Editable;
import android.text.TextWatcher;
import android.util.Log;
import android.view.KeyEvent;
import android.view.MenuItem;
import android.view.View;
import android.view.inputmethod.InputMethodManager;
import android.widget.Button;
import android.widget.EditText;
import android.widget.TextView;
import android.widget.Toast;

import androidx.annotation.NonNull;
import androidx.appcompat.app.AlertDialog;
import androidx.appcompat.app.AppCompatActivity;
import androidx.appcompat.widget.Toolbar;

import com.example.livestreamingapp.MassTvActivity;
import com.example.livestreamingapp.R;
import com.example.livestreamingapp.model.APIError;
import com.example.livestreamingapp.model.LoginResponse;
import com.example.livestreamingapp.utils.AppEditText;
import com.example.livestreamingapp.utils.Constants;
import com.example.livestreamingapp.utils.CustomProgressDialog;
import com.example.livestreamingapp.utils.ErrorUtils;
import com.example.livestreamingapp.utils.SharedPreferenceHandler;
import com.example.livestreamingapp.home.ui.HomeDraweerStreamingActivity;
import com.example.livestreamingapp.webservice.APIClient;
import com.example.livestreamingapp.webservice.APIInterface;
import com.example.livestreamingapp.webservice.FcmService;
import com.google.android.gms.cast.framework.CastContext;
import com.google.android.gms.tasks.OnCompleteListener;
import com.google.android.gms.tasks.Task;
import com.google.firebase.messaging.FirebaseMessaging;
import com.google.gson.Gson;

import java.util.HashMap;
import java.util.Map;

import in.aabhasjindal.otptextview.OtpTextView;
import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;

public class OtpVerificationActivity extends MassTvActivity implements  View.OnClickListener,
        TextWatcher, View.OnFocusChangeListener, View.OnKeyListener {
    private MenuItem mediaRouteMenuItem;
    private Toolbar mToolbar;
    private CastContext mCastContext;
    TextView  tv_label_title;
    private OtpTextView otp_view;

    private AppEditText mPinFirstDigitEditText;
    private AppEditText mPinSecondDigitEditText;
    private AppEditText mPinThirdDigitEditText;
    private AppEditText mPinForthDigitEditText;
    private AppEditText mPinHiddenEditText;
    private Button btn_otp_verify;

    private APIInterface apiInterface;
    private CustomProgressDialog progressDialog;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.layout_otp_verificaton);
        setupActionBar();
        progressDialog = new CustomProgressDialog(OtpVerificationActivity.this);
        progressDialog.setCancelable(false);

        apiInterface = APIClient.getClient().create(APIInterface.class);
        initViews();


    }

    // here all views are initialized
    private void initViews() {

        tv_label_title = findViewById(R.id.tv_label_title);
        btn_otp_verify = findViewById(R.id.btn_otp_verify);

        mPinFirstDigitEditText  = findViewById(R.id.edt_one);
        mPinSecondDigitEditText = findViewById(R.id.edt_two);
        mPinThirdDigitEditText  = findViewById(R.id.edt_three);
        mPinForthDigitEditText  = findViewById(R.id.edt_four);
        mPinHiddenEditText      = findViewById(R.id.pin_hidden_edittext);

        // mPinFirstDigitEditText.requestFocus();


        /* showSoftKeyboard(mPinSecondDigitEditText);
         */
        setPINListeners();


    }

    private void setupActionBar() {
        mToolbar = (Toolbar) findViewById(R.id.toolbar);
        mToolbar.setNavigationIcon(R.drawable.ic_backs_white_24);
        mToolbar.setTitle("");
        setSupportActionBar(mToolbar);
        if(getSupportActionBar()!=null){
            getSupportActionBar().setDisplayHomeAsUpEnabled(true);
        }

    }

    @Override
    public void onClick(View v) {
        switch (v.getId()){
            case R.id.btn_otp_verify:
                if(!mPinHiddenEditText.getText().toString().isEmpty() && mPinHiddenEditText.getText().toString().length()==4){
                    verifyOtp(mPinHiddenEditText.getText().toString());
                }
                break;

        }
    }


/*    @Override
    public boolean onCreateOptionsMenu(Menu menu) {
        // Inflate the menu; this adds items to the action bar if it is present.
        getMenuInflater().inflate(R.menu.menu_cast, menu);
        mediaRouteMenuItem = CastButtonFactory.setUpMediaRouteButton(getApplicationContext(), menu,
                R.id.media_route_menu_item);
        return true;
    }*/

    @Override
    public boolean onOptionsItemSelected(@NonNull MenuItem item) {

        switch (item.getItemId()) {
            case android.R.id.home:
                // todo: goto back activity from here
                finish();
                return true;


            default:
                return super.onOptionsItemSelected(item);
        }
    }


    @Override
    public void beforeTextChanged(CharSequence s, int start, int count, int after) {

    }

    @Override
    public void onTextChanged(CharSequence s, int start, int before, int count) {

        if (s.length() == 0) {
            mPinFirstDigitEditText.setText("");
        } else if (s.length() == 1) {
            ChangeBackground(mPinFirstDigitEditText, 1);
            mPinFirstDigitEditText.setText(s.charAt(0) + "");
            //   mPinSecondDigitEditText.requestFocus();
            mPinSecondDigitEditText.setText("");
            mPinThirdDigitEditText.setText("");
            mPinForthDigitEditText.setText("");
        } else if (s.length() == 2) {
            ChangeBackground(mPinSecondDigitEditText, 1);
            mPinSecondDigitEditText.setText(s.charAt(1) + "");
            //    mPinThirdDigitEditText.requestFocus();
            mPinThirdDigitEditText.setText("");
            mPinForthDigitEditText.setText("");
        } else if (s.length() == 3) {
            ChangeBackground(mPinThirdDigitEditText, 1);
            //      mPinForthDigitEditText.requestFocus();
            mPinThirdDigitEditText.setText(s.charAt(2) + "");
            mPinForthDigitEditText.setText("");
        } else if (s.length() == 4) {
            ChangeBackground(mPinForthDigitEditText, 1);
            mPinForthDigitEditText.setText(s.charAt(3) + "");
            hideSoftKeyboard(mPinForthDigitEditText);
            btn_otp_verify.requestFocus();

        }
    }

    @Override
    public void afterTextChanged(Editable s) {

    }

    public static void setFocus(EditText editText) {
        if (editText == null)
            return;

        editText.setFocusable(true);
        editText.setFocusableInTouchMode(true);
        editText.requestFocus();
    }

    //this method for all edit boxes click and focus
    @Override
    public void onFocusChange(View view, boolean hasFocus) {
        final int id = view.getId();

        switch (id) {



            case R.id.edt_one:
                if (hasFocus) {
                    setFocus(mPinHiddenEditText);
                    showSoftKeyboard(mPinHiddenEditText);
                }
                break;

            case R.id.edt_two:
                if (hasFocus) {
                    setFocus(mPinHiddenEditText);
                    showSoftKeyboard(mPinHiddenEditText);
                }
                break;

            case R.id.edt_three:
                if (hasFocus) {
                    setFocus(mPinHiddenEditText);
                    showSoftKeyboard(mPinHiddenEditText);
                }
                break;

            case R.id.edt_four:
                if (hasFocus) {
                    setFocus(mPinHiddenEditText);
                    showSoftKeyboard(mPinHiddenEditText);
                }
                break;
            default:
                break;

        }



    }

    @Override
    public boolean onKey(View v, int keyCode, KeyEvent event) {
        return false;
    }

    //this method for focus listeners on edit boxes onclick and focus
    private void setPINListeners() {
        mPinHiddenEditText.addTextChangedListener(this);
        mPinFirstDigitEditText.setOnFocusChangeListener(this);
        mPinSecondDigitEditText.setOnFocusChangeListener(this);
        mPinThirdDigitEditText.setOnFocusChangeListener(this);
        mPinForthDigitEditText.setOnFocusChangeListener(this);
        mPinFirstDigitEditText.setOnKeyListener(this);
        mPinSecondDigitEditText.setOnKeyListener(this);
        mPinThirdDigitEditText.setOnKeyListener(this);
        mPinForthDigitEditText.setOnKeyListener(this);
        mPinHiddenEditText.setOnKeyListener(this);
        btn_otp_verify.setOnClickListener(this);
    }

    //this method for change edit boxes background color onclick and focus
    public void ChangeBackground(AppEditText ET, int set) {
        if (set == 1)
            ET.setBackgroundResource(R.drawable.line_bottom);
        else if (set == 2)
            ET.setBackgroundResource(R.drawable.line_bottom);
    }

    //this method for hide keyboard for all edit boxes click and focus
    public void hideSoftKeyboard(EditText editText) {
        if (editText == null)
            return;
        InputMethodManager imm = (InputMethodManager)getSystemService(Context.INPUT_METHOD_SERVICE);
        imm.hideSoftInputFromWindow(editText.getWindowToken(), 0);
    }

    //this method for show keyboard for all edit boxes click and focus
    public void showSoftKeyboard(EditText editText) {
        if (editText == null)
            return;

        InputMethodManager imm = (InputMethodManager)getSystemService(Context.INPUT_METHOD_SERVICE);
        imm.showSoftInput(editText, 0);
    }

    /**
     * @Rehan After api hit successfully we will get the LoginResponse object In LoginResponse if
     * status is 1 that means otp successfully send to to the entered email address
    if status is 0 it means something went wrong.
     */
    private void verifyOtp(String otp) {

        Map<String, String> credential = new HashMap<>();
        credential.put(getString(R.string.email), getIntent().getStringExtra(Constants.EMAIL_ID));
        credential.put(getString(R.string.otp), otp);
        Call<LoginResponse> verifyOtpApiCall = apiInterface.verifyOtp(credential);

        progressDialog.show();

        verifyOtpApiCall.enqueue(
                new Callback<LoginResponse>() {
                    @Override
                    public void onResponse(
                            Call<LoginResponse> call, Response<LoginResponse> response) {
                        progressDialog.dismiss();

                        if (response.code() == 200) {

                            if (response.body().getStatus()) {



                                new SharedPreferenceHandler(OtpVerificationActivity.this)
                                        .setPreference(Constants.PROFILE_INFO,new Gson().toJson(response.body().getUser()));

                                try {
                                    initializeFireBaseToken();
                                }catch (Exception e){

                                }

                                startActivity(new Intent(OtpVerificationActivity.this,HomeDraweerStreamingActivity.class)
                                        .setFlags(Intent.FLAG_ACTIVITY_NEW_TASK | Intent.FLAG_ACTIVITY_CLEAR_TASK));

                            } else {
                                new AlertDialog.Builder(OtpVerificationActivity.this,R.style.AlertDialogStyle)
                                        .setCancelable(false)
                                        .setMessage("Invalid Otp!")
                                        .setPositiveButton(
                                                "Ok",
                                                new DialogInterface.OnClickListener() {
                                                    @Override
                                                    public void onClick(
                                                            DialogInterface dialog, int which) {
                                                    }
                                                })
                                        .show();
                            }
                        }else {
                            try {
                                APIError error = ErrorUtils.parseError(response);
                                String errorMessage = error.getData().getError();

                                new AlertDialog.Builder(OtpVerificationActivity.this,R.style.AlertDialogStyle)
                                        .setCancelable(false)
                                        .setMessage(errorMessage)
                                        .setPositiveButton(
                                                "Ok",
                                                new DialogInterface.OnClickListener() {
                                                    @Override
                                                    public void onClick(
                                                            DialogInterface dialog, int which) {}
                                                })
                                        .show();
                            }catch (Exception e){

                            }
                        }
                    }

                    @Override
                    public void onFailure(Call<LoginResponse> call, Throwable t) {
                        progressDialog.dismiss();
                        new AlertDialog.Builder(OtpVerificationActivity.this,R.style.AlertDialogStyle)
                                .setCancelable(false)
                                .setMessage("" + t.getMessage())
                                .setPositiveButton(
                                        "Ok",
                                        new DialogInterface.OnClickListener() {
                                            @Override
                                            public void onClick(
                                                    DialogInterface dialog, int which) {}
                                        })
                                .show();
                    }
                });
    }

    private void initializeFireBaseToken() {
        FirebaseMessaging.getInstance().getToken()
                .addOnCompleteListener(new OnCompleteListener<String>() {
                    @Override
                    public void onComplete(@NonNull Task<String> task) {
                        if (!task.isSuccessful()) {
                            Log.w("FCM ", "Fetching FCM registration token failed", task.getException());
                            return;
                        }
                        // Get new FCM registration token
                        String token = task.getResult();
                        new SharedPreferenceHandler(OtpVerificationActivity.this).saveFcmToken(token);

                    }

                });
    }


}
