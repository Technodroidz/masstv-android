package com.example.livestreamingapp.login;

import androidx.annotation.NonNull;
import androidx.appcompat.app.AppCompatActivity;

import android.annotation.SuppressLint;
import android.app.DialogFragment;
import android.content.Context;
import android.content.Intent;
import android.content.pm.ShortcutInfo;
import android.content.pm.ShortcutManager;
import android.graphics.drawable.Icon;
import android.media.MediaPlayer;
import android.net.Uri;
import android.os.Bundle;
import android.os.Handler;
import android.util.Log;
import android.view.View;
import android.view.WindowManager;
import android.widget.FrameLayout;
import android.widget.VideoView;

import com.example.livestreamingapp.BuildConfig;
import com.example.livestreamingapp.MassTvActivity;
import com.example.livestreamingapp.R;
import com.example.livestreamingapp.ShortCutActivity;
import com.example.livestreamingapp.model.ProfileData;
import com.example.livestreamingapp.model.VideoByIdResponse;
import com.example.livestreamingapp.ondemand.OnDemandCategoryActivity;
import com.example.livestreamingapp.upload.UploadContentActivity;
import com.example.livestreamingapp.utils.Constants;
import com.example.livestreamingapp.utils.CustomProgressDialog;
import com.example.livestreamingapp.utils.SharedPreferenceHandler;
import com.example.livestreamingapp.home.ui.HomeDraweerStreamingActivity;
import com.example.livestreamingapp.video.VideoDetailActivity;
import com.example.livestreamingapp.webservice.APIClient;
import com.example.livestreamingapp.webservice.APIInterface;
import com.google.android.gms.tasks.OnFailureListener;
import com.google.android.gms.tasks.OnSuccessListener;
import com.google.firebase.dynamiclinks.FirebaseDynamicLinks;
import com.google.firebase.dynamiclinks.PendingDynamicLinkData;
import com.google.gson.Gson;

import java.io.File;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;

public class SplashActivity extends MassTvActivity {

    VideoView videoView;
    FrameLayout fl_frame;
    public boolean complete;
    public Intent intentCreateContent;
    public Intent intentOnDemand;
    public Intent intentLive;
    private ProfileData profileData;
    private CustomProgressDialog customProgressDialog;
    private APIInterface apiInterface;
    private MediaPlayer mediaPlayer;

    @SuppressLint("WrongThread")
    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_splash);
        getWindow().addFlags(WindowManager.LayoutParams.FLAG_FULLSCREEN);
        customProgressDialog = new CustomProgressDialog(SplashActivity.this);
        customProgressDialog.setMessage("Please wait...");
        customProgressDialog.setCancelable(false);
        apiInterface = APIClient.getClient().create(APIInterface.class);



        profileData = new SharedPreferenceHandler(SplashActivity.this).getProfileInfo();

        intentCreateContent = new Intent(Intent.ACTION_VIEW, null, this, ShortCutActivity.class).putExtra("value", 0);
        intentOnDemand = new Intent(Intent.ACTION_VIEW, null, this, ShortCutActivity.class).putExtra("value", 1);
        intentLive = new Intent(Intent.ACTION_VIEW, null, this, ShortCutActivity.class).putExtra("value", 2);

        Intent shareIntent = new Intent(Intent.ACTION_SEND);
        try {
            shareIntent.setType("text/plain");
            shareIntent.putExtra(Intent.EXTRA_SUBJECT, "My application name");
            String shareMessage = "\nLet me recommend you this application\n\n";
            shareMessage = shareMessage + "https://play.google.com/store/apps/details?id=" + BuildConfig.APPLICATION_ID + "\n\n";
            shareIntent.putExtra(Intent.EXTRA_TEXT, shareMessage);
            //  startActivity(Intent.createChooser(shareIntent, "choose one"));
        } catch (Exception e) {
            //e.toString();
        }

         /* if(profileData==null){
               intentCreateContent = new Intent(Intent.ACTION_VIEW, null, this, LoginWithEmailActivity.class);
               intentOnDemand = new Intent(Intent.ACTION_VIEW, null, this, LoginWithEmailActivity.class);
               intentLive = new Intent(Intent.ACTION_VIEW, null, this, LoginWithEmailActivity.class);
          }else {
              intentCreateContent = new Intent(Intent.ACTION_VIEW, null, this, UploadContentActivity.class);
              intentOnDemand = new Intent(Intent.ACTION_VIEW, null, this, OnDemandCategoryActivity.class);
              intentLive = new Intent(Intent.ACTION_VIEW, null, this, HomeDraweerStreamingActivity.class);
          }*/

        List<ShortcutInfo> shortcutInfoList = new ArrayList<>();
        if (android.os.Build.VERSION.SDK_INT >= android.os.Build.VERSION_CODES.N_MR1) {
            ShortcutManager shortcutManager = getSystemService(ShortcutManager.class);
            ShortcutInfo shortcutContent = new ShortcutInfo.Builder(this, "id1")
                    .setShortLabel("Create Content") // Shortcut Icon tab
                    .setLongLabel("Create Content") // Displayed When Long Pressing On App Icon
                    .setIcon(Icon.createWithResource(SplashActivity.this, R.drawable.ic_create))
                    .setIntent(intentCreateContent)
                    .build();
            shortcutInfoList.add(0, shortcutContent);
            ShortcutInfo shortcutOnDemand = new ShortcutInfo.Builder(this, "id2")
                    .setShortLabel("Watch On Demand") // Shortcut Icon tab
                    .setLongLabel("Watch On Demand") // Displayed When Long Pressing On App Icon
                    .setIcon(Icon.createWithResource(SplashActivity.this, R.drawable.ic_demand))
                    .setIntent(intentOnDemand)
                    .build();
            shortcutInfoList.add(1, shortcutOnDemand);
            ShortcutInfo shortcutWatchLive = new ShortcutInfo.Builder(this, "id3")
                    .setShortLabel("Watch Live") // Shortcut Icon tab
                    .setLongLabel("Watch Live") // Displayed When Long Pressing On App Icon
                    .setIcon(Icon.createWithResource(SplashActivity.this, R.drawable.ic_live))
                    .setIntent(intentLive)
                    .build();
            shortcutInfoList.add(2, shortcutWatchLive);

            ShortcutInfo shortcutShare = new ShortcutInfo.Builder(this, "id4")
                    .setShortLabel("Share") // Shortcut Icon tab
                    .setLongLabel("Share") // Displayed When Long Pressing On App Icon
                    .setIcon(Icon.createWithResource(SplashActivity.this, R.drawable.ic_share))
                    .setIntent(shareIntent)
                    .build();
            shortcutInfoList.add(3, shortcutShare);
            shortcutManager.setDynamicShortcuts(shortcutInfoList);

            try {
                FirebaseDynamicLinks.getInstance()
                        .getDynamicLink(getIntent())
                        .addOnSuccessListener(this, new OnSuccessListener<PendingDynamicLinkData>() {
                            @Override
                            public void onSuccess(PendingDynamicLinkData pendingDynamicLinkData) {
                                // Get deep link from result (may be null if no link is found)
                                Uri deepLink = null;
                                if (pendingDynamicLinkData != null) {
                                    deepLink = pendingDynamicLinkData.getLink();
                                    Log.e("check deep link", "" + deepLink);
                          /*  Log.e("check deep link ","receive"+ deepLink.toString());
                            Log.e("check deep link ","receive"+ new Gson().toJson(deepLink.getQuery()));*/
                                    new SharedPreferenceHandler(SplashActivity.this).saveDeepLink(deepLink.toString());
                                }
                            }
                        })
                        .addOnFailureListener(this, new OnFailureListener() {
                            @Override
                            public void onFailure(@NonNull Exception e) {
                                Log.e("", "getDynamicLink:onFailure", e);
                            }
                        });
            } catch (Exception e) {
                e.printStackTrace();
            }


        }


        videoView = findViewById(R.id.videoView);
        fl_frame = findViewById(R.id.fl_frame);

        String path = "android.resource://" + getPackageName() + "/raw/splash_mobile";
        videoView.setVideoURI(Uri.parse(path));

        videoView.setOnCompletionListener(new MediaPlayer.OnCompletionListener() {
            @Override
            public void onCompletion(MediaPlayer mp) {

                complete = true;
                if (complete) {


                  /*  if(profileData==null|| profileData.getEmail()==null){
                        startActivity(new Intent(SplashActivity.this,MultipleLoginSelectionActivity.class));
                        finish();
                    }else {
                        startActivity(new Intent(SplashActivity.this, HomeDraweerStreamingActivity.class));
                        finish();
                    }*/
                    loginUser();
                }
            }
        });

        videoView.setOnPreparedListener(new MediaPlayer.OnPreparedListener() {




            @Override
            public void onPrepared(MediaPlayer mp) {
                fl_frame.setVisibility(View.GONE);
                mediaPlayer = mp;
                mediaPlayer.setVolume(0f,0f);
                complete = true;
                new Handler().postDelayed(new Runnable() {
                    @Override
                    public void run() {
                        fl_frame.setVisibility(View.VISIBLE);
                    }
                }, 800);

            }
        });

        fl_frame.setVisibility(View.VISIBLE);

        videoView.start();

        clearVideoCache(SplashActivity.this);


    }

    private void openVideoUsingReferLink(String videoId) {
    }

    public static void clearVideoCache(Context context) {
        try {
            File dir = new File(context.getCacheDir(), "media/");
            deleteDir(dir);
        } catch (Exception e) {
            e.printStackTrace();
        }
    }

    public static boolean deleteDir(File dir) {
        if (dir != null && dir.isDirectory()) {
            String[] children = dir.list();
            for (String child : children) {
                boolean success = deleteDir(new File(dir, child));
                if (!success) {
                    return false;
                }
            }
            return dir.delete();
        } else if (dir != null && dir.isFile()) {
            return dir.delete();
        } else {
            return false;
        }
    }


    @Override
    protected void onResume() {
        super.onResume();
        if (complete) {
           /* ProfileData profileData = new SharedPreferenceHandler(SplashActivity.this).getProfileInfo();
            if (profileData == null || profileData.getEmail() == null) {
                startActivity(new Intent(SplashActivity.this, MultipleLoginSelectionActivity.class));
                finish();
            } else {
                startActivity(new Intent(SplashActivity.this, HomeDraweerStreamingActivity.class));
                finish();
            }*/
            loginUser();
        }
    }

    @Override
    public void onVolumeButtonPress(int currentVolume, int maxVolume) {
        if(mediaPlayer!=null){
            float volume = ((currentVolume * 100)/maxVolume);
            volume = (float) (volume*0.01);
            mediaPlayer.setVolume(volume,volume);
        }

    }
}