package com.example.livestreamingapp.model;

import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;

import java.lang.reflect.Array;
import java.util.ArrayList;
import java.util.List;

public class APIError {

    private int statusCode;
    private String message;
    private Data data;

    public Data getData() {
        return data;
    }

    public void setData(Data data) {
        this.data = data;
    }

    public class Data {

        private List<String> phone_number;

        private List<String> sources;

        @SerializedName("error")
        @Expose
        private String error;


        public List<String> getPhone_number() {
            return phone_number;
        }

        public void setPhone_number(List<String> phone_number) {
            this.phone_number = phone_number;
        }

        public String getError() {
            return error;
        }

        public void setError(String error) {
            this.error = error;
        }
    }

    public APIError() {
    }

    public int status() {
        return statusCode;
    }

    public String message() {
        return message;
    }
}