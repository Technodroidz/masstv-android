package com.example.livestreamingapp.model;

import com.example.livestreamingapp.model.homescreen.CategoryModel;
import com.example.livestreamingapp.model.homescreen.PromotionBanner;
import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;

import java.util.List;

public class CategoryData {
    @SerializedName("category_list")
    @Expose
    private List<OnDemandCategory> category_list;

    @SerializedName("promotion_banners")
    @Expose
    private List<PromotionBanner> promotion_list;


    public List<PromotionBanner> getPromotion_list() {
        return promotion_list;
    }

    public void setPromotion_list(List<PromotionBanner> promotion_list) {
        this.promotion_list = promotion_list;
    }

    public List<OnDemandCategory> getCategory_list() {
        return category_list;
    }

    public void setCategory_list(List<OnDemandCategory> category_list) {
        this.category_list = category_list;
    }
}
