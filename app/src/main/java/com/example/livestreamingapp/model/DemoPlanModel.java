package com.example.livestreamingapp.model;

import java.io.Serializable;

public class DemoPlanModel implements Serializable {

    private String planType;
    private String planCost;

    public String getPlanCost() {
        return planCost;
    }

    public void setPlanCost(String planCost) {
        this.planCost = planCost;
    }

    public String getPlanType() {
        return planType;
    }

    public void setPlanType(String planType) {
        this.planType = planType;
    }
}
