
package com.example.livestreamingapp.model;

import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;

import java.util.List;

public class LiveShowData {

    @SerializedName("id")
    @Expose
    private Integer id;
    @SerializedName("user_id")
    @Expose
    private Integer userId;
    @SerializedName("title")
    @Expose
    private String title;
    @SerializedName("source")
    @Expose
    private String source;

    @SerializedName("source_ad")
    @Expose
    private String source_ad;

    @SerializedName("srt")
    @Expose
    private String srt;

    @SerializedName("subtitle")
    @Expose
    private String subtitle;
    @SerializedName("thumb")
    @Expose
    private String thumb;
    @SerializedName("schedule_time")
    @Expose
    private String schedule_time;

    @SerializedName("schedule_date")
    @Expose
    private String schedule_date;
    @SerializedName("duration")
    @Expose
    private String duration;

    @SerializedName("video_type")
    @Expose
    private String video_type;

    @SerializedName("advideos_list")
    @Expose
    private List<LiveShowData> nextVideoDetail;

    private String category;

    private long playBackTime;

    public Integer getId() {
        return id;
    }

    public void setId(Integer id) {
        this.id = id;
    }

    public Integer getUserId() {
        return userId;
    }

    public void setUserId(Integer userId) {
        this.userId = userId;
    }

    public String getTitle() {
        return title;
    }

    public void setTitle(String title) {
        this.title = title;
    }

    public String getSource() {
        return source;
    }

    public void setSource(String source) {

        this.source = source;
    }

    public String getSubtitle() {
        return subtitle;
    }

    public void setSubtitle(String subtitle) {
        this.subtitle = subtitle;
    }

    public String getThumb() {
        return thumb;
    }

    public void setThumb(String thumb) {
        this.thumb = thumb;
    }

    public String getCategory() {
        return category;
    }

    public void setCategory(String category) {
        this.category = category;
    }

    public String getSchedule_time() {
        return schedule_time;
    }

    public void setSchedule_time(String schedule_time) {
        this.schedule_time = schedule_time;
    }

    public String getDuration() {
        return duration;
    }

    public void setDuration(String duration) {
        this.duration = duration;
    }

    public String getSource_ad() {
        return source_ad;
    }

    public void setSource_ad(String source_ad) {

        this.source_ad = source_ad;
    }

    public String getSchedule_date() {
        return schedule_date;
    }

    public void setSchedule_date(String schedule_date) {
        this.schedule_date = schedule_date;
    }

    public String getVideo_type() {
        return video_type;
    }

    public void setVideo_type(String video_type) {
        this.video_type = video_type;
    }

    public List<LiveShowData> getNextVideoDetail() {
        return nextVideoDetail;
    }

    public void setNextVideoDetail(List<LiveShowData> nextVideoDetail) {
        this.nextVideoDetail = nextVideoDetail;
    }

    public String getSrt() {
        return srt;
    }

    public void setSrt(String srt) {
        this.srt = srt;
    }

    public long getPlayBackTime() {
        return playBackTime;
    }

    public void setPlayBackTime(long playBackTime) {
        this.playBackTime = playBackTime;
    }
}
