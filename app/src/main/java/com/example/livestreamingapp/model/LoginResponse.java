package com.example.livestreamingapp.model;

import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;

public class LoginResponse {

    @SerializedName("data")
    @Expose
    private ProfileData user;

    @SerializedName("success")
    @Expose
    private boolean success;

    public ProfileData getUser() {
        return user;
    }

    public void setData(ProfileData user) {
        this.user = user;
    }

    public boolean getStatus() {
        return success;
    }

    public void setStatus(boolean status) {
        this.success = status;
    }
}
