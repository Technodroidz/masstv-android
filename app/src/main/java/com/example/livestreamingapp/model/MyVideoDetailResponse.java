package com.example.livestreamingapp.model;

import java.util.List;
import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;

public class MyVideoDetailResponse {

    @SerializedName("success")
    @Expose
    private Boolean success;
    @SerializedName("data")
    @Expose
    private List<MyVideoDetails> data = null;
    @SerializedName("message")
    @Expose
    private String message;

    public Boolean getSuccess() {
        return success;
    }

    public void setSuccess(Boolean success) {
        this.success = success;
    }

    public List<MyVideoDetails> getData() {
        return data;
    }

    public void setData(List<MyVideoDetails> data) {
        this.data = data;
    }

    public String getMessage() {
        return message;
    }

    public void setMessage(String message) {
        this.message = message;
    }

    public class MyVideoDetails {

        @SerializedName("id")
        @Expose
        private Integer id;
        @SerializedName("user_id")
        @Expose
        private Integer userId;
        @SerializedName("title")
        @Expose
        private String title;
        @SerializedName("source")
        @Expose
        private String source;
        @SerializedName("subtitle")
        @Expose
        private String subtitle;
        @SerializedName("thumb")
        @Expose
        private String thumb;
        @SerializedName("category")
        @Expose
        private String category;
        @SerializedName("admin_approved")
        @Expose
        private Integer adminApproved;
        @SerializedName("earning")
        @Expose
        private String earning;

        @SerializedName("avg_rate")
        @Expose
        private String avg_rate;

        @SerializedName("is_rated")
        @Expose
        private boolean is_rated;

        @SerializedName("inactive")
        @Expose
        private boolean isInActive;

        @SerializedName("total_user_rated")
        @Expose
        private String total_user_rated;

        public Integer getId() {
            return id;
        }

        public void setId(Integer id) {
            this.id = id;
        }

        public Integer getUserId() {
            return userId;
        }

        public void setUserId(Integer userId) {
            this.userId = userId;
        }

        public String getTitle() {
            return title;
        }

        public void setTitle(String title) {
            this.title = title;
        }

        public String getSource() {
            return source;
        }

        public void setSource(String source) {
            this.source = source;
        }

        public String getSubtitle() {
            return subtitle;
        }

        public void setSubtitle(String subtitle) {
            this.subtitle = subtitle;
        }

        public String getThumb() {
            return thumb;
        }

        public void setThumb(String thumb) {
            this.thumb = thumb;
        }

        public String getCategory() {
            return category;
        }

        public void setCategory(String category) {
            this.category = category;
        }

        public Integer getAdminApproved() {
            return adminApproved;
        }

        public void setAdminApproved(Integer adminApproved) {
            this.adminApproved = adminApproved;
        }

        public String getEarning() {
            return earning;
        }

        public void setEarning(String earning) {
            this.earning = earning;
        }

        public String getAvg_rate() {
            return avg_rate;
        }

        public void setAvg_rate(String avg_rate) {
            this.avg_rate = avg_rate;
        }

        public boolean isIs_rated() {
            return is_rated;
        }

        public void setIs_rated(boolean is_rated) {
            this.is_rated = is_rated;
        }


        public boolean isInActive() {
            return isInActive;
        }

        public void setInActive(boolean inActive) {
            isInActive = inActive;
        }

        public String getTotal_user_rated() {
            return total_user_rated;
        }

        public void setTotal_user_rated(String total_user_rated) {
            this.total_user_rated = total_user_rated;
        }
    }
}