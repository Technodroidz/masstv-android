package com.example.livestreamingapp.model;

import com.example.livestreamingapp.utils.NotificationMessage;
import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;

import java.util.List;

public class NotificationResponse {

    @SerializedName("data")
    @Expose
    private List<NotificationMessage> notificationMessage;

    @SerializedName("success")
    @Expose
    private boolean success;



    public boolean getStatus() {
        return success;
    }

    public void setStatus(boolean status) {
        this.success = status;
    }

    public List<NotificationMessage> getNotificationMessage() {
        return notificationMessage;
    }

    public void setNotificationMessage(List<NotificationMessage> notificationMessage) {
        this.notificationMessage = notificationMessage;
    }
}
