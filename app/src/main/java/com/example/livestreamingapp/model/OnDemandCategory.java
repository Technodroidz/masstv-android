package com.example.livestreamingapp.model;

import androidx.annotation.NonNull;

import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;

public class OnDemandCategory implements Comparable<OnDemandCategory>{

    @SerializedName("id")
    @Expose
    private Integer id;

    @SerializedName("title")
    @Expose
    private String title;

    @SerializedName("subtitle")
    @Expose
    private String subtitle;

    @SerializedName("name")
    @Expose
    private String name;

    @SerializedName("banner_image")
    @Expose
    private String banner;



    public Integer getId() {
        return id;
    }

    public void setId(Integer id) {
        this.id = id;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public String getBanner() {
        return banner;
    }

    public void setBanner(String banner) {
        this.banner = banner;
    }

    public String getTitle() {
        return title;
    }

    public void setTitle(String title) {
        this.title = title;
    }


    public String getSubtitle() {
        return subtitle;
    }

    public void setSubtitle(String subtitle) {
        this.subtitle = subtitle;
    }

   /* @NonNull
    @Override
    public String toString() {
       return name;
      //  return "name [id=" + id + "]";
    }*/

    @Override
    public String toString() {
        return "OnDemandCategory [id=" + id + "]";
    }


    @Override
    public int compareTo(OnDemandCategory o) {
        return this.getId().compareTo(o.getId());
    }
}