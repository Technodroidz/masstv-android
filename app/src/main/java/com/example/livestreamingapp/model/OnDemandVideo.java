
package com.example.livestreamingapp.model;

import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;

import java.io.Serializable;

public class OnDemandVideo implements Serializable {

    @SerializedName("duration")
    @Expose
    private String duration;

    @SerializedName("id")
    @Expose
    private Integer id;
    @SerializedName("user_id")
    @Expose
    private Integer userId;
    @SerializedName("title")
    @Expose
    private String title;
    @SerializedName("source")
    @Expose
    private String source;

    @SerializedName("source_ad")
    @Expose
    private String source_ad;

    @SerializedName("srt")
    @Expose
    private String srt;

    @SerializedName("subtitle")
    @Expose
    private String subtitle;
    @SerializedName("thumb")
    @Expose
    private String thumb;
    @SerializedName("category")
    @Expose
    private String category;

    @SerializedName("avg_rate")
    @Expose
    private double avg_rate;

    @SerializedName("user_rating")
    @Expose
    private float my_rating;

    @SerializedName("is_watch_later")
    @Expose
    private boolean isWatchList;

    @SerializedName("is_rated")
    @Expose
    private boolean isRated;

    @SerializedName("playback_time")
    @Expose
    private String playback_time;

    @SerializedName("season")
    @Expose
    private String season;

    @SerializedName("trailer_video")
    @Expose
    private String trailer_video;


    @SerializedName("total_user_rated")
    @Expose
    private String total_user_rated;

    @SerializedName("source_url")
    @Expose
    private String source_url;

    @SerializedName("srt_url")
    @Expose
    private String srt_url;

    public Integer getId() {
        return id;
    }

    public void setId(Integer id) {
        this.id = id;
    }

    public Integer getUserId() {
        return userId;
    }

    public void setUserId(Integer userId) {
        this.userId = userId;
    }

    public String getTitle() {
        return title;
    }

    public void setTitle(String title) {
        this.title = title;
    }

    public String getSource() {
        return source;
    }

    public void setSource(String source) {
        this.source = source;
    }

    public String getSubtitle() {
        return subtitle;
    }

    public void setSubtitle(String subtitle) {
        this.subtitle = subtitle;
    }

    public String getThumb() {
        return thumb;
    }

    public void setThumb(String thumb) {
        this.thumb = thumb;
    }

    public String getCategory() {
        return category;
    }

    public void setCategory(String category) {
        this.category = category;
    }

    public String getSource_ad() {
        return source_ad;
    }

    public void setSource_ad(String source_ad) {
        this.source_ad = source_ad;
    }

    public boolean isWatchList() {
        return isWatchList;
    }

    public void setWatchList(boolean watchList) {
        isWatchList = watchList;
    }

    public boolean isRated() {
        return isRated;
    }

    public void setRated(boolean rated) {
        isRated = rated;
    }

    public double getAvg_rate() {
        return avg_rate;
    }

    public void setAvg_rate(double avg_rate) {
        this.avg_rate = avg_rate;
    }

    public float getMy_rating() {
        return my_rating;
    }

    public void setMy_rating(float my_rating) {
        this.my_rating = my_rating;
    }

    public String getSrt() {
        return srt;
    }

    public void setSrt(String srt) {
        this.srt = srt;
    }

    public String getPlayback_time() {
        return playback_time;
    }

    public void setPlayback_time(String playback_time) {
        this.playback_time = playback_time;
    }

    public String getTotal_user_rated() {
        return total_user_rated;
    }

    public void setTotal_user_rated(String total_user_rated) {
        this.total_user_rated = total_user_rated;
    }

    public String getDuration() {
        return duration;
    }

    public void setDuration(String duration) {
        this.duration = duration;
    }

    public String getSeason() {
        return season;
    }

    public void setSeason(String season) {
        this.season = season;
    }

    public String getTrailer_video() {
        return trailer_video;
    }

    public void setTrailer_video(String trailer_video) {
        this.trailer_video = trailer_video;
    }

    public String getSrt_url() {
        return srt_url;
    }

    public void setSrt_url(String srt_url) {
        this.srt_url = srt_url;
    }

    public String getSource_url() {
        return source_url;
    }

    public void setSource_url(String source_url) {
        this.source_url = source_url;
    }
}
