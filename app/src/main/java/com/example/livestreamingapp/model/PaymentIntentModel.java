
package com.example.livestreamingapp.model;

import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;

public class PaymentIntentModel {

    @SerializedName("success")
    @Expose
    private Boolean success;
    @SerializedName("data")
    @Expose
    private PaymentIntentData paymentIntentData;
    @SerializedName("message")
    @Expose
    private String message;

    public Boolean getSuccess() {
        return success;
    }

    public void setSuccess(Boolean success) {
        this.success = success;
    }



    public String getMessage() {
        return message;
    }

    public void setMessage(String message) {
        this.message = message;
    }

    public PaymentIntentData getPaymentIntentData() {
        return paymentIntentData;
    }

    public void setPaymentIntentData(PaymentIntentData paymentIntentData) {
        this.paymentIntentData = paymentIntentData;
    }

    public class PaymentIntentData {
       @SerializedName("client_secret")
       @Expose
        private String client_secret;

       public String getClient_secret() {
           return client_secret;
       }

       public void setClient_secret(String client_secret) {
           this.client_secret = client_secret;
       }
   }
}
