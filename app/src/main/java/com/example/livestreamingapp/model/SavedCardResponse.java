package com.example.livestreamingapp.model;

import com.example.livestreamingapp.model.subscription.PaymentMethod;
import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;

public class SavedCardResponse {
    @SerializedName("success")
    @Expose
    private Boolean success;
    @SerializedName("data")
    @Expose
    private PaymentMethod paymentMethod;
    @SerializedName("message")
    @Expose
    private String message;

    public Boolean getSuccess() {
        return success;
    }

    public void setSuccess(Boolean success) {
        this.success = success;
    }

    public PaymentMethod getPayment() {
        return paymentMethod;
    }

    public void setPayment(PaymentMethod payment) {
        this.paymentMethod = payment;
    }

    public String getMessage() {
        return message;
    }

    public void setMessage(String message) {
        this.message = message;
    }
}
