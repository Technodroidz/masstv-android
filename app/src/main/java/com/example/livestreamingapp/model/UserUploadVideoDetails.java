package com.example.livestreamingapp.model;

import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;

public class UserUploadVideoDetails {

@SerializedName("success")
@Expose
private Boolean success;
@SerializedName("data")
@Expose
private Data data;
@SerializedName("message")
@Expose
private String message;

public Boolean getSuccess() {
return success;
}

public void setSuccess(Boolean success) {
this.success = success;
}

public Data getData() {
return data;
}

public void setData(Data data) {
this.data = data;
}

public String getMessage() {
return message;
}

public void setMessage(String message) {
this.message = message;
}

    public class Data {

        @SerializedName("totalvideos")
        @Expose
        private Integer totalvideos;
        @SerializedName("totalapprovedvideos")
        @Expose
        private Integer totalapprovedvideos;
        @SerializedName("earning")
        @Expose
        private String earning;

        public Integer getTotalvideos() {
            return totalvideos;
        }

        public void setTotalvideos(Integer totalvideos) {
            this.totalvideos = totalvideos;
        }

        public Integer getTotalapprovedvideos() {
            return totalapprovedvideos;
        }

        public void setTotalapprovedvideos(Integer totalapprovedvideos) {
            this.totalapprovedvideos = totalapprovedvideos;
        }

        public String getEarning() {
            return earning;
        }

        public void setEarning(String earning) {
            this.earning = earning;
        }

    }

}