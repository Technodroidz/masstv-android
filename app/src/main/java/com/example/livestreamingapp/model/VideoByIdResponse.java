package com.example.livestreamingapp.model;

import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;

public class VideoByIdResponse {
    @SerializedName("success")
    @Expose
    private boolean success;

    @SerializedName("message")
    @Expose
    private String message;

    @SerializedName("data")
    @Expose
    private OnDemandVideo onDemandVideo;

    public boolean isSuccess() {
        return success;
    }

    public void setSuccess(boolean success) {
        this.success = success;
    }

    public String getMessage() {
        return message;
    }

    public void setMessage(String message) {
        this.message = message;
    }

    public OnDemandVideo getOnDemandVideo() {
        return onDemandVideo;
    }

    public void setOnDemandVideo(OnDemandVideo onDemandVideo) {
        this.onDemandVideo = onDemandVideo;
    }
}
