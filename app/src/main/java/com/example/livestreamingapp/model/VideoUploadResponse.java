package com.example.livestreamingapp.model;

import com.google.gson.annotations.SerializedName;

public class VideoUploadResponse {
    // variable name should be same as in the json response from php  
    @SerializedName("success")  
    private boolean success;
    @SerializedName("message")
   private String message;
    String getMessage() {  
        return message;  
    }  
    boolean getSuccess() {  
        return success;  
    }

    public void setSuccess(boolean success) {
        this.success = success;
    }

    public void setMessage(String message) {
        this.message = message;
    }
}