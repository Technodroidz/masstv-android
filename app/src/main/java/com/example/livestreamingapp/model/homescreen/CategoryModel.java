package com.example.livestreamingapp.model.homescreen;

import com.example.livestreamingapp.model.OnDemandVideo;
import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;

import java.util.List;

public class CategoryModel {

    @SerializedName("id")
    @Expose
    private Integer id;
    @SerializedName("name")
    @Expose
    private String name;
    @SerializedName("banner_image")
    @Expose
    private String bannerImage;
    @SerializedName("source_ad")
    @Expose
    private String sourceAd;
    @SerializedName("video_list")
    @Expose
    private List<OnDemandVideo> videoList = null;

    public Integer getId() {
        return id;
    }

    public void setId(Integer id) {
        this.id = id;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public String getBannerImage() {
        return bannerImage;
    }

    public void setBannerImage(String bannerImage) {
        this.bannerImage = bannerImage;
    }

    public String getSourceAd() {
        return sourceAd;
    }

    public void setSourceAd(String sourceAd) {
        this.sourceAd = sourceAd;
    }

    public List<OnDemandVideo> getVideoList() {
        return videoList;
    }

    public void setVideoList(List<OnDemandVideo> videoList) {
        this.videoList = videoList;
    }

}