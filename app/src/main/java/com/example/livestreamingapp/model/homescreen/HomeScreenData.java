package com.example.livestreamingapp.model.homescreen;

import com.example.livestreamingapp.model.OnDemandCategory;
import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;

import java.util.List;

public class HomeScreenData {

    @SerializedName("category_list")
    @Expose
    private List<CategoryModel> category_list;

    @SerializedName("new_release")
    @Expose
    private CategoryModel categoryData;


    @SerializedName("promotion_banners")
    @Expose
    private List<PromotionBanner> promotion_list;


    public List<PromotionBanner> getPromotion_list() {
        return promotion_list;
    }

    public void setPromotion_list(List<PromotionBanner> promotion_list) {
        this.promotion_list = promotion_list;
    }

    public List<CategoryModel> getCategory_list() {
        return category_list;
    }

    public void setCategory_list(List<CategoryModel> category_list) {
        this.category_list = category_list;
    }

    public CategoryModel getCategoryData() {
        return categoryData;
    }

    public void setCategoryData(CategoryModel categoryData) {
        this.categoryData = categoryData;
    }
}
