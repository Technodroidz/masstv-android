package com.example.livestreamingapp.model.query;

import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;

public class MyQuery {

    @SerializedName("query_id")
    @Expose
    private String queryId;

    @SerializedName("query")
    @Expose
    private String query;
    @SerializedName("response")
    @Expose
    private String response;

    public String getQuery() {
        return query;
    }

    public void setQuery(String query) {
        this.query = query;
    }

    public String getResponse() {
        return response;
    }

    public void setResponse(String response) {
        this.response = response;
    }

    public String getQueryId() {
        return queryId;
    }

    public void setQueryId(String queryId) {
        this.queryId = queryId;
    }
}
