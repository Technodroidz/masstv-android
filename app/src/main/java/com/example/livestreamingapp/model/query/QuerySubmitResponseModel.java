package com.example.livestreamingapp.model.query;

import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;

import java.util.List;

public class QuerySubmitResponseModel {

@SerializedName("success")
@Expose
private Boolean success;
@SerializedName("data")
@Expose
private MyQuery data = null;
@SerializedName("message")
@Expose
private String message;

public Boolean getSuccess() {
return success;
}

public void setSuccess(Boolean success) {
this.success = success;
}


public String getMessage() {
return message;
}

public void setMessage(String message) {
this.message = message;
}

    public MyQuery getData() {
        return data;
    }

    public void setData(MyQuery data) {
        this.data = data;
    }
}