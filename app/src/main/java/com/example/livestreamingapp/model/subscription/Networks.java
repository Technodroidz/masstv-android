
package com.example.livestreamingapp.model.subscription;

import java.util.List;
import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;

public class Networks {

    @SerializedName("available")
    @Expose
    private List<String> available = null;
    @SerializedName("preferred")
    @Expose
    private Object preferred;

    public List<String> getAvailable() {
        return available;
    }

    public void setAvailable(List<String> available) {
        this.available = available;
    }

    public Object getPreferred() {
        return preferred;
    }

    public void setPreferred(Object preferred) {
        this.preferred = preferred;
    }

}
