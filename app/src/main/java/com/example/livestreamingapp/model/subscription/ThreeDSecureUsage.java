
package com.example.livestreamingapp.model.subscription;

import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;

public class ThreeDSecureUsage {

    @SerializedName("supported")
    @Expose
    private Boolean supported;

    public Boolean getSupported() {
        return supported;
    }

    public void setSupported(Boolean supported) {
        this.supported = supported;
    }

}
