
package com.example.livestreamingapp.model.webseries;



import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;

import java.io.Serializable;


public class ContentDetail implements Serializable {

    @SerializedName("content_info")
    @Expose
    private ContentInfo contentInfo;
    @SerializedName("webseries_detail")
    @Expose
    private WebseriesDetail webseriesDetail;

    public ContentInfo getContentInfo() {
        return contentInfo;
    }

    public void setContentInfo(ContentInfo contentInfo) {
        this.contentInfo = contentInfo;
    }

    public WebseriesDetail getWebseriesDetail() {
        return webseriesDetail;
    }

    public void setWebseriesDetail(WebseriesDetail webseriesDetail) {
        this.webseriesDetail = webseriesDetail;
    }

}
