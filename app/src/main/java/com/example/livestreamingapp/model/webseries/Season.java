
package com.example.livestreamingapp.model.webseries;


import java.io.Serializable;
import java.util.List;
import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;

public class Season implements Serializable {

    @SerializedName("id")
    @Expose
    private Integer id;
    @SerializedName("user_id")
    @Expose
    private String userId;
    @SerializedName("title")
    @Expose
    private String title;
    @SerializedName("season")
    @Expose
    private String season;
    @SerializedName("season_name")
    @Expose
    private Object seasonName;
    @SerializedName("episode")
    @Expose
    private String episode;
    @SerializedName("episode_name")
    @Expose
    private String episodeName;
    @SerializedName("season_description")
    @Expose
    private Object seasonDescription;
    @SerializedName("season_banner")
    @Expose
    private Object seasonBanner;
    @SerializedName("episode_description")
    @Expose
    private String episodeDescription;
    @SerializedName("source")
    @Expose
    private String source;
    @SerializedName("subtitle")
    @Expose
    private String subtitle;
    @SerializedName("srt")
    @Expose
    private String srt;
    @SerializedName("thumb")
    @Expose
    private String thumb;
    @SerializedName("category")
    @Expose
    private String category;
    @SerializedName("duration")
    @Expose
    private String duration;
    @SerializedName("uploaded_by")
    @Expose
    private String uploadedBy;
    @SerializedName("admin_approved")
    @Expose
    private String adminApproved;
    @SerializedName("status")
    @Expose
    private String status;
    @SerializedName("tags")
    @Expose
    private String tags;
    @SerializedName("video_type")
    @Expose
    private String videoType;
    @SerializedName("release_date")
    @Expose
    private Object releaseDate;
    @SerializedName("created_at")
    @Expose
    private String createdAt;
    @SerializedName("updated_at")
    @Expose
    private String updatedAt;
    @SerializedName("banner_image")
    @Expose
    private String bannerImage;
    @SerializedName("is_watch_later")
    @Expose
    private Boolean isWatchLater;
    @SerializedName("is_rated")
    @Expose
    private Boolean isRated;
    @SerializedName("avg_rate")
    @Expose
    private double avgRate;
    @SerializedName("user_rating")
    @Expose
    private Integer userRating;
    @SerializedName("total_user_rated")
    @Expose
    private Integer totalUserRated;
    @SerializedName("episodes")
    @Expose
    private List<Episode> episodes = null;

    public Integer getId() {
        return id;
    }

    public void setId(Integer id) {
        this.id = id;
    }

    public String getUserId() {
        return userId;
    }

    public void setUserId(String userId) {
        this.userId = userId;
    }

    public String getTitle() {
        return title;
    }

    public void setTitle(String title) {
        this.title = title;
    }

    public String getSeason() {
        return season;
    }

    public void setSeason(String season) {
        this.season = season;
    }

    public Object getSeasonName() {
        return seasonName;
    }

    public void setSeasonName(Object seasonName) {
        this.seasonName = seasonName;
    }

    public String getEpisode() {
        return episode;
    }

    public void setEpisode(String episode) {
        this.episode = episode;
    }

    public String getEpisodeName() {
        return episodeName;
    }

    public void setEpisodeName(String episodeName) {
        this.episodeName = episodeName;
    }

    public Object getSeasonDescription() {
        return seasonDescription;
    }

    public void setSeasonDescription(Object seasonDescription) {
        this.seasonDescription = seasonDescription;
    }

    public Object getSeasonBanner() {
        return seasonBanner;
    }

    public void setSeasonBanner(Object seasonBanner) {
        this.seasonBanner = seasonBanner;
    }

    public String getEpisodeDescription() {
        return episodeDescription;
    }

    public void setEpisodeDescription(String episodeDescription) {
        this.episodeDescription = episodeDescription;
    }

    public String getSource() {
        return source;
    }

    public void setSource(String source) {
        this.source = source;
    }

    public String getSubtitle() {
        return subtitle;
    }

    public void setSubtitle(String subtitle) {
        this.subtitle = subtitle;
    }

    public String getSrt() {
        return srt;
    }

    public void setSrt(String srt) {
        this.srt = srt;
    }

    public String getThumb() {
        return thumb;
    }

    public void setThumb(String thumb) {
        this.thumb = thumb;
    }

    public String getCategory() {
        return category;
    }

    public void setCategory(String category) {
        this.category = category;
    }

    public String getDuration() {
        return duration;
    }

    public void setDuration(String duration) {
        this.duration = duration;
    }

    public String getUploadedBy() {
        return uploadedBy;
    }

    public void setUploadedBy(String uploadedBy) {
        this.uploadedBy = uploadedBy;
    }

    public String getAdminApproved() {
        return adminApproved;
    }

    public void setAdminApproved(String adminApproved) {
        this.adminApproved = adminApproved;
    }

    public String getStatus() {
        return status;
    }

    public void setStatus(String status) {
        this.status = status;
    }

    public String getTags() {
        return tags;
    }

    public void setTags(String tags) {
        this.tags = tags;
    }

    public String getVideoType() {
        return videoType;
    }

    public void setVideoType(String videoType) {
        this.videoType = videoType;
    }

    public Object getReleaseDate() {
        return releaseDate;
    }

    public void setReleaseDate(Object releaseDate) {
        this.releaseDate = releaseDate;
    }

    public String getCreatedAt() {
        return createdAt;
    }

    public void setCreatedAt(String createdAt) {
        this.createdAt = createdAt;
    }

    public String getUpdatedAt() {
        return updatedAt;
    }

    public void setUpdatedAt(String updatedAt) {
        this.updatedAt = updatedAt;
    }

    public String getBannerImage() {
        return bannerImage;
    }

    public void setBannerImage(String bannerImage) {
        this.bannerImage = bannerImage;
    }

    public Boolean getIsWatchLater() {
        return isWatchLater;
    }

    public void setIsWatchLater(Boolean isWatchLater) {
        this.isWatchLater = isWatchLater;
    }

    public Boolean getIsRated() {
        return isRated;
    }

    public void setIsRated(Boolean isRated) {
        this.isRated = isRated;
    }

    public double getAvgRate() {
        return avgRate;
    }

    public void setAvgRate(double avgRate) {
        this.avgRate = avgRate;
    }

    public Integer getUserRating() {
        return userRating;
    }

    public void setUserRating(Integer userRating) {
        this.userRating = userRating;
    }

    public Integer getTotalUserRated() {
        return totalUserRated;
    }

    public void setTotalUserRated(Integer totalUserRated) {
        this.totalUserRated = totalUserRated;
    }

    public List<Episode> getEpisodes() {
        return episodes;
    }

    public void setEpisodes(List<Episode> episodes) {
        this.episodes = episodes;
    }

}
