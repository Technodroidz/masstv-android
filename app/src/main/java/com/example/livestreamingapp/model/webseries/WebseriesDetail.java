
package com.example.livestreamingapp.model.webseries;

import java.io.Serializable;
import java.util.List;

import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;

public class WebseriesDetail implements Serializable {

    @SerializedName("seasons")
    @Expose
    private List<Season> seasons = null;

    public List<Season> getSeasons() {
        return seasons;
    }

    public void setSeasons(List<Season> seasons) {
        this.seasons = seasons;
    }

}
