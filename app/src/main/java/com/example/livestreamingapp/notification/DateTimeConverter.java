package com.example.livestreamingapp.notification;

import java.text.DateFormat;
import java.text.SimpleDateFormat;
import java.util.Calendar;
import java.util.Date;

public class DateTimeConverter {

    public String getTimeAccToCurrent(long time){
        Calendar now = Calendar.getInstance();
        Calendar before = Calendar.getInstance();
        before.setTimeInMillis(time);
        int beforeYear = before.get(Calendar.YEAR);
        int beforeMonth = before.get(Calendar.MONTH) + 1; // Note: zero based!
        int beforeDay = before.get(Calendar.DAY_OF_MONTH);

        SimpleDateFormat monthFormat = new SimpleDateFormat("dd-MMM");
        SimpleDateFormat yearFormat = new SimpleDateFormat("dd-mm-yyyy");
        SimpleDateFormat dayFormat = new SimpleDateFormat("hh:mm a");

        String date;

        if(now.get(Calendar.DATE)==before.get(Calendar.DATE)){
            long second = now.getTimeInMillis()-before.getTimeInMillis();
            if(second<60000){
                date = second/1000+" sec ago";
            }else {
                date = dayFormat.format(before.getTime());
            }

        }else if(now.get(Calendar.YEAR)!=before.get(Calendar.YEAR)){
            date = yearFormat.format(before.getTime());
        }else {
            date = monthFormat.format(before.getTime());
        }


        return date;



    }

}
