package com.example.livestreamingapp.notification;

import androidx.annotation.NonNull;
import androidx.appcompat.app.AppCompatActivity;
import androidx.appcompat.widget.Toolbar;
import androidx.core.app.NotificationManagerCompat;
import androidx.recyclerview.widget.ItemTouchHelper;
import androidx.recyclerview.widget.LinearLayoutManager;
import androidx.recyclerview.widget.RecyclerView;

import android.app.NotificationManager;
import android.content.Intent;
import android.os.Bundle;
import android.os.Handler;
import android.util.Log;
import android.view.Menu;
import android.view.MenuItem;
import android.view.View;
import android.widget.ImageView;
import android.widget.Toast;

import com.example.livestreamingapp.MassTvActivity;
import com.example.livestreamingapp.MassTvApplication;
import com.example.livestreamingapp.R;
import com.example.livestreamingapp.contact.MyQueryAdapter;
import com.example.livestreamingapp.firebase.LocalBroadCastReceiver;
import com.example.livestreamingapp.home.ui.HomeDraweerStreamingActivity;
import com.example.livestreamingapp.model.NotificationResponse;
import com.example.livestreamingapp.model.ProfileData;
import com.example.livestreamingapp.utils.BottomNavigationHelper;
import com.example.livestreamingapp.utils.CustomProgressDialog;
import com.example.livestreamingapp.utils.NotificationMessage;
import com.example.livestreamingapp.utils.SharedPreferenceHandler;
import com.example.livestreamingapp.utils.SnackBar;
import com.example.livestreamingapp.webservice.APIClient;
import com.example.livestreamingapp.webservice.APIInterface;
import com.example.livestreamingapp.webservice.APIUploadClient;
import com.google.android.gms.cast.framework.CastButtonFactory;
import com.google.android.gms.cast.framework.CastContext;
import com.google.gson.Gson;
import com.tsuryo.swipeablerv.SwipeLeftRightCallback;
import com.tsuryo.swipeablerv.SwipeableRecyclerView;

import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Date;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import okhttp3.ResponseBody;
import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;

public class NotificationActivity extends MassTvActivity implements View.OnClickListener,
        LocalBroadCastReceiver.localBroadCastListener {

    private NotificationAdapter adapter;
    private SwipeableRecyclerView rv_notification;
    private List<NotificationMessage> list;
    private CastContext mCastContext;
    private MenuItem mediaRouteMenuItem;
    private ImageView iv_back;
    public static boolean isOnResume;
    private Toolbar mToolbar;
    private BottomNavigationHelper bottomNavigationHelper;
    private APIInterface apiInterface;
    CustomProgressDialog customProgressDialog ;
    private ProfileData profileData;


    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_notification);
        apiInterface = APIUploadClient.getClient().create(APIInterface.class);
        customProgressDialog = new CustomProgressDialog(this);
        setupActionBar();
        bottomNavigationHelper = new BottomNavigationHelper(this);



        initView();


        rv_notification.setListener(new SwipeLeftRightCallback.Listener() {
            @Override
            public void onSwipedLeft(int position) {
                attemptDeleteNotificaiton(position,list.get(position));

               // adapter.notifyDataSetChanged();
            }

            @Override
            public void onSwipedRight(int position) {
                Toast.makeText(NotificationActivity.this, "Right swipe", Toast.LENGTH_SHORT).show();
            }
        });



    }

    public void attemptDeleteNotificaiton(int position,NotificationMessage notificationMessage){


        customProgressDialog.show();
        Map<String,String> map  = new HashMap<>();
        map.put("notification_id",""+notificationMessage.getId());
        Call<ResponseBody> callDelete = apiInterface.deleteNotificaiton(map);
        callDelete.enqueue(new Callback<ResponseBody>() {
            @Override
            public void onResponse(Call<ResponseBody> call, Response<ResponseBody> response) {
                customProgressDialog.dismiss();
                if(response.code()==200){
                    Toast.makeText(NotificationActivity.this, "Notification deleted successfully!", Toast.LENGTH_SHORT).show();
                }
                list.remove(position);
                adapter.addData(list);
                adapter.notifyItemRemoved(position);
            }

            @Override
            public void onFailure(Call<ResponseBody> call, Throwable t) {

                customProgressDialog.dismiss();

            }
        });


    }

    private void setupActionBar() {
        mToolbar = (Toolbar) findViewById(R.id.toolbar);
        // mToolbar.setTitle("Notification");
        setSupportActionBar(mToolbar);
        if (getSupportActionBar() != null) {
            getSupportActionBar().setDisplayHomeAsUpEnabled(false);
        }
    }

    private void initView() {

        iv_back = findViewById(R.id.iv_back);

        list = new ArrayList<>();
        rv_notification = (SwipeableRecyclerView) findViewById(R.id.rv_notification);
        LinearLayoutManager manager = new LinearLayoutManager(this);
        rv_notification.setLayoutManager(manager);

      /*  list = new SharedPreferenceHandler(this).getAllMessage();
        Log.e("check lsit",""+new Gson().toJson(list));
    */
        new SharedPreferenceHandler(this).setUnReadCount(0);
        adapter = new NotificationAdapter(this, list, new NotificationAdapter.ClickItemListener() {
            @Override
            public void OnClickItem(int position) {
               /* if (list.get(position).getMessage().contains("Enter Bank Detail")) {
                    // add bank detail
                }*/
            }
        });
        rv_notification.setAdapter(adapter);

        getAllNotification();
        setUpClickListener();
    }


    private void getAllNotification() {
        customProgressDialog.show();
         profileData = new SharedPreferenceHandler(NotificationActivity.this).getProfileInfo();
        HashMap<String,String> map = new HashMap<>();
        map.put("user_id",""+profileData.getId());
        apiInterface.getNotification(map).enqueue(new Callback<NotificationResponse>() {
            @Override
            public void onResponse(Call<NotificationResponse> call, Response<NotificationResponse> response) {
                if(response.body()!=null && response.body().getStatus()){
                    Log.e("check noti",""+new Gson().toJson(response.body().getNotificationMessage()));
                    list.addAll(response.body().getNotificationMessage());
                    Log.e("check list",""+new Gson().toJson(list));
                    adapter.addData(list);
                    adapter.notifyDataSetChanged();
                }
                customProgressDialog.dismiss();
            }

            @Override
            public void onFailure(Call<NotificationResponse> call, Throwable t) {
                customProgressDialog.dismiss();
            }
        });
    }

    private void setUpClickListener() {
//        iv_back.setOnClickListener(this);
    }

    @Override
    public void onClick(View v) {
        if (v.getId() == R.id.iv_back) {
            onBackPressed();
        }
    }

    @Override
    public boolean onCreateOptionsMenu(Menu menu) {
        // Inflate the menu; this adds items to the action bar if it is present.
        getMenuInflater().inflate(R.menu.menu_cast, menu);
        mediaRouteMenuItem = CastButtonFactory.setUpMediaRouteButton(getApplicationContext(), menu,
                R.id.media_route_menu_item);
        return true;
    }

    @Override
    protected void onResume() {
        super.onResume();
        isOnResume = true;
        clearAllNotification();
        try {
            MassTvApplication.getInstance().registerLocalReceiver(this);
        } catch (Exception e) {

            e.printStackTrace();
        }
    }

    @Override
    protected void onPause() {
        super.onPause();
        isOnResume = false;
        try {
            MassTvApplication.getInstance().unRegisteredLocalReceiver();
        } catch (Exception e) {

        }
    }

    @Override
    public void onBackPressed() {
        if (HomeDraweerStreamingActivity.isAliveActivity) {
            finish();
        } else {
            startActivity(new Intent(this, HomeDraweerStreamingActivity.class));
            finish();
        }

    }


    @Override
    public void onGetBroadCast(Intent intent) {
        updateMessage();
        clearAllNotification();
    }

    private void updateMessage() {
        try {
            runOnUiThread(new Runnable() {
                @Override
                public void run() {
                    new Handler().postDelayed(new Runnable() {
                        @Override
                        public void run() {
                            //  new SnackBar(NotificationActivity.this).showPositiveSnack("new notification received!");
                            if (bottomNavigationHelper != null) {
                                new SharedPreferenceHandler(NotificationActivity.this).setUnReadCount(0);
                                bottomNavigationHelper.setNotification(0);
                            }

                            List<NotificationMessage> newMessageList = new SharedPreferenceHandler(NotificationActivity.this).getAllMessage();
                            list.add(0,newMessageList.get(newMessageList.size()-1));
                            if (list != null) {
                                adapter.addData(list);
                                adapter.notifyItemInserted(0);
                            }
                            if (bottomNavigationHelper != null) {
                                bottomNavigationHelper.setNotification(0);
                            }
                        }
                    }, 500);

                }
            });


        } catch (Exception e) {

        }

    }


    @Override
    public boolean onOptionsItemSelected(@NonNull MenuItem item) {

        switch (item.getItemId()) {
            case android.R.id.home:
                // todo: goto back activity from here
                finish();
                return true;

            default:
                return super.onOptionsItemSelected(item);
        }
    }

    public void clearAllNotification() {
        try {
            NotificationManagerCompat.from(this).cancelAll();
            new SharedPreferenceHandler(NotificationActivity.this).setUnReadCount(0);
            if (bottomNavigationHelper != null) {
                bottomNavigationHelper.setNotification(0);
            }
        } catch (Exception e) {

        }
    }
}