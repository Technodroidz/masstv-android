package com.example.livestreamingapp.notification;

import android.content.Context;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.TextView;

import androidx.annotation.NonNull;
import androidx.recyclerview.widget.RecyclerView;

import com.bumptech.glide.Glide;
import com.example.livestreamingapp.R;
import com.example.livestreamingapp.model.query.MyQuery;
import com.example.livestreamingapp.utils.NotificationMessage;

import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.time.ZoneId;
import java.util.ArrayList;
import java.util.Date;
import java.util.List;
import java.util.Locale;
import java.util.TimeZone;

public class NotificationAdapter extends RecyclerView.Adapter<NotificationAdapter.QueryHolder> {
    private  List<NotificationMessage> queryList;
    private final Context context;
    private ClickItemListener clickItemListener;

    public interface ClickItemListener {
        public void OnClickItem(int position);
    }

    public NotificationAdapter(Context context, List<NotificationMessage> queryList,ClickItemListener clickItemListener){
        this.context =context;
        if(queryList==null){
            this.queryList = new ArrayList<>();
        }else {
            this.queryList = queryList;
        }
        this.clickItemListener = clickItemListener;

    }
    @NonNull
    @Override
    public QueryHolder onCreateViewHolder(@NonNull ViewGroup parent, int viewType) {
        View view = LayoutInflater.from(context).inflate(R.layout.item_list_notification,parent,false);
        return new QueryHolder(view);
    }

    @Override
    public void onBindViewHolder(@NonNull QueryHolder holder, int position) {

        holder.tv_title.setText(queryList.get(position).getTitle());
        holder.tv_message.setText(queryList.get(position).getMessage());
        try {
            holder.tv_time.setText(""+getTime(getDate(queryList.get(position).getTime()).getTime()));
        }catch (Exception e){

        }

        if(queryList.get(position).getThumb()!=null){
            holder.iv_notification.setVisibility(View.VISIBLE);
            Glide.with(context).load(queryList.get(position).getThumb()).into(holder.iv_notification);
        }else {
            holder.iv_notification.setVisibility(View.GONE);
        }

        holder.itemView.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                clickItemListener.OnClickItem(position);
            }
        });

    }

    private String getTime(long time) {
        return new DateTimeConverter().getTimeAccToCurrent(time);
    }

    @Override
    public int getItemCount() {
        return queryList.size();
    }

    public void addData(List<NotificationMessage> list) {

        this.queryList = list;
    }

    public Date getDate(String dateTime){

        SimpleDateFormat inputFormat = new SimpleDateFormat("yyyy-MM-dd'T'HH:mm:ss.SSS'Z'", Locale.getDefault());
        inputFormat.setTimeZone(TimeZone.getTimeZone("UTC"));
        try {
            return inputFormat.parse(dateTime);
        } catch (ParseException e) {
            e.printStackTrace();
        }
        return new Date();
    }


    public class QueryHolder extends RecyclerView.ViewHolder{
        TextView tv_title;
        TextView tv_message;
        TextView tv_time;
        ImageView iv_notification;
        public QueryHolder(@NonNull View itemView) {
            super(itemView);
            tv_title = itemView.findViewById(R.id.tv_title);
            tv_message = itemView.findViewById(R.id.tv_message);
            tv_time = itemView.findViewById(R.id.tv_time);
            iv_notification = itemView.findViewById(R.id.iv_notification);

        }
    }
}
