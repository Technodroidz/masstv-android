package com.example.livestreamingapp.ondemand;

import android.content.Context;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.TextView;

import androidx.annotation.NonNull;
import androidx.recyclerview.widget.RecyclerView;

import com.bumptech.glide.Glide;
import com.bumptech.glide.request.RequestOptions;
import com.example.livestreamingapp.R;
import com.example.livestreamingapp.home.ui.SearchVideoListActivity;
import com.example.livestreamingapp.model.OnDemandVideo;
import com.example.livestreamingapp.model.WatchLaterResponse;
import com.example.livestreamingapp.utils.CustomProgressDialog;
import com.example.livestreamingapp.utils.SnackBar;
import com.example.livestreamingapp.utils.Utils;
import com.example.livestreamingapp.video.VideoDetailActivity;
import com.example.livestreamingapp.webservice.WatchLaterService;

import java.util.List;

public class AdapterGridVideoList extends RecyclerView.Adapter<AdapterGridVideoList.VideoListHolder> {

    private Context context;
    private List<OnDemandVideo> videoList;
    private AdapterViewClickListener listener;
    private CustomProgressDialog pDialog;

    public AdapterGridVideoList(Context context,
                                List<OnDemandVideo> videoList,
                                AdapterViewClickListener listener){
        this.context = context;
        this.videoList = videoList;
        this.listener = listener;
    }

    @NonNull
    @Override
    public VideoListHolder onCreateViewHolder(@NonNull ViewGroup parent, int viewType) {
        View view = LayoutInflater.from(context).inflate(R.layout._item_list_video_grid,parent,false);
        return new VideoListHolder(view);
    }

    protected void showProgrss() {
        if (pDialog == null) {
            // pDialog = new ProgressDialog(this);
            pDialog = new CustomProgressDialog(context);
            pDialog.setCancelable(false);
        }
        if (!pDialog.isShowing()) {
            // Show progressbar
            pDialog.show();
        }
    }

    protected void dismisProgrss() {
        if (pDialog != null && pDialog.isShowing()) {
            pDialog.dismiss();
        }
    }

    @Override
    public void onBindViewHolder(@NonNull VideoListHolder holder, int position) {

        OnDemandVideo video = videoList.get(position);
        if(context instanceof SearchVideoListActivity){
            holder.ib_play_later.setVisibility(View.GONE);
        }

        holder.tv_rating.setText(""+ Utils.getDecimalOnePoint(video.getAvg_rate()));

        if(context instanceof SearchVideoListActivity){
            if(video.getCategory().equalsIgnoreCase("Web Series")){
                holder.tv_title.setText(""+ videoList.get(position).getTitle()+" - Season "+video.getSeason().substring(1));
            }
        }else {
            holder.tv_title.setText(""+ videoList.get(position).getTitle());
        }


        if(video.isWatchList()){
            holder.ib_play_later.setImageResource(R.drawable.ic_watched_list);
            if(context instanceof VideoListActivity && VideoListActivity.isWatchListScreen){
                holder.ib_play_later.setImageResource(R.drawable.ic_delete);
            }
        }else {
            holder.ib_play_later.setImageResource(R.drawable.ic_watch_later);
        }

        RequestOptions requestOptions = new RequestOptions();
        requestOptions.placeholder(R.drawable.placeholder_black);
        requestOptions.error(R.drawable.placeholder_black);

        Glide.with(context)
                .applyDefaultRequestOptions(requestOptions)
                .load(video.getThumb()).into(holder.iv_image);



    }


    @Override
    public int getItemCount() {
        return videoList.size();
    }

    public void addData(List<OnDemandVideo> videosList) {
        this.videoList = videosList;
    }

    public List<OnDemandVideo> getVideoList() {
        return this.videoList;
    }

    public class VideoListHolder extends RecyclerView.ViewHolder {

        private ImageView iv_image;
        private ImageView ib_play_later;
        private TextView tv_rating;
        private TextView tv_title;
        public VideoListHolder(@NonNull View itemView) {
            super(itemView);
            iv_image = (ImageView)itemView.findViewById(R.id.iv_image);
            ib_play_later = (ImageView)itemView.findViewById(R.id.ib_play_later);
            tv_rating = (TextView) itemView.findViewById(R.id.tv_rating);
            tv_title = (TextView) itemView.findViewById(R.id.tv_title);

            ib_play_later.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View v) {
                    showProgrss();
                    new WatchLaterService(context, new WatchLaterService.OnWatchLaterResponse() {
                        @Override
                        public void onResponse(WatchLaterResponse response) {
                            dismisProgrss();
                            new SnackBar(context).showPositiveSnack(""+response.getMessage());
                            videoList.get(getLayoutPosition()).setWatchList(!videoList.get(getLayoutPosition()).isWatchList());
                            listener.onWatchLater(getLayoutPosition(),videoList.get(getLayoutPosition()));
                            notifyDataSetChanged();
                        }

                        @Override
                        public void onError(String error) {
                            dismisProgrss();
                        }
                    }).addToWatchListVideo(videoList.get(getLayoutPosition()).getId(),
                            videoList.get(getLayoutPosition()).isWatchList()?"remove":"add",
                            videoList.get(getLayoutPosition()).getCategory());
                }
            });

            itemView.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View v) {
                    listener.onVideoSelected(getLayoutPosition(),videoList.get(getAdapterPosition()));


                }
            });

        }
    }

}
