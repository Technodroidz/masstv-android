package com.example.livestreamingapp.ondemand;

import android.content.Context;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.TextView;

import androidx.annotation.NonNull;
import androidx.recyclerview.widget.LinearLayoutManager;
import androidx.recyclerview.widget.RecyclerView;

import com.bumptech.glide.Glide;
import com.bumptech.glide.request.RequestOptions;
import com.example.livestreamingapp.R;
import com.example.livestreamingapp.home.ui.HomeDraweerStreamingActivity;
import com.example.livestreamingapp.model.OnDemandCategory;
import com.example.livestreamingapp.model.OnDemandVideo;
import com.example.livestreamingapp.model.ProfileData;
import com.example.livestreamingapp.model.homescreen.CategoryModel;
import com.example.livestreamingapp.utils.HorizontalItemDecoration;
import com.example.livestreamingapp.utils.SharedPreferenceHandler;
import com.example.livestreamingapp.webservice.GetOnDemandVideoList;
import com.facebook.shimmer.ShimmerFrameLayout;

import java.util.ArrayList;
import java.util.List;

public class AdapterHomeVideoCategory extends RecyclerView.Adapter<AdapterHomeVideoCategory.CategoryHeadingHolder>
{

    private final List<CategoryModel> categoryList;
    private Context context;
    AdapterViewClickListener lister;
    ProfileData profileData;

    public AdapterHomeVideoCategory(Context context, List<CategoryModel> categoryList ,
                                    AdapterViewClickListener listener){
        this.context = context;
        this.categoryList = categoryList;
        this.lister = listener;
        profileData = new SharedPreferenceHandler(context).getProfileInfo();
    }

    @NonNull
    @Override
    public CategoryHeadingHolder onCreateViewHolder(@NonNull ViewGroup parent, int viewType) {
        View view = LayoutInflater.from(context).inflate(R.layout._item_list_category_new,parent,false);
        return new CategoryHeadingHolder(view);
    }

    @Override
    public int getItemViewType(int position) {
        return position;
    }

    @Override
    public long getItemId(int position) {
        return position;
    }

    @Override
    public void onBindViewHolder(@NonNull final CategoryHeadingHolder holder, int position) {
        CategoryModel onDemandCategory = categoryList.get(position);
        holder.tv_video_category.setText(onDemandCategory.getName());
        holder.view_extra_bottom.setVisibility(View.GONE);
        holder.lin_banner.setVisibility(View.GONE);

        holder.tv_not_found.setVisibility(View.GONE);

        if(holder.adapter.getItemCount()<1){
            holder.rv_videos.setVisibility(View.GONE);
            holder.shimmer_view_container.setVisibility(View.VISIBLE);
            holder.shimmer_view_container.startShimmer();
        }

        if(onDemandCategory.getVideoList().size() > 0){
            holder.shimmer_view_container.hideShimmer();
            holder.shimmer_view_container.setVisibility(View.GONE);
            holder.lin_content.setVisibility(View.VISIBLE);
            holder.rv_videos.setVisibility(View.VISIBLE);
            holder.adapter = new AdapterVideoList(context,
                    onDemandCategory.getVideoList(),holder);
            holder.rv_videos.setAdapter(holder.adapter);
            holder.tv_video_category.setVisibility(View.VISIBLE);
        }else {
            // holder.lin_content.setVisibility(View.GONE);
            holder.tv_not_found.setVisibility(View.VISIBLE);
            holder.tv_not_found.setText("No "+onDemandCategory.getName()+" videos found!");
            holder.shimmer_view_container.hideShimmer();
            holder.shimmer_view_container.setVisibility(View.GONE);
            holder.tv_video_category.setVisibility(View.GONE);
            holder.tv_not_found.setVisibility(View.GONE);
            holder.rv_videos.setVisibility(View.GONE);
            //holder.lin_content.setVisibility(View.GONE);
        }


     /*   new GetOnDemandVideoList(context,
                new GetOnDemandVideoList.onDemandVideoListListener() {
                    @Override
                    public void onDemandFetchedVideoList(List<OnDemandVideo> videoData) {

                        List<OnDemandVideo> videosList = new ArrayList<>();
                        videosList.addAll(videoData);

                        holder.shimmer_view_container.hideShimmer();
                        holder.shimmer_view_container.setVisibility(View.GONE);
                        holder.tv_not_found.setVisibility(View.GONE);
                        if(videosList.size() > 0){
                            holder.lin_content.setVisibility(View.VISIBLE);
                            holder.rv_videos.setVisibility(View.VISIBLE);
                            holder.adapter = new AdapterVideoList(context,
                                    videosList,holder);
                            holder.rv_videos.setAdapter(holder.adapter);
                            holder.tv_video_category.setVisibility(View.VISIBLE);
                        }else {
                            // holder.lin_content.setVisibility(View.GONE);
                            holder.tv_video_category.setVisibility(View.GONE);
                        }


                    }

                    @Override
                    public void onDemandFetchingError(String error) {
                        holder.tv_not_found.setVisibility(View.VISIBLE);
                        holder.tv_not_found.setText("No "+onDemandCategory.getName()+" videos found!");
                        holder.shimmer_view_container.hideShimmer();
                        holder.shimmer_view_container.setVisibility(View.GONE);
                        holder.tv_video_category.setVisibility(View.GONE);
                        holder.tv_not_found.setVisibility(View.GONE);
                        holder.rv_videos.setVisibility(View.GONE);
                        //holder.lin_content.setVisibility(View.GONE);


                    }
                }).GetVideoList(onDemandCategory.getName(),0,profileData.getId());

*/

       /* if(context instanceof HomeDraweerStreamingActivity){

            if(categoryList.get(position).getBannerImage()!=null && !categoryList.get(position).getBannerImage().endsWith("/")
            ) {

                   *//* Glide.with(context).load(categoryList.get(position).getBanner())
                            .placeholder(R.drawable.place_holder_banner).into(holder.iv_banner);
*//*
                try {
                    if(categoryList.size()-1==position){
                        holder.iv_banner.setVisibility(View.VISIBLE);
                        holder.lin_banner.setVisibility(View.VISIBLE);
                        RequestOptions requestOptions = new RequestOptions();
                        requestOptions.placeholder(R.drawable.place_holder_banner);
                        requestOptions.error(R.drawable.place_holder_banner);

                        Glide.with(context)
                                .applyDefaultRequestOptions(requestOptions)
                                .load(categoryList.get(position).getBannerImage()).into(holder.iv_banner);
                    }

                } catch (Exception e) {

                }

            }
        }*/
        if(position==categoryList.size()-1 ){
            holder.view_extra_bottom.setVisibility(View.VISIBLE);
        }

    }

    @Override
    public int getItemCount() {
        return categoryList.size();
    }

    public List<CategoryModel> getCategoryList() {
        return categoryList;
    }


    public  class CategoryHeadingHolder extends RecyclerView.ViewHolder
            implements AdapterViewClickListener {
        final private TextView tv_video_category;
        final private RecyclerView rv_videos;
        private View view_extra_bottom;
        private ShimmerFrameLayout shimmer_view_container;
        AdapterVideoList adapter;

        private ImageView iv_banner;
        private TextView tv_not_found;
        private LinearLayout lin_banner;
        private LinearLayout lin_content;



        public CategoryHeadingHolder(@NonNull View itemView) {
            super(itemView);
            tv_video_category = itemView.findViewById(R.id.tv_video_category);
            rv_videos = itemView.findViewById(R.id.rv_videos);
            shimmer_view_container = itemView.findViewById(R.id.shimmer_view_container);
            view_extra_bottom = itemView.findViewById(R.id.view_extra_bottom);
            iv_banner = itemView.findViewById(R.id.iv_banner);
            lin_banner = itemView.findViewById(R.id.lin_banner);
            tv_not_found = itemView.findViewById(R.id.tv_not_found);
            lin_content = itemView.findViewById(R.id.lin_content);

            LinearLayoutManager layoutManager = new LinearLayoutManager(context);
            layoutManager.setOrientation(LinearLayoutManager.HORIZONTAL);
            rv_videos.setLayoutManager(layoutManager);
            rv_videos.addItemDecoration(new HorizontalItemDecoration(16));


            adapter = new AdapterVideoList(context,
                    new ArrayList<OnDemandVideo>(),this);
            rv_videos.setAdapter(adapter);


        }

        @Override
        public void onVideoSelected(int position,OnDemandVideo video) {

            lister.onVideoSelected(position,video);

        }

        @Override
        public void onCategorySelected(int position, OnDemandCategory onDemandCategory) {

        }

        @Override
        public void onWatchLater(int i, OnDemandVideo onDemandVideo) {
            // new SnackBar(context).showPositiveSnack("Added to WatchList!");
        }
    }
}
