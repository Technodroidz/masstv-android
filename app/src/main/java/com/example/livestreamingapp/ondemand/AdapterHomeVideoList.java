package com.example.livestreamingapp.ondemand;

import android.content.Context;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.ProgressBar;
import android.widget.TextView;

import androidx.annotation.NonNull;
import androidx.recyclerview.widget.RecyclerView;

import com.bumptech.glide.Glide;
import com.bumptech.glide.request.RequestOptions;
import com.example.livestreamingapp.R;
import com.example.livestreamingapp.model.OnDemandVideo;
import com.example.livestreamingapp.model.WatchLaterResponse;
import com.example.livestreamingapp.utils.CustomProgressDialog;
import com.example.livestreamingapp.utils.SnackBar;
import com.example.livestreamingapp.utils.Utils;
import com.example.livestreamingapp.webservice.WatchLaterService;

import java.util.List;

public class AdapterHomeVideoList extends RecyclerView.Adapter<AdapterHomeVideoList.VideoListHolder> {

    private Context context;
    private List<OnDemandVideo> videosList;
    AdapterViewClickListener listener;
    private CustomProgressDialog pDialog;

    public AdapterHomeVideoList(Context context, List<OnDemandVideo> videosList,
                                AdapterViewClickListener listener){
        this.context = context;
        this.videosList = videosList;
        this.listener = listener;
    }

    protected void showProgrss() {
        if (pDialog == null) {
            // pDialog = new ProgressDialog(this);
            pDialog = new CustomProgressDialog(context);
            pDialog.setCancelable(false);
        }
        if (!pDialog.isShowing()) {
            // Show progressbar
            pDialog.show();
        }
    }

    protected void dismisProgrss() {
        if (pDialog != null && pDialog.isShowing()) {
            pDialog.dismiss();
        }
    }


    @NonNull
    @Override
    public VideoListHolder onCreateViewHolder(@NonNull ViewGroup parent, int viewType) {
        View view = LayoutInflater.from(context).inflate(R.layout._item_list_video,parent,false);

        return new VideoListHolder(view);
    }

    @Override
    public void onBindViewHolder(@NonNull VideoListHolder holder, int position) {

        //  holder.btn_play_stream.setVisibility(View.GONE);
        holder.lin_more_videos.setVisibility(View.GONE);



        if(position<videosList.size()) {

            holder.tv_rating.setText(""+ Utils.getDecimalOnePoint(videosList.get(position).getAvg_rate())+"(" +
                    "" +videosList.get(position).getTotal_user_rated()+
                    ")");
            holder.tv_title.setText(""+ videosList.get(position).getTitle());

            if(videosList.get(position).isWatchList()){

                holder.ib_play_later.setImageResource(R.drawable.ic_watched_list);
            }else {
                holder.ib_play_later.setImageResource(R.drawable.ic_watch_later);

            }

            OnDemandVideo video = videosList.get(position);
            RequestOptions requestOptions = new RequestOptions();
            requestOptions.placeholder(R.drawable.placeholder_black);
            requestOptions.error(R.drawable.placeholder_black);

            Glide.with(context)
                    .applyDefaultRequestOptions(requestOptions)
                    .load(video.getThumb()).into(holder.iv_image);

            // Glide.with(context).load(video.getThumb()).into(holder.iv_image);

        }else {
            //  holder.iv_image.setImageDrawable(context.getResources().getDrawable(R.drawable.upload_background));
            /**
             * This else part will be executed to show More Video Option
             * When video list size is greater than 5 else Visibility gone
             */
            if(videosList.size()>10 && position==videosList.size()){
                holder.lin_more_videos.setVisibility(View.VISIBLE);

            }else {
                holder.itemView.setVisibility(View.GONE);
            }
        }

    }

    /**
     * Add +1 extra size to show 'More Video' option
     * @return
     */
    @Override
    public int getItemCount() {
        return this.videosList.size()+1;
    }

    public class VideoListHolder extends RecyclerView.ViewHolder {
        private ImageView ib_play_later;
        private ImageView iv_image;
        private LinearLayout lin_more_videos;
        private TextView tv_rating;
        private TextView tv_title;
        private ProgressBar progress_playback;
        public VideoListHolder(@NonNull View itemView) {
            super(itemView);
            ib_play_later = (ImageView)itemView.findViewById(R.id.ib_play_later);
            iv_image = (ImageView)itemView.findViewById(R.id.iv_image);
            lin_more_videos = (LinearLayout) itemView.findViewById(R.id.lin_more_videos);
            tv_rating = (TextView) itemView.findViewById(R.id.tv_rating);
            tv_title = (TextView) itemView.findViewById(R.id.tv_title);
            progress_playback = (ProgressBar) itemView.findViewById(R.id.progress_playback);

            itemView.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View v) {
                    if(getLayoutPosition()==videosList.size()){
                        listener.onVideoSelected(-1,videosList.get(0));

                    }else {
                        listener.onVideoSelected(getLayoutPosition(),videosList.get(getAdapterPosition()));

                    }
                }
            });

            ib_play_later.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View v) {
                    showProgrss();
                    new WatchLaterService(context, new WatchLaterService.OnWatchLaterResponse() {
                        @Override
                        public void onResponse(WatchLaterResponse response) {
                            dismisProgrss();
                            new SnackBar(context).showPositiveSnack(""+response.getMessage());
                            // listener.onWatchLater(getLayoutPosition(),videosList.get(getLayoutPosition()));
                            videosList.get(getLayoutPosition()).setWatchList(!videosList.get(getLayoutPosition()).isWatchList());
                            notifyItemChanged(getLayoutPosition());
                        }

                        @Override
                        public void onError(String error) {
                            dismisProgrss();
                        }
                    }).addToWatchListVideo(videosList.get(getLayoutPosition()).getId(),
                            videosList.get(getLayoutPosition()).isWatchList()?"remove":"add",
                            videosList.get(getLayoutPosition()).getCategory());
                }
            });

        }
    }

    public int getSize(){
        return videosList.size();
    }


    @Override
    public int getItemViewType(int position) {
        return super.getItemViewType(position);

    }

}
