package com.example.livestreamingapp.ondemand;

import com.example.livestreamingapp.model.OnDemandCategory;
import com.example.livestreamingapp.model.OnDemandVideo;

public interface AdapterViewClickListener{
        public void onVideoSelected(int position, OnDemandVideo onDemandVideo);
        public void onCategorySelected(int position, OnDemandCategory onDemandCategory);
        public void onWatchLater(int i, OnDemandVideo onDemandVideo);
    }