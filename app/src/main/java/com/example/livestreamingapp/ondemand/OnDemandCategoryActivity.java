package com.example.livestreamingapp.ondemand;

import android.content.Intent;
import android.graphics.Color;
import android.graphics.drawable.ColorDrawable;
import android.graphics.drawable.InsetDrawable;
import android.net.Uri;
import android.os.Bundle;
import android.os.Handler;
import android.util.Log;
import android.view.Menu;
import android.view.MenuItem;
import android.view.View;
import android.widget.Button;
import android.widget.LinearLayout;
import android.widget.TextView;

import androidx.annotation.NonNull;
import androidx.appcompat.app.AlertDialog;
import androidx.appcompat.widget.Toolbar;
import androidx.recyclerview.widget.LinearLayoutManager;
import androidx.recyclerview.widget.RecyclerView;
import androidx.viewpager.widget.ViewPager;

import com.example.livestreamingapp.MassTvActivity;
import com.example.livestreamingapp.MassTvApplication;
import com.example.livestreamingapp.R;
import com.example.livestreamingapp.firebase.LocalBroadCastReceiver;
import com.example.livestreamingapp.home.ui.HomeDraweerStreamingActivity;
import com.example.livestreamingapp.home.ui.adapter.EndlessPagerAdapter;
import com.example.livestreamingapp.home.ui.adapter.ViewPagerAdapter;
import com.example.livestreamingapp.model.CategoryData;
import com.example.livestreamingapp.model.MySubscription;
import com.example.livestreamingapp.model.OnDemandCategory;
import com.example.livestreamingapp.model.OnDemandVideo;
import com.example.livestreamingapp.model.homescreen.CategoryModel;
import com.example.livestreamingapp.model.homescreen.HomeScreenData;
import com.example.livestreamingapp.model.homescreen.PromotionBanner;
import com.example.livestreamingapp.model.mastercategory.OnDemandMasterCategory;
import com.example.livestreamingapp.model.webseries.ContentDetail;
import com.example.livestreamingapp.model.webseries.ContentInfo;
import com.example.livestreamingapp.subscription.SubscriptionSelectorActivity;
import com.example.livestreamingapp.utils.BottomNavigationHelper;
import com.example.livestreamingapp.utils.CompleteProfile;
import com.example.livestreamingapp.utils.ConnectivityReceiver;
import com.example.livestreamingapp.utils.Constants;
import com.example.livestreamingapp.utils.SharedPreferenceHandler;
import com.example.livestreamingapp.video.VideoDetailActivity;
import com.example.livestreamingapp.webseries.WebSeriesDetailActivity;
import com.example.livestreamingapp.webservice.GenericResponseClass;
import com.example.livestreamingapp.webservice.GetVideoCategoryList;
import com.example.livestreamingapp.webservice.SubscriptionService;
import com.facebook.shimmer.ShimmerFrameLayout;
import com.google.android.gms.cast.framework.CastButtonFactory;
import com.google.android.gms.cast.framework.CastContext;
import com.google.android.material.snackbar.Snackbar;
import com.google.gson.Gson;

import java.util.ArrayList;
import java.util.List;

public class OnDemandCategoryActivity extends MassTvActivity implements
        AdapterViewClickListener, View.OnClickListener, ConnectivityReceiver.ConnectivityReceiverListener,
        LocalBroadCastReceiver.localBroadCastListener{

    Toolbar mToolbar;
    private CastContext mCastContext;
    private MenuItem mediaRouteMenuItem;

    public boolean isScreenVisible;

    private AdapterHomeVideoCategory adapterVideoCategory;
    private RecyclerView rv_video_category;

    private ShimmerFrameLayout shimmer_view_container;
    private LinearLayout lin_content;
    private MySubscription mySubscription;
    private SharedPreferenceHandler sharedPreferenceHandler;
    private ArrayList<CategoryModel> categoryList;
    private BottomNavigationHelper bottomNavigationHelper;
    public boolean secondTime;

    private ViewPager viewPagerBanner;
    private Handler bannerHandler;
    private Runnable bannerRunnable;
    private List<PromotionBanner> promotionBanners;
    private ViewPagerAdapter mViewPagerAdapter;
    private int bannerPosition;
    private EndlessPagerAdapter endlessPagerAdapter;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_on_demand);
        setupActionBar();
        sharedPreferenceHandler = new SharedPreferenceHandler(OnDemandCategoryActivity.this);
        bottomNavigationHelper = new BottomNavigationHelper(this);
        initViews();
        getMySubscriptionDetail(null);
        mCastContext = CastContext.getSharedInstance(this);

    }

    @Override
    public void onBackPressed() {
        super.onBackPressed();
        if (HomeDraweerStreamingActivity.isAliveActivity) {
            finish();
        } else {
            startActivity(new Intent(this, HomeDraweerStreamingActivity.class));
            finish();
        }
    }

    private void setupActionBar() {
        mToolbar = (Toolbar) findViewById(R.id.toolbar);
       // mToolbar.setTitle("VIDEO ON DEMAND");
        setSupportActionBar(mToolbar);

        if(getSupportActionBar()!=null){
            getSupportActionBar().setDisplayHomeAsUpEnabled(false);
        }

    }

    @Override
    public boolean onOptionsItemSelected(@NonNull MenuItem item) {

        switch (item.getItemId()) {
            case android.R.id.home:
                // todo: goto back activity from here
                onBackPressed();
                return true;

            default:
                return super.onOptionsItemSelected(item);
        }
    }

    private void initViews() {
        promotionBanners = new ArrayList<>();
        viewPagerBanner = findViewById(R.id.viewPagerBanner);
        //viewPagerBanner.setVisibility(View.GONE);

        //initalize viewpager
        // Initializing the ViewPagerAdapter
        //   viewPagerBanner.setOnPageChangeListener(new CircularViewPagerHandler(viewPagerBanner));
        mViewPagerAdapter = new ViewPagerAdapter(OnDemandCategoryActivity.this, promotionBanners, new ViewPagerAdapter.BannerListener() {
            @Override
            public void onBannerListener(PromotionBanner promotionBanner) {
                Intent intentBrowser = new Intent(Intent.ACTION_VIEW);
                Uri u = Uri.parse(promotionBanner.getWebsiteUrl());
                intentBrowser.setData(u);
                startActivity(intentBrowser);
            }
        });


        // Adding the Adapter to the ViewPager
        viewPagerBanner.setAdapter(mViewPagerAdapter);


        shimmer_view_container = findViewById(R.id.shimmer_view_container);
        lin_content = findViewById(R.id.lin_content);

        rv_video_category = (RecyclerView) findViewById(R.id.rv_video_category);
        LinearLayoutManager categoryLayoutManager = new LinearLayoutManager(OnDemandCategoryActivity.this);
        rv_video_category.setLayoutManager(categoryLayoutManager);

        adapterVideoCategory = new AdapterHomeVideoCategory(OnDemandCategoryActivity.this,
                new ArrayList<CategoryModel>(), OnDemandCategoryActivity.this);
        rv_video_category.setAdapter(adapterVideoCategory);


        shimmer_view_container.startShimmer();
        categoryList = new ArrayList<>();
        if (checkConnection()) {
            secondTime = false;
            shimmer_view_container.startShimmer();
            shimmer_view_container.setVisibility(View.VISIBLE);
            getCategoryList();
        } else {
            secondTime = true;
            shimmer_view_container.hideShimmer();
            shimmer_view_container.setVisibility(View.GONE);
            showSnack(checkConnection());
        }


       /* rv_reality_show = (RecyclerView)findViewById(R.id.rv_reality_show);
        rv_short_movies = (RecyclerView)findViewById(R.id.rv_short_movies);
        rv_entertainment = (RecyclerView)findViewById(R.id.rv_entertainment);

        // Horizontal layout manager for recyclerview
        LinearLayoutManager layoutManagerReality = new LinearLayoutManager(OnDemandCategoryActivity.this);
        LinearLayoutManager layoutManagerEntertainment = new LinearLayoutManager(OnDemandCategoryActivity.this);
        LinearLayoutManager layoutManagerMovies = new LinearLayoutManager(OnDemandCategoryActivity.this);
        layoutManagerReality.setOrientation(LinearLayoutManager.HORIZONTAL);
        layoutManagerEntertainment.setOrientation(LinearLayoutManager.HORIZONTAL);
        layoutManagerMovies.setOrientation(LinearLayoutManager.HORIZONTAL);

        // set EnterTainMent adapter
        adapterEntertainment = new AdapterVideoCategoryList(OnDemandCategoryActivity.this,Constants.CATEGORY_ENTERTAINMENT,this);
        rv_entertainment.setLayoutManager(layoutManagerEntertainment);
        rv_entertainment.addItemDecoration(new HorizontalItemDecoration(16));
        rv_entertainment.setAdapter(adapterEntertainment);

        // set adapterVideoOnDemand Adapter for recyclerView
        adapterShortMovies = new AdapterVideoCategoryList(OnDemandCategoryActivity.this,Constants.CATEGORY_SHORT,this);
        rv_short_movies.setLayoutManager(layoutManagerMovies);
        rv_short_movies.addItemDecoration(new HorizontalItemDecoration(16));
        rv_short_movies.setAdapter(adapterShortMovies);

        // set Trending Adapter for recyclerView
        adapterReality = new AdapterVideoCategoryList(OnDemandCategoryActivity.this,Constants.CATEGORY_REALITY,this);
        rv_reality_show.setLayoutManager(layoutManagerReality);
        rv_reality_show.addItemDecoration(new HorizontalItemDecoration(16));
        rv_reality_show.setAdapter(adapterReality);*/

        /**
         * this logic used for getting user subscription details
         * Uncomment this when subscription is required for playing video
         */
       /* sharedPreferenceHandler = new SharedPreferenceHandler(this);
        mySubscription = sharedPreferenceHandler.getMySubscriptionDetail();
        if (mySubscription == null || mySubscription.getRemainingDays()==null ||
                mySubscription.getRemainingDays().equalsIgnoreCase("0") ||
                !mySubscription.getLastRefreshDate().
                        equalsIgnoreCase(DateCalculation.getCurrentDateInDDMMYY())){
            getMySubscriptionDetail(null);

        }*/

        // banner change every 2 second
        bannerRunnable = new Runnable() {
            @Override
            public void run() {
                if (promotionBanners != null && promotionBanners.size() > 1) {
                  /*  bannerPosition = viewPagerBanner.getCurrentItem()==promotionBanners.size()-1?--bannerPosition:++bannerPosition;
                    viewPagerBanner.setCurrentItem(bannerPosition);
                    startBannerHandler();*/
                    bannerPosition = viewPagerBanner.getCurrentItem();
                    viewPagerBanner.setCurrentItem(++bannerPosition, true);
                    startBannerHandler();
                }
            }
        };


    }

    private void getCategoryList() {

     /*  getValidCategoryList();

        hideShimmer();
*/


        fetchVideoList();

    }

    private void fetchVideoList() {

        new GenericResponseClass(OnDemandCategoryActivity.this, new GenericResponseClass.GenericApiListener() {
            @Override
            public void onSuccess(HomeScreenData videoData) {

                hideShimmer();
                try {
                    categoryList.addAll(videoData.getCategory_list());
                    adapterVideoCategory = new AdapterHomeVideoCategory(OnDemandCategoryActivity.this,
                            videoData.getCategory_list(), OnDemandCategoryActivity.this);
                    rv_video_category.setAdapter(adapterVideoCategory);
                    if(categoryList.size()==0 && adapterVideoCategory!=null &&  adapterVideoCategory.getItemCount()==0 && isScreenVisible){
                        getCategoryList();
                    }


                    if (videoData.getPromotion_list() != null && videoData.getPromotion_list().size() > 0) {
                        Log.e("check banners ", " " + new Gson().toJson(videoData.getPromotion_list()));

                        viewPagerBanner.setVisibility(View.VISIBLE);

                        promotionBanners.clear();
                        promotionBanners = new ArrayList<>();
                        promotionBanners.addAll(videoData.getPromotion_list());

                        if (promotionBanners.size() > 0) {
                            mViewPagerAdapter.addBannerList(videoData.getPromotion_list());
                            mViewPagerAdapter.notifyDataSetChanged();
                            endlessPagerAdapter = new EndlessPagerAdapter(mViewPagerAdapter, viewPagerBanner);
                            viewPagerBanner.setAdapter(endlessPagerAdapter);
                            startBannerHandler();
                        }


                    }

                }catch (Exception e){

                }
            }

            @Override
            public void onError(String error) {
                if (adapterVideoCategory != null && adapterVideoCategory.getItemCount() == 0 && isScreenVisible) {
                    fetchVideoList();
                }

            }
        }).FetchAllVideoData();
    }






  /*  public void getValidCategoryList(){
        new GetVideoCategoryList(this, new GetVideoCategoryList.onDemandCategoryListListener() {


            @Override
            public void onFetchedCategoryList(CategoryData videoData) {
                hideShimmer();

                try {
                    categoryList.addAll(videoData.getCategory_list());
                    adapterVideoCategory = new AdapterHomeVideoCategory(OnDemandCategoryActivity.this,
                            videoData.getCategory_list(), OnDemandCategoryActivity.this);
                    rv_video_category.setAdapter(adapterVideoCategory);
                    if(categoryList.size()==0 && adapterVideoCategory!=null &&  adapterVideoCategory.getItemCount()==0 && isScreenVisible){
                        getCategoryList();
                    }


                    if (videoData.getPromotion_list() != null && videoData.getPromotion_list().size() > 0) {
                        Log.e("check banners ", " " + new Gson().toJson(videoData.getPromotion_list()));

                        viewPagerBanner.setVisibility(View.VISIBLE);

                        promotionBanners.clear();
                        promotionBanners = new ArrayList<>();
                        promotionBanners.addAll(videoData.getPromotion_list());

                        if (promotionBanners.size() > 0) {
                            mViewPagerAdapter.addBannerList(videoData.getPromotion_list());
                            mViewPagerAdapter.notifyDataSetChanged();
                            endlessPagerAdapter = new EndlessPagerAdapter(mViewPagerAdapter, viewPagerBanner);
                            viewPagerBanner.setAdapter(endlessPagerAdapter);
                            startBannerHandler();
                        }


                    }

                }catch (Exception e){

                }
            }

            @Override
            public void onFetchedMasterCategoryList(List<OnDemandMasterCategory> videoData) {

            }

            @Override
            public void onCategoryFetchingError(String error) {
                hideShimmer();
                if(adapterVideoCategory!=null && adapterVideoCategory.getItemCount()==0){
                    getValidCategoryList();
                }

            }
        }).GetCategoryList();
    }
*/

    private void startBannerHandler() {
        stopBannerHandler();
        try {
            if (bannerHandler != null) {
                bannerHandler.postDelayed(bannerRunnable, 5000);
            }
        } catch (Exception e) {

        }
    }

    private void stopBannerHandler() {
        try {
            bannerHandler.removeCallbacks(bannerRunnable);
        } catch (Exception e) {

        }
    }

    private void hideShimmer() {
        shimmer_view_container.hideShimmer();
        shimmer_view_container.setVisibility(View.GONE);
        lin_content.setVisibility(View.VISIBLE);
    }


    public void showSubscriptionAlert() {
        // Create an alert builder
        AlertDialog.Builder builder
                = new AlertDialog.Builder(this);
        // set the custom layout
        final View customLayout
                = getLayoutInflater()
                .inflate(
                        R.layout.layout_alert_subscribe,
                        null);
        builder.setView(customLayout);

        Button btn_subscribe = customLayout.findViewById(R.id.btn_subscribe);

        final AlertDialog dialog
                = builder.create();
        dialog.getWindow().setBackgroundDrawableResource(android.R.color.transparent);

        ColorDrawable back = new ColorDrawable(Color.TRANSPARENT);
        InsetDrawable inset = new InsetDrawable(back, 40, 0, 40, 0);
        if (dialog.getWindow() != null) {
            dialog.getWindow().setBackgroundDrawable(inset);

        }

        btn_subscribe.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                dialog.dismiss();
                startActivity(new Intent(OnDemandCategoryActivity.this, SubscriptionSelectorActivity.class));
            }
        });

        // create and show
        // the alert dialog

        dialog.show();
    }

    @Override
    public void onClick(View v) {
        switch (v.getId()) {
            case R.id.iv_back:
                finish();
                break;
        }
    }


    @Override
    public boolean onCreateOptionsMenu(Menu menu) {
        // Inflate the menu; this adds items to the action bar if it is present.
        getMenuInflater().inflate(R.menu.menu_cast, menu);
        mediaRouteMenuItem = CastButtonFactory.setUpMediaRouteButton(getApplicationContext(), menu,
                R.id.media_route_menu_item);
        return true;
    }

    @Override
    public void onVideoSelected(int position, OnDemandVideo onDemandVideo) {
        if (position == -1) {
            startActivity(new Intent(OnDemandCategoryActivity.this,
                    VideoListActivity.class).putExtra(Constants.CATEGORY_TYPE, "" + onDemandVideo.getCategory()));
        } else {

            /**
             * this below logic for checking user has subscription or not
             *  uncomment this logic when subscription is required
             */
           /* if (mySubscription != null && Integer.parseInt(mySubscription.getRemainingDays()) != 0) {
                if (!mySubscription.getLastRefreshDate().equalsIgnoreCase(DateCalculation.getCurrentDateInDDMMYY())) {
                    getMySubscriptionDetail(onDemandVideo);
                } else {
                    startActivity(new Intent(OnDemandCategoryActivity.this,
                            VideoDetailActivity.class).
                            putExtra(Constants.SELECTED_ON_DEMAND_VIDEO, onDemandVideo));
                }

            } else {
                showSubscriptionAlert();
            }*/




            if(onDemandVideo.getCategory().equalsIgnoreCase("Web Series")){

                ContentInfo contentInfo = new ContentInfo();
                contentInfo.setSubtitle(onDemandVideo.getSubtitle());
                contentInfo.setTitle(String.valueOf(onDemandVideo.getTitle()));
                contentInfo.setBannerImage(onDemandVideo.getThumb());
                contentInfo.setThumb(onDemandVideo.getThumb());
                contentInfo.setAvgRate(onDemandVideo.getAvg_rate());
                contentInfo.setIsWatchLater(onDemandVideo.isWatchList());
                contentInfo.setIsRated(onDemandVideo.isRated());
                contentInfo.setTotalUserRated(Integer.parseInt(onDemandVideo.getTotal_user_rated()));
                contentInfo.setCategory(onDemandVideo.getCategory());
                contentInfo.setId(onDemandVideo.getId());

                ContentDetail content_detail = new ContentDetail();
                content_detail.setContentInfo(contentInfo);

                startActivity(new Intent(OnDemandCategoryActivity.this,
                        WebSeriesDetailActivity.class).
                        putExtra(Constants.SELECTED_ON_DEMAND_VIDEO, content_detail));

            }else {
                startActivity(new Intent(OnDemandCategoryActivity.this,
                        VideoDetailActivity.class).
                        putExtra(Constants.SELECTED_ON_DEMAND_VIDEO, onDemandVideo));

            }
        }
    }

    @Override
    public void onCategorySelected(int position, OnDemandCategory onDemandCategory) {

    }

    @Override
    public void onWatchLater(int i, OnDemandVideo onDemandVideo) {

    }

    /**
     * Update Subscription detail
     *
     * @param videoToOpen
     */
    private void getMySubscriptionDetail(final OnDemandVideo videoToOpen) {
        new SubscriptionService(OnDemandCategoryActivity.this).
                getMySubscriptionInfo(new SubscriptionService.onSubscriptionInfoListener() {
                    @Override
                    public void onSubscriptionInfo(MySubscription subscriptionInfo) {
                        if (subscriptionInfo != null) {
                            sharedPreferenceHandler.saveSubscription(subscriptionInfo);
                            mySubscription = sharedPreferenceHandler.getMySubscriptionDetail();

                            if (videoToOpen != null && !subscriptionInfo.getRemainingDays().equalsIgnoreCase("0")) {
                                startActivity(new Intent(OnDemandCategoryActivity.this,
                                        VideoDetailActivity.class).
                                        putExtra(Constants.SELECTED_ON_DEMAND_VIDEO, videoToOpen));
                            }
                        }
                    }

                    @Override
                    public void onFailure(String error) {

                    }
                });
    }

    @Override
    protected void onResume() {
        super.onResume();
        isScreenVisible = true;
        new CompleteProfile(OnDemandCategoryActivity.this, new CompleteProfile.profileAlertListener() {
            @Override
            public void isAlertShowing(boolean isShowing) {

            }
        });
        MassTvApplication.getInstance().setConnectivityListener(this);

        try {
            MassTvApplication.getInstance().registerLocalReceiver(this);
        } catch (Exception e) {

            e.printStackTrace();
        }
    }

    @Override
    protected void onPause() {
        super.onPause();
        isScreenVisible = false;
        try {
            MassTvApplication.getInstance().setUnRegister();
        } catch (Exception e) {

        }

        try {
            MassTvApplication.getInstance().unRegisteredLocalReceiver();
        } catch (Exception e) {

        }
        stopBannerHandler();
    }

    @Override
    public void onNetworkConnectionChanged(boolean isConnected) {

        if (adapterVideoCategory.getItemCount() == 0 && isConnected && secondTime) {
            shimmer_view_container.startShimmer();
            shimmer_view_container.setVisibility(View.VISIBLE);
            getCategoryList();
        } else {
            secondTime = true;
            showSnack(isConnected);
        }

    }

    /**
     * Check internet connection Manually
     */
    private boolean checkConnection() {
        return ConnectivityReceiver.isConnected();
    }

    // Showing the status in Snackbar for intenet connectivity
    private void showSnack(boolean isConnected) {
        String message;
        int color;
        if (isConnected) {
            message = "Connection back Online!";
            color = Color.WHITE;
        } else {
            message = "Offline! Not connected to internet";
            color = Color.RED;


        }
        Snackbar snackbar = Snackbar
                .make(findViewById(R.id.parent), message, Snackbar.LENGTH_LONG);
        View sbView = snackbar.getView();
        TextView textView = (TextView) sbView.findViewById(R.id.snackbar_text);
        textView.setTextColor(color);

        if(!isConnected){
            snackbar.show();
        }


    }

    @Override
    public void onGetBroadCast(Intent intent) {
        updateNotificationCount();
    }

    private void updateNotificationCount() {
        runOnUiThread(new Runnable() {
            @Override
            public void run() {
                int count = sharedPreferenceHandler.getUnReadCount();
                bottomNavigationHelper.setNotification(count);
            }
        });
    }
}