package com.example.livestreamingapp.ondemand;

import android.content.Intent;
import android.graphics.Color;
import android.graphics.drawable.ColorDrawable;
import android.graphics.drawable.InsetDrawable;
import android.os.Bundle;
import android.util.Log;
import android.view.Menu;
import android.view.MenuItem;
import android.view.View;
import android.widget.Button;
import android.widget.TextView;

import androidx.annotation.NonNull;
import androidx.appcompat.app.AlertDialog;
import androidx.appcompat.widget.Toolbar;
import androidx.recyclerview.widget.GridLayoutManager;
import androidx.recyclerview.widget.RecyclerView;

import com.example.livestreamingapp.MassTvActivity;
import com.example.livestreamingapp.MassTvApplication;
import com.example.livestreamingapp.R;
import com.example.livestreamingapp.model.MySubscription;
import com.example.livestreamingapp.model.OnDemandCategory;
import com.example.livestreamingapp.model.OnDemandVideo;
import com.example.livestreamingapp.model.ProfileData;
import com.example.livestreamingapp.model.VideoListResponse;
import com.example.livestreamingapp.model.webseries.ContentDetail;
import com.example.livestreamingapp.model.webseries.ContentInfo;
import com.example.livestreamingapp.subscription.SubscriptionSelectorActivity;
import com.example.livestreamingapp.utils.ConnectivityReceiver;
import com.example.livestreamingapp.utils.Constants;
import com.example.livestreamingapp.utils.SharedPreferenceHandler;
import com.example.livestreamingapp.utils.SpacesItemDecoration;
import com.example.livestreamingapp.video.VideoDetailActivity;
import com.example.livestreamingapp.webseries.WebSeriesDetailActivity;
import com.example.livestreamingapp.webservice.APIClient;
import com.example.livestreamingapp.webservice.APIInterface;
import com.example.livestreamingapp.webservice.GetOnDemandVideoList;
import com.example.livestreamingapp.webservice.SubscriptionService;
import com.example.livestreamingapp.webservice.WebServiceConstant;
import com.facebook.shimmer.ShimmerFrameLayout;
import com.google.android.gms.cast.framework.CastButtonFactory;
import com.google.android.gms.cast.framework.CastContext;
import com.google.android.material.snackbar.Snackbar;
import com.google.gson.Gson;


import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;

public class VideoListActivity extends MassTvActivity implements View.OnClickListener,
        AdapterViewClickListener, ConnectivityReceiver.ConnectivityReceiverListener {

    Toolbar mToolbar;
    private RecyclerView rv_video_list;
    private CastContext mCastContext;
    private MenuItem mediaRouteMenuItem;
    private TextView tv_toolbar_title;
    private AdapterGridVideoList adapterVideoOnDemand;
    private ShimmerFrameLayout shimmer_view_container;

    private MySubscription mySubscription;
    private SharedPreferenceHandler sharedPreferenceHandler;

    private int INDEX_SIZE = 0;
    private List<OnDemandVideo> videosList;
    private String category;
    private ProfileData profileData;
    private boolean watchLater;

    public static boolean isWatchListScreen;
    private boolean notfound;
    private APIInterface apiInterface;
    private boolean connectionAgain;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_on_demand_video_list);
        apiInterface = APIClient.getClient().create(APIInterface.class);

        category = getIntent().getStringExtra(Constants.CATEGORY_TYPE);
       /* if (category != null) {
            tv_toolbar_title.setText(category);
        }*/
        profileData = new SharedPreferenceHandler(this).getProfileInfo();
        watchLater = getIntent().getBooleanExtra("WatchLater", false);
        isWatchListScreen = watchLater;

        tv_toolbar_title = findViewById(R.id.tv_toolbar_title);

        setupActionBar(category);
        initViews();
        mCastContext = CastContext.getSharedInstance(this);

        getVideoList(INDEX_SIZE);

    }

    @Override
    protected void onStop() {
        super.onStop();
        isWatchListScreen = false;
    }

    private void setupActionBar(String title) {
        mToolbar = (Toolbar) findViewById(R.id.toolbar);
        //  mToolbar.setTitle(""+title);
        mToolbar.setTitle("");
        setSupportActionBar(mToolbar);
        tv_toolbar_title.setText("" + title);

        if (getSupportActionBar() != null) {
            getSupportActionBar().setDisplayHomeAsUpEnabled(true);
        }

    }

    @Override
    protected void onStart() {
        super.onStart();
        isWatchListScreen = watchLater;
    }

    @Override
    public boolean onOptionsItemSelected(@NonNull MenuItem item) {

        switch (item.getItemId()) {
            case android.R.id.home:
                // todo: goto back activity from here
                finish();
                return true;

            default:
                return super.onOptionsItemSelected(item);
        }
    }

    @Override
    protected void onResume() {
        super.onResume();
        MassTvApplication.getInstance().setConnectivityListener(this);

    }

    @Override
    protected void onPause() {
        super.onPause();
        try {
            MassTvApplication.getInstance().setUnRegister();
        } catch (Exception e) {

        }
    }

    public boolean isTablet() {
       /* return (getResources().getConfiguration().screenLayout
                & Configuration.SCREENLAYOUT_SIZE_MASK)
                >= Configuration.SCREENLAYOUT_SIZE_LARGE;*/

        return getResources().getBoolean(R.bool.isTablet);

    }

    public void initViews() {

        profileData = new SharedPreferenceHandler(this).getProfileInfo();
        rv_video_list = (RecyclerView) findViewById(R.id.rv_video_list);
        shimmer_view_container = findViewById(R.id.shimmer_view_container);
        // set adapterVideoOnDemand Adapter for recyclerView
        GridLayoutManager layoutManagerDemand;

        if (isTablet()) {

            layoutManagerDemand = new GridLayoutManager(this, 3);
        } else {

            layoutManagerDemand = new GridLayoutManager(this, 2);
        }

        adapterVideoOnDemand = new AdapterGridVideoList(this, new ArrayList<OnDemandVideo>(), this);
        rv_video_list.setLayoutManager(layoutManagerDemand);
        rv_video_list.addItemDecoration(new SpacesItemDecoration(16));
        rv_video_list.setAdapter(adapterVideoOnDemand);
        shimmer_view_container.startShimmer();
        videosList = new ArrayList<>();
        rv_video_list.addOnScrollListener(new RecyclerView.OnScrollListener() {
            @Override
            public void onScrolled(@NonNull RecyclerView recyclerView, int dx, int dy) {
                super.onScrolled(recyclerView, dx, dy);
                if (!watchLater) {
                    int lastItem = layoutManagerDemand.findLastVisibleItemPosition();
                    if (INDEX_SIZE - 1 == lastItem && INDEX_SIZE % 15 == 0) {
                        Log.e("chec size ", "" + INDEX_SIZE);
                        if (checkConnection()) {
                            getVideoList(INDEX_SIZE);
                        } else {
                            showSnack(false);
                        }
                    }
                }

            }
        });



    }

    private void getVideoList(int index_size) {

        if (watchLater) {
          /*  new GetOnDemandVideoList(this, new GetOnDemandVideoList.onDemandVideoListListener() {
                @Override
                public void onDemandFetchedVideoList(List<OnDemandVideo> videoData) {

                    hideShimmer();



                    //  INDEX_SIZE = videosList.size();


                }

                @Override
                public void onDemandFetchingError(String error) {
                    hideShimmer();
                }
            }).
                    GetWatchList();*/

            ProfileData profileData = new SharedPreferenceHandler(this).getProfileInfo();
            Map<String, String> categoryMap = new HashMap<>();
            categoryMap.put(WebServiceConstant.USER_ID, "" + profileData.getId());
            final Call<VideoListResponse> videoListCall = apiInterface.getWatchListVideos(categoryMap);
            videoListCall.enqueue(new Callback<VideoListResponse>() {
                @Override
                public void onResponse(Call<VideoListResponse> call, Response<VideoListResponse> response) {
                    if (response != null && response.body() != null && response.body().getSuccess()) {
                        String jsonString = new Gson().toJson(response.body());
                        VideoListResponse videoListResponse = (VideoListResponse) new Gson().fromJson(jsonString,
                                VideoListResponse.class);
                        hideShimmer();
                        videosList.clear();
                        videosList = new ArrayList<>();

                        for (int i = 0; i < videoListResponse.getData().size(); i++) {
                            videoListResponse.getData().get(i).setWatchList(true);
                            videosList.add(i, videoListResponse.getData().get(i));
                        }
                        adapterVideoOnDemand = new AdapterGridVideoList(VideoListActivity.this,
                                videosList, VideoListActivity.this);
                        rv_video_list.setAdapter(adapterVideoOnDemand);


                    } else {
                       /* if(response!=null && response.body()!=null && response.body().getMessage()!=null){
                            onDemandVideoListListener.onDemandFetchingError(response.body().getMessage());
                        }else {
                            onDemandVideoListListener.onDemandFetchingError("SomeThing went wrong! Please try again!");

                        }*/

                        hideShimmer();
                    }

                }

                @Override
                public void onFailure(Call<VideoListResponse> call, Throwable t) {

                    hideShimmer();
                }
            });
        } else {
            new GetOnDemandVideoList(this, new GetOnDemandVideoList.onDemandVideoListListener() {
                @Override
                public void onDemandFetchedVideoList(List<OnDemandVideo> videoData) {

                    hideShimmer();

                    if (videoData.size() > 0) {

                        if (INDEX_SIZE == 0) {

                            videosList.clear();
                            videosList = new ArrayList<>();

                            videosList.addAll(videoData);
                            adapterVideoOnDemand = new AdapterGridVideoList(VideoListActivity.this,
                                    videosList, VideoListActivity.this);
                            rv_video_list.setAdapter(adapterVideoOnDemand);
                            INDEX_SIZE = videosList.size();


                        } else {

                            if (!videosList.get(videosList.size() - 1).getId().equals(videoData.get(videoData.size() - 1).getId())) {
                                INDEX_SIZE = INDEX_SIZE + 15;
                                videosList.addAll(videoData);
                                adapterVideoOnDemand.addData(videosList);
                                adapterVideoOnDemand.notifyItemInserted(INDEX_SIZE);
                            }
                        }
                    }
                }

                @Override
                public void onDemandFetchingError(String error) {
                    hideShimmer();
                }
            }).
                    GetVideoList("" + category, INDEX_SIZE, profileData.getId());
        }


    }

    private void hideShimmer() {

        shimmer_view_container.hideShimmer();
        shimmer_view_container.setVisibility(View.GONE);
        rv_video_list.setVisibility(View.VISIBLE);

    }

    @Override
    public void onVideoSelected(int position, OnDemandVideo onDemandVideo) {

        /**
         * this below logic for checking user has subscription or not
         *  uncomment this logic when subscription is required
         */
     /*   if (mySubscription != null && Integer.parseInt(mySubscription.getRemainingDays()) != 0) {
            if (!mySubscription.getLastRefreshDate().equalsIgnoreCase(DateCalculation.getCurrentDateInDDMMYY())) {
                getMySubscriptionDetail(onDemandVideo);
            } else {
                startActivity(new Intent(VideoListActivity.this,
                        VideoDetailActivity.class).
                        putExtra(Constants.SELECTED_ON_DEMAND_VIDEO, onDemandVideo));
            }

        } else {
            showSubscriptionAlert();
        }*/

        if (onDemandVideo.getCategory().equalsIgnoreCase("Web Series")) {

            ContentInfo contentInfo = new ContentInfo();
            contentInfo.setSubtitle(onDemandVideo.getSubtitle());
            contentInfo.setTitle(String.valueOf(onDemandVideo.getTitle()));
            contentInfo.setBannerImage(onDemandVideo.getThumb());
            contentInfo.setThumb(onDemandVideo.getThumb());
            contentInfo.setAvgRate(onDemandVideo.getAvg_rate());
            contentInfo.setIsWatchLater(onDemandVideo.isWatchList());
            contentInfo.setIsRated(onDemandVideo.isRated());
            contentInfo.setTotalUserRated(Integer.parseInt(onDemandVideo.getTotal_user_rated()));
            contentInfo.setCategory(onDemandVideo.getCategory());
            contentInfo.setId(onDemandVideo.getId());

            ContentDetail content_detail = new ContentDetail();
            content_detail.setContentInfo(contentInfo);

            startActivity(new Intent(VideoListActivity.this,
                    WebSeriesDetailActivity.class).
                    putExtra(Constants.SELECTED_ON_DEMAND_VIDEO, content_detail));

        } else {
            startActivity(new Intent(VideoListActivity.this,
                    VideoDetailActivity.class).setFlags(Intent.FLAG_ACTIVITY_NO_HISTORY).
                    putExtra(Constants.SELECTED_ON_DEMAND_VIDEO, onDemandVideo));


        }


    }

    @Override
    public void onCategorySelected(int position, OnDemandCategory onDemandCategory) {

    }

    @Override
    public void onWatchLater(int i, OnDemandVideo onDemandVideo) {

        if (watchLater) {

            videosList.remove(i);
            adapterVideoOnDemand = new AdapterGridVideoList(this, videosList, this);
            adapterVideoOnDemand.notifyDataSetChanged();
        } else {
         /*   videosList.clear();
            videosList =new ArrayList<>();
            videosList = adapterVideoOnDemand.getVideoList();
            adapterVideoOnDemand = new AdapterGridVideoList(this,videosList,this);*/
            adapterVideoOnDemand.notifyDataSetChanged();
        }

    }

    /**
     * Update Subscription detail
     *
     * @param videoToOpen
     */
    private void getMySubscriptionDetail(final OnDemandVideo videoToOpen) {
        new SubscriptionService(VideoListActivity.this).
                getMySubscriptionInfo(new SubscriptionService.onSubscriptionInfoListener() {
                    @Override
                    public void onSubscriptionInfo(MySubscription subscriptionInfo) {
                        if (subscriptionInfo != null) {
                            sharedPreferenceHandler.saveSubscription(subscriptionInfo);
                            mySubscription = sharedPreferenceHandler.getMySubscriptionDetail();

                            if (videoToOpen != null && !subscriptionInfo.getRemainingDays().equalsIgnoreCase("0")) {
                                startActivity(new Intent(VideoListActivity.this,
                                        VideoDetailActivity.class).
                                        putExtra(Constants.SELECTED_ON_DEMAND_VIDEO, videoToOpen));
                            }
                        }
                    }

                    @Override
                    public void onFailure(String error) {

                    }
                });
    }


    public void showSubscriptionAlert() {
        // Create an alert builder
        AlertDialog.Builder builder
                = new AlertDialog.Builder(this);
        // set the custom layout
        final View customLayout
                = getLayoutInflater()
                .inflate(
                        R.layout.layout_alert_subscribe,
                        null);
        builder.setView(customLayout);

        Button btn_subscribe = customLayout.findViewById(R.id.btn_subscribe);

        final AlertDialog dialog
                = builder.create();

        dialog.getWindow().setBackgroundDrawableResource(android.R.color.transparent);


        ColorDrawable back = new ColorDrawable(Color.TRANSPARENT);
        InsetDrawable inset = new InsetDrawable(back, 40, 0, 40, 0);
        if (dialog.getWindow() != null) {
            dialog.getWindow().setBackgroundDrawable(inset);

        }

        btn_subscribe.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                dialog.dismiss();
                startActivity(new Intent(VideoListActivity.this, SubscriptionSelectorActivity.class));
            }
        });

        // create and show
        // the alert dialog

        dialog.show();
    }

    @Override
    public void onClick(View v) {
        switch (v.getId()) {
            case R.id.iv_back:
                finish();
                break;
        }
    }


    @Override
    public boolean onCreateOptionsMenu(Menu menu) {
        // Inflate the menu; this adds items to the action bar if it is present.
        getMenuInflater().inflate(R.menu.menu_cast, menu);
        mediaRouteMenuItem = CastButtonFactory.setUpMediaRouteButton(getApplicationContext(), menu,
                R.id.media_route_menu_item);
        return true;
    }

    @Override
    public void onNetworkConnectionChanged(boolean isConnected) {
        if ((videosList == null || videosList.size() == 0) && isConnected && connectionAgain) {
            connectionAgain  = false;
            getVideoList(INDEX_SIZE);
        } else {
            connectionAgain = true;
            showSnack(isConnected);
        }
    }

    /**
     * Check internet connection Manually
     */
    private boolean checkConnection() {
        return ConnectivityReceiver.isConnected();
    }

    // Showing the status in Snackbar for intenet connectivity
    private void showSnack(boolean isConnected) {
        String message;
        int color;
        if (isConnected) {
            message = "Connection back Online!";
            color = Color.WHITE;
        } else {
            message = "Offline! Not connected to internet";
            color = Color.RED;


        }
        Snackbar snackbar = Snackbar
                .make(findViewById(R.id.parent), message, Snackbar.LENGTH_LONG);
        View sbView = snackbar.getView();
        TextView textView = (TextView) sbView.findViewById(R.id.snackbar_text);
        textView.setTextColor(color);
        snackbar.show();

    }
}