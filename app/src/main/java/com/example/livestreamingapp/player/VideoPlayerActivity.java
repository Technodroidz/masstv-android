package com.example.livestreamingapp.player;

import android.content.Intent;
import android.net.Uri;
import android.os.Bundle;
import android.os.Handler;
import android.util.Log;
import android.view.MotionEvent;
import android.view.View;
import android.view.Window;
import android.view.WindowManager;
import android.widget.FrameLayout;
import android.widget.ImageView;
import android.widget.ProgressBar;
import android.widget.SeekBar;
import android.widget.TextView;

import androidx.appcompat.widget.AppCompatSeekBar;

import com.example.livestreamingapp.MassTvActivity;
import com.example.livestreamingapp.MediaItem;
import com.example.livestreamingapp.R;
import com.example.livestreamingapp.expandedcontrols.ExpandedControlsActivity;
import com.example.livestreamingapp.model.OnDemandVideo;
import com.example.livestreamingapp.utils.Constants;
import com.example.livestreamingapp.utils.Utils;
import com.google.android.exoplayer2.DefaultLoadControl;
import com.google.android.exoplayer2.ExoPlaybackException;
import com.google.android.exoplayer2.ExoPlayerFactory;
import com.google.android.exoplayer2.LoadControl;
import com.google.android.exoplayer2.PlaybackParameters;
import com.google.android.exoplayer2.SimpleExoPlayer;
import com.google.android.exoplayer2.Timeline;
import com.google.android.exoplayer2.extractor.DefaultExtractorsFactory;
import com.google.android.exoplayer2.extractor.ExtractorsFactory;
import com.google.android.exoplayer2.source.ExtractorMediaSource;
import com.google.android.exoplayer2.source.MediaSource;
import com.google.android.exoplayer2.source.TrackGroupArray;
import com.google.android.exoplayer2.trackselection.DefaultTrackSelector;
import com.google.android.exoplayer2.trackselection.TrackSelectionArray;
import com.google.android.exoplayer2.ui.AspectRatioFrameLayout;
import com.google.android.exoplayer2.ui.PlayerView;
import com.google.android.exoplayer2.upstream.BandwidthMeter;
import com.google.android.exoplayer2.upstream.DataSource;
import com.google.android.exoplayer2.upstream.DefaultAllocator;
import com.google.android.exoplayer2.upstream.DefaultBandwidthMeter;
import com.google.android.exoplayer2.upstream.DefaultDataSourceFactory;
import com.google.android.exoplayer2.util.Util;
import com.google.android.gms.cast.MediaInfo;
import com.google.android.gms.cast.MediaLoadRequestData;
import com.google.android.gms.cast.MediaMetadata;
import com.google.android.gms.cast.framework.CastContext;
import com.google.android.gms.cast.framework.CastSession;
import com.google.android.gms.cast.framework.SessionManagerListener;
import com.google.android.gms.cast.framework.media.RemoteMediaClient;

import java.util.Timer;
import java.util.TimerTask;




public class VideoPlayerActivity extends MassTvActivity implements
        SimpleExoPlayer.EventListener, View.OnClickListener {

    private static final String TAG = VideoPlayerActivity.class.getName();
    private SimpleExoPlayer player;
    private PlayerView simpleExoPlayerView;
    private ImageView iv_play_pause;
    FrameLayout fl_info_layout;
    private int playbackPosition = 0;
    AppCompatSeekBar mSeekbar;
    boolean isVideoPlaying;
    private PlaybackState mPlaybackState;
    private Timer mSeekbarTimer;
    private Timer mControllersTimer;
    private PlaybackLocation mLocation;
    private Handler mHandler;
    private CastSession mCastSession;
    private SessionManagerListener<CastSession> mSessionManagerListener;
    private MediaItem mSelectedMedia;
    private CastContext mCastContext;

    public static final int DEFAULT_MIN_BUFFER_MS = 10000;
    public static final int DEFAULT_MAX_BUFFER_MS = 60000;
    public static final int DEFAULT_BUFFER_FOR_PLAYBACK_MS = 5000;
    public static final int DEFAULT_BUFFER_FOR_PLAYBACK_AFTER_REBUFFER_MS = 5000;

    OnDemandVideo onDemandVideo;
    private ProgressBar progressBar_buffering;

    Handler userInteractionHandler;
    Runnable userInteractionRunnable;

    private TextView tv_title;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        this.requestWindowFeature(Window.FEATURE_NO_TITLE); //Remove title bar
        setContentView(R.layout.activity_video_player);
        this.getWindow().setFlags(WindowManager.LayoutParams.FLAG_FULLSCREEN, WindowManager.LayoutParams.FLAG_FULLSCREEN);
        getWindow().addFlags(WindowManager.LayoutParams.FLAG_KEEP_SCREEN_ON);

        setupCastListener();
        mCastContext = CastContext.getSharedInstance(this);
        mCastSession = mCastContext.getSessionManager().getCurrentCastSession();
        onDemandVideo = (OnDemandVideo) getIntent().getSerializableExtra(Constants.SELECTED_ON_DEMAND_VIDEO);


        mSelectedMedia = new  MediaItem();
        mSelectedMedia.setUrl(onDemandVideo.getSource());
        mSelectedMedia.setTitle(""+onDemandVideo.getTitle());
        mSelectedMedia.setSubTitle(""+onDemandVideo.getSubtitle());

        mHandler = new Handler();
        initViews();
        initializePlayer();
        mPlaybackState = PlaybackState.IDLE;
        updatePlayButton(mPlaybackState);
        if (mCastSession != null && mCastSession.isConnected()) {
            updatePlaybackLocation(PlaybackLocation.REMOTE);
        } else {
            updatePlaybackLocation(PlaybackLocation.LOCAL);
            play(mSeekbar.getProgress()*1000);
        }


    }

    public void initViews(){
        simpleExoPlayerView = findViewById(R.id.exoplayerView);
        progressBar_buffering = findViewById(R.id.progressbar_buffering);
        simpleExoPlayerView.setResizeMode(AspectRatioFrameLayout.RESIZE_MODE_FIT);

        mSeekbar = findViewById(R.id.progressBar);
        //  mSeekbar.setProgressDrawable(getResources().getDrawable(R.drawable.shape_seekbar));

        fl_info_layout = (FrameLayout)findViewById(R.id.fl_info_layout);
        iv_play_pause = (ImageView) findViewById(R.id.iv_play_pause);
        tv_title = (TextView)findViewById(R.id.tv_title);
        tv_title.setText(""+onDemandVideo.getTitle() +" "+ onDemandVideo.getSubtitle());

        iv_play_pause.setVisibility(View.VISIBLE);
        iv_play_pause.setOnClickListener(this);

        fl_info_layout.setOnClickListener(this);


        simpleExoPlayerView.setOnTouchListener(new View.OnTouchListener() {
            @Override
            public boolean onTouch(View v, MotionEvent event) {
                fl_info_layout.setVisibility(View.VISIBLE);
                startUserInteractionHandler();
                return false;
            }
        });


        mSeekbar.setOnSeekBarChangeListener(new SeekBar.OnSeekBarChangeListener() {

            @Override
            public void onStopTrackingTouch(SeekBar seekBar) {
               /* if (mPlaybackState == PlaybackState.PLAYING) {
                    play(seekBar.getProgress()*1000);
                } else if (mPlaybackState != PlaybackState.IDLE) {
                    player.seekTo(seekBar.getProgress()*1000);
                }*/

                mPlaybackState = PlaybackState.PLAYING;
                play(seekBar.getProgress()*1000);

                startControllersTimer();
            }

            @Override
            public void onStartTrackingTouch(SeekBar seekBar) {
                stopTrickplayTimer();
                player.setPlayWhenReady(false);
                mPlaybackState = PlaybackState.PAUSED;
                stopControllersTimer();
            }

            @Override
            public void onProgressChanged(SeekBar seekBar, int progress,
                                          boolean fromUser) {
                //  mStartText.setText(Utils.formatMillis(progress));
            }
        });


        // hide control after 3 second
        userInteractionHandler = new Handler();
        userInteractionRunnable = new Runnable() {
            @Override
            public void run() {
                if (mPlaybackState == PlaybackState.PLAYING) {
                    fl_info_layout.setVisibility(View.GONE);
                }

            }
        };

    }

    /**
     * indicates whether we are doing a local or a remote playback
     */
    public enum PlaybackLocation {
        LOCAL,
        REMOTE
    }

    /**
     * List of various states that we can be in
     */
    public enum PlaybackState {
        PLAYING, PAUSED, BUFFERING, IDLE,END;
    }

    private void play(int position) {
        startControllersTimer();
        switch (mLocation) {
            case LOCAL:
                player.seekTo(position);
                playMedia();
                mSeekbar.setVisibility(View.VISIBLE);
                break;
            case REMOTE:
                mPlaybackState = PlaybackState.BUFFERING;
                updatePlayButton(mPlaybackState);
                mCastSession.getRemoteMediaClient().seek(position);
                mSeekbar.setVisibility(View.GONE);
                break;
            default:
                break;
        }
        restartTrickplayTimer();
    }

    private void updatePlayButton(PlaybackState state) {
        Log.d(TAG, "Controls: PlayBackState: " + state);
        boolean isConnected = (mCastSession != null)
                && (mCastSession.isConnected() || mCastSession.isConnecting());

        switch (state) {
            case PLAYING:
                fl_info_layout.setVisibility(View.INVISIBLE);
                hideBuffering();
                // playMedia();
                break;
            case IDLE:
                // pauseMediaPlayer();
                fl_info_layout.setVisibility(View.VISIBLE);
                break;
            case PAUSED:
                fl_info_layout.setVisibility(View.VISIBLE);
                break;
            case BUFFERING:
                //  playMedia();
                // mLoading.setVisibility(View.VISIBLE);
                showBuffering();
                break;

            case END:
                pauseMediaPlayer();
                //  playMedia();
                // mLoading.setVisibility(View.VISIBLE);
                break;
            default:
                break;
        }
    }


    private void stopTrickplayTimer() {
        Log.d(TAG, "Stopped TrickPlay Timer");
        if (mSeekbarTimer != null) {
            mSeekbarTimer.cancel();
        }
    }

    private void restartTrickplayTimer() {
        stopTrickplayTimer();
        mSeekbarTimer = new Timer();
        mSeekbarTimer.scheduleAtFixedRate(new UpdateSeekbarTask(), 100, 1000);
        Log.d(TAG, "Restarted TrickPlay Timer");
    }

    private class UpdateSeekbarTask extends TimerTask {

        @Override
        public void run() {
            mHandler.post(new Runnable() {

                @Override
                public void run() {
                    if (mLocation == PlaybackLocation.LOCAL) {
                        int currentPos = (int) player.getCurrentPosition()/1000;
                        updateSeekbar(currentPos, (int)player.getDuration()/1000);
                    }
                }
            });
        }
    }



    private void updateSeekbar(int position, int duration) {
        mSeekbar.setProgress(position);
        mSeekbar.setMax(duration);

        mSeekbar.setProgress(position);
        mSeekbar.setMax(duration);

    }

    private void stopControllersTimer() {
        if (mControllersTimer != null) {
            mControllersTimer.cancel();
        }
    }

    private void startControllersTimer() {
        if (mControllersTimer != null) {
            mControllersTimer.cancel();
        }
        if (mLocation  == PlaybackLocation.REMOTE) {
            return;
        }
        mControllersTimer = new Timer();
        mControllersTimer.schedule(new HideControllersTask(), 5000);
    }

    private class HideControllersTask extends TimerTask {

        @Override
        public void run() {
            mHandler.post(new Runnable() {
                @Override
                public void run() {
              /*      updateControllersVisibility(false);
                    mControllersVisible = false;*/
                }
            });

        }
    }

    @Override
    protected void onPause() {
        super.onPause();
        isVideoPlaying = false;
        if(player!=null){
            player.setPlayWhenReady(false);

        }
        releaseAllHandler();


    }

    private void pauseMediaPlayer() {
        if(player!=null ){
            mPlaybackState = PlaybackState.PAUSED;
            iv_play_pause.setImageDrawable(getResources().getDrawable(R.drawable.ic_play_video_36));
            fl_info_layout.setVisibility(View.VISIBLE);
            player.setPlayWhenReady(false);
        }
    }

    private void initializePlayer(){
// Create a default TrackSelector
        BandwidthMeter bandwidthMeter = new DefaultBandwidthMeter();
      /*  TrackSelection.Factory videoTrackSelectionFactory =
                new AdaptiveTrackSelection.Factory(bandwidthMeter);
        TrackSelector trackSelector =
                new DefaultTrackSelector(videoTrackSelectionFactory);

        DefaultLoadControl loadControl = new DefaultLoadControl(
                new DefaultAllocator(true, C.DEFAULT_BUFFER_SEGMENT_SIZE),
                DEFAULT_MIN_BUFFER_MS
                ,DEFAULT_MAX_BUFFER_MS,
                DEFAULT_BUFFER_FOR_PLAYBACK_MS,
                DEFAULT_BUFFER_FOR_PLAYBACK_AFTER_REBUFFER_MS,
                DEFAULT_MAX_BUFFER_MS,
                true
        );*/

        LoadControl loadControl = new DefaultLoadControl.Builder()
                .setAllocator(new DefaultAllocator(true, 16))
                .setBufferDurationsMs( DEFAULT_MIN_BUFFER_MS,
                        DEFAULT_MAX_BUFFER_MS,
                        DEFAULT_BUFFER_FOR_PLAYBACK_MS,
                        DEFAULT_BUFFER_FOR_PLAYBACK_AFTER_REBUFFER_MS)
                .setTargetBufferBytes(-1)
                .setPrioritizeTimeOverSizeThresholds(true).createDefaultLoadControl();

//Initialize the player
        player = ExoPlayerFactory.newSimpleInstance(this, new DefaultTrackSelector(),loadControl);


//Initialize simpleExoPlayerView
        simpleExoPlayerView.setPlayer(player);

        simpleExoPlayerView.setUseController(false);

// Produces DataSource instances through which media data is loaded.
        DataSource.Factory dataSourceFactory =
                new DefaultDataSourceFactory(this, Util.getUserAgent(this, "Video Player"));

// Produces Extractor instances for parsing the media data.
        ExtractorsFactory extractorsFactory = new DefaultExtractorsFactory();

// This is the MediaSource representing the media to be played.
        Uri videoUri = Uri.parse(onDemandVideo.getSource());
        MediaSource videoSource = new ExtractorMediaSource(videoUri,
                dataSourceFactory, extractorsFactory, null, null);

// Prepare the player with the source.
        player.prepare(videoSource);

        player.addListener(this);

        simpleExoPlayerView.setResizeMode(AspectRatioFrameLayout.RESIZE_MODE_FILL);
        //  player.setVideoScalingMode(C.VIDEO_SCALING_MODE_SCALE_TO_FIT_WITH_CROPPING);


        if(player!=null){
            int max = (int)player.getDuration()/1000;

            //  progressBar.setMax(max);
        }

    }


    @Override
    public void onTimelineChanged(Timeline timeline, Object manifest, int reason) {

    }

    @Override
    public void onTracksChanged(TrackGroupArray trackGroups, TrackSelectionArray trackSelections) {

    }

    @Override
    public void onLoadingChanged(boolean isLoading) {

    }

    @Override
    public void onPlayerStateChanged(boolean playWhenReady, int playbackState) {

        if(playbackState == android.media.session.PlaybackState.STATE_PLAYING){
            mPlaybackState = PlaybackState.PLAYING;
            startUserInteractionHandler();
            //  play(mSeekbar.getProgress());
        }else if(playbackState == android.media.session.PlaybackState.STATE_PAUSED) {
            //  mPlaybackState = PlaybackState.PAUSED;
        }else if(playbackState ==android.media.session.PlaybackState.STATE_BUFFERING ){
            mPlaybackState = PlaybackState.BUFFERING;
            showBuffering();
        }else if(playbackState == 4){
            mPlaybackState = PlaybackState.END;
        }

        // updatePlayButton(mPlaybackState);
    }

    @Override
    public void onRepeatModeChanged(int repeatMode) {

    }

    @Override
    public void onShuffleModeEnabledChanged(boolean shuffleModeEnabled) {

    }

    @Override
    public void onPlayerError(ExoPlaybackException error) {


        Utils.showErrorDialog(VideoPlayerActivity.this, "Please Check your internet connection!");
        mPlaybackState = PlaybackState.IDLE;
        pauseMediaPlayer();

    }

    @Override
    public void onPositionDiscontinuity(int reason) {

    }

    @Override
    public void onPlaybackParametersChanged(PlaybackParameters playbackParameters) {


    }

    @Override
    public void onSeekProcessed() {

    }

    @Override
    public void onClick(View v) {
        switch (v.getId()){
            case R.id.iv_play_pause:
                if(mPlaybackState != PlaybackState.PLAYING){
                    play(mSeekbar.getProgress()*1000);
                }else {

                    pauseMediaPlayer();

                }
                break;

            case R.id.fl_info_layout:

                if(mPlaybackState == PlaybackState.PLAYING){
                    fl_info_layout.setVisibility(View.GONE);
                }
                // pauserMediaPlayer();
                break;
        }
    }



    /**
     * Play media player and change icon to indicate it is play
     * Handle both landscape and portrait mode
     */
    private void playMedia() {
        iv_play_pause.setImageDrawable(getResources().getDrawable(R.drawable.ic_pause_video_24));

        if (player != null) {
            mPlaybackState = PlaybackState.PLAYING;
            player.setPlayWhenReady(true);
            startUserInteractionHandler();
        }

    }


    // listener for casting
    private void setupCastListener() {
        mSessionManagerListener = new SessionManagerListener<CastSession>() {

            @Override
            public void onSessionEnded(CastSession session, int error) {
                onApplicationDisconnected();
            }

            @Override
            public void onSessionResumed(CastSession session, boolean wasSuspended) {
                onApplicationConnected(session);
            }

            @Override
            public void onSessionResumeFailed(CastSession session, int error) {
                onApplicationDisconnected();
            }

            @Override
            public void onSessionStarted(CastSession session, String sessionId) {
                onApplicationConnected(session);
            }

            @Override
            public void onSessionStartFailed(CastSession session, int error) {
                onApplicationDisconnected();
            }

            @Override
            public void onSessionStarting(CastSession session) {
            }

            @Override
            public void onSessionEnding(CastSession session) {
            }

            @Override
            public void onSessionResuming(CastSession session, String sessionId) {
            }

            @Override
            public void onSessionSuspended(CastSession session, int reason) {
            }

            private void onApplicationConnected(CastSession castSession) {
                mCastSession = castSession;
                if (null != mSelectedMedia) {

                    if (mPlaybackState == PlaybackState.PLAYING) {
                        playMedia();
                        loadRemoteMedia(mSeekbar.getProgress(), true);
                        return;
                    } else {
                        mPlaybackState = PlaybackState.IDLE;
                        updatePlaybackLocation(PlaybackLocation.REMOTE);
                    }
                }
                updatePlayButton(mPlaybackState);
                invalidateOptionsMenu();
            }

            private void onApplicationDisconnected() {
                updatePlaybackLocation(PlaybackLocation.LOCAL);
                mPlaybackState = PlaybackState.IDLE;
                mLocation = PlaybackLocation.LOCAL;
                updatePlayButton(mPlaybackState);
                invalidateOptionsMenu();
            }
        };
    }


    private void updatePlaybackLocation(PlaybackLocation location) {
        mLocation = location;
        if (location == PlaybackLocation.LOCAL) {
            if (mPlaybackState == PlaybackState.PLAYING
                    || mPlaybackState == PlaybackState.BUFFERING) {
                // setCoverArtStatus(null);
                startControllersTimer();
            } else {
                stopControllersTimer();
                //  setCoverArtStatus(mSelectedMedia.getImage(0));
            }
        } else {
            stopControllersTimer();
            // setCoverArtStatus(mSelectedMedia.getImage(0));
            fl_info_layout.setVisibility(View.GONE);
        }
    }


    private MediaInfo buildMediaInfo() {
        MediaMetadata movieMetadata = new MediaMetadata(MediaMetadata.MEDIA_TYPE_MOVIE);

        movieMetadata.putString(MediaMetadata.KEY_SUBTITLE, "Bunny");
        movieMetadata.putString(MediaMetadata.KEY_TITLE, mSelectedMedia.getTitle());
       /* movieMetadata.addImage(new WebImage(Uri.parse(mSelectedMedia.getImage(0))));
        movieMetadata.addImage(new WebImage(Uri.parse(mSelectedMedia.getImage(1))));
*/
        return new MediaInfo.Builder(mSelectedMedia.getUrl())
                .setStreamType(MediaInfo.STREAM_TYPE_BUFFERED)
                .setContentType("videos/mp4")
                .setMetadata(movieMetadata)
                .setStreamDuration(mSelectedMedia.getDuration() * 1000)
                .build();
    }
    private void loadRemoteMedia(int position, boolean autoPlay) {
        if (mCastSession == null) {
            return;
        }
        final RemoteMediaClient remoteMediaClient = mCastSession.getRemoteMediaClient();
        if (remoteMediaClient == null) {
            return;
        }
        remoteMediaClient.registerCallback(new RemoteMediaClient.Callback() {
            @Override
            public void onStatusUpdated() {
                Intent intent = new Intent(VideoPlayerActivity.this, ExpandedControlsActivity.class);
                startActivity(intent);
                remoteMediaClient.unregisterCallback(this);
            }
        });
        remoteMediaClient.load(new MediaLoadRequestData.Builder()
                .setMediaInfo(buildMediaInfo())
                .setAutoplay(autoPlay)
                .setCurrentTime(position).build());
    }


    private void showBuffering() {
        progressBar_buffering.setVisibility(View.VISIBLE);

    }

    private void hideBuffering() {
        progressBar_buffering.setVisibility(View.GONE);
    }

    /**
     * start the handler so that if we play any media and donot do any interaction
     * then i will hide after 3 second we can set any amount of time
     */
    public void startUserInteractionHandler() {
        try {
            userInteractionHandler.removeCallbacks(userInteractionRunnable);
        } catch (Exception e) {

        }
        if (userInteractionHandler != null) {
            userInteractionHandler.postDelayed(userInteractionRunnable, 3 * 1000);

        }

    }

    public void releaseAllHandler(){
        try {
            userInteractionHandler.removeCallbacks(userInteractionRunnable);
        } catch (Exception e) {

        }
    }

    @Override
    protected void onDestroy() {
        super.onDestroy();
        releaseAllHandler();
    }
}