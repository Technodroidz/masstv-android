package com.example.livestreamingapp.profile;

import android.app.ProgressDialog;
import android.content.DialogInterface;
import android.content.Intent;
import android.graphics.Color;
import android.graphics.drawable.ColorDrawable;
import android.graphics.drawable.InsetDrawable;
import android.os.Bundle;
import android.text.InputType;
import android.util.Log;
import android.view.Menu;
import android.view.MenuItem;
import android.view.View;
import android.widget.Button;
import android.widget.EditText;
import android.widget.ImageView;
import android.widget.RelativeLayout;
import android.widget.TextView;

import androidx.annotation.NonNull;
import androidx.appcompat.app.AlertDialog;
import androidx.appcompat.app.AppCompatActivity;
import androidx.appcompat.widget.Toolbar;

import com.example.livestreamingapp.MassTvActivity;
import com.example.livestreamingapp.R;
import com.example.livestreamingapp.model.APIError;
import com.example.livestreamingapp.model.BankAccountModel;
import com.example.livestreamingapp.model.CustomerModelStripe;
import com.example.livestreamingapp.model.LiveShowData;
import com.example.livestreamingapp.model.LoginResponse;
import com.example.livestreamingapp.model.ProfileData;
import com.example.livestreamingapp.ondemand.OnDemandCategoryActivity;
import com.example.livestreamingapp.subscription.PaymentActivity;
import com.example.livestreamingapp.utils.ConnectivityReceiver;
import com.example.livestreamingapp.utils.Constants;
import com.example.livestreamingapp.utils.ErrorUtils;
import com.example.livestreamingapp.utils.SharedPreferenceHandler;
import com.example.livestreamingapp.utils.SnackBar;
import com.example.livestreamingapp.webservice.APIClient;
import com.example.livestreamingapp.webservice.APIInterface;
import com.example.livestreamingapp.webservice.WebServiceConstant;
import com.google.android.gms.cast.framework.CastButtonFactory;
import com.google.android.gms.cast.framework.CastContext;
import com.google.gson.Gson;
import com.google.gson.annotations.SerializedName;
import com.stripe.android.ApiResultCallback;
import com.stripe.android.Stripe;
import com.stripe.android.model.BankAccount;
import com.stripe.android.model.BankAccountTokenParams;
import com.stripe.android.model.Token;
import com.stripe.stripeterminal.callable.ConnectionTokenCallback;
import com.stripe.stripeterminal.model.external.ConnectionTokenException;
import com.vikktorn.picker.Country;
import com.vikktorn.picker.CountryPicker;
import com.vikktorn.picker.OnCountryPickerListener;
import com.vikktorn.picker.State;
import com.vikktorn.picker.StatePicker;

import org.jetbrains.annotations.NotNull;

import java.util.HashMap;
import java.util.Map;
import java.util.Objects;

import javax.annotation.Nullable;

import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;

public class AccountActivity extends MassTvActivity implements View.OnClickListener, OnCountryPickerListener {

    private Toolbar mToolbar;

    private Button btn_proceed;
    private APIInterface apiInterface;
    private ProgressDialog progressDialog;
    private CastContext mCastContext;
    private MenuItem mediaRouteMenuItem;
    private ImageView iv_back;
    private TextView tv_progress;

    private RelativeLayout waitScreen;

    private EditText et_account_number;
    private EditText et_account_verify;
    private EditText et_holder_name;
    private EditText et_routing;
    private TextView tv_country;

    private boolean isEdit;
    private ProfileData profileData;
    private View view;
    private BankAccountModel accountDetail;
    private Stripe stripe;
    private EditText et_paypal;

    private CountryPicker countryPicker;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.layout_edit_account);
        setupActionBar();
        countryPicker = new CountryPicker.Builder().with(this).listener(this).build();

        mCastContext = CastContext.getSharedInstance(this);
        profileData = new SharedPreferenceHandler(this).getProfileInfo();
       /* stripe = new Stripe(
                getApplicationContext(),
                // Objects.requireNonNull("pk_live_51Hr4iOGRw0lHExl0MEIDzpaRhMvD60CHeMa41E72yMhYq0J1gjK7uLaTAteO0VpL4NnpRBFf7pPGBU8cDrrsZZoW00vdMSN1OE")
                Objects.requireNonNull("pk_test_51Hr4iOGRw0lHExl0m7rCaOItmtVivMoP1m6bpDArtTUZUKm9xbGBpEoBdI0q9kkgRHbtqhWB8GANn0ifc3OVeLgs001c04BF9R")
        );*/
        initViews();
    }

    /**
     *
     */
    private void initViews() {
        et_account_number = findViewById(R.id.et_account_number);
        et_account_verify = findViewById(R.id.et_verify_number);
        et_holder_name = findViewById(R.id.et_account_holder);
        et_routing = findViewById(R.id.et_routing);
        tv_country = findViewById(R.id.tv_country);

        iv_back = findViewById(R.id.iv_back);
        btn_proceed = findViewById(R.id.btn_proceed);
        tv_progress = findViewById(R.id.tv_progress);
        waitScreen = findViewById(R.id.waitScreen);
        et_paypal = findViewById(R.id.et_paypal);

        tv_progress.setVisibility(View.GONE);

        btn_proceed.setOnClickListener(this);
        iv_back.setOnClickListener(this);

        apiInterface = APIClient.getClient().create(APIInterface.class);
        tv_country.setOnClickListener(this);
        if (profileData.getPaypal_id() != null) {
            et_paypal.setText(profileData.getPaypal_id());
        }
       /* if (profileData.getPaypal_id() != null && !profileData.getPaypal_id().isEmpty()) {
            hideEdit();
        } else {
            showEdit();
        }*/
        showEdit();
        //  getBankDetail();
    }

    private void setupActionBar() {
        mToolbar = (Toolbar) findViewById(R.id.toolbar);
        mToolbar.setTitle("");
        setSupportActionBar(mToolbar);
    }

    @Override
    public void onClick(View v) {
        switch (v.getId()) {
            case R.id.btn_proceed:

                if (isEdit) {
                    // perform update
                    if (checkConnection()) {
                        if (isBankDetailValid()) {
                            updateBankDetail();
                        }
                    } else {
                        new SnackBar(AccountActivity.this).showErrorSnack("No internet connection!");
                    }

                } else {

                    showEdit();
                }

                break;
            case R.id.iv_back:
                finish();
                break;
            case R.id.tv_country:
                countryPicker.showDialog(getSupportFragmentManager());
                break;
        }
    }


    // Method to manually check connection status
    private boolean checkConnection() {
        return ConnectivityReceiver.isConnected();
    }


    private void showEdit() {
       /* et_routing.setEnabled(true);
        et_holder_name.setEnabled(true);
        et_account_verify.setEnabled(true);
        et_account_number.setEnabled(true);
        et_account_verify.setVisibility(View.VISIBLE);
        btn_proceed.setText("UPDATE");
        btn_proceed.setEnabled(true);
        et_account_number.setText("");
        et_account_verify.setText("");
        et_holder_name.setText("");
        et_routing.setText("");
        et_account_number.setInputType(InputType.TYPE_CLASS_TEXT | InputType.TYPE_TEXT_VARIATION_PASSWORD);
        isEdit = true;*/
        isEdit = true;
        et_paypal.setEnabled(true);
        btn_proceed.setText("UPDATE");
        btn_proceed.setEnabled(true);
        isEdit = true;
    }

    private void hideEdit() {
        isEdit = false;
/*        et_routing.setEnabled(false);
        et_holder_name.setEnabled(false);
        et_account_verify.setEnabled(false);
        et_account_number.setEnabled(false);*/
        et_paypal.setEnabled(false);
       // et_account_verify.setVisibility(View.GONE);
        // et_account_number.setInputType(InputType.TYPE_CLASS_TEXT | InputType.TYPE_NUMBER_VARIATION_NORMAL);
        btn_proceed.setText("EDIT");

        if (profileData.getPaypal_id() != null) {
            et_paypal.setText(profileData.getPaypal_id());
        }
    }

    /**
     * @Rehan After api hit successfully we will get the LoginResponse object In LoginResponse if
     * status is 1 that means otp successfully send to to the entered email address
     * if status is 0 it means something went wrong.
     */
    public boolean isBankDetailValid() {
        boolean isValid = false;
       /* if (!et_account_number.getText().toString().isEmpty() && et_account_number.getText().toString().length() < 9) {
            et_account_number.requestFocus();
            et_account_number.setError("Please enter valid account number");
        } else if (!et_account_verify.getText().toString().equalsIgnoreCase(et_account_number.getText().toString())) {
            et_account_verify.requestFocus();
            et_account_verify.setError("Account number do not match!");
        } else if (et_holder_name.getText().toString().isEmpty()) {
            et_holder_name.requestFocus();
            et_holder_name.setError("Field can't be left blank!");
        } else if (et_routing.getText().toString().isEmpty() || et_routing.getText().toString().length()!=9) {
            et_routing.requestFocus();
            et_routing.setError("Field can't be left blank!");
        } else {
            isValid = true;
        }*/
        if (!et_paypal.getText().toString().isEmpty()) {
            isValid = true;
        }
        return isValid;
    }

    @Override
    public boolean onCreateOptionsMenu(Menu menu) {
        // Inflate the menu; this adds items to the action bar if it is present.
        getMenuInflater().inflate(R.menu.menu_cast, menu);
        mediaRouteMenuItem = CastButtonFactory.setUpMediaRouteButton(getApplicationContext(), menu,
                R.id.media_route_menu_item);

        return true;
    }

    @Override
    public boolean onOptionsItemSelected(@NonNull MenuItem item) {

        switch (item.getItemId()) {
            case android.R.id.home:
                // todo: goto back activity from here
                finish();
                return true;


            default:
                return super.onOptionsItemSelected(item);
        }

    }

    private void updateBankDetail() {
        btn_proceed.setEnabled(false);
        waitScreen.setVisibility(View.VISIBLE);

       /* BankAccountTokenParams bankAccount = new BankAccountTokenParams("", "", "", null, "", "");
        stripe.createBankAccountToken(bankAccount, new ApiResultCallback<Token>() {
            @Override
            public void onSuccess(@NotNull Token token) {
                Log.e("check ", "token ");

            }

            @Override
            public void onError(@NotNull Exception e) {

            }
        });*/

        final ProfileData data = new SharedPreferenceHandler(AccountActivity.this).getProfileInfo();

        Map<String, String> credential = new HashMap<>();
        credential.put("paypal_id", et_paypal.getText().toString());
        credential.put("name", profileData.getUserName());
        credential.put("phone_number",profileData.getPhoneNumber());
        if(data.getCity()!=null){
            credential.put("country", data.getCountry());
            credential.put("state", data.getState());
            credential.put("city", data.getCity());
        }else {
            credential.put("country", "");
            credential.put("state", "");
            credential.put("city", "");
        }
        credential.put("user_id", "" + data.getId());
        // credential.put("profile_picture", null);
        Call<LoginResponse> editProfile = apiInterface.editProfile(credential);

        // progressDialog.show();

        editProfile.enqueue(
                new Callback<LoginResponse>() {
                    @Override
                    public void onResponse(
                            Call<LoginResponse> call, Response<LoginResponse> response) {
                        // progressDialog.dismiss();
                        waitScreen.setVisibility(View.GONE);

                        if (response.code() == 200) {

                            if (response.body().getStatus()) {

                                data.setPaypal_id(et_paypal.getText().toString());

                                new SharedPreferenceHandler(AccountActivity.this)
                                        .setPreference(Constants.PROFILE_INFO, new Gson().toJson(data));
                                showSuccessFullUpdate();

                            } else {
                                new AlertDialog.Builder(AccountActivity.this, R.style.AlertDialogStyle)
                                        .setCancelable(false)
                                        .setMessage(getString(R.string.something_went_wrong))
                                        .setPositiveButton(
                                                "Ok",
                                                new DialogInterface.OnClickListener() {
                                                    @Override
                                                    public void onClick(
                                                            DialogInterface dialog, int which) {
                                                        btn_proceed.setEnabled(true);
                                                    }
                                                })
                                        .show();
                            }


                        }else {

                        }
                    }

                    @Override
                    public void onFailure(Call<LoginResponse> call, Throwable t) {
                        try {
                            waitScreen.setVisibility(View.GONE);
                            new AlertDialog.Builder(AccountActivity.this, R.style.AlertDialogStyle)
                                    .setCancelable(false)
                                    .setMessage("" + t.getMessage())
                                    .setPositiveButton(
                                            "Ok",
                                            new DialogInterface.OnClickListener() {
                                                @Override
                                                public void onClick(
                                                        DialogInterface dialog, int which) {
                                                    btn_proceed.setEnabled(true);
                                                }
                                            })
                                    .show();
                        } catch (Exception e) {

                        }

                    }
                });

    }


    private void showSuccessFullUpdate() {
        AlertDialog.Builder builder
                = new AlertDialog.Builder(this);
        // set the custom layout
        final View customLayout
                = getLayoutInflater()
                .inflate(
                        R.layout.layout_alert_loaded,
                        null);
        builder.setView(customLayout);

        Button btn_subscribe = customLayout.findViewById(R.id.tv_btn_upload);
        Button btn_cancel = customLayout.findViewById(R.id.btn_cancel);
        TextView tv_result = customLayout.findViewById(R.id.tv_result);
        TextView tv_message = customLayout.findViewById(R.id.tv_message);

     //   tv_result.setText("Success");

        tv_result.setText("Your Paypal id  updated successfully!");

        btn_cancel.setVisibility(View.GONE);
        btn_subscribe.setText("Ok");

        final AlertDialog dialog
                = builder.create();
        dialog.setCancelable(false);


        ColorDrawable back = new ColorDrawable(Color.TRANSPARENT);
        InsetDrawable inset = new InsetDrawable(back, 60, 0, 60, 0);
        if (dialog.getWindow() != null) {
            dialog.getWindow().setBackgroundDrawable(inset);

        }

        btn_subscribe.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                dialog.dismiss();
                finish();
            }
        });

        // create and show
        // the alert dialog

        dialog.show();
    }

    private void displayAlert(@NonNull String title,
                              @Nullable String message) {
        AlertDialog.Builder builder = new AlertDialog.Builder(this, R.style.AlertDialogStyle)
                .setTitle(title)
                .setMessage(message);
        builder.setPositiveButton("Ok", null);
        builder.create().show();
    }

    @Override
    public void onSelectCountry(Country country) {
        tv_country.setText("" + country.getName());
    }
}
