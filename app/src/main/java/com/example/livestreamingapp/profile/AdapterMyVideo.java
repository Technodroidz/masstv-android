package com.example.livestreamingapp.profile;

import android.content.Context;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.TextView;

import androidx.annotation.NonNull;
import androidx.recyclerview.widget.RecyclerView;

import com.bumptech.glide.Glide;
import com.bumptech.glide.request.RequestOptions;
import com.example.livestreamingapp.R;
import com.example.livestreamingapp.model.MyVideoDetailResponse;
import com.example.livestreamingapp.model.OnDemandVideo;
import com.example.livestreamingapp.model.WatchLaterResponse;
import com.example.livestreamingapp.ondemand.AdapterViewClickListener;
import com.example.livestreamingapp.utils.SnackBar;
import com.example.livestreamingapp.utils.Utils;
import com.example.livestreamingapp.webservice.WatchLaterService;

import java.util.List;

public class AdapterMyVideo extends RecyclerView.Adapter<AdapterMyVideo.VideoListHolder> {

    private Context context;
    private List<MyVideoDetailResponse.MyVideoDetails> videosList;
    AdapterViewClickListener listener;

    public AdapterMyVideo(Context context, List<MyVideoDetailResponse.MyVideoDetails> videosList,
                          AdapterViewClickListener listener){
        this.context = context;
        this.videosList = videosList;
        this.listener = listener;
    }


    @NonNull
    @Override
    public VideoListHolder onCreateViewHolder(@NonNull ViewGroup parent, int viewType) {
        View view = LayoutInflater.from(context).inflate(R.layout._item_list__my_video,parent,false);

        return new VideoListHolder(view);
    }

    @Override
    public void onBindViewHolder(@NonNull VideoListHolder holder, int position) {

        //  holder.btn_play_stream.setVisibility(View.GONE);
     //   holder.lin_more_videos.setVisibility(View.GONE);

        MyVideoDetailResponse.MyVideoDetails video = videosList.get(position);
        RequestOptions requestOptions = new RequestOptions();
        requestOptions.placeholder(R.drawable.placeholder_black);
        requestOptions.error(R.drawable.placeholder_black);

        try {
            holder.tv_rating.setText(""+ Utils.getDecimalOnePoint(Double.parseDouble(video.getAvg_rate())));

        }catch (Exception e){

        }


        holder.tv_subtitle.setVisibility(View.GONE);

        Glide.with(context)
                .applyDefaultRequestOptions(requestOptions)
                .load(video.getThumb()).into(holder.iv_image);

        holder.tv_title.setText("Video Id : "+video.getId());
        holder.tv_subtitle.setText(video.getSubtitle()!=null?video.getSubtitle():"subtitle");
        holder.tv_earn.setText("Earn : $"+video.getEarning());
        holder.tv_status.setText("Status : "+ (video.getAdminApproved()==1? "Approved":"In Review"));


    }

    /**
     * Add +1 extra size to show 'More Video' option
     * @return
     */
    @Override
    public int getItemCount() {
        return this.videosList.size();
    }

    public class VideoListHolder extends RecyclerView.ViewHolder {
        private ImageView ib_play_later;
        private ImageView iv_image;
        private LinearLayout lin_more_videos;
        private TextView tv_title;
        private TextView tv_subtitle;
        private TextView tv_status;
        private TextView tv_earn;
        private TextView tv_rating;
        public VideoListHolder(@NonNull View itemView) {
            super(itemView);
            ib_play_later = (ImageView)itemView.findViewById(R.id.ib_play_later);
            iv_image = (ImageView)itemView.findViewById(R.id.iv_image);
            tv_title= (TextView)itemView.findViewById(R.id.tv_title);
            tv_subtitle= (TextView)itemView.findViewById(R.id.tv_subtitle);
            tv_status= (TextView)itemView.findViewById(R.id.tv_status);
            tv_earn= (TextView)itemView.findViewById(R.id.tv_earn);
            tv_rating= (TextView)itemView.findViewById(R.id.tv_rating);


        }
    }

    public int getSize(){
        return videosList.size();
    }


    @Override
    public int getItemViewType(int position) {

        return super.getItemViewType(position);

    }
}
