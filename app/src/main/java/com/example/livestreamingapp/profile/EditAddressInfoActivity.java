package com.example.livestreamingapp.profile;

import android.app.ProgressDialog;
import android.content.DialogInterface;
import android.graphics.Color;
import android.graphics.drawable.ColorDrawable;
import android.graphics.drawable.InsetDrawable;
import android.os.Bundle;
import android.util.Log;
import android.view.Menu;
import android.view.MenuItem;
import android.view.View;
import android.widget.Button;
import android.widget.EditText;
import android.widget.ImageView;
import android.widget.RelativeLayout;
import android.widget.TextView;

import androidx.annotation.NonNull;
import androidx.appcompat.app.AlertDialog;
import androidx.appcompat.app.AppCompatActivity;
import androidx.appcompat.widget.Toolbar;
import androidx.collection.ArraySet;

import com.example.livestreamingapp.MassTvActivity;
import com.example.livestreamingapp.R;
import com.example.livestreamingapp.model.APIError;
import com.example.livestreamingapp.model.LoginResponse;
import com.example.livestreamingapp.model.ProfileData;
import com.example.livestreamingapp.utils.Constants;
import com.example.livestreamingapp.utils.ErrorUtils;
import com.example.livestreamingapp.utils.SharedPreferenceHandler;
import com.example.livestreamingapp.utils.SnackBar;
import com.example.livestreamingapp.webservice.APIClient;
import com.example.livestreamingapp.webservice.APIInterface;
import com.example.livestreamingapp.webservice.WebServiceConstant;
import com.google.android.gms.cast.framework.CastButtonFactory;
import com.google.android.gms.cast.framework.CastContext;
import com.google.gson.Gson;
import com.vikktorn.picker.City;
import com.vikktorn.picker.CityPicker;
import com.vikktorn.picker.CityPickerDialog;
import com.vikktorn.picker.Country;
import com.vikktorn.picker.CountryPicker;
import com.vikktorn.picker.CountryPickerDialog;
import com.vikktorn.picker.OnCityPickerListener;
import com.vikktorn.picker.OnCountryPickerListener;
import com.vikktorn.picker.OnStatePickerListener;
import com.vikktorn.picker.State;
import com.vikktorn.picker.StatePicker;
import com.vikktorn.picker.StatePickerDialog;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import java.io.IOException;
import java.io.InputStream;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;

public class EditAddressInfoActivity extends MassTvActivity implements View.OnClickListener,
        OnStatePickerListener,
        OnCountryPickerListener,
        OnCityPickerListener {

    private Toolbar mToolbar;
    private EditText et_email;
    private Button btn_proceed;
    private APIInterface apiInterface;
    private ProgressDialog progressDialog;
    private CastContext mCastContext;
    private MenuItem mediaRouteMenuItem;
    private ImageView iv_back;
    private TextView tv_progress;

    private RelativeLayout waitScreen;

    private TextView tv_country;
    private TextView tv_state;
    private TextView tv_city;

    // Pickers
    private CountryPicker countryPicker;
    private StatePicker statePicker;
    private CityPicker cityPicker;
    private ArrayList<State> stateObject;
    private ArrayList<City> cityObject;
    private int stateID;
    private int countryID;
    private View view;

    ProfileData profileData;


    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.layout_edit_profile_address);
        setupActionBar();
        view = getWindow().getDecorView().getRootView();
        profileData = new SharedPreferenceHandler(this).getProfileInfo();

        mCastContext = CastContext.getSharedInstance(this);
        initViews();
    }

    private void initViews() {
        et_email = findViewById(R.id.et_email);
        iv_back = findViewById(R.id.iv_back);
        btn_proceed = findViewById(R.id.btn_proceed);
        tv_progress = findViewById(R.id.tv_progress);
        waitScreen = findViewById(R.id.waitScreen);
        tv_country = findViewById(R.id.tv_country);
        tv_state = findViewById(R.id.tv_state);
        tv_city = findViewById(R.id.tv_city);

        tv_progress.setVisibility(View.GONE);

        btn_proceed.setOnClickListener(this);
        iv_back.setOnClickListener(this);
        tv_city.setOnClickListener(this);
        tv_state.setOnClickListener(this);
        tv_country.setOnClickListener(this);

        stateObject = new ArrayList<>();
        cityObject = new ArrayList<>();


        // get state from assets JSON
        try {
            getStateJson();
        } catch (JSONException e) {
            e.printStackTrace();
        }
        // get City from assets JSON
        try {
            getCityJson();
        } catch (JSONException e) {
            e.printStackTrace();
        }

        // initialize country picker
        countryPicker = new CountryPicker.Builder().with(this).listener(this).build();

        /*progressDialog = new ProgressDialog(EditProfileInfoActivity.this);
        progressDialog.setMessage("Please wait...");
        progressDialog.setCancelable(false);*/
        apiInterface = APIClient.getClient().create(APIInterface.class);
        setAddress();
    }

    private void setAddress() {
        profileData = new SharedPreferenceHandler(this).getProfileInfo();
        if (profileData.getCity() != null && !profileData.getCity().isEmpty()) {
            tv_country.setText(profileData.getCountry());
            tv_state.setText(profileData.getState());
            tv_city.setText(profileData.getCity());
        }
    }

    private void setupActionBar() {
        mToolbar = (Toolbar) findViewById(R.id.toolbar);
        mToolbar.setTitle("");
        setSupportActionBar(mToolbar);
    }

    @Override
    public void onClick(View v) {
        switch (v.getId()) {
            case R.id.btn_proceed:
                if (tv_city.getText() != null && !tv_city.getText().toString().isEmpty()) {

                    updateAddress();
                } else {
                    new SnackBar(this).showErrorSnack("Please select all required field!");
                }

                break;

            case R.id.tv_country:
                countryPicker.showDialog(getSupportFragmentManager());
                break;

            case R.id.tv_state:
                if (!tv_country.getText().toString().isEmpty() && countryID!=0) {

                    if(cityObject!=null){
                        try {
                            statePicker.showDialog(getSupportFragmentManager());
                        }catch (Exception e){

                        }
                    }else {
                        new SnackBar(this).showErrorSnack("No city in this state");
                    }


                } else {
                    new SnackBar(this).showErrorSnack("Please select country first!");
                }

                break;

            case R.id.tv_city:
                if (!tv_state.getText().toString().isEmpty() && cityObject!=null && cityObject.size()>0 && stateID!=0) {
                    try {
                        cityPicker.showDialog(getSupportFragmentManager());
                    }catch (Exception e){

                    }

                } else {
                    new SnackBar(this).showErrorSnack("Please select state first!");
                }

                break;


            case R.id.iv_back:
                finish();
                break;
        }
    }

    private void updateAddress() {
        //  btn_proceed.setEnabled(false);
        waitScreen.setVisibility(View.VISIBLE);
        final Map<String, String> bank = new HashMap<>();

        if (profileData.getUserName() == null) {
            bank.put(WebServiceConstant.NAME, "" + profileData.getId());

        } else {
            bank.put(WebServiceConstant.NAME, "" + profileData.getUserName());

        }

        bank.put(WebServiceConstant.USER_ID, "" + profileData.getId());
        bank.put("phone_number",profileData.getPhoneNumber());
        bank.put(WebServiceConstant.COUNTRY, "" + tv_country.getText().toString());
        bank.put(WebServiceConstant.STATE, "" + tv_state.getText().toString());
        bank.put(WebServiceConstant.CITY, "" + tv_country.getText().toString());

        final Call<LoginResponse> customerModelCall = apiInterface.editProfile(bank);
        customerModelCall.enqueue(new Callback<LoginResponse>() {
            @Override
            public void onResponse(Call<LoginResponse> call, Response<LoginResponse> response) {
                waitScreen.setVisibility(View.GONE);
                if (response.code() == 200) {
                    if (response.body().getStatus()) {

                        profileData.setCity(tv_city.getText().toString());
                        profileData.setState(tv_state.getText().toString());
                        profileData.setCountry(tv_country.getText().toString());
                        new SharedPreferenceHandler(EditAddressInfoActivity.this).setProfileData(profileData);
                        showSuccessFullUpdate();
                    }
                } else {
                    try {
                        APIError error = ErrorUtils.parseError(response);
                        String errorMessage = error.getData().getError();

                        if(errorMessage==null){
                            errorMessage = "Something went wrong";
                        }
                        //   displayAlert("Error",errorMessage);
                    } catch (Exception e) {

                    }

                }

            }

            @Override
            public void onFailure(Call<LoginResponse> call, Throwable t) {
                waitScreen.setVisibility(View.GONE);
                //       displayAlert("Error","Please check your internet connection!");
            }
        });

    }

    private void showSuccessFullUpdate() {
        AlertDialog.Builder builder
                = new AlertDialog.Builder(this);
        // set the custom layout
        final View customLayout
                = getLayoutInflater()
                .inflate(
                        R.layout.layout_alert_loaded,
                        null);
        builder.setView(customLayout);

        TextView btn_subscribe = customLayout.findViewById(R.id.tv_btn_upload);
        TextView tv_result = customLayout.findViewById(R.id.tv_result);

        tv_result.setText("Profile detail updated successfully!");

        final AlertDialog dialog
                = builder.create();
        dialog.setCancelable(false);

        ColorDrawable back = new ColorDrawable(Color.TRANSPARENT);
        InsetDrawable inset = new InsetDrawable(back, 60, 0, 60, 0);
        if (dialog.getWindow() != null) {
            dialog.getWindow().setBackgroundDrawable(inset);

        }

        btn_subscribe.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                finish();
                dialog.dismiss();

            }
        });

        // create and show
        // the alert dialog

        dialog.show();

    }

    /**
     * @Rehan After api hit successfully we will get the LoginResponse object In LoginResponse if
     * status is 1 that means otp successfully send to to the entered email address
     * if status is 0 it means something went wrong.
     */
    private void AttempEditProfile(final String name) {

        waitScreen.setVisibility(View.VISIBLE);

        final ProfileData data = new SharedPreferenceHandler(EditAddressInfoActivity.this).getProfileInfo();

        Map<String, String> credential = new HashMap<>();
        credential.put("name", name);
        credential.put("user_id", "" + data.getId());
        // credential.put("profile_picture", null);
        Call<LoginResponse> editProfile = apiInterface.editProfile(credential);

        // progressDialog.show();

        editProfile.enqueue(
                new Callback<LoginResponse>() {
                    @Override
                    public void onResponse(
                            Call<LoginResponse> call, Response<LoginResponse> response) {
                        // progressDialog.dismiss();
                        waitScreen.setVisibility(View.GONE);

                        if (response.code() == 200) {

                            if (response.body().getStatus()) {

                                data.setUserName(name);

                                new SharedPreferenceHandler(EditAddressInfoActivity.this)
                                        .setPreference(Constants.PROFILE_INFO, new Gson().toJson(data));
                                finish();

                            } else {
                                new AlertDialog.Builder(EditAddressInfoActivity.this, R.style.AlertDialogStyle)
                                        .setCancelable(false)
                                        .setMessage(getString(R.string.something_went_wrong))
                                        .setPositiveButton(
                                                "Ok",
                                                new DialogInterface.OnClickListener() {
                                                    @Override
                                                    public void onClick(
                                                            DialogInterface dialog, int which) {
                                                    }
                                                })
                                        .show();
                            }


                        }
                    }

                    @Override
                    public void onFailure(Call<LoginResponse> call, Throwable t) {
                        progressDialog.dismiss();
                        waitScreen.setVisibility(View.GONE);
                        new AlertDialog.Builder(EditAddressInfoActivity.this, R.style.AlertDialogStyle)
                                .setCancelable(false)
                                .setMessage("" + t.getMessage())
                                .setPositiveButton(
                                        "Ok",
                                        new DialogInterface.OnClickListener() {
                                            @Override
                                            public void onClick(
                                                    DialogInterface dialog, int which) {
                                            }
                                        })
                                .show();
                    }
                });
    }

    public boolean isValidUsername(String email) {
        return !email.isEmpty();
    }

    @Override
    public boolean onCreateOptionsMenu(Menu menu) {
        // Inflate the menu; this adds items to the action bar if it is present.
        getMenuInflater().inflate(R.menu.menu_cast, menu);
        mediaRouteMenuItem = CastButtonFactory.setUpMediaRouteButton(getApplicationContext(), menu,
                R.id.media_route_menu_item);

        return true;
    }

    @Override
    public boolean onOptionsItemSelected(@NonNull MenuItem item) {

        switch (item.getItemId()) {
            case android.R.id.home:
                // todo: goto back activity from here
                finish();
                return true;


            default:
                return super.onOptionsItemSelected(item);
        }

    }


    @Override
    public void onSelectCity(City city) {

        tv_city.setText("" + city.getCityName());
    }

    @Override
    public void onSelectCountry(Country country) {
        statePicker = new StatePicker.Builder().with(this).listener(this).build();
        tv_country.setText("" + country.getName());
        countryID = country.getCountryId();
        clearState();


        // GET STATES OF SELECTED COUNTRY
        for (int i = 0; i < stateObject.size(); i++) {
            // init state picker
            statePicker = new StatePicker.Builder().with(this).listener(this).build();
            State stateData = new State();
            if (stateObject.get(i).getCountryId() == countryID) {

                stateData.setStateId(stateObject.get(i).getStateId());
                stateData.setStateName(stateObject.get(i).getStateName());
                stateData.setCountryId(stateObject.get(i).getCountryId());
                stateData.setFlag(country.getFlag());
                statePicker.equalStateObject.add(stateData);
            }
        }

    }

    private void clearState() {
        tv_state.setText("");
        tv_city.setText("");
        statePicker.equalStateObject.clear();
        cityPicker.equalCityObject.clear();
    }

    private void clearCity() {

        tv_city.setText("");
        cityPicker.equalCityObject.clear();
    }

    @Override
    public void onSelectState(State state) {
        stateID = state.getStateId();

        tv_state.setText("" + state.getStateName());
        clearCity();

        for (int i = 0; i < cityObject.size(); i++) {
            cityPicker = new CityPicker.Builder().with(this).listener(this).build();
            City cityData = new City();
            if (cityObject.get(i).getStateId() == stateID) {
                cityData.setCityId(cityObject.get(i).getCityId());
                cityData.setCityName(cityObject.get(i).getCityName());
                cityData.setStateId(cityObject.get(i).getStateId());

                cityPicker.equalCityObject.add(cityData);
            }
        }
    }


    // GET STATE FROM ASSETS JSON
    public void getStateJson() throws JSONException {
        String json = null;
        try {
            InputStream inputStream = getAssets().open("states.json");
            int size = inputStream.available();
            byte[] buffer = new byte[size];
            inputStream.read(buffer);
            inputStream.close();
            json = new String(buffer, "UTF-8");



        } catch (IOException e) {
            e.printStackTrace();
        }


        JSONObject jsonObject = new JSONObject(json);
        JSONArray events = jsonObject.getJSONArray("states");
        for (int j = 0; j < events.length(); j++) {
            JSONObject cit = events.getJSONObject(j);
            State stateData = new State();

            stateData.setStateId(Integer.parseInt(cit.getString("id")));
            stateData.setStateName(cit.getString("name"));
            stateData.setCountryId(Integer.parseInt(cit.getString("country_id")));
            stateObject.add(stateData);
        }
    }

    // GET CITY FROM ASSETS JSON
    public void getCityJson() throws JSONException {
        String json = null;
        try {
            InputStream inputStream = getAssets().open("cities.json");
            int size = inputStream.available();
            byte[] buffer = new byte[size];
            inputStream.read(buffer);
            inputStream.close();
            json = new String(buffer, "UTF-8");
            Log.e("check json file","hello"+new Gson().toJson(json));

        } catch (IOException e) {
            e.printStackTrace();
        }


        JSONObject jsonObject = new JSONObject(json);
        JSONArray events = jsonObject.getJSONArray("cities");
        for (int j = 0; j < events.length(); j++) {
            JSONObject cit = events.getJSONObject(j);
            City cityData = new City();

            cityData.setCityId(Integer.parseInt(cit.getString("id")));
            cityData.setCityName(cit.getString("name"));
            cityData.setStateId(Integer.parseInt(cit.getString("state_id")));
            cityObject.add(cityData);
        }
    }
}
