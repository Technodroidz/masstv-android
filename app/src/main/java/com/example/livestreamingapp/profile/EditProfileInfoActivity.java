package com.example.livestreamingapp.profile;

import android.app.ProgressDialog;
import android.content.DialogInterface;
import android.graphics.Color;
import android.os.Bundle;
import android.util.Log;
import android.view.Menu;
import android.view.MenuItem;
import android.view.View;
import android.widget.Button;
import android.widget.EditText;
import android.widget.ImageView;
import android.widget.RelativeLayout;
import android.widget.TextView;

import androidx.annotation.NonNull;
import androidx.appcompat.app.AlertDialog;
import androidx.appcompat.app.AppCompatActivity;
import androidx.appcompat.widget.Toolbar;

import com.example.livestreamingapp.MassTvActivity;
import com.example.livestreamingapp.R;
import com.example.livestreamingapp.model.LoginResponse;
import com.example.livestreamingapp.model.ProfileData;
import com.example.livestreamingapp.utils.ConnectivityReceiver;
import com.example.livestreamingapp.utils.Constants;
import com.example.livestreamingapp.utils.SharedPreferenceHandler;
import com.example.livestreamingapp.webservice.APIClient;
import com.example.livestreamingapp.webservice.APIInterface;
import com.google.android.gms.cast.framework.CastButtonFactory;
import com.google.android.gms.cast.framework.CastContext;
import com.google.android.material.snackbar.Snackbar;
import com.google.gson.Gson;

import java.util.HashMap;
import java.util.Map;

import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;

public class EditProfileInfoActivity extends MassTvActivity implements View.OnClickListener{

    private Toolbar mToolbar;
    private EditText et_email;
    private Button btn_proceed;
    private APIInterface apiInterface;

    private CastContext mCastContext;
    private MenuItem mediaRouteMenuItem;
    private ImageView iv_back;
    private TextView tv_progress;

    private RelativeLayout waitScreen;


    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.layout_edit_profile);
        setupActionBar();
        mCastContext = CastContext.getSharedInstance(this);
        initViews();
    }

    private void initViews() {
        et_email = findViewById(R.id.et_email);
        iv_back = findViewById(R.id.iv_back);
        btn_proceed = findViewById(R.id.btn_proceed);
        tv_progress = findViewById(R.id.tv_progress);
        waitScreen = findViewById(R.id.waitScreen);

        tv_progress.setVisibility(View.GONE);

        btn_proceed.setOnClickListener(this);
        iv_back.setOnClickListener(this);

        /*progressDialog = new ProgressDialog(EditProfileInfoActivity.this);
        progressDialog.setMessage("Please wait...");
        progressDialog.setCancelable(false);*/
         apiInterface = APIClient.getClient().create(APIInterface.class);
    }

    private void setupActionBar() {
        mToolbar = (Toolbar) findViewById(R.id.toolbar);
        mToolbar.setTitle("");
        setSupportActionBar(mToolbar);
    }

    @Override
    public void onClick(View v) {
        switch (v.getId()){
            case R.id.btn_proceed:
                if(isValidUsername(et_email.getText().toString())){
                    if(checkConnection()){
                        AttempEditProfile(et_email.getText().toString());
                    }else {
                        showSnack(checkConnection());
                    }

                }else {
                    et_email.requestFocus();
                    et_email.setError("Field can't be empty!");
                }

                break;
            case R.id.iv_back:
                finish();
                break;
        }
    }

    // Showing the status in Snackbar for intenet connectivity
    private void showSnack(boolean isConnected) {
        String message;
        int color;
        if (isConnected) {
            message = "Connection back Online!";
            color = Color.WHITE;
        } else {
            message = "Offline! Not connected to internet";
            color = Color.RED;


        }
        Snackbar snackbar = Snackbar
                .make(findViewById(R.id.parent), message, Snackbar.LENGTH_LONG);
        View sbView = snackbar.getView();
        TextView textView = (TextView) sbView.findViewById(R.id.snackbar_text);
        textView.setTextColor(color);
        snackbar.show();

    }

    /**
     * Check internet connection Manually
     */
    private boolean checkConnection() {
        return ConnectivityReceiver.isConnected();
    }


    /**
     * @Rehan Edit Address
     */
    private void AttempEditProfile(final String name) {

        waitScreen.setVisibility(View.VISIBLE);

        final ProfileData data = new SharedPreferenceHandler(EditProfileInfoActivity.this).getProfileInfo();

        Map<String, String> credential = new HashMap<>();
        credential.put("name", name);
        credential.put("phone_number", ""+data.getPhoneNumber());

        if(data.getCity()!=null){
            credential.put("country", data.getCountry());
            credential.put("state", data.getState());
            credential.put("city", data.getCity());
        }else {
            credential.put("country", "");
            credential.put("state", "");
            credential.put("city", "");
        }
        credential.put("user_id",""+data.getId());
     //   Log.e("check profile ","json "+new Gson().toJson(credential));
       // credential.put("profile_picture", null);
        Call<LoginResponse> editProfile = apiInterface.editProfile(credential);

       // progressDialog.show();

        editProfile.enqueue(
                new Callback<LoginResponse>() {
                    @Override
                    public void onResponse(
                            Call<LoginResponse> call, Response<LoginResponse> response) {
                       // progressDialog.dismiss();
                        waitScreen.setVisibility(View.GONE);

                        if (response.code() == 200) {

                          //  Log.e("Check ","res success "+new Gson().toJson(response.body()));

                            if (response.body().getStatus()) {

                                data.setUserName(name);

                                new SharedPreferenceHandler(EditProfileInfoActivity.this)
                                        .setPreference(Constants.PROFILE_INFO,new Gson().toJson(data));
                                finish();

                            } else {
                                new AlertDialog.Builder(EditProfileInfoActivity.this,R.style.AlertDialogStyle)
                                        .setCancelable(false)
                                        .setMessage(getString(R.string.something_went_wrong))
                                        .setPositiveButton(
                                                "Ok",
                                                new DialogInterface.OnClickListener() {
                                                    @Override
                                                    public void onClick(
                                                            DialogInterface dialog, int which) {}
                                                })
                                        .show();
                            }


                        }
                    }

                    @Override
                    public void onFailure(Call<LoginResponse> call, Throwable t) {
                        try {
                            waitScreen.setVisibility(View.GONE);
                            new AlertDialog.Builder(EditProfileInfoActivity.this,R.style.AlertDialogStyle)
                                    .setCancelable(false)
                                    .setMessage("" + t.getMessage())
                                    .setPositiveButton(
                                            "Ok",
                                            new DialogInterface.OnClickListener() {
                                                @Override
                                                public void onClick(
                                                        DialogInterface dialog, int which) {}
                                            })
                                    .show();
                        }catch (Exception e){

                        }

                    }
                });
    }

    public boolean isValidUsername(String email){
        return !email.isEmpty();
    }

    @Override
    public boolean onCreateOptionsMenu(Menu menu) {
        // Inflate the menu; this adds items to the action bar if it is present.
        getMenuInflater().inflate(R.menu.menu_cast, menu);
        mediaRouteMenuItem = CastButtonFactory.setUpMediaRouteButton(getApplicationContext(), menu,
                R.id.media_route_menu_item);
        return true;
    }

    @Override
    public boolean onOptionsItemSelected(@NonNull MenuItem item) {

        switch (item.getItemId()) {
            case android.R.id.home:
                // todo: goto back activity from here
                finish();
                return true;


            default:
                return super.onOptionsItemSelected(item);
        }

    }

}
