package com.example.livestreamingapp.profile;

import android.content.Intent;
import android.graphics.Color;
import android.graphics.drawable.ColorDrawable;
import android.graphics.drawable.InsetDrawable;
import android.os.Bundle;
import android.util.Log;
import android.view.Menu;
import android.view.MenuItem;
import android.view.View;
import android.widget.Button;
import android.widget.LinearLayout;
import android.widget.RelativeLayout;
import android.widget.TextView;

import androidx.annotation.NonNull;
import androidx.appcompat.app.AlertDialog;
import androidx.appcompat.app.AppCompatActivity;
import androidx.appcompat.widget.Toolbar;
import androidx.recyclerview.widget.GridLayoutManager;
import androidx.recyclerview.widget.LinearLayoutManager;
import androidx.recyclerview.widget.RecyclerView;

import com.example.livestreamingapp.MassTvActivity;
import com.example.livestreamingapp.MassTvApplication;
import com.example.livestreamingapp.R;
import com.example.livestreamingapp.model.MySubscription;
import com.example.livestreamingapp.model.MyVideoDetailResponse;
import com.example.livestreamingapp.model.OnDemandCategory;
import com.example.livestreamingapp.model.OnDemandVideo;
import com.example.livestreamingapp.model.ProfileData;
import com.example.livestreamingapp.ondemand.AdapterGridVideoList;
import com.example.livestreamingapp.ondemand.AdapterViewClickListener;
import com.example.livestreamingapp.subscription.SubscriptionSelectorActivity;
import com.example.livestreamingapp.utils.ConnectivityReceiver;
import com.example.livestreamingapp.utils.Constants;
import com.example.livestreamingapp.utils.SharedPreferenceHandler;
import com.example.livestreamingapp.utils.SpacesItemDecoration;
import com.example.livestreamingapp.video.VideoDetailActivity;
import com.example.livestreamingapp.webservice.GetOnDemandVideoList;
import com.example.livestreamingapp.webservice.SubscriptionService;
import com.facebook.shimmer.ShimmerFrameLayout;
import com.google.android.gms.cast.framework.CastButtonFactory;
import com.google.android.gms.cast.framework.CastContext;
import com.google.android.material.snackbar.Snackbar;

import java.text.BreakIterator;
import java.util.ArrayList;
import java.util.List;

public class MyVideoActivity extends MassTvActivity implements View.OnClickListener,
        AdapterViewClickListener, ConnectivityReceiver.ConnectivityReceiverListener {

    Toolbar mToolbar;
    private RecyclerView rv_video_list;
    private CastContext mCastContext;
    private MenuItem mediaRouteMenuItem;
    private TextView tv_toolbar_title;
    private AdapterMyVideo adapterVideoOnDemand;
    private int INDEX_SIZE = 0;
    private  List<MyVideoDetailResponse.MyVideoDetails> videosList;
    private String category;
    private ProfileData profileData;
    private LinearLayout ln_total_earning;
    private ShimmerFrameLayout shimmerFrameLayout;
    private TextView tv_total_earning;

    private TextView tv_active_videos;
    private TextView tv_rejected_videos;
    private TextView tv_not_found;

    private RelativeLayout waitScreen;

    public boolean isAllVideo = true;


    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_my_video_list);

        category = getIntent().getStringExtra(Constants.CATEGORY_TYPE);
        profileData = new SharedPreferenceHandler(this).getProfileInfo();

        setupActionBar(category);
        initViews();
        mCastContext = CastContext.getSharedInstance(this);

    }

    private void setupActionBar(String title) {
        mToolbar = (Toolbar) findViewById(R.id.toolbar);
        mToolbar.setNavigationIcon(R.drawable.ic_backs_white_24);
       // mToolbar.setTitle(""+title);
        setSupportActionBar(mToolbar);

        if(getSupportActionBar()!=null){
            getSupportActionBar().setDisplayHomeAsUpEnabled(true);

        }

    }

    @Override
    public boolean onOptionsItemSelected(@NonNull MenuItem item) {

        switch (item.getItemId()) {
            case android.R.id.home:
                // todo: goto back activity from here
                finish();
                return true;

            default:
                return super.onOptionsItemSelected(item);
        }
    }

    @Override
    protected void onResume() {
        super.onResume();
        MassTvApplication.getInstance().setConnectivityListener(this);
    }

    @Override
    protected void onPause() {
        super.onPause();
        try {
            MassTvApplication.getInstance().setUnRegister();
        }catch (Exception e){

        }
    }

    public void initViews() {

        ln_total_earning = findViewById(R.id.ln_total_earning);
        tv_total_earning = findViewById(R.id.tv_total_earning);
        shimmerFrameLayout = findViewById(R.id.shimmer_view_container);
        waitScreen = findViewById(R.id.waitScreen);

        tv_active_videos = findViewById(R.id.tv_active_videos);
        tv_rejected_videos = findViewById(R.id.tv_rejected_videos);
        tv_not_found = findViewById(R.id.tv_not_found);

        profileData = new SharedPreferenceHandler(this).getProfileInfo();
        rv_video_list = (RecyclerView) findViewById(R.id.rv_video_list);
        videosList = new ArrayList<>();
        // set adapterVideoOnDemand Adapter for recyclerView
        LinearLayoutManager layoutManagerDemand = new LinearLayoutManager(this);
        adapterVideoOnDemand = new AdapterMyVideo(this, videosList, this);
        rv_video_list.setLayoutManager(layoutManagerDemand);
        rv_video_list.setAdapter(adapterVideoOnDemand);

        tv_active_videos.setOnClickListener(this);
        tv_rejected_videos.setOnClickListener(this);

    }

    public void getVideos(TextView textView,boolean all){
        tv_rejected_videos.setTextColor(getResources().getColor(R.color.white));
        tv_active_videos.setTextColor(getResources().getColor(R.color.white));
        textView.setTextColor(getResources().getColor(R.color.green));

        this.isAllVideo = all;

        getVideoList(0);
    }



    private void getVideoList(int index_size) {
        tv_not_found.setVisibility(View.GONE);
        waitScreen.setVisibility(View.VISIBLE);
       // ln_total_earning.setVisibility(View.GONE);
        adapterVideoOnDemand = new AdapterMyVideo(MyVideoActivity.this,
                new ArrayList<>(), MyVideoActivity.this);
        rv_video_list.setAdapter(adapterVideoOnDemand);

       new GetOnDemandVideoList(this, new GetOnDemandVideoList.videoListListener() {
           @Override
           public void onDemandFetchedVideoList(List<MyVideoDetailResponse.MyVideoDetails> videoData) {
               hideShimmer();
              // ln_total_earning.setVisibility(View.VISIBLE);
               waitScreen.setVisibility(View.GONE);
               videosList.clear();
               videosList = new ArrayList<>();
               for(int i=0;i<videoData.size();i++){
                   if(isAllVideo){
                       videosList.add(videoData.get(i));
                   }else {
                       if(videoData.get(i).getAdminApproved()>0){
                           videosList.add(videoData.get(i));
                       }
                   }
               }
              // videosList.addAll(videoData);
               adapterVideoOnDemand = new AdapterMyVideo(MyVideoActivity.this,
                       videosList, MyVideoActivity.this);
               rv_video_list.setAdapter(adapterVideoOnDemand);
               if(videosList.size()>0){
                //   showTotalEarning();
               }else {
                   tv_not_found.setVisibility(View.VISIBLE);
               }

           }

           @Override
           public void onDemandFetchingError(String error) {
             //  hideShimmer();
               waitScreen.setVisibility(View.GONE);
           }
       }).getMyVideoList();




    }

    public void showTotalEarning(){
        long total = 0;
        for(int i=0;i<videosList.size();i++){
            String earn = videosList.get(i).getEarning();
            if(earn!=null && !earn.isEmpty())
            total = total+ Integer.parseInt(earn);
        }
        tv_total_earning.setText("$"+total);
    }

    private void hideShimmer() {

        rv_video_list.setVisibility(View.VISIBLE);
        shimmerFrameLayout.hideShimmer();
        shimmerFrameLayout.setVisibility(View.GONE);


    }

    @Override
    public void onVideoSelected(int position, OnDemandVideo onDemandVideo) {

        /**
         * this below logic for checking user has subscription or not
         *  uncomment this logic when subscription is required
         */
     /*   if (mySubscription != null && Integer.parseInt(mySubscription.getRemainingDays()) != 0) {
            if (!mySubscription.getLastRefreshDate().equalsIgnoreCase(DateCalculation.getCurrentDateInDDMMYY())) {
                getMySubscriptionDetail(onDemandVideo);
            } else {
                startActivity(new Intent(VideoListActivity.this,
                        VideoDetailActivity.class).
                        putExtra(Constants.SELECTED_ON_DEMAND_VIDEO, onDemandVideo));
            }

        } else {
            showSubscriptionAlert();
        }*/

        startActivity(new Intent(MyVideoActivity.this,
                VideoDetailActivity.class).
                putExtra(Constants.SELECTED_ON_DEMAND_VIDEO, onDemandVideo));

    }

    @Override
    public void onCategorySelected(int position, OnDemandCategory onDemandCategory) {

    }

    @Override
    public void onWatchLater(int i, OnDemandVideo onDemandVideo) {

    }


    @Override
    public void onClick(View v) {
        switch (v.getId()) {
            case R.id.iv_back:
                finish();
                break;

            case R.id.tv_rejected_videos:
                getVideos(tv_rejected_videos,false);
                break;

            case R.id.tv_active_videos:
                getVideos(tv_active_videos,true);
                break;
        }
    }


    @Override
    public boolean onCreateOptionsMenu(Menu menu) {
        // Inflate the menu; this adds items to the action bar if it is present.
        getMenuInflater().inflate(R.menu.menu_cast, menu);
        mediaRouteMenuItem = CastButtonFactory.setUpMediaRouteButton(getApplicationContext(), menu,
                R.id.media_route_menu_item);
        return true;
    }

    @Override
    public void onNetworkConnectionChanged(boolean isConnected) {
        if((videosList==null||videosList.size()==0) && isConnected){
            getVideoList(INDEX_SIZE);
        }else {
            showSnack(isConnected);
        }
    }

    /**
     * Check internet connection Manually
     */
    private boolean checkConnection() {
        return ConnectivityReceiver.isConnected();
    }

    // Showing the status in Snackbar for intenet connectivity
    private void showSnack(boolean isConnected) {
        String message;
        int color;
        if (isConnected) {
            message = "Connection back Online!";
            color = Color.WHITE;
        } else {
            message = "Offline! Not connected to internet";
            color = Color.RED;


        }
        Snackbar snackbar = Snackbar
                .make(findViewById(R.id.parent), message, Snackbar.LENGTH_LONG);
        View sbView = snackbar.getView();
        TextView textView = (TextView) sbView.findViewById(R.id.snackbar_text);
        textView.setTextColor(color);
        snackbar.show();

    }
}