package com.example.livestreamingapp.profile;

import androidx.annotation.NonNull;
import androidx.appcompat.app.AppCompatActivity;
import androidx.appcompat.widget.Toolbar;

import android.content.Intent;
import android.os.Bundle;
import android.view.Menu;
import android.view.MenuItem;
import android.view.View;
import android.widget.LinearLayout;
import android.widget.RelativeLayout;
import android.widget.TextView;

import com.example.livestreamingapp.MassTvActivity;
import com.example.livestreamingapp.R;
import com.example.livestreamingapp.home.ui.HomeDraweerStreamingActivity;
import com.example.livestreamingapp.model.ProfileData;
import com.example.livestreamingapp.model.UserUploadVideoDetails;
import com.example.livestreamingapp.upload.AddPhoneNumberActivity;
import com.example.livestreamingapp.upload.UploadContentActivity;
import com.example.livestreamingapp.utils.Constants;
import com.example.livestreamingapp.utils.SharedPreferenceHandler;
import com.example.livestreamingapp.webservice.APIClient;
import com.example.livestreamingapp.webservice.APIInterface;
import com.example.livestreamingapp.webservice.WebServiceConstant;
import com.google.android.gms.cast.framework.CastButtonFactory;
import com.google.android.gms.cast.framework.CastContext;

import java.util.HashMap;
import java.util.Map;

import okhttp3.ResponseBody;
import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;

public class ProfileActivity extends MassTvActivity implements View.OnClickListener {

    private Toolbar mToolbar;
    private MenuItem mediaRouteMenuItem;
    private CastContext mCastContext;
    private ProfileData profileInfo;
    private TextView tv_email;
    private TextView tv_phone_number;
    private TextView tv_username;
    private TextView tv_letter_name;
    private TextView tv_address;
    private TextView tv_bank_detail;
    private APIInterface apiInterface;
    private TextView tv_approved_video;
    private TextView tv_total_earning;
    private LinearLayout lin_video;
    private RelativeLayout waitScreen;
    private TextView tv_title;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_profile);
        setupActionBar();
        mCastContext = CastContext.getSharedInstance(this);
        profileInfo = new SharedPreferenceHandler(this).getProfileInfo();
        apiInterface = APIClient.getClient().create(APIInterface.class);
        initViews();


    }

    private void initViews() {
        waitScreen = findViewById(R.id.waitScreen);
        tv_email = findViewById(R.id.tv_email);
        tv_phone_number = findViewById(R.id.tv_phone_number);
        tv_username = findViewById(R.id.tv_username);
        tv_letter_name = findViewById(R.id.tv_letter_name);
        tv_address = findViewById(R.id.tv_address);
        tv_bank_detail = findViewById(R.id.tv_bank_detail);
        tv_approved_video = findViewById(R.id.tv_approved_video);
        tv_total_earning = findViewById(R.id.tv_total_earning);
        lin_video = findViewById(R.id.lin_video);
        setProfileData();
        fetchUserUploadVideoDetails();
        setUpClickListener();
    }

    private void fetchUserUploadVideoDetails() {
        waitScreen.setVisibility(View.VISIBLE);
        Map<String, String> body = new HashMap<>();
        body.put(WebServiceConstant.USER_ID, "" + profileInfo.getId());
        Call<UserUploadVideoDetails> call = apiInterface.getUserVideoDetails(body);
        call.enqueue(new Callback<UserUploadVideoDetails>() {
            @Override
            public void onResponse(Call<UserUploadVideoDetails> call, Response<UserUploadVideoDetails> response) {
                waitScreen.setVisibility(View.GONE);
                if (response.code() == 200) {
                    tv_approved_video.setText(response.body().getData().getTotalapprovedvideos() + "/" + response.body().getData().getTotalvideos());
                    tv_total_earning.setText(getResources().getString(R.string.currency_dollor) + response.body().getData().getEarning());
                }
            }

            @Override
            public void onFailure(Call<UserUploadVideoDetails> call, Throwable t) {
                waitScreen.setVisibility(View.GONE);
            }
        });
    }

    private void setProfileData() {
        profileInfo = new SharedPreferenceHandler(this).getProfileInfo();
        tv_email.setText("" + profileInfo.getEmail());
        tv_phone_number.setText("");

        tv_phone_number.setHint("Add Phone Number");
        tv_phone_number.setHintTextColor(getResources().getColor(R.color.white_semi));

        tv_username.setText("User Name");
        tv_username.setHint("Add Your Name");
        tv_username.setHintTextColor(getResources().getColor(R.color.white_semi));

        if (profileInfo.getPhoneNumber() != null) {
            tv_phone_number.setText(profileInfo.getPhoneNumber());
          //  tv_phone_number.setCompoundDrawables(null, null, null, null);
        }

        if (profileInfo.getUserName() != null) {
            tv_username.setText(profileInfo.getUserName());
            //  tv_username.setCompoundDrawables(null,null,null,null);
        }

        if (profileInfo.getCity() != null) {
            tv_address.setText(profileInfo.getCountry() + ", " + profileInfo.getState() + ", " + profileInfo.getCity());
        } else {
            tv_address.setText("Add Address");
        }

        tv_letter_name.setText("" + tv_username.getText().toString().charAt(0));


    }

    private void setUpClickListener() {

        tv_phone_number.setOnClickListener(this);
        tv_email.setOnClickListener(this);
        tv_username.setOnClickListener(this);
        tv_address.setOnClickListener(this);
        tv_bank_detail.setOnClickListener(this);
        lin_video.setOnClickListener(this);
    }

    public void onBack() {
        onBackPressed();
    }

    private void setupActionBar() {
        mToolbar = (Toolbar) findViewById(R.id.toolbar);
        //mToolbar.setNavigationIcon(R.drawable.ic_backs_white_24);
        mToolbar.setTitle("");
        setSupportActionBar(mToolbar);
        //  getSupportActionBar().setDisplayHomeAsUpEnabled(true);

    }

    @Override
    public boolean onCreateOptionsMenu(Menu menu) {
        // Inflate the menu; this adds items to the action bar if it is present.
        getMenuInflater().inflate(R.menu.menu_cast, menu);
        mediaRouteMenuItem = CastButtonFactory.setUpMediaRouteButton(getApplicationContext(), menu,
                R.id.media_route_menu_item);
        return true;
    }

    @Override
    public boolean onOptionsItemSelected(@NonNull MenuItem item) {

        switch (item.getItemId()) {
            case android.R.id.home:
                // todo: goto back activity from here
                finish();
                return true;


            default:
                return super.onOptionsItemSelected(item);
        }

    }

    @Override
    public void onClick(View v) {
        switch (v.getId()) {
            case R.id.iv_back:
                finish();
                break;

            case R.id.lin_video:
                startActivity(new Intent(ProfileActivity.this, MyVideoActivity.class).putExtra(Constants.CATEGORY_TYPE,"My Videos"));
                break;

            case R.id.tv_username:
                startActivity(new Intent(ProfileActivity.this, EditProfileInfoActivity.class));

                break;

            case R.id.tv_address:
                startActivity(new Intent(ProfileActivity.this, EditAddressInfoActivity.class));

                break;

            case R.id.tv_phone_number:
                startActivity(new Intent(ProfileActivity.this, AddPhoneNumberActivity.class).
                        putExtra(Constants.ACTIVITY_TO_OPEN, ProfileActivity.class.getName()));

                break;

            case R.id.tv_bank_detail:
                startActivity(new Intent(ProfileActivity.this, AccountActivity.class));

                break;
        }
    }

    @Override
    protected void onResume() {
        super.onResume();
        setProfileData();
    }
}