package com.example.livestreamingapp.subscription;

import android.content.Context;
import android.media.Image;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.ListView;
import android.widget.TextView;

import androidx.annotation.NonNull;
import androidx.recyclerview.widget.RecyclerView;

import com.example.livestreamingapp.R;
import com.example.livestreamingapp.model.subscription.PaymentMethod;
import com.example.livestreamingapp.model.subscription.PaymentMethodData;
import com.example.livestreamingapp.utils.Constants;
import com.example.livestreamingapp.utils.SharedPreferenceHandler;
import com.example.livestreamingapp.webservice.WebServiceConstant;

import java.util.ArrayList;
import java.util.List;

public class AdapterPaymentMethod extends RecyclerView.Adapter<AdapterPaymentMethod.PaymentMethodHolder> {

    private final Context context;
    PaymentMethodListener paymentMethodListener;
    List<PaymentMethodData> paymentMethodList;
    private int selectedCardPosition = -2;

    public AdapterPaymentMethod(Context context, PaymentMethodListener paymentMethodListener, List<PaymentMethodData> paymentMethods){
        this.context = context;
        this.paymentMethodListener = paymentMethodListener;
        this.paymentMethodList = new ArrayList<>();
        this.paymentMethodList = paymentMethods;
    }

    @NonNull
    @Override
    public PaymentMethodHolder onCreateViewHolder(@NonNull ViewGroup parent, int viewType) {
        View view = LayoutInflater.from(context).inflate(R.layout._item_list_paymentmethod,parent,false);
        return new PaymentMethodHolder(view);
    }

    @Override
    public void onBindViewHolder(@NonNull PaymentMethodHolder holder, int position) {

        holder.iv_card_selected.setVisibility(View.GONE);
        holder.tv_card_number.setText(context.getResources().getString(R.string.card_start_place_holder)+""+
                paymentMethodList.get(position).getCard().getLast4());

        holder.lin_card.setBackground(context.getResources().getDrawable(R.drawable.shape_card_selection_default));

        if(selectedCardPosition==position){
            holder.lin_card.setBackground(context.getResources().getDrawable(R.drawable.shape_card_selection_selected));
            holder.iv_card_selected.setVisibility(View.VISIBLE);
        }




        holder.lin_card.setOnClickListener(new View.OnClickListener() {

            @Override
            public void onClick(View v) {
                holder.lin_card.setBackground(context.getResources().getDrawable(R.drawable.shape_card_selection_selected));
                paymentMethodListener.onSelectedPaymentMethod(paymentMethodList.get(position),position);
                holder.iv_card_selected.setVisibility(View.VISIBLE);
                selectedCardPosition = position;
                notifyDataSetChanged();
            }
        });

    }

    @Override
    public int getItemCount() {
        return paymentMethodList.size();
    }

    public class PaymentMethodHolder extends RecyclerView.ViewHolder{
        private ImageView iv_card;
        private TextView tv_card_number;
        private ImageView iv_card_selected;
        private LinearLayout lin_card;
        public PaymentMethodHolder(@NonNull View itemView) {
            super(itemView);
            iv_card = itemView.findViewById(R.id.iv_card);
            iv_card_selected = itemView.findViewById(R.id.iv_card_selected);
            tv_card_number = itemView.findViewById(R.id.tv_card_number);
            lin_card = itemView.findViewById(R.id.lin_card);
        }
    }


}
