package com.example.livestreamingapp.subscription;

import com.example.livestreamingapp.model.SubscriptionPlan;

import java.util.List;

public interface ApplyPromoListener {
    public void onPromoApplied(List<SubscriptionPlan> list,String promoCode);
}
