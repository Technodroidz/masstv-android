package com.example.livestreamingapp.subscription;

import android.content.Context;
import android.graphics.Color;
import android.graphics.drawable.ColorDrawable;
import android.graphics.drawable.InsetDrawable;
import android.os.Bundle;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.view.inputmethod.InputMethodManager;
import android.widget.Button;
import android.widget.EditText;
import android.widget.FrameLayout;
import android.widget.LinearLayout;
import android.widget.RelativeLayout;
import android.widget.TextView;
import android.widget.Toast;

import androidx.annotation.NonNull;
import androidx.appcompat.app.AlertDialog;
import androidx.fragment.app.Fragment;

import com.example.livestreamingapp.R;
import com.example.livestreamingapp.model.APIError;
import com.example.livestreamingapp.model.PaymentIntentModel;
import com.example.livestreamingapp.model.ProfileData;
import com.example.livestreamingapp.model.SubscriptionPlan;
import com.example.livestreamingapp.model.SubscriptionPlanResponse;
import com.example.livestreamingapp.utils.ConnectivityReceiver;
import com.example.livestreamingapp.utils.ErrorUtils;
import com.example.livestreamingapp.utils.SharedPreferenceHandler;
import com.example.livestreamingapp.utils.SnackBar;
import com.example.livestreamingapp.webservice.APIClient;
import com.example.livestreamingapp.webservice.APIInterface;
import com.example.livestreamingapp.webservice.WebServiceConstant;

import java.util.HashMap;
import java.util.List;
import java.util.Map;

import javax.annotation.Nullable;

import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;

/**
 * A simple {@link Fragment} subclass.
 * create an instance of this fragment.
 */
public class ApplyPromotionFragment extends Fragment implements View.OnClickListener {

    // TODO: Rename and change types and number of parameters

    EditText et_promo_code;
    Button btn_apply;
    TextView tv_error;
    FrameLayout ll_parent;
    ApplyPromoListener promoListener;
    private APIInterface apiInterface;
    private TextView tv_progress;
    private RelativeLayout waitScreen;


    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        // Inflate the layout for this fragment
        View view = inflater.inflate(R.layout.fragment_apply_promotion, container, false);
        promoListener = ((SubscriptionSelectorActivity) getActivity());
        apiInterface = APIClient.getClient().create(APIInterface.class);
        initViews(view);
        return view;
    }

    private void initViews(View view) {
        et_promo_code = view.findViewById(R.id.et_promo_code);
        btn_apply = view.findViewById(R.id.btn_apply);
        tv_error = view.findViewById(R.id.tv_error);
        tv_progress = view.findViewById(R.id.tv_progress);
        tv_progress.setText("Applying...");
        ll_parent = view.findViewById(R.id.parent);
        waitScreen = view.findViewById(R.id.waitScreen);

        tv_error.setVisibility(View.GONE);

        btn_apply.setOnClickListener(this);
        ll_parent.setOnClickListener(this);

    }

    @Override
    public void onClick(View v) {
        if (v.getId() == R.id.btn_apply) {
            // do action hit api
            if (!et_promo_code.getText().toString().isEmpty()) {
                if (checkConnection()) {
                    getPromoApplication(et_promo_code.getText().toString());
                } else {
                    new SnackBar(getContext()).showErrorSnack("No internet connection!");
                }

            } else {
                displayAlert("Validation Error", "Field can't be left blank!");
            }

        } else {
            // close
            hideKeyboard(ll_parent,getContext());
            if (getActivity() != null) {
                ((SubscriptionSelectorActivity) getActivity()).slideOutFragment();

            }
        }
    }

    private boolean checkConnection() {
        return ConnectivityReceiver.isConnected();
    }

    private void displayAlert(@NonNull String title,
                              @Nullable String message) {
        AlertDialog.Builder builder = new AlertDialog.Builder(getContext(), R.style.AlertDialogStyle)
                .setTitle(title)
                .setMessage(message);
        builder.setPositiveButton("Ok", null);
        builder.create().show();
    }

    private void getPromoApplication(String promoCode) {

        hideKeyboard(et_promo_code,getContext());
        waitScreen.setVisibility(View.VISIBLE);
        tv_error.setVisibility(View.GONE);
        Map<String, String> paymentIntent = new HashMap<>();
        ProfileData profileData = new SharedPreferenceHandler(getContext()).getProfileInfo();
        paymentIntent.put(WebServiceConstant.USER_ID, "" + profileData.getId());
        paymentIntent.put(WebServiceConstant.PROMO_CODE, "" + promoCode);

        Call<SubscriptionPlanResponse> applyPromo = apiInterface.applySubscription(paymentIntent);
        applyPromo.enqueue(new Callback<SubscriptionPlanResponse>() {
            @Override
            public void onResponse(Call<SubscriptionPlanResponse> call, Response<SubscriptionPlanResponse> response) {

                waitScreen.setVisibility(View.GONE);
                if (response.code() == 200) {
                    if (response.body().getSuccess()) {
                        List<SubscriptionPlan> list = response.body().getData();
                        promoListener.onPromoApplied(list, et_promo_code.getText().toString());
                       showSuccessFullUpdate();
                    }
                } else {

                    APIError error = ErrorUtils.parseError(response);
                    String errorMessage = error.getData().getError();

                    if(errorMessage==null){
                        errorMessage = "Something went wrong";
                    }
                    tv_error.setVisibility(View.VISIBLE);
                    tv_error.setText(errorMessage);

                }

            }

            @Override
            public void onFailure(Call<SubscriptionPlanResponse> call, Throwable t) {
                waitScreen.setVisibility(View.GONE);
            }
        });

    }

    private void showSuccessFullUpdate() {
        AlertDialog.Builder builder
                = new AlertDialog.Builder(getContext());
        // set the custom layout
        final View customLayout
                = getLayoutInflater()
                .inflate(
                        R.layout.layout_alert_loaded,
                        null);
        builder.setView(customLayout);

        TextView btn_subscribe = customLayout.findViewById(R.id.tv_btn_upload);
        TextView tv_result = customLayout.findViewById(R.id.tv_result);

        tv_result.setText("Promo Code '" +
                "" +et_promo_code.getText().toString()+
                "' Applied!");

        final AlertDialog dialog
                = builder.create();
        dialog.setCancelable(false);

        ColorDrawable back = new ColorDrawable(Color.TRANSPARENT);
        InsetDrawable inset = new InsetDrawable(back, 60, 0, 60, 0);
        if (dialog.getWindow() != null) {
            dialog.getWindow().setBackgroundDrawable(inset);

        }

     btn_subscribe.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                dialog.dismiss();
                if(getActivity()!=null){
                    ((SubscriptionSelectorActivity)getActivity()).slideOutFragment();
                }

            }
        });

        // create and show
        // the alert dialog

        dialog.show();
    }

    private void hideKeyboard(View view, Context context)
    {

        if (view != null) {

            // now assign the system
            // service to InputMethodManager
            InputMethodManager manager
                    = (InputMethodManager)
                    context.getSystemService(
                            Context.INPUT_METHOD_SERVICE);
            manager.hideSoftInputFromWindow(
                    view.getWindowToken(), 0);
        }
    }
}