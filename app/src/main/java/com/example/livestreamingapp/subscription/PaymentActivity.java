package com.example.livestreamingapp.subscription;

import android.content.DialogInterface;
import android.content.Intent;
import android.graphics.Color;
import android.graphics.drawable.ColorDrawable;
import android.graphics.drawable.InsetDrawable;
import android.os.Bundle;
import android.view.View;
import android.widget.Button;
import android.widget.RelativeLayout;
import android.widget.TextView;
import android.widget.Toast;

import androidx.annotation.NonNull;
import androidx.appcompat.app.AlertDialog;

import com.example.livestreamingapp.MassTvActivity;
import com.example.livestreamingapp.MassTvApplication;
import com.example.livestreamingapp.R;
import com.example.livestreamingapp.model.MySubscription;
import com.example.livestreamingapp.model.PaymentIntentModel;
import com.example.livestreamingapp.model.ProfileData;
import com.example.livestreamingapp.model.SubscriptionPlan;
import com.example.livestreamingapp.model.subscription.PaymentMethodData;
import com.example.livestreamingapp.model.success.PaymentSuccess;
import com.example.livestreamingapp.ondemand.OnDemandCategoryActivity;
import com.example.livestreamingapp.utils.ConnectivityReceiver;
import com.example.livestreamingapp.utils.CurrencyUtil;
import com.example.livestreamingapp.utils.SharedPreferenceHandler;
import com.example.livestreamingapp.webservice.APIClient;
import com.example.livestreamingapp.webservice.APIInterface;
import com.example.livestreamingapp.webservice.SubscriptionService;
import com.example.livestreamingapp.webservice.WebServiceConstant;
import com.google.android.material.snackbar.Snackbar;
import com.google.gson.Gson;
import com.google.gson.GsonBuilder;
import com.google.gson.reflect.TypeToken;
import com.stripe.android.ApiResultCallback;
import com.stripe.android.PaymentIntentResult;
import com.stripe.android.Stripe;
import com.stripe.android.model.ConfirmPaymentIntentParams;
import com.stripe.android.model.PaymentIntent;
import com.stripe.android.model.PaymentMethodCreateParams;
import com.stripe.android.view.CardInputWidget;
import com.stripe.okhttp3.OkHttpClient;

import java.io.IOException;
import java.lang.ref.WeakReference;
import java.lang.reflect.Type;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import java.util.Objects;

import javax.annotation.Nullable;

import retrofit2.Response;

public class PaymentActivity extends MassTvActivity implements
        ConnectivityReceiver.ConnectivityReceiverListener, View.OnClickListener {

    private OkHttpClient httpClient = new OkHttpClient();
    private String paymentIntentClientSecret;
    private Stripe stripe;
    private APIInterface apiInterface;
    private RelativeLayout waitScreen;
    private TextView tv_progress_message;
    private Button payButton;
    private ConnectivityReceiver connectivityReceiver;
    private List<PaymentMethodData> savedCardList;
    private SubscriptionPlan selectedPlan;
    private SharedPreferenceHandler sharedPreferenceHandler;
    private ProfileData profileData;


    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_payment);

        apiInterface = APIClient.getClient().create(APIInterface.class);
        waitScreen = findViewById(R.id.waitScreen);
        waitScreen.setVisibility(View.GONE);
        tv_progress_message = findViewById(R.id.tv_progress);
        tv_progress_message.setText("Payment in processing. \nDo not exit!");

        stripe = new Stripe(
                getApplicationContext(),
               // Objects.requireNonNull("pk_live_51Hr4iOGRw0lHExl0MEIDzpaRhMvD60CHeMa41E72yMhYq0J1gjK7uLaTAteO0VpL4NnpRBFf7pPGBU8cDrrsZZoW00vdMSN1OE")
                Objects.requireNonNull("pk_test_51Hr4iOGRw0lHExl0m7rCaOItmtVivMoP1m6bpDArtTUZUKm9xbGBpEoBdI0q9kkgRHbtqhWB8GANn0ifc3OVeLgs001c04BF9R")
        );

        Type listType = new TypeToken<ArrayList<PaymentMethodData>>() {
        }.getType();
        savedCardList =
                new Gson().fromJson(new SharedPreferenceHandler(this).
                        getPrefernceString(SharedPreferenceHandler.SAVED_CARDS), listType);
        sharedPreferenceHandler = new SharedPreferenceHandler(this);
        selectedPlan = sharedPreferenceHandler.getSelectedPlan();
        profileData = sharedPreferenceHandler.getProfileInfo();

        startCheckout();

    }


    private void startCheckout() {
        // Create a PaymentIntent by calling the server's endpoint.
        if (checkConnection()) {
            getClientSecretKey();
        } else {
            showSnack(checkConnection());
        }

        // Hook up the pay button to the card widget and stripe instance
        payButton = findViewById(R.id.payButton);
        payButton.setOnClickListener((View view) -> {
            if (checkConnection()) {
                CardInputWidget cardInputWidget = findViewById(R.id.cardInputWidget);
                PaymentMethodCreateParams params = cardInputWidget.getPaymentMethodCreateParams();
                if (params != null) {

                    showConfirmPaymentAlert(params);

                }
            } else {
                showSnack(checkConnection());
            }

        });
    }

    private void showConfirmPaymentAlert(PaymentMethodCreateParams params) {
        AlertDialog.Builder builder
                = new AlertDialog.Builder(this);
        // set the custom layout
        final View customLayout
                = getLayoutInflater()
                .inflate(
                        R.layout.layout_alert_confirm_pay,
                        null);
        builder.setView(customLayout);

        Button btn_pay = customLayout.findViewById(R.id.btn_pay);
        Button btn_cancel = customLayout.findViewById(R.id.btn_cancel);

        TextView tv_alert_heading = customLayout.findViewById(R.id.tv_alert_heading);

        TextView tv_plan_type = customLayout.findViewById(R.id.tv_plan_type);
        TextView tv_plan_cost = customLayout.findViewById(R.id.tv_plan_cost);


        tv_plan_cost.setText(CurrencyUtil.getCurrencySymbol(selectedPlan.getCurrency())+"" + selectedPlan.getPrice());
        tv_plan_type.setText("" + selectedPlan.getPlan());

        //Are your confirm to pay using Visa Card ending with 4142
        tv_alert_heading.setText("Are you sure to pay using this card?");


        final AlertDialog dialog
                = builder.create();
        dialog.setCancelable(false);

        ColorDrawable back = new ColorDrawable(Color.TRANSPARENT);
        InsetDrawable inset = new InsetDrawable(back, 60, 0, 60, 0);
        if (dialog.getWindow() != null) {
            dialog.getWindow().setBackgroundDrawable(inset);

        }

        btn_pay.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                dialog.dismiss();
                if (checkConnection()) {
                    if (paymentIntentClientSecret != null) {
                        paymentThroughCard(params);
                    } else {
                        getClientSecretKey();
                    }
                } else {
                    showSnack(checkConnection());
                }

            }
        });

        btn_cancel.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                dialog.dismiss();


            }
        });

        // create and show
        // the alert dialog

        dialog.show();

    }

    private void paymentThroughCard(PaymentMethodCreateParams params) {
        waitScreen.setVisibility(View.VISIBLE);
        Map<String, String> extraParams = new HashMap<>();
        extraParams.put("setup_future_usage", "off_session");

        ConfirmPaymentIntentParams confirmParams = ConfirmPaymentIntentParams
                .createWithPaymentMethodCreateParams(params, paymentIntentClientSecret, null, false, extraParams);
        stripe.confirmPayment(this, confirmParams);

    }


    private void getClientSecretKey() {

        Map<String, String> paymentIntent = new HashMap<>();
        paymentIntent.put(WebServiceConstant.SUBSCRIPTION_ID,""+selectedPlan.getId());
        paymentIntent.put(WebServiceConstant.USER_ID, ""+profileData.getId());
        paymentIntent.put(WebServiceConstant.CURRENCY, ""+selectedPlan.getCurrency());

        final retrofit2.Call<PaymentIntentModel> paymentIntentCall = apiInterface.getPaymentIntent(paymentIntent);

        paymentIntentCall.enqueue(new retrofit2.Callback<PaymentIntentModel>() {
            @Override
            public void onResponse(retrofit2.Call<PaymentIntentModel> call, retrofit2.Response<PaymentIntentModel> response) {

                if (!response.isSuccessful()) {
                    payButton.setEnabled(false);
                    runOnUiThread(() ->
                            Toast.makeText(
                                    PaymentActivity.this, "Error: " + "Please Check your internet connection", Toast.LENGTH_LONG
                            ).show()
                    );
                } else {
                    try {
                        onPaymentSuccess(response);
                    } catch (IOException e) {
                        e.printStackTrace();
                    }
                }
            }

            @Override
            public void onFailure(retrofit2.Call<PaymentIntentModel> call, Throwable t) {
                final PaymentActivity activity = PaymentActivity.this;

                activity.runOnUiThread(() ->
                        Toast.makeText(
                                activity, "Error: " + t.toString(), Toast.LENGTH_LONG
                        ).show()
                );
            }
        });
    }


    private void onPaymentSuccess(@NonNull final Response<PaymentIntentModel> response) throws IOException {
        payButton.setEnabled(true);

        String jsonString = new Gson().toJson(response.body());
        PaymentIntentModel paymentIntentModel = (PaymentIntentModel) new Gson().fromJson(jsonString,
                PaymentIntentModel.class);
        paymentIntentClientSecret = paymentIntentModel.getPaymentIntentData().getClient_secret();
    }

    // Method to manually check connection status
    private boolean checkConnection() {
        return ConnectivityReceiver.isConnected();
    }


    // Showing the status in Snackbar
    private void showSnack(boolean isConnected) {
        String message;
        int color;
        if (isConnected) {
            message = "Good! Connected to Internet";
            color = Color.WHITE;
        } else {
            message = "Offline! Not connected to internet";
            color = Color.RED;
            Snackbar snackbar = Snackbar
                    .make(findViewById(R.id.coordinator), message, Snackbar.LENGTH_LONG);

            View sbView = snackbar.getView();
            TextView textView = (TextView) sbView.findViewById(R.id.snackbar_text);
            textView.setTextColor(color);
            snackbar.show();
        }


    }

    @Override
    protected void onResume() {
        super.onResume();
        MassTvApplication.getInstance().setConnectivityListener(this);
    }

    @Override
    protected void onPause() {
        super.onPause();
        try {
            MassTvApplication.getInstance().setUnRegister();
        } catch (Exception e) {

        }
    }

    @Override
    public void onNetworkConnectionChanged(boolean isConnected) {
        showSnack(isConnected);
        if (paymentIntentClientSecret == null && isConnected) {
            getClientSecretKey();
        }
    }

    @Override
    public void onClick(View v) {
        switch (v.getId()) {
            case R.id.iv_back:  if(waitScreen.getVisibility()==View.VISIBLE){
                // since payment is in process
            }else {
                startActivity(new Intent(PaymentActivity.this, PaymentMethodSelectorActivity.class));
                finish();
            }

        }
    }

    private class PaymentResultCallback
            implements ApiResultCallback<PaymentIntentResult> {
        @NonNull
        private final WeakReference<PaymentActivity> activityRef;

        PaymentResultCallback(@NonNull PaymentActivity activity) {
            activityRef = new WeakReference<>(activity);
        }

        @Override
        public void onSuccess(@NonNull PaymentIntentResult result) {


            final PaymentActivity activity = activityRef.get();
            if (activity == null) {
                try {
                    waitScreen.setVisibility(View.GONE);
                } catch (Exception e) {

                }
                return;
            }


            PaymentIntent paymentIntent = result.getIntent();
            PaymentIntent.Status status = paymentIntent.getStatus();
            if (status == PaymentIntent.Status.Succeeded) {
                Gson gson = new GsonBuilder().setPrettyPrinting().create();
                PaymentSuccess paymentSuccess = new Gson().fromJson(gson.toJson(paymentIntent),PaymentSuccess.class);
                getSubscriptionDetail(paymentSuccess);

            } else if (status == PaymentIntent.Status.RequiresPaymentMethod) {
                // Payment failed – allow retrying using a different payment method
                AlertDialog.Builder builder = new AlertDialog.Builder(PaymentActivity.this,
                        R.style.AlertDialogStyle)
                        .setTitle("Payment failed")
                        .setMessage(""+paymentIntent.getLastPaymentError());
                builder.setPositiveButton("Ok", new DialogInterface.OnClickListener() {
                    @Override
                    public void onClick(DialogInterface dialog, int which) {
                        finish();
                    }
                });
                builder.create().show();

            }
        }

        @Override
        public void onError(@NonNull Exception e) {
            final PaymentActivity activity = activityRef.get();
            if (activity == null) {
                return;
            }

            try {
                waitScreen.setVisibility(View.GONE);
            } catch (Exception e1) {

            }
            // Payment request failed – allow retrying using the same payment method
            activity.displayAlert("Error", e.toString());
        }
    }

    /**
     * String userId, String subscription_id,
     *                             String payment_id,String clientKey,String method_id,String status
     * @param paymentSuccess
     */

    private void getSubscriptionDetail(PaymentSuccess paymentSuccess) {
        new SubscriptionService(PaymentActivity.this).
                verifyPayment(""+profileData.getId(),
                        ""+selectedPlan.getId(),
                        paymentSuccess.getId(),
                        paymentIntentClientSecret,
                        paymentSuccess.getPaymentMethodId(),
                        paymentSuccess.getStatus(),
                        new SubscriptionService.onSubscriptionInfoListener() {
                    @Override
                    public void onSubscriptionInfo(MySubscription subscriptionInfo) {
                        waitScreen.setVisibility(View.GONE);
                        new SharedPreferenceHandler(PaymentActivity.this).
                                setPreferenceBoolean(SharedPreferenceHandler.SUBSCRIBED, true);

                        if (subscriptionInfo != null) {
                            new SharedPreferenceHandler(PaymentActivity.this).saveSubscription(subscriptionInfo);
                        }
                       showSuccessFullSubscription();
                    }

                    @Override
                    public void onFailure(String error) {

                        waitScreen.setVisibility(View.GONE);

                        new AlertDialog.Builder(PaymentActivity.this, R.style.AlertDialogStyle).setCancelable(false)
                                .setMessage("" + error).setPositiveButton("Ok", new DialogInterface.OnClickListener() {
                            @Override
                            public void onClick(DialogInterface dialog, int which) {
                                finish();
                            }
                        }).show();

                    }
                });
    }


    private void showSuccessFullSubscription() {
        AlertDialog.Builder builder
                = new AlertDialog.Builder(this);
        // set the custom layout
        final View customLayout
                = getLayoutInflater()
                .inflate(
                        R.layout.layout_alert_loaded,
                        null);
        builder.setView(customLayout);

        TextView btn_subscribe = customLayout.findViewById(R.id.tv_btn_upload);
        TextView tv_result = customLayout.findViewById(R.id.tv_result);

        tv_result.setText("Subscription Successful!");

        final AlertDialog dialog
                = builder.create();
        dialog.setCancelable(false);

        ColorDrawable back = new ColorDrawable(Color.TRANSPARENT);
        InsetDrawable inset = new InsetDrawable(back, 60, 0, 60, 0);
        if (dialog.getWindow() != null) {
            dialog.getWindow().setBackgroundDrawable(inset);

        }

        btn_subscribe.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                dialog.dismiss();
                SubscriptionSelectorActivity.promoCode = null;
                startActivity(new Intent(PaymentActivity.this, OnDemandCategoryActivity.class));
                finish();
            }
        });

        // create and show
        // the alert dialog

        dialog.show();
    }


    private void displayAlert(@NonNull String title,
                              @Nullable String message) {
        AlertDialog.Builder builder = new AlertDialog.Builder(this, R.style.AlertDialogStyle)
                .setTitle(title)
                .setMessage(message);
        builder.setPositiveButton("Ok", null);
        builder.create().show();
    }

    @Override
    protected void onActivityResult(int requestCode, int resultCode, Intent data) {
        super.onActivityResult(requestCode, resultCode, data);

        // Handle the result of stripe.confirmPayment
        stripe.onPaymentResult(requestCode, data, new PaymentResultCallback(this));
    }

    @Override
    public void onBackPressed() {
        //super.onBackPressed();
        if(waitScreen.getVisibility()==View.VISIBLE){
            // since payment is in process
        }else {
            startActivity(new Intent(PaymentActivity.this, PaymentMethodSelectorActivity.class));
            finish();
        }
    }
}