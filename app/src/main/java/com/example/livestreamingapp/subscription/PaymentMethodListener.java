package com.example.livestreamingapp.subscription;

import com.example.livestreamingapp.model.subscription.PaymentMethod;
import com.example.livestreamingapp.model.subscription.PaymentMethodData;

public interface PaymentMethodListener {
    public void onSelectedPaymentMethod(PaymentMethodData paymentMethodData,int position);
}
