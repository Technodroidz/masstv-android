package com.example.livestreamingapp.subscription;

import android.content.DialogInterface;
import android.content.Intent;
import android.graphics.Color;
import android.graphics.drawable.ColorDrawable;
import android.graphics.drawable.InsetDrawable;
import android.os.Bundle;
import android.util.Log;
import android.view.Menu;
import android.view.MenuInflater;
import android.view.MenuItem;
import android.view.View;
import android.widget.Button;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.ListView;
import android.widget.RelativeLayout;
import android.widget.TextView;
import android.widget.Toast;

import androidx.annotation.NonNull;
import androidx.appcompat.app.AlertDialog;
import androidx.appcompat.app.AppCompatActivity;
import androidx.appcompat.widget.Toolbar;
import androidx.cardview.widget.CardView;
import androidx.recyclerview.widget.LinearLayoutManager;
import androidx.recyclerview.widget.RecyclerView;

import com.example.livestreamingapp.MassTvActivity;
import com.example.livestreamingapp.MassTvApplication;
import com.example.livestreamingapp.R;
import com.example.livestreamingapp.model.CustomerModelStripe;
import com.example.livestreamingapp.model.DemoPlanModel;
import com.example.livestreamingapp.model.MySubscription;
import com.example.livestreamingapp.model.PaymentIntentModel;
import com.example.livestreamingapp.model.ProfileData;
import com.example.livestreamingapp.model.SavedCardResponse;
import com.example.livestreamingapp.model.SubscriptionPlan;
import com.example.livestreamingapp.model.subscription.PaymentMethod;
import com.example.livestreamingapp.model.subscription.PaymentMethodData;
import com.example.livestreamingapp.model.success.PaymentSuccess;
import com.example.livestreamingapp.ondemand.OnDemandCategoryActivity;
import com.example.livestreamingapp.utils.ConnectivityReceiver;
import com.example.livestreamingapp.utils.CurrencyUtil;
import com.example.livestreamingapp.utils.SharedPreferenceHandler;
import com.example.livestreamingapp.webservice.APIClient;
import com.example.livestreamingapp.webservice.APIInterface;
import com.example.livestreamingapp.webservice.SubscriptionService;
import com.example.livestreamingapp.webservice.WebServiceConstant;
import com.google.android.gms.cast.framework.CastButtonFactory;
import com.google.android.gms.cast.framework.CastContext;
import com.google.android.material.snackbar.Snackbar;
import com.google.gson.Gson;
import com.google.gson.GsonBuilder;
import com.stripe.android.ApiResultCallback;
import com.stripe.android.PaymentIntentResult;
import com.stripe.android.Stripe;
import com.stripe.android.model.ConfirmPaymentIntentParams;
import com.stripe.android.model.PaymentIntent;
import com.stripe.android.model.PaymentMethodCreateParams;
import com.stripe.android.model.PaymentMethodOptionsParams;
import com.stripe.android.view.CardInputWidget;

import java.io.IOException;
import java.lang.ref.WeakReference;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import java.util.Objects;

import javax.annotation.Nullable;

import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;

public class PaymentMethodSelectorActivity extends MassTvActivity implements View.OnClickListener,
        ConnectivityReceiver.ConnectivityReceiverListener, PaymentMethodListener {

    private Toolbar mToolbar;
    private LinearLayout lin_debit;
    private LinearLayout lin_credit;
    private LinearLayout lin_paypal;
    private ImageView iv_debit;
    private ImageView iv_credit;
    private ImageView iv_paypal;
    private MenuItem mediaRouteMenuItem;
    private CastContext mCastContext;
    private RelativeLayout waitScreen;

    private TextView tv_progress;

    private RecyclerView rv_payment_method;

    private CardView cv_saved_card_list;
    private APIInterface apiInterface;
    CustomerModelStripe.CustomerData customerData;
    private MenuItem actionDone;
    List<PaymentMethodData> paymentMethodList;
    private AdapterPaymentMethod adapterPaymentMethod;

    PaymentMethodData defaultPaymentMethod;
    private Button btn_add_card;
    private Stripe stripe;

    private TextView tv_default_payment_method;
    private String paymentIntentClientSecret;

    private SharedPreferenceHandler sharedPreferenceHandler;
    private String defaultPaymentMethodId;

    private SubscriptionPlan selectedPlan;
    private ProfileData profileData;


    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_payment_method_selector);

        mToolbar = (Toolbar) findViewById(R.id.toolbar);
        mToolbar.setNavigationIcon(R.drawable.ic_backs_white_24);
        mToolbar.setTitle("");
        setSupportActionBar(mToolbar);
        getSupportActionBar().setDisplayHomeAsUpEnabled(true);
        mCastContext = CastContext.getSharedInstance(this);
        apiInterface = APIClient.getClient().create(APIInterface.class);
        sharedPreferenceHandler = new SharedPreferenceHandler(this);
        defaultPaymentMethodId = sharedPreferenceHandler.getPrefernceString(SharedPreferenceHandler.DEFAULT_CARD);
        selectedPlan = sharedPreferenceHandler.getSelectedPlan();

        initializeStripe();

        initViews();
    }

    private void initializeStripe() {
        stripe = new Stripe(
                getApplicationContext(),
                Objects.requireNonNull("pk_live_51Hr4iOGRw0lHExl0MEIDzpaRhMvD60CHeMa41E72yMhYq0J1gjK7uLaTAteO0VpL4NnpRBFf7pPGBU8cDrrsZZoW00vdMSN1OE")
        );
    }


    private void initViews() {


        waitScreen = findViewById(R.id.waitScreen);
        tv_progress = findViewById(R.id.tv_progress);
        tv_progress.setText("Please wait...");
        rv_payment_method = findViewById(R.id.rv_saved_card);
        cv_saved_card_list = findViewById(R.id.cv_saved_card_list);
        tv_default_payment_method = findViewById(R.id.tv_default_payment_method);

        btn_add_card = findViewById(R.id.btn_add_card);
        btn_add_card.setOnClickListener(this);


        LinearLayoutManager layoutManager = new LinearLayoutManager(this);
        rv_payment_method.setLayoutManager(layoutManager);
        adapterPaymentMethod = new AdapterPaymentMethod(this, this, new ArrayList<>());
        profileData = new SharedPreferenceHandler(this).getProfileInfo();

        rv_payment_method.setAdapter(adapterPaymentMethod);
        getCustomerId();


    }

    private void getCustomerId() {
        btn_add_card.setEnabled(false);
        waitScreen.setVisibility(View.VISIBLE);
        final Map<String, String> customer = new HashMap<>();
        customer.put(WebServiceConstant.USER_ID, "" + profileData.getId());
        final Call<CustomerModelStripe> customerModelCall = apiInterface.getStripeCustomerId(customer);
        customerModelCall.enqueue(new Callback<CustomerModelStripe>() {
            @Override
            public void onResponse(Call<CustomerModelStripe> call, Response<CustomerModelStripe> response) {
                // waitScreen.setVisibility(View.GONE);
                customerData = response.body().getCustomerData();
                if (customerData != null && customerData.getCustomer_id() != null) {
                    getSavedCardList();
                }

            }

            @Override
            public void onFailure(Call<CustomerModelStripe> call, Throwable t) {
                waitScreen.setVisibility(View.GONE);
            }
        });

    }


    private void getSavedCardList() {

        tv_progress.setText("Please wait...");

        waitScreen.setVisibility(View.VISIBLE);
        actionDone.setVisible(false);

        final Map<String, String> subscription = new HashMap<>();
        subscription.put(WebServiceConstant.CUSTOMER_ID, "" + customerData.getCustomer_id());
        final Call<SavedCardResponse> savedCardCall = apiInterface.getMySavedCardList(subscription);
        savedCardCall.enqueue(new Callback<SavedCardResponse>() {
            @Override
            public void onResponse(Call<SavedCardResponse> call, Response<SavedCardResponse> response) {
                waitScreen.setVisibility(View.GONE);
                if (response.code() == 200) {

                    List<PaymentMethodData> allCardList = new ArrayList<>();
                    paymentMethodList = new ArrayList<>();
                    allCardList = response.body().getPayment().getData();
                    btn_add_card.setEnabled(true);
                    if (allCardList != null && allCardList.size() > 0) {
                        for (int i = 0; i < allCardList.size(); i++) {

                            if (paymentMethodList.size() > 0) {
                                boolean hasData = false;
                                for (int j = 0; j < paymentMethodList.size(); j++) {
                                    if (paymentMethodList.get(j).getCard().getLast4().
                                            equalsIgnoreCase(allCardList.get(i).getCard().getLast4())) {
                                        hasData = true;
                                        break;
                                    }
                                }
                                if (!hasData) {
                                    paymentMethodList.add(allCardList.get(i));
                                }
                            } else {
                                paymentMethodList.add(allCardList.get(i));
                            }

                        }
                    } else {
                        tv_default_payment_method.setText("No Saved Card");
                    }
                  /*  try {
                        sharedPreferenceHandler.setPreference(SharedPreferenceHandler.SAVED_CARDS, new Gson().toJson(paymentMethodList));
                    } catch (Exception e) {

                    }*/
                    if (paymentMethodList.size() > 0) {
                        if (defaultPaymentMethodId != null) {
                            for (int i = 0; i < paymentMethodList.size(); i++) {
                                if (paymentMethodList.get(i).getId().equalsIgnoreCase(defaultPaymentMethodId)) {
                                    defaultPaymentMethod = paymentMethodList.get(i);
                                    break;
                                }
                            }
                        } else {
                            defaultPaymentMethod = paymentMethodList.get(0);
                        }

                        try {
                            tv_default_payment_method.setText(getResources().getString(R.string.card_start_place_holder) +
                                    "" + defaultPaymentMethod.getCard().getLast4());
                            cv_saved_card_list.setVisibility(View.VISIBLE);
                            actionDone.setVisible(true);

                        } catch (Exception e) {

                        }
                        adapterPaymentMethod = new AdapterPaymentMethod(PaymentMethodSelectorActivity.this,
                                PaymentMethodSelectorActivity.this, paymentMethodList);
                        rv_payment_method.setAdapter(adapterPaymentMethod);


                    }

                }


            }

            @Override
            public void onFailure(Call<SavedCardResponse> call, Throwable t) {
                waitScreen.setVisibility(View.GONE);
                btn_add_card.setEnabled(true);
            }
        });

    }


    private void getClientSecretKey() {
        waitScreen.setVisibility(View.VISIBLE);
        tv_progress.setText("Payment in processing. \nDo not exit!");

        btn_add_card.setEnabled(false);

        ProfileData profileData = new SharedPreferenceHandler(this).getProfileInfo();
        Map<String, String> paymentIntent = new HashMap<>();
        paymentIntent.put(WebServiceConstant.SUBSCRIPTION_ID,""+selectedPlan.getId());
        paymentIntent.put(WebServiceConstant.USER_ID, ""+profileData.getId());
        paymentIntent.put(WebServiceConstant.CURRENCY, ""+selectedPlan.getCurrency());

        final retrofit2.Call<PaymentIntentModel> paymentIntentCall = apiInterface.getPaymentIntent(paymentIntent);
        paymentIntentCall.enqueue(new retrofit2.Callback<PaymentIntentModel>() {
            @Override
            public void onResponse(retrofit2.Call<PaymentIntentModel> call, retrofit2.Response<PaymentIntentModel> response) {

                btn_add_card.setEnabled(true);

                if (!response.isSuccessful()) {
                    runOnUiThread(() ->
                            displayAlert("Error", "Error: Please Check your internet connection")
                    );
                } else {
                    try {
                        onPaymentSuccess(response);
                    } catch (IOException e) {
                        e.printStackTrace();
                    }
                }
            }

            @Override
            public void onFailure(retrofit2.Call<PaymentIntentModel> call, Throwable t) {
                final PaymentMethodSelectorActivity activity = PaymentMethodSelectorActivity.this;
                btn_add_card.setEnabled(true);
                activity.runOnUiThread(() ->
                        displayAlert("Error", "Error: Please Check your internet connection")
                );
            }
        });
    }

    @Override
    public boolean onOptionsItemSelected(@NonNull MenuItem item) {

        switch (item.getItemId()) {
            case android.R.id.home:
                // todo: goto back activity from here
                onBackPressed();
                return true;

            case R.id.action_done:
                // todo: goto back activity from here
            /*    startActivity(new Intent(PaymentMethodSelectorActivity.this, OnDemandCategoryActivity.class));
                finish();*/
                //  buySubscription();

                if (checkConnection()) {
                    if (defaultPaymentMethod != null) {
                        showConfirmPaymentAlert();
                    } else {
                        Toast.makeText(this, "Please pay using add new method!", Toast.LENGTH_LONG).show();
                    }

                } else {
                    showSnack(checkConnection());
                }


                return true;

            default:
                return super.onOptionsItemSelected(item);
        }
    }

    private void buySubscription() {

    }

    private void showSuccessFullSubscription() {
        AlertDialog.Builder builder
                = new AlertDialog.Builder(this);
        // set the custom layout
        final View customLayout
                = getLayoutInflater()
                .inflate(
                        R.layout.layout_alert_loaded,
                        null);
        builder.setView(customLayout);

        TextView btn_subscribe = customLayout.findViewById(R.id.tv_btn_upload);
        TextView tv_result = customLayout.findViewById(R.id.tv_result);

        tv_result.setText("Subscription Successful!");

        final AlertDialog dialog
                = builder.create();
        dialog.setCancelable(false);

        ColorDrawable back = new ColorDrawable(Color.TRANSPARENT);
        InsetDrawable inset = new InsetDrawable(back, 60, 0, 60, 0);
        if (dialog.getWindow() != null) {
            dialog.getWindow().setBackgroundDrawable(inset);

        }

        btn_subscribe.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                dialog.dismiss();
                SubscriptionSelectorActivity.promoCode = null;
                startActivity(new Intent(PaymentMethodSelectorActivity.this, OnDemandCategoryActivity.class));
                finish();
            }
        });

        // create and show
        // the alert dialog

        dialog.show();
    }


    private void showConfirmPaymentAlert() {
        AlertDialog.Builder builder
                = new AlertDialog.Builder(this);
        // set the custom layout
        final View customLayout
                = getLayoutInflater()
                .inflate(
                        R.layout.layout_alert_confirm_pay,
                        null);
        builder.setView(customLayout);

        Button btn_pay = customLayout.findViewById(R.id.btn_pay);
        Button btn_cancel = customLayout.findViewById(R.id.btn_cancel);

        TextView tv_alert_heading = customLayout.findViewById(R.id.tv_alert_heading);

        TextView tv_plan_type = customLayout.findViewById(R.id.tv_plan_type);
        TextView tv_plan_cost = customLayout.findViewById(R.id.tv_plan_cost);

        tv_alert_heading.setText("Are you sure to pay using " +
                defaultPaymentMethod.getCard().getBrand() +
                " card " + "ending with " + defaultPaymentMethod.getCard().getLast4() + "?");


        tv_plan_cost.setText(CurrencyUtil.getCurrencySymbol(selectedPlan.getCurrency())+"" + selectedPlan.getPrice());
        tv_plan_type.setText("" + selectedPlan.getPlan());

        //Are your confirm to pay using Visa Card ending with 4142


        final AlertDialog dialog
                = builder.create();
        dialog.setCancelable(false);

        ColorDrawable back = new ColorDrawable(Color.TRANSPARENT);
        InsetDrawable inset = new InsetDrawable(back, 60, 0, 60, 0);
        if (dialog.getWindow() != null) {
            dialog.getWindow().setBackgroundDrawable(inset);

        }

        btn_pay.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                dialog.dismiss();
                if (checkConnection()) {
                    if (paymentIntentClientSecret != null) {
                        tv_progress.setText("Payment in processing. \nDo not exit!");
                        paymentThroughCard();
                    } else {
                        getClientSecretKey();
                    }
                } else {
                    showSnack(checkConnection());
                }

            }
        });

        btn_cancel.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                dialog.dismiss();


            }
        });

        // create and show
        // the alert dialog

        dialog.show();

    }


    @Override
    public void onClick(View v) {

        switch (v.getId()) {
            case R.id.btn_add_card:
                startActivity(new Intent(PaymentMethodSelectorActivity.this, PaymentActivity.class));
                finish();
                break;
        }


       /* iv_paypal.setVisibility(View.GONE);
        iv_credit.setVisibility(View.GONE);
        iv_debit.setVisibility(View.GONE);

        lin_paypal.setBackground(getResources().getDrawable(R.drawable.shape_card_selection_default));
        lin_credit.setBackground(getResources().getDrawable(R.drawable.shape_card_selection_default));
        lin_debit.setBackground(getResources().getDrawable(R.drawable.shape_card_selection_default));
*/
    }


    @Override
    public boolean onCreateOptionsMenu(Menu menu) {
        // Inflate the menu; this adds items to the action bar if it is present.
        MenuInflater inflater = getMenuInflater();
        inflater.inflate(R.menu.menu_done, menu);
        actionDone = menu.findItem(R.id.action_done);

        mediaRouteMenuItem = CastButtonFactory.setUpMediaRouteButton(getApplicationContext(), menu,
                R.id.media_route_menu_item);

        return true;
    }

    @Override
    protected void onResume() {
        super.onResume();
        MassTvApplication.getInstance().setConnectivityListener(this);
    }

    @Override
    protected void onPause() {
        super.onPause();
        try {
            MassTvApplication.getInstance().setUnRegister();
        } catch (Exception e) {

        }
    }

    @Override
    public void onBackPressed() {
        // super.onBackPressed();
        startActivity(new Intent(PaymentMethodSelectorActivity.this, SubscriptionSelectorActivity.class));
        finish();
    }

    // Showing the status in Snackbar
    private void showSnack(boolean isConnected) {
        String message;
        int color;
        if (isConnected) {
            message = "Good! Connected to Internet";
            color = Color.WHITE;
        } else {
            message = "Offline! Not connected to internet";
            color = Color.RED;
            Snackbar snackbar = Snackbar
                    .make(findViewById(R.id.parent), message, Snackbar.LENGTH_LONG);

            View sbView = snackbar.getView();
            TextView textView = (TextView) sbView.findViewById(R.id.snackbar_text);
            textView.setTextColor(color);
            snackbar.show();
        }


    }

    @Override
    public void onNetworkConnectionChanged(boolean isConnected) {
        showSnack(isConnected);
        if (stripe == null && isConnected) {
            initializeStripe();
        }
        if (isConnected && customerData == null) {

            getCustomerId();
        }
    }

    // Method to manually check connection status
    private boolean checkConnection() {
        return ConnectivityReceiver.isConnected();
    }

    @Override
    public void onSelectedPaymentMethod(PaymentMethodData paymentMethodData, int postiton) {

        this.defaultPaymentMethod = paymentMethodData;
        if (!rv_payment_method.isComputingLayout()) {
            rv_payment_method.scrollToPosition(postiton);
        }

    }

    private class PaymentResultCallback
            implements ApiResultCallback<PaymentIntentResult> {
        @NonNull
        private final WeakReference<PaymentMethodSelectorActivity> activityRef;

        PaymentResultCallback(@NonNull PaymentMethodSelectorActivity activity) {
            activityRef = new WeakReference<>(activity);
        }

        @Override
        public void onSuccess(@NonNull PaymentIntentResult result) {

            btn_add_card.setEnabled(true);

            final PaymentMethodSelectorActivity activity = activityRef.get();
            if (activity == null) {
                try {
                    waitScreen.setVisibility(View.GONE);
                } catch (Exception e) {

                }
                return;
            }

          /*  try {
                waitScreen.setVisibility(View.GONE);
            } catch (Exception e) {

            }*/
            PaymentIntent paymentIntent = result.getIntent();
            PaymentIntent.Status status = paymentIntent.getStatus();
            if (status == PaymentIntent.Status.Succeeded) {
                Gson gson = new GsonBuilder().setPrettyPrinting().create();
                PaymentSuccess paymentSuccess = new Gson().fromJson(gson.toJson(paymentIntent),PaymentSuccess.class);
                getSubscriptionDetail(paymentSuccess);

            } else if (status == PaymentIntent.Status.RequiresPaymentMethod) {
                // Payment failed – allow retrying using a different payment method
                AlertDialog.Builder builder = new AlertDialog.Builder(PaymentMethodSelectorActivity.this,
                        R.style.AlertDialogStyle)
                        .setTitle("Payment failed")
                        .setMessage(""+paymentIntent.getLastPaymentError());
                builder.setPositiveButton("Ok", new DialogInterface.OnClickListener() {
                    @Override
                    public void onClick(DialogInterface dialog, int which) {
                        finish();
                    }
                });
                builder.create().show();

            }
        }

        @Override
        public void onError(@NonNull Exception e) {
            btn_add_card.setEnabled(true);
            final PaymentMethodSelectorActivity activity = activityRef.get();
            if (activity == null) {
                return;
            }


            try {
                waitScreen.setVisibility(View.GONE);
            } catch (Exception e1) {

            }
            // Payment request failed – allow retrying using the same payment method
            activity.displayAlert("Error", e.toString());
        }
    }



    /**
     * String userId, String subscription_id,
     *                             String payment_id,String clientKey,String method_id,String status
     * @param paymentSuccess
     */
    private void getSubscriptionDetail(PaymentSuccess paymentSuccess) {
        waitScreen.setVisibility(View.VISIBLE);
        new SubscriptionService(PaymentMethodSelectorActivity.this).
                verifyPayment(""+profileData.getId(),""+selectedPlan.getId(),
                        paymentSuccess.getId(),
                        paymentIntentClientSecret,
                        paymentSuccess.getPaymentMethodId(),
                        paymentSuccess.getStatus(),
                        new SubscriptionService.onSubscriptionInfoListener() {
                    @Override
                    public void onSubscriptionInfo(MySubscription subscriptionInfo) {
                        waitScreen.setVisibility(View.GONE);
                        new SharedPreferenceHandler(PaymentMethodSelectorActivity.this).
                                setPreferenceBoolean(SharedPreferenceHandler.SUBSCRIBED, true);

                        if (subscriptionInfo != null) {
                            new SharedPreferenceHandler(PaymentMethodSelectorActivity.this).saveSubscription(subscriptionInfo);
                        }
                        showSuccessFullSubscription();
                    }

                    @Override
                    public void onFailure(String error) {

                        waitScreen.setVisibility(View.GONE);

                        new AlertDialog.Builder(PaymentMethodSelectorActivity.this, R.style.AlertDialogStyle)
                                .setCancelable(false)
                                .setMessage("" + error).setPositiveButton("Ok", new DialogInterface.OnClickListener() {
                            @Override
                            public void onClick(DialogInterface dialog, int which) {

                                finish();

                            }
                        }).show();

                    }
                });
    }

    private void displayAlert(@NonNull String title,
                              @Nullable String message) {
        AlertDialog.Builder builder = new AlertDialog.Builder(this, R.style.AlertDialogStyle)
                .setTitle(title)
                .setMessage(message);
        builder.setPositiveButton("Ok", null);
        builder.create().show();
    }


    @Override
    protected void onActivityResult(int requestCode, int resultCode, Intent data) {
        super.onActivityResult(requestCode, resultCode, data);

        // Handle the result of stripe.confirmPayment
        stripe.onPaymentResult(requestCode, data, new PaymentResultCallback(this));
    }

    private void onPaymentSuccess(@NonNull final Response<PaymentIntentModel> response) throws IOException {

        String jsonString = new Gson().toJson(response.body());
        PaymentIntentModel paymentIntentModel = (PaymentIntentModel) new Gson().fromJson(jsonString,
                PaymentIntentModel.class);
        paymentIntentClientSecret = paymentIntentModel.getPaymentIntentData().getClient_secret();

        if (paymentIntentClientSecret != null) {
            paymentThroughCard();
        }

    }


    public void paymentThroughCard() {
        Map<String, String> extraParams = new HashMap<>();
        extraParams.put("", "cvc");
        btn_add_card.setEnabled(false);
        waitScreen.setVisibility(View.VISIBLE);

        final ConfirmPaymentIntentParams params = ConfirmPaymentIntentParams.createWithPaymentMethodId(defaultPaymentMethod.getId(),
                paymentIntentClientSecret, null, null, null);
        stripe.confirmPayment(this, params);
    }

}