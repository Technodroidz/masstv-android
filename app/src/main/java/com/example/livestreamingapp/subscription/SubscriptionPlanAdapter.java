package com.example.livestreamingapp.subscription;

import android.content.Context;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.TextView;

import androidx.annotation.NonNull;
import androidx.recyclerview.widget.RecyclerView;

import com.example.livestreamingapp.R;
import com.example.livestreamingapp.model.MySubscription;
import com.example.livestreamingapp.model.SubscriptionPlan;
import com.example.livestreamingapp.model.SubscriptionResponse;
import com.example.livestreamingapp.utils.CurrencyUtil;
import com.example.livestreamingapp.utils.SharedPreferenceHandler;
import com.example.livestreamingapp.webservice.SubscriptionService;
import com.stripe.stripeterminal.Terminal;

import java.util.List;

public class SubscriptionPlanAdapter extends RecyclerView.Adapter<SubscriptionPlanAdapter.SubscriptionPlanHolder> {



    public interface planSelectionListener{
        public void onPlanSelected(int position,SubscriptionPlan subscriptionPlan);
    }
    private final planSelectionListener planSelectionListener;
    public int selectedposition = 0;
    private final Context context;
    private final List<SubscriptionPlan> subscriptionPlans;

    public SubscriptionPlanAdapter(Context context, List<SubscriptionPlan> subscriptionPlans,planSelectionListener planSelectionListener){
        this.context = context;
        this.subscriptionPlans = subscriptionPlans;
        this.planSelectionListener = planSelectionListener;
    }

    @NonNull
    @Override
    public SubscriptionPlanHolder onCreateViewHolder(@NonNull ViewGroup parent, int viewType) {
        View view = LayoutInflater.from(context).inflate(R.layout.item_subscription_plan,parent,false);
        return new SubscriptionPlanHolder(view);
    }

    @Override
    public void onBindViewHolder(@NonNull SubscriptionPlanHolder holder, int position) {

        MySubscription mySubscription = new SharedPreferenceHandler(context).getMySubscriptionDetail();
        setSubscription(position,holder.lin_subscription_plan,holder.iv_plan_selected,holder.tv_plan_type);
        holder.tv_plan_cost.setText(CurrencyUtil.getCurrencySymbol(subscriptionPlans.get(position).getCurrency())+
                ""+subscriptionPlans.get(position).getPrice());
        holder.tv_plan_name.setText(""+subscriptionPlans.get(position).getName());
        holder.tv_plan_type.setText(""+subscriptionPlans.get(position).getPlan());

        if(mySubscription!=null && mySubscription.getSubscriptionType().equalsIgnoreCase(subscriptionPlans.get(position).getPlan())){
            holder.tv_plan_name.setText(""+subscriptionPlans.get(position).getName()+"(Current Plan)");
        }

        holder.lin_subscription_plan.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                selectedposition = position;
                planSelectionListener.onPlanSelected(position,subscriptionPlans.get(position));
                notifyDataSetChanged();
            }
        });

    }

    public List<SubscriptionPlan> getSubscriptionPlanList(){
        return subscriptionPlans;
    }

    @Override
    public int getItemCount() {
        return subscriptionPlans.size();
    }

    public class SubscriptionPlanHolder extends RecyclerView.ViewHolder{
        private ImageView iv_plan_selected;
        private TextView tv_plan_name;
        private TextView tv_plan_cost;
        private TextView tv_plan_type;
        private LinearLayout lin_subscription_plan;

        public SubscriptionPlanHolder(@NonNull View itemView) {
            super(itemView);
            iv_plan_selected = itemView.findViewById(R.id.iv_plan_selected);
            tv_plan_name = itemView.findViewById(R.id.tv_plan_name);
            tv_plan_cost = itemView.findViewById(R.id.tv_plan_cost);
            tv_plan_type = itemView.findViewById(R.id.tv_plan_type);
            lin_subscription_plan = itemView.findViewById(R.id.lin_subscription_plan);
        }


    }

    public void setSubscription(int position,View itemView,ImageView iv_selected,TextView tv_plan_type ){

        itemView.setBackground(context.getResources().getDrawable(R.drawable.shape_subscription_border_default));
        iv_selected.setVisibility(View.INVISIBLE);
        tv_plan_type.setBackground(context.getResources().getDrawable(R.drawable.shape_background_subscription_text_default));

        if(selectedposition == position){
            itemView.setBackground(context.getResources().getDrawable(R.drawable.shape_subscription_border_selected));
            iv_selected.setVisibility(View.VISIBLE);
            tv_plan_type.setBackground(context.getResources().getDrawable(R.drawable.shape_background_subscription_text));
        }

    }
}
