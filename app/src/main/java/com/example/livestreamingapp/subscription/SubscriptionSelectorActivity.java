package com.example.livestreamingapp.subscription;

import android.Manifest;
import android.content.DialogInterface;
import android.content.Intent;
import android.content.pm.PackageManager;
import android.os.Build;
import android.os.Bundle;
import android.telephony.SubscriptionInfo;
import android.view.Menu;
import android.view.MenuItem;
import android.view.View;
import android.widget.RelativeLayout;
import android.widget.TextView;

import androidx.annotation.NonNull;
import androidx.appcompat.app.AlertDialog;
import androidx.appcompat.app.AppCompatActivity;
import androidx.appcompat.widget.Toolbar;
import androidx.fragment.app.FragmentTransaction;
import androidx.recyclerview.widget.GridLayoutManager;
import androidx.recyclerview.widget.RecyclerView;

import com.example.livestreamingapp.MassTvActivity;
import com.example.livestreamingapp.R;
import com.example.livestreamingapp.home.ui.HomeDraweerStreamingActivity;
import com.example.livestreamingapp.model.APIError;
import com.example.livestreamingapp.model.MySubscription;
import com.example.livestreamingapp.model.ProfileData;
import com.example.livestreamingapp.model.SubscriptionPlan;
import com.example.livestreamingapp.model.SubscriptionPlanResponse;
import com.example.livestreamingapp.utils.ConnectivityReceiver;
import com.example.livestreamingapp.utils.DateCalculation;
import com.example.livestreamingapp.utils.ErrorUtils;
import com.example.livestreamingapp.utils.SharedPreferenceHandler;
import com.example.livestreamingapp.utils.SpacesItemDecoration;
import com.example.livestreamingapp.webservice.APIClient;
import com.example.livestreamingapp.webservice.APIInterface;
import com.example.livestreamingapp.webservice.SubscriptionService;
import com.example.livestreamingapp.webservice.WebServiceConstant;
import com.google.android.gms.cast.framework.CastButtonFactory;
import com.google.android.gms.cast.framework.CastContext;
import com.google.gson.Gson;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import javax.annotation.Nullable;

import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;


public class SubscriptionSelectorActivity extends MassTvActivity implements View.OnClickListener,
        SubscriptionPlanAdapter.planSelectionListener,
        SubscriptionService.onSubscriptionInfoListener,
        ConnectivityReceiver.ConnectivityReceiverListener,
ApplyPromoListener{

    private static final int PERMISSION_CODE = 9992;

    private Toolbar mToolbar;
    private MenuItem mediaRouteMenuItem;
    private CastContext mCastContext;
    private String[] permission;
    private APIInterface apiInterface;

    SubscriptionPlan selectedSubscriptionPlan;
    private RecyclerView rv_subscription;
    private SubscriptionPlanAdapter adapterSubscriptionPlan;
    private RelativeLayout waitScreen;
    private MySubscription mySubscription;
    private SubscriptionService subscriptionService;
    private TextView tv_progress;
    private TextView tv_remaining_days;
    private SharedPreferenceHandler sharedPreferenceHandler;

    private TextView tv_apply_promo;
    private TextView tv_apply_text;
    private TextView tv_applied_promo;

    public static String promoCode = null;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_subscribe);

        mToolbar = (Toolbar) findViewById(R.id.toolbar);
        mToolbar.setNavigationIcon(R.drawable.ic_backs_white_24);
        mToolbar.setTitle("");
        setSupportActionBar(mToolbar);
        getSupportActionBar().setDisplayHomeAsUpEnabled(true);
        mCastContext = CastContext.getSharedInstance(this);

        apiInterface = APIClient.getClient().create(APIInterface.class);

        initViews();
    }

    @Override
    public void onBackPressed() {
        SubscriptionSelectorActivity.promoCode = null;
        super.onBackPressed();

    }

    public void getMySubscription() {
        waitScreen.setVisibility(View.VISIBLE);
        subscriptionService = new SubscriptionService(this);
        subscriptionService.getMySubscriptionInfo(this);
    }

    private void initViews() {

        tv_remaining_days = findViewById(R.id.tv_remaining_days);

        rv_subscription = findViewById(R.id.rv_subscription);
        tv_progress = findViewById(R.id.tv_progress);
        tv_progress.setText("Loading plan...");
        tv_apply_promo = findViewById(R.id.tv_apply_promo);
        tv_apply_text = findViewById(R.id.tv_promo_text);
        tv_applied_promo = findViewById(R.id.tv_applied_promo);
        tv_applied_promo.setVisibility(View.GONE);

        waitScreen = findViewById(R.id.waitScreen);

       /* permission =
                new String[]{
                        *//*"android.permission.Fb OREGROUND_SERVICE",*//*
                        Manifest.permission.ACCESS_FINE_LOCATION, Manifest.permission.ACCESS_COARSE_LOCATION
                };*/

        GridLayoutManager layoutManagerDemand = new GridLayoutManager(this, 2);
        adapterSubscriptionPlan = new SubscriptionPlanAdapter(this, new ArrayList<SubscriptionPlan>(), this);
        rv_subscription.setLayoutManager(layoutManagerDemand);
        rv_subscription.addItemDecoration(new SpacesItemDecoration(0, 16));
        rv_subscription.setAdapter(adapterSubscriptionPlan);

        sharedPreferenceHandler = new SharedPreferenceHandler(this);
        mySubscription = sharedPreferenceHandler.getMySubscriptionDetail();
        /*if (mySubscription == null || mySubscription.getRemainingDays()==null ||
                mySubscription.getRemainingDays().equalsIgnoreCase("0") ||
                !mySubscription.getLastRefreshDate().
                        equalsIgnoreCase(DateCalculation.getCurrentDateInDDMMYY())){
           getMySubscription();
        }*/
        getMySubscription();
        if(mySubscription!=null && mySubscription.getRemainingDays()!=null
                && !mySubscription.getRemainingDays().equalsIgnoreCase("0")){
            tv_remaining_days.setText("Current Plan Remaining "+mySubscription.getRemainingDays()+" Days");
            tv_remaining_days.setVisibility(View.VISIBLE);
            tv_apply_promo.setVisibility(View.GONE);
            tv_apply_text.setVisibility(View.GONE);
        }else {
            tv_remaining_days.setVisibility(View.GONE);
        }
        if(promoCode==null){
            getSubscriptionPlan();
        }else {
            getPromoApplication(promoCode);
        }


       tv_apply_promo.setOnClickListener(this);


    }


    private void getSubscriptionPlan() {
        waitScreen.setVisibility(View.VISIBLE);
        SubscriptionService subscriptionService = new SubscriptionService(this);
        subscriptionService.getSubscriptionPlan(new SubscriptionService.onGetSubscriptionPlanListener() {
            @Override
            public void onGetSubscriptionPlan(List<SubscriptionPlan> subscriptionPlan, String error) {

                waitScreen.setVisibility(View.GONE);
                if (subscriptionPlan != null && subscriptionPlan.size() > 0) {

                    adapterSubscriptionPlan = new SubscriptionPlanAdapter(SubscriptionSelectorActivity.this,
                            subscriptionPlan, SubscriptionSelectorActivity.this);
                    rv_subscription.setAdapter(adapterSubscriptionPlan);
                    adapterSubscriptionPlan.notifyDataSetChanged();

                } else {
                    waitScreen.setVisibility(View.GONE);
                    displayAlert("Error", error);

                }

            }
        });
    }

    private void displayAlert(@NonNull String title,
                              @Nullable String message) {
        AlertDialog.Builder builder = new AlertDialog.Builder(this, R.style.AlertDialogStyle)
                .setCancelable(false)
                .setTitle(title)
                .setMessage(message);

        builder.setPositiveButton("Ok", new DialogInterface.OnClickListener() {
            @Override
            public void onClick(DialogInterface dialog, int which) {
                finish();
            }
        });
        builder.create().show();
    }

    /**
     * Request permission
     */
    private void requestAllPermission() {

        if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.M) {

            requestPermissions(permission, PERMISSION_CODE);
        }
    }

    /**
     * @return true if granted false if any of the permission is not granted
     * @Rehan This method check if all permission granted
     */
  /*  public boolean isAllPermissionGranted() {
        boolean granted = false;
        if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.M) {
            if ((checkSelfPermission(Manifest.permission.ACCESS_FINE_LOCATION) == PackageManager.PERMISSION_GRANTED)
            ) {

                granted = true;
            }
        }

        return granted;
    }*/

   /* *//**
     * @param
     * @return
     * @Rehan This method for checking specified permission
     *//*
    private boolean hasPermission(String permission) {

        if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.M) {
            return (checkSelfPermission(permission) == PackageManager.PERMISSION_GRANTED);
        }

        return true;
    }*/



    @Override
    public boolean onOptionsItemSelected(@NonNull MenuItem item) {

        switch (item.getItemId()) {
            case android.R.id.home:
                // todo: goto back activity from here
                onBackPressed();
                return true;

            default:
                return super.onOptionsItemSelected(item);
        }
    }

    @Override
    public void onClick(View v) {

        if(v.getId()==R.id.btn_subscribe){
            /*if (isAllPermissionGranted()) {
                getCustomerId();
            } else {
                requestAllPermission();
            }
*/
            getCustomerId();
        }else if(v.getId()==R.id.tv_apply_promo){

            ApplyPromotionFragment fragment = new ApplyPromotionFragment();
            FragmentTransaction transaction = getSupportFragmentManager().beginTransaction();
            transaction.setCustomAnimations(R.anim.slide_in_up,0,0, R.anim.slide_out_up);
            transaction.addToBackStack(null);
            transaction.add(R.id.rl_container, fragment).commit();
        }


    }

    private void getCustomerId() {

        if (adapterSubscriptionPlan.getSubscriptionPlanList() != null && adapterSubscriptionPlan.getSubscriptionPlanList().size() > 0) {

            if (selectedSubscriptionPlan == null) {
                selectedSubscriptionPlan = adapterSubscriptionPlan.getSubscriptionPlanList().get(0);
            }

            if (mySubscription == null || Integer.parseInt(mySubscription.getRemainingDays()) < 1) {
                // buy subscription plan
                new SharedPreferenceHandler(SubscriptionSelectorActivity.this).
                        setPreference(SharedPreferenceHandler.SELECTED_PLAN, new Gson().toJson(selectedSubscriptionPlan));
                startActivity(new Intent(SubscriptionSelectorActivity.this, PaymentMethodSelectorActivity.class));
                // startActivity(new Intent(SubscriptionSelectorActivity.this,PaymentActivity.class));
                finish();
            } else {

                AlertDialog.Builder builder = new AlertDialog.Builder(this, R.style.AlertDialogStyle)
                        .setCancelable(false)
                        .setTitle("Already Have an Active Plan!")
                        .setMessage("Please Wait Current plan to expire to re-new your plan!");

                builder.setPositiveButton("Ok", new DialogInterface.OnClickListener() {
                    @Override
                    public void onClick(DialogInterface dialog, int which) {
                     /*   new SharedPreferenceHandler(SubscriptionSelectorActivity.this).
                                setPreference(SharedPreferenceHandler.SELECTED_PLAN, new Gson().toJson(selectedSubscriptionPlan));
                        startActivity(new Intent(SubscriptionSelectorActivity.this, PaymentMethodSelectorActivity.class));
                        // startActivity(new Intent(SubscriptionSelectorActivity.this,PaymentActivity.class));
                        finish();*/
                    }
                })/*.setNegativeButton("Cancel", new DialogInterface.OnClickListener() {
                    @Override
                    public void onClick(DialogInterface dialog, int which) {

                    }
                })*/;
                builder.create().show();

            }


        }

    }

  /*  @Override
    public void onRequestPermissionsResult(
            int requestCode, final String[] permissions, int[] grantResults) {

        switch (requestCode) {
            case PERMISSION_CODE:
                ArrayList<String> permissionRejected = new ArrayList<>();
                for (String perms : permissions) {
                    if (hasPermission(perms)) {

                    } else {
                        permissionRejected.add(perms);
                    }
                }

                if (permissionRejected.size() > 0) {

                    if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.M) {
                        if (shouldShowRequestPermissionRationale(permissionRejected.get(0))) {
                            showMessageOKCancel(
                                    "These permissions are mandatory for the application. Please allow access.",
                                    new DialogInterface.OnClickListener() {
                                        @Override
                                        public void onClick(DialogInterface dialog, int which) {
                                            if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.M) {

                                                requestPermissions(permissions, PERMISSION_CODE);
                                            }
                                        }
                                    });
                            return;
                        }
                    }
                } else {
                    // do  your work
                }

                break;
        }
    }
*/
    private void showMessageOKCancel(String message, DialogInterface.OnClickListener okListener) {
        new AlertDialog.Builder(SubscriptionSelectorActivity.this, R.style.AlertDialogStyle)
                .setMessage(message)
                .setPositiveButton("OK", okListener)
                .setNegativeButton("Cancel", null)
                .create()
                .show();
    }


    @Override
    public boolean onCreateOptionsMenu(Menu menu) {
        // Inflate the menu; this adds items to the action bar if it is present.
        getMenuInflater().inflate(R.menu.menu_cast, menu);
        mediaRouteMenuItem = CastButtonFactory.setUpMediaRouteButton(getApplicationContext(), menu,
                R.id.media_route_menu_item);
        return true;
    }

    @Override
    public void onPlanSelected(int position, SubscriptionPlan subscriptionPlan) {
        this.selectedSubscriptionPlan = subscriptionPlan;
    }

    @Override
    public void onSubscriptionInfo(MySubscription subscriptionInfo) {
        waitScreen.setVisibility(View.GONE);
        this.mySubscription = subscriptionInfo;
        if (subscriptionInfo != null) {
            new SharedPreferenceHandler(this).saveSubscription(subscriptionInfo);
        }else {
            new SharedPreferenceHandler(this).saveSubscription(null);
        }
        this.mySubscription = new SharedPreferenceHandler(this).getMySubscriptionDetail();
        if(mySubscription!=null && mySubscription.getRemainingDays()!=null
                && !mySubscription.getRemainingDays().equalsIgnoreCase("0")){
            tv_remaining_days.setText("Current Plan Remaining "+subscriptionInfo.getRemainingDays()+" Days");
            tv_remaining_days.setVisibility(View.VISIBLE);
        }else {
            tv_remaining_days.setVisibility(View.GONE);
        }
    }

    @Override
    public void onFailure(String error) {
        waitScreen.setVisibility(View.GONE);
      //  displayAlert("Error", error);
    }


    @Override
    public void onNetworkConnectionChanged(boolean isConnected) {
        if (selectedSubscriptionPlan == null && isConnected) {
            getSubscriptionPlan();
        }
    }

    @Override
    public void onPromoApplied(List<SubscriptionPlan> list,String promoCode) {
        // get promo applied plan list

        SubscriptionSelectorActivity.promoCode = promoCode;
        adapterSubscriptionPlan = new SubscriptionPlanAdapter(SubscriptionSelectorActivity.this,
                list, SubscriptionSelectorActivity.this);
        rv_subscription.setAdapter(adapterSubscriptionPlan);
        adapterSubscriptionPlan.notifyDataSetChanged();
        AppliedPromoText(promoCode);
    }

    public void slideOutFragment() {
        getSupportFragmentManager().popBackStackImmediate();
    }

    public void AppliedPromoText(String promocode){
        tv_apply_promo.setText("Change?");
        tv_apply_text.setText("Promo Code Applied! ");
        tv_applied_promo.setVisibility(View.VISIBLE);
        tv_applied_promo.setText(""+promocode);

    }



    private void getPromoApplication(String promoCode) {

        waitScreen.setVisibility(View.VISIBLE);
        Map<String, String> paymentIntent = new HashMap<>();
        ProfileData profileData = new SharedPreferenceHandler(this).getProfileInfo();
        paymentIntent.put(WebServiceConstant.USER_ID, "" + profileData.getId());
        paymentIntent.put(WebServiceConstant.PROMO_CODE, "" + promoCode);

        Call<SubscriptionPlanResponse> applyPromo = apiInterface.applySubscription(paymentIntent);
        applyPromo.enqueue(new Callback<SubscriptionPlanResponse>() {
            @Override
            public void onResponse(Call<SubscriptionPlanResponse> call, Response<SubscriptionPlanResponse> response) {

                waitScreen.setVisibility(View.GONE);
                if (response.code() == 200) {
                    if (response.body().getSuccess()) {
                        List<SubscriptionPlan> list = response.body().getData();
                        onPromoApplied(list, promoCode);
                    }
                } else {

                   getSubscriptionPlan();

                }

            }

            @Override
            public void onFailure(Call<SubscriptionPlanResponse> call, Throwable t) {
                waitScreen.setVisibility(View.GONE);
            }
        });

    }
}