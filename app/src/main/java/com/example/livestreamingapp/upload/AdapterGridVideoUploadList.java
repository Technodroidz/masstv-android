package com.example.livestreamingapp.upload;

import android.content.Context;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Button;
import android.widget.ImageButton;
import android.widget.ImageView;

import androidx.annotation.NonNull;
import androidx.recyclerview.widget.RecyclerView;

import com.example.livestreamingapp.R;

public class AdapterGridVideoUploadList extends RecyclerView.Adapter<AdapterGridVideoUploadList.VideoListHolder> {

    private Context context;
    private String videoType;
    private AdapterViewClickListener listener;

    public AdapterGridVideoUploadList(Context context, String videoType, AdapterViewClickListener listener){
        this.context = context;
        this.videoType = videoType;
        this.listener = listener;
    }

    @NonNull
    @Override
    public VideoListHolder onCreateViewHolder(@NonNull ViewGroup parent, int viewType) {
        View view = LayoutInflater.from(context).inflate(R.layout._item_list_video_grid,parent,false);

        return new VideoListHolder(view);
    }

    @Override
    public void onBindViewHolder(@NonNull VideoListHolder holder, int position) {

        holder.iv_image.setImageDrawable(context.getResources().getDrawable(R.drawable.upload_background));


    }


    @Override
    public int getItemCount() {
        return 6;
    }

    public class VideoListHolder extends RecyclerView.ViewHolder {
        private ImageView btn_play_stream;
        private ImageView iv_image;
        public VideoListHolder(@NonNull View itemView) {
            super(itemView);
            btn_play_stream = (ImageView) itemView.findViewById(R.id.ib_play_later);
            iv_image = (ImageView)itemView.findViewById(R.id.iv_image);

            itemView.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View v) {
                    listener.onViewClicked(getLayoutPosition());

                }
            });

        }
    }

    public interface AdapterViewClickListener{
        public void onViewClicked(int position);
    }
}
