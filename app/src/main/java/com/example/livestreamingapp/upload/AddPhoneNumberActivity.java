package com.example.livestreamingapp.upload;

import androidx.annotation.NonNull;
import androidx.appcompat.app.AlertDialog;
import androidx.appcompat.app.AppCompatActivity;
import androidx.appcompat.widget.Toolbar;

import android.Manifest;
import android.app.ProgressDialog;
import android.content.DialogInterface;
import android.content.Intent;
import android.content.pm.PackageManager;
import android.os.Build;
import android.os.Bundle;
import android.util.Log;
import android.view.Menu;
import android.view.MenuItem;
import android.view.View;
import android.widget.Button;
import android.widget.EditText;
import android.widget.TextView;

import com.example.livestreamingapp.MassTvActivity;
import com.example.livestreamingapp.R;
import com.example.livestreamingapp.model.APIError;
import com.example.livestreamingapp.model.AddPhoneResponse;
import com.example.livestreamingapp.model.LoginResponse;
import com.example.livestreamingapp.model.Phone;
import com.example.livestreamingapp.model.ProfileData;
import com.example.livestreamingapp.upload.AddPhoneNumberActivity;
import com.example.livestreamingapp.profile.ProfileActivity;
import com.example.livestreamingapp.utils.Constants;
import com.example.livestreamingapp.utils.CustomProgressDialog;
import com.example.livestreamingapp.utils.ErrorUtils;
import com.example.livestreamingapp.utils.SharedPreferenceHandler;
import com.example.livestreamingapp.webservice.APIClient;
import com.example.livestreamingapp.webservice.APIInterface;
import com.google.android.gms.cast.framework.CastButtonFactory;
import com.google.android.gms.cast.framework.CastContext;
import com.google.gson.Gson;
import com.hbb20.CountryCodePicker;

import java.util.ArrayList;
import java.util.Arrays;
import java.util.HashMap;
import java.util.Map;

import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;

public class AddPhoneNumberActivity extends MassTvActivity implements View.OnClickListener {

    private Toolbar mToolbar;
    private CastContext mCastContext;
    private MenuItem mediaRouteMenuItem;

    private static final int PERMISSION_CODE = 111;
    private Button btn_verify_mobile_no;

    private SharedPreferenceHandler sharedPreferenceHandler;
    private CustomProgressDialog progressDialog;
    private APIInterface apiInterface;
    private CountryCodePicker country_picker;
    private String[] permission;
    ProfileData profileData;
    private EditText et_phone;
    private TextView tv_title;
    private String activityToOpen;


    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_upload_video);

        mToolbar = (Toolbar) findViewById(R.id.toolbar);
        mToolbar.setNavigationIcon(R.drawable.ic_backs_white_24);
        mToolbar.setTitle("");
        setSupportActionBar(mToolbar);
        getSupportActionBar().setDisplayHomeAsUpEnabled(true);
        mCastContext = CastContext.getSharedInstance(this);
        sharedPreferenceHandler = new SharedPreferenceHandler(this);
        progressDialog = new CustomProgressDialog(this);
        progressDialog.setMessage("Please wait...");
        progressDialog.setCancelable(false);
        apiInterface = APIClient.getClient().create(APIInterface.class);

        profileData = new SharedPreferenceHandler(this).getProfileInfo();

        initViews();

    }



    private void initViews() {
        btn_verify_mobile_no = findViewById(R.id.btn_verify_mobile_no);
        et_phone = findViewById(R.id.et_phone);
        country_picker =  findViewById(R.id.country_picker);
        btn_verify_mobile_no.setText("PROCEED");
        tv_title = findViewById(R.id.tv_title);

         activityToOpen = getIntent().getStringExtra(Constants.ACTIVITY_TO_OPEN);
        if(activityToOpen!=null && activityToOpen.equalsIgnoreCase(ProfileActivity.class.getName())){
            tv_title.setText("Update Phone Number");
        }

        country_picker.setCountryForPhoneCode(91);
        country_picker.setEnabled(true);

        /**
         * @Rehan define permissions here that we want to have the application Here we only need SMS
         * Permission for auto read otp
         */
    /*    permission =
                new String[] {
                        *//*"android.permission.FOREGROUND_SERVICE",*//*
                        Manifest.permission.READ_SMS, Manifest.permission.RECEIVE_SMS
                };*/


        btn_verify_mobile_no.setOnClickListener(this);
        country_picker.registerCarrierNumberEditText(et_phone);

        country_picker.setOnCountryChangeListener(
                new CountryCodePicker.OnCountryChangeListener() {
                    @Override
                    public void onCountrySelected() {
                        et_phone.setText("");
                    }
                });
    }

    @Override
    public void onClick(View v) {

        switch (v.getId()){
            case R.id.btn_verify_mobile_no:
                if (et_phone.getText().toString()!=null && et_phone.getText().toString().length()>=10) {
                    AttempEditProfile(et_phone.getText().toString());
                }else {
                    new AlertDialog.Builder(AddPhoneNumberActivity.this,R.style.AlertDialogStyle)
                            .setMessage("Please enter valid number phone number")
                            .setPositiveButton(
                                    "Ok",
                                    new DialogInterface.OnClickListener() {
                                        @Override
                                        public void onClick(DialogInterface dialog, int which) {}
                                    })
                            .show();
                }
                break;
        }

    }

    /**
     * The method check that prefix and phone number together is correct and valid
     * @return
     */
    boolean arePhoneNumberAndPrefixOk() {
/*
        if(et_phone.getText().toString().length()!=10){
            new AlertDialog.Builder(AddPhoneNumberActivity.this,R.style.AlertDialogStyle)
                    .setMessage("Please enter valid number with country code")
                    .setPositiveButton(
                            "Ok",
                            new DialogInterface.OnClickListener() {
                                @Override
                                public void onClick(DialogInterface dialog, int which) {}
                            })
                    .show();
            return false;
        }*/

        if (!country_picker.isValidFullNumber()) {
            new AlertDialog.Builder(AddPhoneNumberActivity.this,R.style.AlertDialogStyle)
                    .setMessage("Please enter valid number with country code")
                    .setPositiveButton(
                            "Ok",
                            new DialogInterface.OnClickListener() {
                                @Override
                                public void onClick(DialogInterface dialog, int which) {}
                            })
                    .show();

            return false;
        }

        return true;
    }
  /*  *//** Request permission *//*
    private void requestAllPermission() {

        if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.M) {

            requestPermissions(permission, PERMISSION_CODE);
        }
    }*/
    /**
     * @Rehan This method for checking specified permission
     *
     * @param permission
     * @return
     */
    private boolean hasPermission(String permission) {

        if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.M) {
            return (checkSelfPermission(permission) == PackageManager.PERMISSION_GRANTED);
        }

        return true;
    }

    private void showMessageOKCancel(String message, DialogInterface.OnClickListener okListener) {
        new AlertDialog.Builder(AddPhoneNumberActivity.this,R.style.AlertDialogStyle)
                .setMessage(message)
                .setPositiveButton("OK", okListener)
                .setNegativeButton("Cancel", null)
                .create()
                .show();
    }

  /*  @Override
    public void onRequestPermissionsResult(
            int requestCode, final String[] permissions, int[] grantResults) {

        switch (requestCode) {
            case PERMISSION_CODE:
                ArrayList<String> permissionRejected = new ArrayList<>();
                for (String perms : permissions) {
                    if (hasPermission(perms)) {

                    } else {
                        permissionRejected.add(perms);
                    }
                }

                if (permissionRejected.size() > 0) {

                    if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.M) {
                        if (shouldShowRequestPermissionRationale(permissionRejected.get(0))) {
                            showMessageOKCancel(
                                    "These permissions are mandatory for the application. Please allow access.",
                                    new DialogInterface.OnClickListener() {
                                        @Override
                                        public void onClick(DialogInterface dialog, int which) {
                                            if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.M) {

                                                requestPermissions(permissions, PERMISSION_CODE);
                                            }
                                        }
                                    });
                            return;
                        }
                    }
                } else {
                    attemptSendOtp();
                }

                break;
        }
    }*/

    private void attemptSendOtp() {

        Map<String, String> credential = new HashMap<>();
        credential.put("user_id", ""+profileData.getId());
        credential.put("phone_number", country_picker.getFullNumber());

        Call<AddPhoneResponse> loginApiCall = apiInterface.addPhoneNumber(credential);

        progressDialog.show();

        loginApiCall.enqueue(
                new Callback<AddPhoneResponse>() {
                    @Override
                    public void onResponse(
                            Call<AddPhoneResponse> call, Response<AddPhoneResponse> response) {
                        progressDialog.dismiss();

//                        startActivity(new Intent(AddPhoneNumberActivity.this, OtpUploadVerificationActivity.class)
//                                .putExtra(Constants.PhoneNumber,country_picker.getFullNumberWithPlus())
//                        );
                        if(response.code() == 200){
                            if(activityToOpen==null){
                                startActivity(new Intent(AddPhoneNumberActivity.this, OtpUploadVerificationActivity.class)
                                        .putExtra(Constants.PhoneNumber,country_picker.getFullNumber())
                                );
                                finish();
                            }else {
                                startActivity(new Intent(AddPhoneNumberActivity.this, OtpUploadVerificationActivity.class)
                                        .putExtra(Constants.PhoneNumber,country_picker.getFullNumber()).
                                                putExtra(Constants.ACTIVITY_TO_OPEN,activityToOpen)
                                );
                                finish();
                            }
                        }else if(response.code()==404){
                             APIError error = ErrorUtils.parseError(response);


                            String errorMessage = error.getData().getError();

                            if(errorMessage==null){
                                errorMessage = "Something went wrong";
                            }

                            new AlertDialog.Builder(AddPhoneNumberActivity.this,R.style.AlertDialogStyle)
                                    .setCancelable(false)
                                    .setMessage(errorMessage)
                                    .setPositiveButton(
                                            "Ok",
                                            new DialogInterface.OnClickListener() {
                                                @Override
                                                public void onClick(
                                                        DialogInterface dialog, int which) {}
                                            })
                                    .show();
                        }else {
                            new AlertDialog.Builder(AddPhoneNumberActivity.this,R.style.AlertDialogStyle)
                                    .setCancelable(false)
                                    .setTitle("Error")
                                    .setMessage("Something went wrong! please try again later")
                                    .setPositiveButton(
                                            "Ok",
                                            new DialogInterface.OnClickListener() {
                                                @Override
                                                public void onClick(
                                                        DialogInterface dialog, int which) {}
                                            })
                                    .show();
                        }



                      /*  if (response.code()==200) {

                            if(response.body().isSuccess()){
                                startActivity(new Intent(AddPhoneNumberActivity.this, OtpUploadVerificationActivity.class)
                                        .putExtra(Constants.PhoneNumber,country_picker.getFullNumberWithPlus())
                                );
                                finish();
                            }else {
                                new AlertDialog.Builder(AddPhoneNumberActivity.this,R.style.AlertDialogStyle)
                                        .setCancelable(false)
                                        .setMessage(getString(R.string.something_went_wrong))
                                        .setPositiveButton(
                                                "Ok",
                                                new DialogInterface.OnClickListener() {
                                                    @Override
                                                    public void onClick(
                                                            DialogInterface dialog, int which) {}
                                                })
                                        .show();
                            }


                        } else {
                            new AlertDialog.Builder(AddPhoneNumberActivity.this,R.style.AlertDialogStyle)
                                    .setCancelable(false)
                                    .setMessage(getString(R.string.something_went_wrong))
                                    .setPositiveButton(
                                            "Ok",
                                            new DialogInterface.OnClickListener() {
                                                @Override
                                                public void onClick(
                                                        DialogInterface dialog, int which) {}
                                            })
                                    .show();
                        }*/

                    }

                    @Override
                    public void onFailure(Call<AddPhoneResponse> call, Throwable t) {
                        progressDialog.dismiss();
                        new AlertDialog.Builder(AddPhoneNumberActivity.this,R.style.AlertDialogStyle)
                                .setCancelable(false)
                                .setMessage("" + t.getMessage())
                                .setPositiveButton(
                                        "Ok",
                                        new DialogInterface.OnClickListener() {
                                            @Override
                                            public void onClick(
                                                    DialogInterface dialog, int which) {}
                                        })
                                .show();
                    }
                });
    }


    @Override
    public void onBackPressed() {
        super.onBackPressed();
    }

    @Override
    public boolean onCreateOptionsMenu(Menu menu) {
        // Inflate the menu; this adds items to the action bar if it is present.
        getMenuInflater().inflate(R.menu.menu_cast, menu);
        mediaRouteMenuItem = CastButtonFactory.setUpMediaRouteButton(getApplicationContext(), menu,
                R.id.media_route_menu_item);
        return true;
    }

    @Override
    public boolean onOptionsItemSelected(@NonNull MenuItem item) {

        switch (item.getItemId()) {
            case android.R.id.home:
                // todo: goto back activity from here
                finish();
                return true;

            default:
                return super.onOptionsItemSelected(item);
        }
    }


    /**
     * @Rehan Edit Address
     */
    private void AttempEditProfile(final String phone) {

       progressDialog.show();

        final ProfileData data = new SharedPreferenceHandler(AddPhoneNumberActivity.this).getProfileInfo();

        Map<String, String> credential = new HashMap<>();
        credential.put("name", data.getUserName());
        credential.put("phone_number", ""+phone);

        if(data.getCity()!=null){
            credential.put("country", data.getCountry());
            credential.put("state", data.getState());
            credential.put("city", data.getCity());
        }else {
            credential.put("country", "");
            credential.put("state", "");
            credential.put("city", "");
        }
        credential.put("user_id",""+data.getId());
        //   Log.e("check profile ","json "+new Gson().toJson(credential));
        // credential.put("profile_picture", null);
        Call<LoginResponse> editProfile = apiInterface.editProfile(credential);

        // progressDialog.show();

        editProfile.enqueue(
                new Callback<LoginResponse>() {
                    @Override
                    public void onResponse(
                            Call<LoginResponse> call, Response<LoginResponse> response) {
                        // progressDialog.dismiss();
                        progressDialog.dismiss();

                        if (response.code() == 200) {

                            //  Log.e("Check ","res success "+new Gson().toJson(response.body()));

                            if (response.body().getStatus()) {

                                data.setPhoneNumber(phone);

                                new SharedPreferenceHandler(AddPhoneNumberActivity.this)
                                        .setPreference(Constants.PROFILE_INFO,new Gson().toJson(data));
                                finish();

                            } else {
                                new AlertDialog.Builder(AddPhoneNumberActivity.this,R.style.AlertDialogStyle)
                                        .setCancelable(false)
                                        .setMessage(getString(R.string.something_went_wrong))
                                        .setPositiveButton(
                                                "Ok",
                                                new DialogInterface.OnClickListener() {
                                                    @Override
                                                    public void onClick(
                                                            DialogInterface dialog, int which) {}
                                                })
                                        .show();
                            }


                        }
                    }

                    @Override
                    public void onFailure(Call<LoginResponse> call, Throwable t) {
                        try {
                            progressDialog.dismiss();
                            new AlertDialog.Builder(AddPhoneNumberActivity.this,R.style.AlertDialogStyle)
                                    .setCancelable(false)
                                    .setMessage("" + t.getMessage())
                                    .setPositiveButton(
                                            "Ok",
                                            new DialogInterface.OnClickListener() {
                                                @Override
                                                public void onClick(
                                                        DialogInterface dialog, int which) {}
                                            })
                                    .show();
                        }catch (Exception e){

                        }

                    }
                });
    }



}