package com.example.livestreamingapp.upload;

import android.Manifest;
import android.app.Activity;
import android.app.Dialog;
import android.content.ContentResolver;
import android.content.Context;
import android.content.Intent;
import android.content.pm.PackageManager;
import android.graphics.Color;
import android.graphics.drawable.ColorDrawable;
import android.net.Uri;
import android.os.AsyncTask;
import android.os.Build;
import android.view.View;
import android.view.Window;
import android.view.WindowManager;
import android.widget.Button;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.Toast;


import androidx.annotation.NonNull;
import androidx.annotation.Nullable;

import com.bumptech.glide.Glide;
import com.example.livestreamingapp.R;

import java.io.File;
import java.io.FileOutputStream;
import java.io.IOException;
import java.io.InputStream;
import java.io.OutputStream;

import static android.app.Activity.RESULT_OK;

public class CaptureImageDialog {

    private static final int PERMISSION_CODE = 789;
    private static  Context context;
    private CaptureImageListener captureImageListener;

    private File thumbnail;
    private ImageView iv_upload;
    private File newImageFile;

    public interface CaptureImageListener {
        public void onSignatureCapture(File file);
    }

    public CaptureImageDialog(Context context){
        this.context = context;
    }

    public void showSignatureDialog(File thumbnail, CaptureImageListener captureImageListener){
        this.captureImageListener = captureImageListener;
        this.thumbnail = thumbnail;

        View layout =((Activity)context).getLayoutInflater().inflate(R.layout.layout_upload_thumbnail,
                null);
        final Dialog dialog = setDialog(context, layout);

        Button btn_upload = dialog.findViewById(R.id.tv_btn_upload);
        Button btn_cancel = dialog.findViewById(R.id.btn_cancel);
         iv_upload = dialog.findViewById(R.id.iv_upload);
        LinearLayout lin_upload = dialog.findViewById(R.id.lin_upload);

        if(thumbnail!=null&& iv_upload!=null){
            Glide.with(context).load(thumbnail.getAbsolutePath()).into(iv_upload);
        }

        btn_cancel.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                dialog.dismiss();
            }
        });

        lin_upload.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                // upload image work
                if(isAllPermissionGranted()){

                    selectFromGallery();
                }else {
                    requestAllPermission();
                }

            }
        });

        btn_upload.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                // pass data
                captureImageListener.onSignatureCapture(newImageFile);
                dialog.dismiss();
            }
        });

        dialog.setCancelable(false);
        dialog.getWindow().setLayout(WindowManager.LayoutParams.WRAP_CONTENT, WindowManager.LayoutParams.WRAP_CONTENT);

        dialog.show();

    }

    public static Dialog setDialog(Context context, View view) {
        Dialog dialog_post_type = new Dialog(context);
        dialog_post_type.requestWindowFeature(Window.FEATURE_NO_TITLE);
        dialog_post_type.getWindow().setBackgroundDrawable(new ColorDrawable(Color.TRANSPARENT));
        dialog_post_type.setCancelable(true);
        dialog_post_type.setContentView(view);
        return dialog_post_type;
    }

    public boolean isAllPermissionGranted() {
        boolean granted = false;
        if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.M) {
            if ((context.checkSelfPermission(Manifest.permission.READ_EXTERNAL_STORAGE) == PackageManager.PERMISSION_GRANTED)
                    && (context.checkSelfPermission(Manifest.permission.WRITE_EXTERNAL_STORAGE)
                    == PackageManager.PERMISSION_GRANTED)) {

                granted = true;
            }
        }

        return granted;
    }

    /**
     * Request permission
     */
    private void requestAllPermission() {

        if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.M) {

           String [] permission =
                    new String[]{
                            /*"android.permission.FOREGROUND_SERVICE",*/
                            Manifest.permission.WRITE_EXTERNAL_STORAGE, Manifest.permission.READ_EXTERNAL_STORAGE
                    };

            ((Activity) context).requestPermissions(permission, PERMISSION_CODE);
        }
    }


    /**
     * @Rehan
     * Get Image from gallery
     */
    private void selectFromGallery() {
        Intent intent = new Intent();
        intent.setType("image/*");
        intent.setAction(Intent.ACTION_GET_CONTENT);
        ((Activity)context).startActivityForResult(Intent.createChooser(intent, "Select Image"), UploadContentActivity.REQUEST_TAKE_GALLERY_IMAGE);

    }

    public void onActivityResult(int requestCode, int resultCode, Intent data) {
        if (resultCode == RESULT_OK) {
            if (requestCode == UploadContentActivity.REQUEST_TAKE_GALLERY_IMAGE) {
                Uri selectedImageUri = data.getData();
                newImageFile = createCopyRealPathThumbnail(context,selectedImageUri);
                Glide.with(context).load(newImageFile.getAbsolutePath()).into(iv_upload);
            }
        }
    }


    @Nullable
    public File createCopyRealPathThumbnail(
            @NonNull Context context, @NonNull Uri uri) {
        final ContentResolver contentResolver = context.getContentResolver();
        if (contentResolver == null)
            return null;
        File directory = new File(context.getCacheDir(), "media/thumbnail");
        if(!directory.exists()){
            try {
                directory.mkdir();
            }catch (Exception e){
            }
        }



        // Create file path inside app's data dir
        String  name = new File(uri.toString()).getName()+ System.currentTimeMillis() + ".jpg";
        //  newfile = new File(directory, filePath);
        File file = new File(directory, name);
       String filePath = file.getAbsolutePath();
        try {
            InputStream inputStream = contentResolver.openInputStream(uri);
            if (inputStream == null)
                return null;
            OutputStream outputStream = new FileOutputStream(file);
            byte[] buf = new byte[1024];
            int len;
            while ((len = inputStream.read(buf)) > 0)
                outputStream.write(buf, 0, len);
            outputStream.close();
            inputStream.close();
        } catch (IOException ignore) {
            return null;
        }
        return file;
    }

    public static void clearThumbnailFile(){
        new deleteThumbAsync().execute();
    }



    public static class deleteThumbAsync extends AsyncTask<String, String, Boolean> {

        @Override
        protected Boolean doInBackground(String... strings) {
            try {

                File directory = new File(context.getCacheDir(), "media/thumbnail");
                if (directory.exists()) {
                    deleteDir(directory);
                    Toast.makeText(context, "Deleted", Toast.LENGTH_SHORT).show();
                }
            } catch (Exception e) {
                e.printStackTrace();
            }
            return true;
        }

        public boolean deleteDir(File dir) {
            if (dir.isDirectory()) {
                String[] children = dir.list();
                for (int i = 0; i < children.length; i++) {
                    boolean success = deleteDir(new File(dir, children[i]));
                    if (!success) {
                        return false;
                    }
                }
            }

            // The directory is now empty so delete it
            return dir.delete();
        }

        @Override
        protected void onPreExecute() {
            super.onPreExecute();

        }

        @Override
        protected void onPostExecute(Boolean aBoolean) {
            super.onPostExecute(aBoolean);

        }
    }


}
