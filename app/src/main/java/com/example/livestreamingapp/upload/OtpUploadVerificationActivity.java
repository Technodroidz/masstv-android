package com.example.livestreamingapp.upload;

import android.app.ProgressDialog;
import android.content.BroadcastReceiver;
import android.content.Context;
import android.content.DialogInterface;
import android.content.Intent;
import android.content.IntentFilter;
import android.os.Build;
import android.os.Bundle;
import android.telephony.SmsMessage;
import android.text.Editable;
import android.text.TextWatcher;
import android.util.Log;
import android.view.KeyEvent;
import android.view.Menu;
import android.view.MenuItem;
import android.view.View;
import android.view.inputmethod.InputMethodManager;
import android.widget.Button;
import android.widget.EditText;
import android.widget.TextView;

import androidx.annotation.NonNull;
import androidx.appcompat.app.AlertDialog;
import androidx.appcompat.app.AppCompatActivity;
import androidx.appcompat.widget.Toolbar;

import com.example.livestreamingapp.MassTvActivity;
import com.example.livestreamingapp.R;
import com.example.livestreamingapp.model.ProfileData;
import com.example.livestreamingapp.model.LoginResponse;
import com.example.livestreamingapp.profile.ProfileActivity;
import com.example.livestreamingapp.utils.AppEditText;
import com.example.livestreamingapp.utils.Constants;
import com.example.livestreamingapp.utils.SharedPreferenceHandler;
import com.example.livestreamingapp.webservice.APIClient;
import com.example.livestreamingapp.webservice.APIInterface;
import com.google.android.gms.cast.framework.CastButtonFactory;
import com.google.android.gms.cast.framework.CastContext;

import java.util.HashMap;
import java.util.Map;

import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;

public class OtpUploadVerificationActivity extends MassTvActivity implements View.OnClickListener,
        TextWatcher, View.OnFocusChangeListener, View.OnKeyListener {
    private MenuItem mediaRouteMenuItem;
    private Toolbar mToolbar;
    private CastContext mCastContext;
    TextView tv_label_title;

    private AppEditText mPinFirstDigitEditText;
    private AppEditText mPinSecondDigitEditText;
    private AppEditText mPinThirdDigitEditText;
    private AppEditText mPinForthDigitEditText;
    private AppEditText mPinHiddenEditText;
    private Button btn_otp_verify;
    private ProgressDialog progressDialog;
    private APIInterface apiInterface;
    private SMSReceiver smsReceiver;
    private String activityToOpen;


    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.layout_otp_verificaton);
        setupActionBar();

        progressDialog = new ProgressDialog(OtpUploadVerificationActivity.this);
        progressDialog.setMessage("Please wait...");
        progressDialog.setCancelable(false);
        apiInterface = APIClient.getClient().create(APIInterface.class);

        activityToOpen = getIntent().getStringExtra(Constants.ACTIVITY_TO_OPEN);

        initViews();

        smsReceiver = new SMSReceiver();
        IntentFilter filter = new IntentFilter();
        filter.addAction("android.provider.Telephony.SMS_RECEIVED");
        registerReceiver(smsReceiver, filter);

        mCastContext = CastContext.getSharedInstance(this);


    }

    private void initViews() {

        tv_label_title = findViewById(R.id.tv_label_title);
        tv_label_title.setText("Phone Verification");
        btn_otp_verify = findViewById(R.id.btn_otp_verify);

        mPinFirstDigitEditText = findViewById(R.id.edt_one);
        mPinSecondDigitEditText = findViewById(R.id.edt_two);
        mPinThirdDigitEditText = findViewById(R.id.edt_three);
        mPinForthDigitEditText = findViewById(R.id.edt_four);
        mPinHiddenEditText = findViewById(R.id.pin_hidden_edittext);

        // mPinFirstDigitEditText.requestFocus();


        /* showSoftKeyboard(mPinSecondDigitEditText);
         */
        setPINListeners();


    }


    private void setupActionBar() {
        mToolbar = (Toolbar) findViewById(R.id.toolbar);
        mToolbar.setNavigationIcon(R.drawable.ic_backs_white_24);
        mToolbar.setTitle("");
        setSupportActionBar(mToolbar);
        if (getSupportActionBar() != null) {
            getSupportActionBar().setDisplayHomeAsUpEnabled(true);
        }

    }

    @Override
    public void onClick(View v) {
        switch (v.getId()) {
            case R.id.btn_otp_verify:

                if (mPinHiddenEditText.getText().toString().isEmpty()) {
                    showMessageOK(getString(R.string.otp_empty_error), new DialogInterface.OnClickListener() {
                        @Override
                        public void onClick(DialogInterface dialog, int which) {

                        }
                    });
                } else if (mPinHiddenEditText.getText().toString().length() != 4) {
                    showMessageOK(getString(R.string.otp_valid_error), new DialogInterface.OnClickListener() {
                        @Override
                        public void onClick(DialogInterface dialog, int which) {

                        }
                    });
                } else {
                    verifyOtp(mPinHiddenEditText.getText().toString());
                  //  byPassOtp();
                }

                break;

        }
    }


    private void byPassOtp() {
        ProfileData data = new SharedPreferenceHandler(OtpUploadVerificationActivity.this).getProfileInfo();
        data.setPhoneNumber("" + getIntent().getStringExtra(Constants.PhoneNumber));
        new SharedPreferenceHandler(OtpUploadVerificationActivity.this).setProfileData(data);

        if(activityToOpen==null){
            startActivity(new Intent(OtpUploadVerificationActivity.this, UploadContentActivity.class));
            finish();
        }else {
            startActivity(new Intent(OtpUploadVerificationActivity.this, ProfileActivity.class));
            finish();
        }
    }


    @Override
    public boolean onOptionsItemSelected(@NonNull MenuItem item) {

        switch (item.getItemId()) {
            case android.R.id.home:
                // todo: goto back activity from here
                onBackPressed();
                return true;


            default:
                return super.onOptionsItemSelected(item);
        }
    }

    @Override
    public void beforeTextChanged(CharSequence s, int start, int count, int after) {

    }

    @Override
    public void onTextChanged(CharSequence s, int start, int before, int count) {

        if (s.length() == 0) {
            mPinFirstDigitEditText.setText("");
        } else if (s.length() == 1) {
            ChangeBackground(mPinFirstDigitEditText, 1);
            mPinFirstDigitEditText.setText(s.charAt(0) + "");
            //   mPinSecondDigitEditText.requestFocus();
            mPinSecondDigitEditText.setText("");
            mPinThirdDigitEditText.setText("");
            mPinForthDigitEditText.setText("");
        } else if (s.length() == 2) {
            ChangeBackground(mPinSecondDigitEditText, 1);
            mPinSecondDigitEditText.setText(s.charAt(1) + "");
            //    mPinThirdDigitEditText.requestFocus();
            mPinThirdDigitEditText.setText("");
            mPinForthDigitEditText.setText("");
        } else if (s.length() == 3) {
            ChangeBackground(mPinThirdDigitEditText, 1);
            //      mPinForthDigitEditText.requestFocus();
            mPinThirdDigitEditText.setText(s.charAt(2) + "");
            mPinForthDigitEditText.setText("");
        } else if (s.length() == 4) {
            ChangeBackground(mPinForthDigitEditText, 1);
            mPinForthDigitEditText.setText(s.charAt(3) + "");
            hideSoftKeyboard(mPinForthDigitEditText);
            btn_otp_verify.requestFocus();

        }
    }

    @Override
    public void afterTextChanged(Editable s) {

    }

    public static void setFocus(EditText editText) {
        if (editText == null)
            return;

        editText.setFocusable(true);
        editText.setFocusableInTouchMode(true);
        editText.requestFocus();
    }

    //this method for all edit boxes click and focus
    @Override
    public void onFocusChange(View view, boolean hasFocus) {
        final int id = view.getId();

        switch (id) {


            case R.id.edt_one:
                if (hasFocus) {
                    setFocus(mPinHiddenEditText);
                    showSoftKeyboard(mPinHiddenEditText);
                }
                break;

            case R.id.edt_two:
                if (hasFocus) {
                    setFocus(mPinHiddenEditText);
                    showSoftKeyboard(mPinHiddenEditText);
                }
                break;

            case R.id.edt_three:
                if (hasFocus) {
                    setFocus(mPinHiddenEditText);
                    showSoftKeyboard(mPinHiddenEditText);
                }
                break;

            case R.id.edt_four:
                if (hasFocus) {
                    setFocus(mPinHiddenEditText);
                    showSoftKeyboard(mPinHiddenEditText);
                }
                break;
            default:
                break;

        }


    }

    @Override
    public boolean onKey(View v, int keyCode, KeyEvent event) {
        return false;
    }

    //this method for focus listeners on edit boxes onclick and focus
    private void setPINListeners() {
        mPinHiddenEditText.addTextChangedListener(this);
        mPinFirstDigitEditText.setOnFocusChangeListener(this);
        mPinSecondDigitEditText.setOnFocusChangeListener(this);
        mPinThirdDigitEditText.setOnFocusChangeListener(this);
        mPinForthDigitEditText.setOnFocusChangeListener(this);
        mPinFirstDigitEditText.setOnKeyListener(this);
        mPinSecondDigitEditText.setOnKeyListener(this);
        mPinThirdDigitEditText.setOnKeyListener(this);
        mPinForthDigitEditText.setOnKeyListener(this);
        mPinHiddenEditText.setOnKeyListener(this);
        btn_otp_verify.setOnClickListener(this);
    }

    //this method for change edit boxes background color onclick and focus
    public void ChangeBackground(AppEditText ET, int set) {
        if (set == 1)

            ET.setBackgroundResource(R.drawable.line_bottom);
        else if (set == 2)
            ET.setBackgroundResource(R.drawable.line_bottom);
    }

    //this method for hide keyboard for all edit boxes click and focus
    public void hideSoftKeyboard(EditText editText) {
        if (editText == null)
            return;
        InputMethodManager imm = (InputMethodManager) getSystemService(Context.INPUT_METHOD_SERVICE);
        imm.hideSoftInputFromWindow(editText.getWindowToken(), 0);
    }

    //this method for show keyboard for all edit boxes click and focus
    public void showSoftKeyboard(EditText editText) {
        if (editText == null)
            return;

        InputMethodManager imm = (InputMethodManager) getSystemService(Context.INPUT_METHOD_SERVICE);
        imm.showSoftInput(editText, 0);
    }

    @Override
    public boolean onCreateOptionsMenu(Menu menu) {
        // Inflate the menu; this adds items to the action bar if it is present.
        getMenuInflater().inflate(R.menu.menu_cast, menu);
        mediaRouteMenuItem = CastButtonFactory.setUpMediaRouteButton(getApplicationContext(), menu,
                R.id.media_route_menu_item);
        return true;
    }

    /**
     * @Rehan After api hit successfully we will get the LoginResponse object In LoginResponse if
     * status is 1 that means otp successfully send to to the entered email address
     * if status is 0 it means something went wrong.
     */
    private void verifyOtp(String otp) {

        Map<String, String> credential = new HashMap<>();
        credential.put(getString(R.string.phone_number), getIntent().getStringExtra(Constants.PhoneNumber));
        credential.put(getString(R.string.otp), otp);
        Call<LoginResponse> verifyOtpApiCall = apiInterface.verifyOtpPhone(credential);


        progressDialog.show();


        progressDialog.show();

        verifyOtpApiCall.enqueue(
                new Callback<LoginResponse>() {
                    @Override
                    public void onResponse(
                            Call<LoginResponse> call, Response<LoginResponse> response) {
                        progressDialog.dismiss();

                        if (response.code() == 200) {

                            if (response.body().getStatus()) {

                                /*new SharedPreferenceHandler(OtpUploadVerificationActivity.this)
                                        .setPreference(Constants.PROFILE_INFO,new Gson().toJson(response.body().getUser()));*/
                                ProfileData data = new SharedPreferenceHandler(OtpUploadVerificationActivity.this).getProfileInfo();
                                data.setPhoneNumber("" + getIntent().getStringExtra(Constants.PhoneNumber));
                                new SharedPreferenceHandler(OtpUploadVerificationActivity.this).setProfileData(data);

                                if(activityToOpen==null){
                                    startActivity(new Intent(OtpUploadVerificationActivity.this, UploadContentActivity.class));
                                    finish();
                                }else {
                                    startActivity(new Intent(OtpUploadVerificationActivity.this, ProfileActivity.class));
                                    finish();
                                }


                            } else {
                                new AlertDialog.Builder(OtpUploadVerificationActivity.this, R.style.AlertDialogStyle)
                                        .setCancelable(false)
                                        .setMessage("Invalid Otp!")
                                        .setPositiveButton(
                                                "Ok",
                                                new DialogInterface.OnClickListener() {
                                                    @Override
                                                    public void onClick(
                                                            DialogInterface dialog, int which) {
                                                    }
                                                })
                                        .show();
                            }
                        } else {
                            new AlertDialog.Builder(OtpUploadVerificationActivity.this, R.style.AlertDialogStyle)
                                    .setCancelable(false)
                                    .setMessage("Otp does not match!")
                                    .setPositiveButton(
                                            "Ok",
                                            new DialogInterface.OnClickListener() {
                                                @Override
                                                public void onClick(
                                                        DialogInterface dialog, int which) {
                                                }
                                            })
                                    .show();
                        }
                    }

                    @Override
                    public void onFailure(Call<LoginResponse> call, Throwable t) {
                        progressDialog.dismiss();
                        new AlertDialog.Builder(OtpUploadVerificationActivity.this)
                                .setCancelable(false)
                                .setMessage("" + t.getMessage())
                                .setPositiveButton(
                                        "Ok",
                                        new DialogInterface.OnClickListener() {
                                            @Override
                                            public void onClick(
                                                    DialogInterface dialog, int which) {
                                            }
                                        })
                                .show();
                    }
                });
    }


    @Override
    protected void onResume() {
        super.onResume();

    }

    /**
     * @Rehan BroadcastReceiver to wait for SMS messages. This can be registered either in the
     * AndroidManifest or at runtime. Should filter Intents on SmsRetriever.SMS_RETRIEVED_ACTION.
     * '51457' is the sender name of this otp. so we identify the sms by identify the sender
     * name currentSMS.getDisplayMessageBody().substring(0, 6).trim() here 0 and 6 indicate the
     * starting and ending character of otp
     */
    private class SMSReceiver extends BroadcastReceiver {
        private Bundle bundle;
        private SmsMessage currentSMS;
        private String message;

        @Override
        public void onReceive(Context context, Intent intent) {

            if (intent.getAction().equals("android.provider.Telephony.SMS_RECEIVED")) {
                final Bundle bundle = intent.getExtras();
                if (bundle != null) {
                    Object[] pdu_Objects = (Object[]) bundle.get("pdus");
                    if (pdu_Objects != null) {

                        for (Object aObject : pdu_Objects) {

                            currentSMS = getIncomingMessage(aObject, bundle);

                            String senderNo = currentSMS.getDisplayOriginatingAddress();

                            if (senderNo.equalsIgnoreCase("51457")) {
                                if (currentSMS.getDisplayMessageBody().toString().contains("Twilio")) {
                                    String otp =
                                            currentSMS.getDisplayMessageBody().substring(51).trim();
                                    String one = currentSMS.getDisplayMessageBody().substring(51, 52);
                                    String two = currentSMS.getDisplayMessageBody().substring(52, 53);
                                    String three = currentSMS.getDisplayMessageBody().substring(53, 54);
                                    String four = currentSMS.getDisplayMessageBody().substring(54, 55);

                                   mPinFirstDigitEditText.setText(one);
                                    mPinSecondDigitEditText.setText(two);
                                    mPinThirdDigitEditText.setText(three);
                                    mPinForthDigitEditText.setText(four);
                                    //mPinHiddenEditText.setText(otp);
                                    // mPinFirstDigitEditText.setText("");

                                    if (!((OtpUploadVerificationActivity) context).isFinishing()) {
                                        //  Activity is running
                                       verifyOtp(otp);
                                     //   byPassOtp();
                                    } else {
                                        //  Activity has been finished
                                    }

                                }


                            }
                        }
                    }
                }
            } // bundle null
        }
    }


    /**
     * Parse sms
     *
     * @param aObject
     * @param bundle
     * @return
     */
    private SmsMessage getIncomingMessage(Object aObject, Bundle bundle) {
        SmsMessage currentSMS;
        if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.M) {
            String format = bundle.getString("format");
            currentSMS = SmsMessage.createFromPdu((byte[]) aObject, format);
        } else {
            currentSMS = SmsMessage.createFromPdu((byte[]) aObject);
        }

        return currentSMS;
    }

    private void showMessageOK(String message, DialogInterface.OnClickListener okListener) {
        new AlertDialog.Builder(this, R.style.AlertDialogStyle)
                .setMessage(message)
                .setPositiveButton("OK", okListener)
                .create()
                .show();
    }

    @Override
    public void onBackPressed() {
        if(activityToOpen!=null){
            startActivity(new Intent(OtpUploadVerificationActivity.this, AddPhoneNumberActivity.class).
                    putExtra(Constants.ACTIVITY_TO_OPEN,activityToOpen)
            );
            finish();
        }else {
            startActivity(new Intent(OtpUploadVerificationActivity.this, AddPhoneNumberActivity.class)
            );
            finish();
        }

    }
}
