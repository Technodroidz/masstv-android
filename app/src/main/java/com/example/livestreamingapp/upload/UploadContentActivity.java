package com.example.livestreamingapp.upload;

import androidx.annotation.NonNull;
import androidx.annotation.Nullable;
import androidx.appcompat.app.AlertDialog;
import androidx.appcompat.app.AppCompatActivity;
import androidx.appcompat.widget.Toolbar;
import androidx.cardview.widget.CardView;
import androidx.core.app.NotificationCompat;
import androidx.core.app.ShareCompat;
import androidx.core.content.FileProvider;
import androidx.recyclerview.widget.GridLayoutManager;
import androidx.recyclerview.widget.RecyclerView;

import android.Manifest;
import android.app.Activity;
import android.app.NotificationManager;
import android.content.ActivityNotFoundException;
import android.content.ContentResolver;
import android.content.Context;
import android.content.ContextWrapper;
import android.content.DialogInterface;
import android.content.Intent;
import android.content.pm.PackageManager;
import android.content.pm.ResolveInfo;
import android.content.res.AssetManager;
import android.database.Cursor;
import android.graphics.Bitmap;
import android.graphics.BitmapFactory;
import android.graphics.Color;
import android.graphics.drawable.ColorDrawable;
import android.graphics.drawable.InsetDrawable;
import android.media.ThumbnailUtils;
import android.net.Uri;
import android.os.AsyncTask;
import android.os.Build;
import android.os.Bundle;
import android.os.Environment;
import android.os.Handler;
import android.os.Looper;
import android.provider.MediaStore;
import android.text.Spannable;
import android.text.SpannableString;
import android.text.Spanned;
import android.text.TextPaint;
import android.text.method.LinkMovementMethod;
import android.text.style.ClickableSpan;
import android.text.style.ForegroundColorSpan;
import android.util.Log;
import android.view.View;
import android.widget.ArrayAdapter;
import android.widget.Button;
import android.widget.CheckBox;
import android.widget.EditText;
import android.widget.FrameLayout;
import android.widget.ImageView;
import android.widget.ProgressBar;
import android.widget.RelativeLayout;
import android.widget.Spinner;
import android.widget.TextView;
import android.widget.Toast;

import com.bumptech.glide.Glide;
import com.example.livestreamingapp.BuildConfig;
import com.example.livestreamingapp.MainActivity;
import com.example.livestreamingapp.MassTvActivity;
import com.example.livestreamingapp.MassTvApplication;
import com.example.livestreamingapp.R;
import com.example.livestreamingapp.home.ui.HomeDraweerStreamingActivity;
import com.example.livestreamingapp.home.ui.SearchActivity;
import com.example.livestreamingapp.home.ui.adapter.AdapterCategory;
import com.example.livestreamingapp.model.CategoryData;
import com.example.livestreamingapp.model.OnDemandCategory;
import com.example.livestreamingapp.model.ProfileData;
import com.example.livestreamingapp.model.VideoUploadResponse;
import com.example.livestreamingapp.model.mastercategory.OnDemandMasterCategory;
import com.example.livestreamingapp.utils.BottomNavigationHelper;
import com.example.livestreamingapp.utils.CompleteProfile;
import com.example.livestreamingapp.utils.ConnectivityReceiver;
import com.example.livestreamingapp.utils.CustomProgressDialog;
import com.example.livestreamingapp.utils.DateCalculation;
import com.example.livestreamingapp.utils.FileUtils;
import com.example.livestreamingapp.utils.SharedPreferenceHandler;
import com.example.livestreamingapp.utils.SpacesItemDecoration;
import com.example.livestreamingapp.utils.Utils;
import com.example.livestreamingapp.webservice.APIClient;
import com.example.livestreamingapp.webservice.APIInterface;
import com.example.livestreamingapp.webservice.APIUploadClient;
import com.example.livestreamingapp.webservice.GetVideoCategoryList;
import com.example.livestreamingapp.webservice.ProgressListener;
import com.example.livestreamingapp.webservice.ProgressRequestBody;
import com.example.livestreamingapp.webservice.Setting;
import com.google.android.gms.cast.framework.CastContext;
import com.google.gson.Gson;

import java.io.BufferedInputStream;
import java.io.BufferedReader;
import java.io.ByteArrayOutputStream;
import java.io.DataOutputStream;
import java.io.File;
import java.io.FileInputStream;
import java.io.FileNotFoundException;
import java.io.FileOutputStream;
import java.io.IOException;
import java.io.InputStream;
import java.io.InputStreamReader;
import java.io.OutputStream;
import java.net.HttpURLConnection;
import java.net.MalformedURLException;
import java.net.URISyntaxException;
import java.net.URL;
import java.text.DateFormat;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Date;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import io.tus.android.client.TusAndroidUpload;
import okhttp3.MediaType;
import okhttp3.MultipartBody;
import okhttp3.OkHttpClient;
import okhttp3.RequestBody;
import okio.BufferedSink;
import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;
import retrofit2.Retrofit;

public class UploadContentActivity extends AppCompatActivity implements
        AdapterGridVideoUploadList.AdapterViewClickListener,
        ConnectivityReceiver.ConnectivityReceiverListener
        , View.OnClickListener, ProgressRequestBody.UploadCallbacks, CaptureImageDialog.CaptureImageListener {

    public static final String PROGRESS_UPDATE = "upload_progress";


    public static final int REQUEST_TAKE_GALLERY_IMAGE = 234;
    private CastContext mCastContext;
    private RecyclerView rv_upload;
    Toolbar mToolbar;
    private ImageView iv_upload_video;
    private FrameLayout fl_take_image;

    private String selectedVideoPath;
    private String[] permission;
    private static final int REQUEST_TAKE_GALLERY_VIDEO = 1234;
    private static final int PERMISSION_CODE = 1112;

    private RelativeLayout waitScreen;
    private Call<VideoUploadResponse> callUpload;
    private TextView tv_progress;
    private boolean secondTime;
    private boolean isStartUploading;
    private Bitmap bitmap;
    private File selectedVideoImageFile;
    private FrameLayout fl_add_content;
    private CheckBox cb_copyright;
    private TextView tv_term;
    private String videoDuration;

    private EditText et_title;
    private EditText et_description;
    private EditText et_tags;
    private Spinner sp_category;
    private ArrayList<OnDemandMasterCategory> categoryList;
    private BottomNavigationHelper bottomNavigationHelper;
    private String response;
    private OkHttpClient client;
    private String filePath;
    private uploadFilesEx uploadFilesTask;
    private boolean isTaskCancel;
    private TextView tv_link_thumbnail;
    private long time;
    private CaptureImageDialog captureImageDialog;
    private TextView tv_upload_new_thumbnail;
    private TextView tv_selected_thumbnail;


    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_upload_content);
        mToolbar = (Toolbar) findViewById(R.id.toolbar);
        client = new OkHttpClient();
        // mToolbar.setNavigationIcon(R.drawable.ic_backs_white_24);
        mToolbar.setTitle("");
        setSupportActionBar(mToolbar);
        // bottomNavigationHelper = new BottomNavigationHelper(this);
        categoryList = new ArrayList<>();
        captureImageDialog = new CaptureImageDialog(this);

        // getSupportActionBar().setDisplayHomeAsUpEnabled(true);

        try {
            if (uploadFilesTask != null) {
                uploadFilesTask.cancel(true);
            }
        } catch (Exception e) {

        }


        initViews();
        if (checkConnection()) {
            getCategoryList();
            secondTime = false;
        } else {
            secondTime = true;
        }

        mCastContext = CastContext.getSharedInstance(this);

    }


    public void initViews() {

        tv_selected_thumbnail = findViewById(R.id.tv_selected_thumbnail);
        tv_upload_new_thumbnail = findViewById(R.id.tv_upload_new_thumbnail);

        tv_progress = findViewById(R.id.tv_progress);
        fl_add_content = findViewById(R.id.fl_add_content);
        cb_copyright = findViewById(R.id.cb_copyright);
        tv_term = findViewById(R.id.tv_term);
        String url = "http://www.masstv.app";
        Intent intentBrowser = new Intent(Intent.ACTION_VIEW);
        Uri u = Uri.parse(url);

        tv_link_thumbnail = findViewById(R.id.tv_link_thumbnail);
     /*   String thumbnail = tv_link_thumbnail.getText().toString();
        ClickableSpan clickableSpan1 = new ClickableSpan() {
            @Override
            public void onClick(@NonNull View widget) {
                intentBrowser.setData(u);
                startActivity(intentBrowser);
                // open masstv.app
            }

            @Override
            public void updateDrawState(@NonNull TextPaint ds) {
                super.updateDrawState(ds);
            }
        };
        SpannableString spannableString = new SpannableString(thumbnail);
        //only the word 'link' is clickable
        spannableString.setSpan(clickableSpan1, 32, thumbnail.length(), Spanned.SPAN_EXCLUSIVE_EXCLUSIVE);
        spannableString.setSpan(new ForegroundColorSpan(getResources().getColor(R.color.green)), 32, thumbnail.length(),
                Spannable.SPAN_EXCLUSIVE_EXCLUSIVE);

        //The following is to set the new text in the TextView
        //no styles for an already clicked link
        tv_link_thumbnail.setText(spannableString);
        tv_link_thumbnail.setMovementMethod(LinkMovementMethod.getInstance());
        tv_link_thumbnail.setHighlightColor(Color.TRANSPARENT);*/


        tv_progress = findViewById(R.id.tv_progress);
        et_description = findViewById(R.id.et_description);
        et_title = findViewById(R.id.et_title);
        et_tags = findViewById(R.id.et_tags);
        sp_category = findViewById(R.id.sp_category);

        // setMessageWithClickableLink(tv_term);

        waitScreen = findViewById(R.id.waitScreen);
        permission =
                new String[]{
                        /*"android.permission.FOREGROUND_SERVICE",*/
                        Manifest.permission.WRITE_EXTERNAL_STORAGE, Manifest.permission.READ_EXTERNAL_STORAGE
                };

        iv_upload_video = findViewById(R.id.iv_upload_video);
        fl_take_image = findViewById(R.id.fl_take_image);

        fl_take_image.setOnClickListener(this);
        tv_upload_new_thumbnail.setOnClickListener(this);

    }

    private void getCategoryList() {

        new GetVideoCategoryList(this, new GetVideoCategoryList.onDemandCategoryListListener() {

            @Override
            public void onFetchedCategoryList(CategoryData videoData) {

            }

            @Override
            public void onFetchedMasterCategoryList(List<OnDemandMasterCategory> videoData) {
                categoryList.clear();
                categoryList = new ArrayList<>();
                categoryList.addAll(videoData);
                OnDemandMasterCategory category = new OnDemandMasterCategory();
                category.setName("Select Category");
                categoryList.add(0, category);


                ArrayAdapter<OnDemandMasterCategory> categoryAdapter = new ArrayAdapter<OnDemandMasterCategory>(UploadContentActivity.this,
                        R.layout.item_spinner_text, categoryList);
                categoryAdapter.setDropDownViewResource(android.R.layout.simple_spinner_dropdown_item);
                sp_category.setAdapter(categoryAdapter);

            }

            @Override
            public void onCategoryFetchingError(String error) {


            }
        }).GetMasterCategoryList();

    }


    public void setMessageWithClickableLink(TextView textView) {
        //The text and the URL
        String content = textView.getText().toString();
        //Clickable Span will help us to make clickable a text

        ClickableSpan clickableSpan1 = new ClickableSpan() {
            @Override
            public void onClick(@NonNull View widget) {
                // show term and condition
                showTermOfServiceAlert();
            }

            @Override
            public void updateDrawState(@NonNull TextPaint ds) {
                super.updateDrawState(ds);
            }
        };

        ClickableSpan clickableSpan2 = new ClickableSpan() {
            @Override
            public void onClick(@NonNull View widget) {
                // show term and condition
                //showTermOfServiceAlert();
                // show privacy policy pdf
                new FileUtils().showPdf(UploadContentActivity.this);
             //   File file = FileUtils.copyFile(UploadContentActivity.this, "policy.pdf");
/*
                try {
                    Uri uri = FileProvider.getUriForFile(UploadContentActivity.this, BuildConfig.APPLICATION_ID, file);
                    Intent intent = ShareCompat.IntentBuilder.from(UploadContentActivity.this)
                            .setStream(uri) // uri from FileProvider
                            .getIntent()
                            .setAction(Intent.ACTION_VIEW) //Change if needed
                            .setDataAndType(uri, "application/pdf")
                            .addFlags(Intent.FLAG_GRANT_READ_URI_PERMISSION);

                    if (Build.VERSION.SDK_INT <= Build.VERSION_CODES.KITKAT) {
                        List<ResolveInfo> resInfoList = getPackageManager().queryIntentActivities(intent, PackageManager.MATCH_DEFAULT_ONLY);
                        for (ResolveInfo resolveInfo : resInfoList) {
                            String packageName = resolveInfo.activityInfo.packageName;
                            grantUriPermission(packageName, uri, Intent.FLAG_GRANT_READ_URI_PERMISSION);
                        }
                    }

                    if (file.exists()) {
                        startActivity(intent);
                    }


                } catch (Exception e) {
                    e.printStackTrace();
                }*/

                FileUtils.copyFile(UploadContentActivity.this,"policy");
            }

            @Override
            public void updateDrawState(@NonNull TextPaint ds) {
                super.updateDrawState(ds);
            }
        };

        //SpannableString will be created with the full content and
        // the clickable content all together
        SpannableString spannableString = new SpannableString(content);
        //only the word 'link' is clickable
        spannableString.setSpan(clickableSpan1, 55, 76, Spanned.SPAN_EXCLUSIVE_EXCLUSIVE);
        spannableString.setSpan(clickableSpan2, 80, content.length(), Spanned.SPAN_EXCLUSIVE_EXCLUSIVE);
        spannableString.setSpan(new ForegroundColorSpan(getResources().getColor(R.color.green)), 55, 76, Spannable.SPAN_EXCLUSIVE_EXCLUSIVE);
        spannableString.setSpan(new ForegroundColorSpan(getResources().getColor(R.color.green)), 80, content.length(), Spannable.SPAN_EXCLUSIVE_EXCLUSIVE);

        //The following is to set the new text in the TextView
        //no styles for an already clicked link
        textView.setText(spannableString);
        textView.setMovementMethod(LinkMovementMethod.getInstance());
        textView.setHighlightColor(Color.TRANSPARENT);
    }

    @Override
    protected void onStop() {
        super.onStop();
    }

    @Override
    public void onNetworkConnectionChanged(boolean isConnected) {
        if (categoryList == null || categoryList.size() == 0 && secondTime) {
            getCategoryList();
        } else {
            secondTime = true;
        }
    }

    @Override
    public void onSignatureCapture(File selectedVideoImageFile) {
        this.selectedVideoImageFile = null;
        this.selectedVideoImageFile = selectedVideoImageFile;
        // this.selectedVideoImageFile = selectedVideoImageFile;
        tv_selected_thumbnail.setVisibility(View.VISIBLE);
        tv_selected_thumbnail.setText(this.selectedVideoImageFile.getName());
        Glide.with(UploadContentActivity.this).load(selectedVideoImageFile.getAbsolutePath()).load(iv_upload_video);
    }

    /**
     * Http request upload file
     */
    public class UploadFile extends AsyncTask<Object, String, String> {

        @Override
        protected void onPreExecute() {
            super.onPreExecute();
            waitScreen.setVisibility(View.VISIBLE);
        }

        public boolean isUrlValid(String url) {
            try {
                URL obj = new URL(url);
                obj.toURI();
                return true;
            } catch (MalformedURLException e) {
                return false;
            } catch (URISyntaxException e) {
                return false;
            }
        }

        @Override
        protected String doInBackground(Object[] params) {
            try {
                String response = "error";

                HttpURLConnection connection = null;
                DataOutputStream outputStream = null;
                // DataInputStream inputStream = null;

                String pathToOurFile = selectedVideoPath;
                String pathToOurImageFile = selectedVideoImageFile.getAbsolutePath();
                String urlServer = "https://qa4.allnewlb.com/file/upload/user/user/user_picture?_format=json";
                String lineEnd = "\r\n";
                String twoHyphens = "--";
                String boundary = "*****";
                DateFormat df = new SimpleDateFormat("yyyy_MM_dd_HH:mm:ss");

                int bytesRead, bytesAvailable, bufferSize;
                byte[] buffer;
                int maxBufferSize = 1 * 1024;
                try {
                    FileInputStream fileInputStream = new FileInputStream(new File(
                            pathToOurFile));

                    FileInputStream fileImageInputStream = new FileInputStream(new File(
                            pathToOurImageFile));

                    URL url = new URL(urlServer);
                    connection = (HttpURLConnection) url.openConnection();

                    // Allow Inputs & Outputs
                    connection.setDoInput(true);
                    connection.setDoOutput(true);
                    connection.setUseCaches(false);
                    connection.setConnectTimeout(15000);
                    connection.setReadTimeout(15000);
                    connection.setChunkedStreamingMode(1024);
                    // Enable POST method
                    connection.setRequestMethod("POST");

                    connection.setRequestProperty("Connection", "Keep-Alive");
                    connection.setRequestProperty("Content-Type",
                            "multipart/form-data;boundary=" + boundary);


                    outputStream = new DataOutputStream(connection.getOutputStream());
                    outputStream.writeBytes(twoHyphens + boundary + lineEnd);


                    if (et_tags != null) {
                        outputStream.writeBytes("Content-Disposition: form-data; name=\"tags\"" + lineEnd);
                        outputStream.writeBytes(lineEnd);
                        outputStream.writeBytes(et_tags.getText().toString());
                        outputStream.writeBytes(lineEnd);
                        outputStream.writeBytes(twoHyphens + boundary + lineEnd);
                    }


                    String connstrImage = null;
                    connstrImage = "Content-Disposition: form-data; name=\"thumb\";filename=\""
                            + pathToOurImageFile + "\"" + lineEnd;
                    Log.i("Connstr", connstrImage);

                    outputStream.writeBytes(connstrImage);
                    outputStream.writeBytes(lineEnd);

                    bytesAvailable = fileImageInputStream.available();
                    bufferSize = Math.min(bytesAvailable, maxBufferSize);
                    buffer = new byte[bufferSize];

                    // Read file
                    bytesRead = fileImageInputStream.read(buffer, 0, bufferSize);

                    try {
                        while (bytesRead > 0) {
                            if (uploadFilesTask.isCancelled()) {
                                isTaskCancel = true;
                                try {
                                    Thread.sleep(200);
                                } catch (Exception e) {

                                }

                                break;
                            }

                            try {
                                outputStream.write(buffer, 0, bufferSize);
                            } catch (OutOfMemoryError e) {
                                e.printStackTrace();
                                response = "outofmemoryerror";
                                return response;
                            }
                            bytesAvailable = fileImageInputStream.available();
                            bufferSize = Math.min(bytesAvailable, maxBufferSize);
                            bytesRead = fileImageInputStream.read(buffer, 0, bufferSize);
                        }
                    } catch (Exception e) {
                        e.printStackTrace();
                        response = "error";

                    }
                    outputStream.writeBytes(lineEnd);
                    outputStream.writeBytes(twoHyphens + boundary + lineEnd);

                    String connstr = null;
                    connstr = "Content-Disposition: form-data; name=\"source\";filename=\""
                            + pathToOurFile + "\"" + lineEnd;
                    Log.i("Connstr", connstr);

                    outputStream.writeBytes(connstr);
                    outputStream.writeBytes(lineEnd);

                    bytesAvailable = fileInputStream.available();
                    bufferSize = Math.min(bytesAvailable, maxBufferSize);
                    buffer = new byte[bufferSize];

                    // Read file
                    long uploaded = 0;
                    long fileLength = new File(selectedVideoPath).length();
                    bytesRead = fileInputStream.read(buffer, 0, bufferSize);
                    Log.e("Image length", bytesAvailable + "");
                    try {
                        Handler handler = new Handler(Looper.getMainLooper());
                        while (bytesRead > 0) {
                            try {

                                if (uploadFilesTask.isCancelled()) {
                                    isTaskCancel = true;
                                    try {
                                        Thread.sleep(200);
                                    } catch (Exception e) {

                                    }
                                    break;
                                }

                                handler.post(new ProgressUpdater(uploaded, fileLength));

                                uploaded += bytesRead;

                                outputStream.write(buffer, 0, bufferSize);
                            } catch (OutOfMemoryError e) {
                                e.printStackTrace();
                                response = "outofmemoryerror";
                                return response;
                            }
                            bytesAvailable = fileInputStream.available();
                            bufferSize = Math.min(bytesAvailable, maxBufferSize);
                            bytesRead = fileInputStream.read(buffer, 0, bufferSize);
                        }
                    } catch (Exception e) {
                        e.printStackTrace();
                        response = "error";
                        return response;
                    }

                    outputStream.writeBytes(lineEnd);
                    outputStream.writeBytes(twoHyphens + boundary + twoHyphens
                            + lineEnd);

                    // Responses from the server (code and message)
                    int serverResponseCode = connection.getResponseCode();
                    String serverResponseMessage = connection.getResponseMessage();
                    Log.e("Server Response Code ", "" + serverResponseCode);
                    Log.e("Server Response Message", serverResponseMessage);

                    if (serverResponseCode == 200) {
                        response = "true";
                    }

                    String CDate = null;
                    Date serverTime = new Date(connection.getDate());
                    try {
                        CDate = df.format(serverTime);
                    } catch (Exception e) {
                        e.printStackTrace();

                    }


                    fileInputStream.close();
                    outputStream.flush();
                    outputStream.close();
                    outputStream = null;
                } catch (Exception ex) {
                    // Exception handling
                    response = "error";

                    ex.printStackTrace();
                }
                return response;
            } catch (Exception e) {
                e.printStackTrace();
            }
            return null;
        }

        @Override
        protected void onPostExecute(String s) {
            super.onPostExecute(s);
            Log.e("response ", "val " + s);
            waitScreen.setVisibility(View.VISIBLE);
        }
    }

    /**
     * @Rehan Term of service and privacy policy alert
     */
    private void showTermOfServiceAlert() {
        new AlertDialog.Builder(UploadContentActivity.this, R.style.AlertDialogStyle).setCancelable(false)
                .setTitle("Welcome to MASS TV")
                .setMessage(
                        "1. DESCRIPTION OF SERVICE AND ACCEPTANCE OF TERMS OF USE \n" +
                                "\n" +
                                "Mass TV (\"we\" or \"us\") provides an online video service offering a selection of shows, movies, clips, originals and other content (collectively, the \"Content\"). Our video service, the Content, our player for viewing the Content (the \"Video Player\") and any other features, tools, applications, materials, or other services offered from time to time by Mass TV in connection with its business, however accessed, are referred to collectively as the \"Mass TV Services.\" \n" +
                                "\n" +
                                "The Content is available for permissible viewing on or through the following (collectively, the \"Properties\"): \n" +
                                "\n" +
                                "Mass TV iOS APP \n" +
                                "\n" +
                                "Mass TV on Apple TV \n" +
                                "\n" +
                                "Mass TV Android APP \n" +
                                "\n" +
                                "Mass TV on Android TV \n" +
                                "\n" +
                                "Mass TV on Mass TV Website \n" +
                                "\n" +
                                "Mass TV authorized applications features or devices. \n" +
                                "\n" +
                                "Use of the Mass TV Services (including access to the Content) on the Properties is subject to compliance with these Terms of Use (\"Terms of Use\" or \"Terms\") and any end user license agreement that might accompany a Mass TV application, feature or device. \n" +
                                "\n" +
                                "It is our pleasure to provide the Mass TV Services for your personal enjoyment and entertainment in accordance with these Terms of Use. To access and enjoy the Mass TV Services, you must agree to, and follow, the terms and conditions set forth in these Terms of Use. By visiting the Mass TV Site or using any of the Mass TV Services (including accessing any Content) you are agreeing to these Terms. Please take a moment to carefully read through these Terms. It is our goal to provide you with a first-class user experience, so if you have any questions or comments about these Terms, please contact us at: admin@masstv.app \n" +
                                "\n" +
                                " \n" +
                                "\n" +
                                " \n" +
                                "\n" +
                                "2. CHANGES TO THE TERMS OF USE BY Mass TV \n" +
                                "\n" +
                                "Mass TV may amend these Terms of Use at any time by posting the amended Terms of Use on the Mass TV Site at https://masstv.app/terms/. If we make a material amendment to these Terms of Use, we will notify you by posting notice of the amendment on the Mass TV website. Any material amendment to these Terms of Use shall be effective automatically 30 days after it is initially posted or, for users who register or otherwise provide opt-in consent during this 30-day period, at the time of registration or consent, as applicable. \n" +
                                "\n" +
                                " \n" +
                                "\n" +
                                "3. ACCESS AND USE OF THE MASS TV.COM SERVICES \n" +
                                "\n" +
                                "Age Limitations. If you are under the age of 13, then you are not permitted to register with Mass TV or use any feature or other part of the Mass TV Services that requires registration. In order to subscribe to Mass TV, you must be at least 18 years of age. Please note for those under 18 years of age, if your parent or guardian subscribes to Mass TV and authorizes you to access his or her Mass TV account then you will not be able to access all the Content offered as part of the Mass TV service if we have been informed during the registration process for the account that the primary user of such account will be under 18 years of age. Parents and guardians and users under the age of 18 must review these Terms with each other to make sure that each understands and agrees with them. You represent that you are at least 13 years of age if you are registering an account, and, if you are a Mass TV user, that you are at least 18 years of age or you have received consent from your parent or guardian to access his or her Mass TV account. \n" +
                                "\n" +
                                "Your License. Mass TV is pleased to grant you a non-exclusive limited license to use the Mass TV Services on the Properties, including accessing and viewing the Content on a streaming-only basis through the Video Player, for personal, non-commercial purposes as set forth in these Terms. \n" +
                                "\n" +
                                "The Content. You may only access and view the Content personally and for a non-commercial purpose in compliance with these Terms. You may not either directly or using any device, software, internet site, web-based service, or other means remove, alter, bypass, avoid, interfere with, or circumvent any copyright, trademark, or other proprietary notices marked on the Content or any digital rights management mechanism, device, or other content protection or access control measure associated with the Content including geo-filtering mechanisms. You may not either directly or through the use of any device, software, internet site, web-based service, or other means copy, download, stream capture, reproduce, duplicate, archive, distribute, upload, publish, modify, translate, broadcast, perform, display, sell, transmit or retransmit the Content unless expressly permitted by Mass TV in writing. You may not incorporate the Content into, or stream or retransmit the Content via, any hardware or software application or make it available via frames or in-line links unless expressly permitted by Mass TV in writing. Furthermore, you may not create, recreate, distribute or advertise an index of any significant portion of the Content unless authorized by Mass TV. You may not build a business utilizing the Content, whether or not for profit. The Content covered by these restrictions includes without limitation any text, graphics, layout, interface, logos, photographs, audio and video materials, and stills. In addition, you are strictly prohibited from creating derivative works or materials that otherwise are derived from or based on in any way the Content, including montages, mash-ups and similar videos, wallpaper, desktop themes, greeting cards, and merchandise, unless it is expressly permitted by Mass TV in writing. This prohibition applies even if you intend to give away the derivative materials free of charge. \n" +
                                "\n" +
                                "The Video Player. You may not modify, enhance, remove, interfere with, or otherwise alter in any way any portion of the Video Player, its underlying technology, any digital rights management mechanism, device, or other content protection or access control measure incorporated into the Video Player. This restriction includes, without limitation, disabling, reverse engineering, modifying, interfering with or otherwise circumventing the Video Player in any manner that enables users to view the Content without: (i) displaying visibly both the Video Player and all surrounding elements (including the graphical user interface, any advertising, copyright notices, and trademarks) of the webpage where the Video Player is located; and (ii) having full access to all functionality of the Video Player, including, without limitation, all video quality and display functionality and all interactive, elective, or click-through advertising functionality. \n" +
                                "\n" +
                                "Embedding a Video Using the Video Player. Where Mass TV has incorporated an embed option in connection with Content on the Mass TV Services, you may embed videos using the Video Player, provided you do not embed the Video Player on any website or other location that (i) contains or hosts content that is unlawful, infringing, pornographic, obscene, defamatory, libelous, threatening, harassing, vulgar, indecent, profane, hateful, racially or ethnically offensive, encourages criminal conduct, gives rise to civil liability, violates any law, rule, or regulation, infringes any right of any third party including intellectual property rights, or is otherwise inappropriate or objectionable to Mass TV (in Mass TV's sole discretion), or (ii) links to infringing or unauthorized content (collectively, \"Unsuitable Material\"). You may not embed the Video Player into any hardware or software application, even for non-commercial purposes. Mass TV reserves the right to prevent embedding to any website or other location that Mass TV finds inappropriate or objectionable (as determined by Mass TV in its sole discretion). \n" +
                                "\n" +
                                "Ownership. You agree that Mass TV owns and retains all rights to the Mass TV Services. You further agree that the Content you access and view as part of the Mass TV Services is owned or controlled by Mass TV and Mass TV's licensors. The Mass TV Services and the Content are protected by copyright, trademark, and other intellectual property laws. \n" +
                                "\n" +
                                "Your Responsibilities. In order for us to keep the Mass TV Services safe and available for everyone to use, we all have to follow the same rules of the road. You and other users must use the Mass TV Services for lawful, non-commercial, and appropriate purposes only. Your commitment to this principle is critical. You agree to observe the Mass TV Services, Content, Video Player and embedding restrictions detailed above, and further agree that you will not access the Mass TV Site or use the Mass TV Services in a way that: \n" +
                                "\n" +
                                "violates the rights of others, including patent, trademark, trade secret, copyright, privacy, publicity, or other proprietary rights; \n" +
                                "\n" +
                                "uses technology or other means to access, index, frame or link to the Mass TV Services (including the Content) that is not authorized by Mass TV (including by removing, disabling, bypassing, or circumventing any content protection or access control mechanisms intended to prevent the unauthorized download, stream capture, linking, framing, reproduction, access to, or distribution of the Mass TV Services); \n" +
                                "\n" +
                                "involves accessing the Mass TV Services (including the Content)through any automated means, including \"robots,\" \"spiders,\" or \"offline readers\" (other than by individually performed searches on publicly accessible search engines for the sole purpose of, and solely to the extent necessary for, creating publicly available search indices - but not caches or archives - of the Mass TV Services and excluding those search engines or indices that host, promote, or link primarily to infringing or unauthorized content); \n" +
                                "\n" +
                                "introduces viruses or any other computer code, files, or programs that interrupt, destroy, or limit the functionality of any computer software or hardware or telecommunications equipment; \n" +
                                "\n" +
                                "damages, disables, overburdens, impairs, or gains unauthorized access to the Mass TV Services, including Mass TV's servers, computer network, or user accounts; \n" +
                                "\n" +
                                "removes, modifies, disables, blocks, obscures or otherwise impairs any advertising in connection with the Mass TV Services (including the Content); \n" +
                                "\n" +
                                "uses the Mass TV Services to advertise or promote services that are not expressly approved in advance in writing by Mass TV; \n" +
                                "\n" +
                                "collects personally identifiable information in violation of Mass TV's Privacy Policy; \n" +
                                "\n" +
                                "encourages conduct that would constitute a criminal offense or give rise to civil liability; \n" +
                                "\n" +
                                "violates these Terms or any guidelines or policies posted by Mass TV; \n" +
                                "\n" +
                                "interferes with any other party's use and enjoyment of the Mass TV Services; or \n" +
                                "\n" +
                                "attempts to do any of the foregoing. \n" +
                                "\n" +
                                "If Mass TV determines in its sole discretion that you are violating any of these Terms, we may (i) notify you, and (ii) use technical measures to block or restrict your access or use of the Mass TV Services. In either case, you agree to immediately stop accessing or using in any way (or attempting to access or use) the Mass TV Services, and you agree not to circumvent, avoid, or bypass such restrictions, or otherwise restore or attempt to restore such access or use. \n" +
                                "\n" +
                                "No Spam/Unsolicited Communications. We know how annoying and upsetting it can be to receive unwanted email or instant messages from people you do not know. Therefore, no one may use the Mass TV Services to harvest information about users for the purpose of sending, or to facilitate or encourage the sending of, unsolicited bulk or other communications. You understand that we may take any technical remedies to prevent spam or unsolicited bulk or other communications from entering, utilizing, or remaining within our computer or communications networks. If you Post (as defined below in Section 7) or otherwise send spam, advertising, or other unsolicited communications of any kind through the Mass TV Services, you acknowledge that you will have caused substantial harm to Mass TV and that the amount of such harm would be extremely difficult to measure. As a reasonable estimation of such harm, you agree to pay Mass TV $50 for each such unsolicited communication you send through the Mass TV Services. \n" +
                                "\n" +
                                "Downloads. In order to participate in certain Mass TV Services or access certain Content, you may be notified that it is necessary to download software or other materials or agree to additional terms and conditions. Unless otherwise provided by these additional terms and conditions, they are hereby incorporated into these Terms. \n" +
                                "\n" +
                                "Internet Access Charges. You are responsible for any costs you incur to access the internet. \n" +
                                "\n" +
                                "Customer Service. If we can be of help to you, please do not hesitate to contact us via the contact box at the bottom of every page. It would be our pleasure to serve you. \n" +
                                "\n" +
                                " \n" +
                                "\n" +
                                "4. ACCOUNTS AND REGISTRATION \n" +
                                "\n" +
                                "We may from time to time offer various features that require registration or the creation of an account with Mass TV. If at any time you choose to register or create an account with us, the additional terms and conditions set forth below also will apply. \n" +
                                "\n" +
                                "All registration information you submit must be accurate and updated. Please keep your password confidential. You will not have to reveal it to any Mass TV representative. You are responsible for all use on your account, including unauthorized use by any third party, so please be very careful to guard the security of your password. Please notify us as soon as you know of, or suspect any unauthorized use of, your account. Please also make sure to notify us if your registration information changes, in case we need to contact you. \n" +
                                "\n" +
                                "We reserve the right to immediately terminate or restrict your account or your use of the Mass TV Services or access to Content at any time, without notice or liability, if Mass TV determines in its sole discretion that you have breached these Terms of Use, violated any law, rule, or regulation, engaged in other inappropriate conduct, or for any other business reason. We also reserve the right to terminate your account or your use of the Mass TV Services or access to Content if such use places an undue burden on our networks or servers. Of course, we would prefer to avoid such termination; therefore, we may use technology to limit activities, such as the number of calls to the Mass TV servers being made or the volume of User Material (as defined below in Section 7) being Posted, and you agree to respect these limitations and not take any steps to circumvent, avoid, or bypass them. \n" +
                                "\n" +
                                " \n" +
                                "\n" +
                                "5. COLLECTION AND USE OF PERSONAL INFORMATION \n" +
                                "\n" +
                                "The Privacy Policy is incorporated by reference and made part of these Terms of Use. Thus, by agreeing to these Terms of Use, you agree that your presence on the Mass TV Site and use of the Mass TV Services on any of the Properties are governed by the Mass TV Privacy Policy in effect at the time of your use. We do not collect your sell or share your personal information with other parties or affiliates. We only collect this information for ad targeting and providing our users with a better user experience when accessing Mass TV services. Also, available is our privacy policy at www. masstv.app/privacy.  \n" +
                                "\n" +
                                " \n" +
                                "\n" +
                                " \n" +
                                "\n" +
                                "6. USER REVIEWS, COMMENTS, AND OTHER MATERIAL \n" +
                                "\n" +
                                "Your Posts. As part of the Mass TV Services, users may have an opportunity to publish, transmit, submit, or otherwise post (collectively, \"Post\") reviews, comments, articles, or other materials (collectively, \"User Material\"). In order to keep the Mass TV Services enjoyable for all of our users, you must adhere to the rules below. \n" +
                                "\n" +
                                "Please choose carefully the User Material that you Post. Please limit yourself to User Material directly relevant to the Mass TV Services. Moreover, you must not Post User Material that: (i) contains Unsuitable Material (as defined above in Section 3); or (ii) improperly claims the identity of another person. Please note that we use your first and last name as your user ID and therefore your first and last name will appear to the public each time you Post. We advise that you do not, and you should also be careful if you decide to, Post additional personally identifiable information, such as your email address, telephone number, or street address. \n" +
                                "\n" +
                                "You must be, or have first obtained permission from, the rightful owner of any User Material you Post. By submitting User Material, you represent and warrant that you own the User Material or otherwise have the right to grant Mass TV the license provided below. You also represent and warrant that the Posting of your User Material does not violate any right of any party, including privacy rights, publicity rights, and intellectual property rights. In addition, you agree to pay for all royalties, fees, and other payments owed to any party by reason of your Posting User Material. Mass TV will remove all User Material if we are properly notified that such User Material infringes on another person's rights. You acknowledge that Mass TV does not guarantee any confidentiality with respect to any User Material. \n" +
                                "\n" +
                                "By Posting User Material, you are not forfeiting any ownership rights in such material to Mass TV. After Posting your User Material, you continue to retain all of the same ownership rights you had prior to Posting. By Posting your User Material, you grant Mass TV a limited license to use, display, reproduce, distribute, modify, delete from, add to, prepare derivative works of, publicly perform, and publish such User Material through the Mass TV Services worldwide, including on or through any Property, in perpetuity, in any media formats and any media channels now known or hereinafter created. The license you grant to Mass TV is non-exclusive (meaning you are not prohibited by us from licensing your User Material to anyone else in addition to Mass TV), fully-paid, royalty-free (meaning that Mass TV is not required to pay you for the use of your User Material), and sublicensable (so that Mass TV is able to use its affiliates, subcontractors, and other partners, such as internet content delivery networks, to provide the Mass TV Services). By Posting your User Material, you also hereby grant each user of the Mass TV Services a non-exclusive, limited license to access your User Material, and to use, display, reproduce, distribute, and perform such User Material as permitted through the functionality of the Mass TV Services and under these Terms of Use. \n" +
                                "\n" +
                                "Third Party Posts. Despite these restrictions, please be aware that some material provided by users may be objectionable, unlawful, inaccurate, or inappropriate. Mass TV does not endorse any User Material, and User Material that is Posted does not reflect the opinions or policies of Mass TV. We reserve the right, but have no obligation, to monitor User Material and to restrict or remove User Material that we determine, in our sole discretion, is inappropriate or for any other business reason. In no event does Mass TV assume any responsibility or liability whatsoever for any User Material, and you agree to waive any legal or equitable rights or remedies you may have against Mass TV with respect to such User Material. You can help us tremendously by notifying us of any inappropriate User Material you find. If a \"report\" feature through the Mass TV Services is not available for a specific instance of inappropriate User Material, please email admin@masstv.app (subject line: \"Inappropriate User Material\"). \n" +
                                "\n" +
                                " \n" +
                                "\n" +
                                "7. LINKED DESTINATIONS AND ADVERTISING \n" +
                                "\n" +
                                "Third Party Destinations. If we provide links or pointers to other websites or destinations, you should not infer or assume that Mass TV operates, controls, or is otherwise connected with these other websites or destinations. When you click on a link within the Mass TV Services, we will not warn you that you have left the Mass TV Services and are subject to the terms and conditions (including privacy policies) of another website or destination. In some cases, it may be less obvious than others that you have left the Mass TV Services and reached another website or destination. Please be careful to read the terms of use and privacy policy of any other website or destination before you provide any confidential information or engage in any transactions. You should not rely on these Terms to govern your use of another website or destination. \n" +
                                "\n" +
                                "Mass TV is not responsible for the content or practices of any website or destination other than the Mass TV Site, even if it links to the Mass TV Site and even if the website or destination is operated by a company affiliated or otherwise connected with Mass TV. By using the Mass TV Services, you acknowledge and agree that Mass TV is not responsible or liable to you for any content or other materials hosted and served from any website or destination other than the Mass TV Site. \n" +
                                "\n" +
                                "Advertisements. Mass TV takes no responsibility for advertisements or any third party material Posted on any of the Properties, nor does it take any responsibility for the products or services provided by advertisers. Any dealings you have with advertisers found while using the Mass TV Services are between you and the advertiser, and you agree that Mass TV is not liable for any loss or claim that you may have against an advertiser. \n" +
                                "\n" +
                                " \n" +
                                "\n" +
                                "8. TRADEMARKS \n" +
                                "\n" +
                                "Mass TV, the Mass TV logo, https://masstv.app/ and other Mass TV marks, graphics, logos, scripts, and sounds are trademarks of Mass TV. None of the Mass TV trademarks may be copied, downloaded, or otherwise exploited. This has been filed with USPTO as in pending approval.  \n" +
                                "\n" +
                                " \n" +
                                "\n" +
                                "9. UNSOLICITED SUBMISSIONS \n" +
                                "\n" +
                                "It is Mass TV's policy not to accept unsolicited submissions, including scripts, story lines, articles, fan fiction, characters, drawings, information, suggestions, ideas, or concepts. Mass TV's policy is to delete any such submission without reading it. Therefore, any similarity between an unsolicited submission and any elements in any Mass TV creative work, including a film, series, story, title, or concept, would be purely coincidental. \n" +
                                "\n" +
                                " \n" +
                                "\n" +
                                "10. DISCLAIMER OF WARRANTIES, LIMITATION OF LIABILITY AND INDEMNITY \n" +
                                "\n" +
                                "WHILE WE DO OUR BEST TO PROVIDE THE OPTIMAL PERFORMANCE OF THE MASS TV SERVICES, YOU AGREE THAT USE OF THE MASS TV SERVICES IS AT YOUR OWN RISK. THE MASS TV SERVICES, INCLUDING THE MASS TV SITE AND THE OTHER PROPERTIES, THE CONTENT, THE VIDEO PLAYER, USER MATERIAL, AND ANY OTHER MATERIALS CONTAINED ON OR PROVIDED THROUGH THE PROPERTIES, ARE PROVIDED \"AS IS\" AND, TO THE FULLEST EXTENT PERMITTED BY LAW, ARE PROVIDED WITHOUT WARRANTIES OF ANY KIND, EITHER EXPRESS OR IMPLIED. WITHOUT LIMITING THE FOREGOING, MASS TV DOES NOT MAKE ANY WARRANTIES OF FITNESS FOR A PARTICULAR PURPOSE, TITLE, MERCHANTABILITY, COMPLETENESS, AVAILABILITY, SECURITY, COMPATIBILITY OR NONINFRINGEMENT; OR THAT THE MASS TV SERVICES WILL BE UNINTERRUPTED, FREE OF VIRUSES AND OTHER HARMFUL COMPONENTS, ACCURATE, ERROR FREE, OR RELIABLE. \n" +
                                "\n" +
                                "IN NO EVENT SHALL MASS TV OR ITS AFFILIATES, SUCCESSORS, AND ASSIGNS, AND EACH OF THEIR RESPECTIVE INVESTORS, DIRECTORS, OFFICERS, EMPLOYEES, AGENTS, AND SUPPLIERS (INCLUDING DISTRIBUTORS AND CONTENT LICENSORS) (COLLECTIVELY, THE \"MASS TV PARTIES\"), BE LIABLE FOR ANY DIRECT, INDIRECT, PUNITIVE, INCIDENTAL, SPECIAL, CONSEQUENTIAL, OR OTHER DAMAGES, INCLUDING LOSS OF PROFITS, ARISING OUT OF OR IN ANY WAY RELATED TO THE USE OF THE MASS TV SERVICES (INCLUDING ANY INFORMATION, PRODUCTS, OR SERVICES ADVERTISED IN, OBTAINED ON, OR PROVIDED THROUGH THE PROPERTIES), WHETHER BASED IN CONTRACT, TORT, STRICT LIABILITY, OR OTHER THEORY, EVEN IF THE MASS TV PARTIES HAVE BEEN ADVISED OF THE POSSIBILITY OF DAMAGES. CERTAIN STATE LAWS DO NOT ALLOW LIMITATIONS ON IMPLIED WARRANTIES OR THE EXCLUSION OR LIMITATION OF CERTAIN DAMAGES. IF THESE LAWS APPLY TO YOU, SOME OR ALL OF THE ABOVE DISCLAIMERS, EXCLUSIONS, OR LIMITATIONS MAY NOT APPLY TO YOU. IN NO EVENT SHALL OUR TOTAL LIABILITY TO YOU FOR ALL DAMAGES, LOSSES AND CAUSES OF ACTION WHETHER IN CONTRACT, TORT (INCLUDING NEGLIGENCE) OR OTHERWISE EXCEED THE AMOUNT PAID BY YOU TO US, IF ANY, FOR ACCESSING OR PARTICIPATING IN ANY ACTIVITY RELATED TO USE OF THE MASS TV SERVICE OR $50 (WHICHEVER IS LESS). \n" +
                                "\n" +
                                "YOU AGREE TO DEFEND, INDEMNIFY, AND HOLD HARMLESS THE MASS TV PARTIES FROM AND AGAINST ANY AND ALL LIABILITIES, CLAIMS, DAMAGES, EXPENSES (INCLUDING REASONABLE ATTORNEYS' FEES AND COSTS), AND OTHER LOSSES ARISING OUT OF OR IN ANY WAY RELATED TO YOUR BREACH OR ALLEGED BREACH OF THESE TERMS OR YOUR USE OF THE MASS TV SERVICES (INCLUDING YOUR USE OF THE CONTENT). MASS TV RESERVES THE RIGHT, AT OUR OWN EXPENSE, TO EMPLOY SEPARATE COUNSEL AND ASSUME THE EXCLUSIVE DEFENSE AND CONTROL OF ANY MATTER OTHERWISE SUBJECT TO INDEMNIFICATION BY YOU. \n" +
                                "\n" +
                                " \n" +
                                "\n" +
                                "11. NOTICE AND PROCEDURE FOR CLAIMS OF COPYRIGHT INFRINGEMENT \n" +
                                "\n" +
                                "Mass TV respects the intellectual property rights of others and takes claims of copyright and trademark infringement very seriously. We license the works displayed on our website from artists and other rights holders to ensure our compliance with applicable law. If you believe any materials accessible on or from this site infringe your intellectual property, you may request removal of those materials (or access to them) by submitting at report here. \n" +
                                "\n" +
                                " \n" +
                                "\n" +
                                "12. INFORMATION SHARING \n\n" +
                                "We may share information that we collect about you to provide the Mass TV Services, including:\n" +
                                "\n" +
                                "with social networking services with your consent;\n" +
                                "with other business partners with your consent;\n" +
                                "in aggregated or other non-personally identifiable form; or\n" +
                                "when necessary to protect or defend the rights of Mass TV or our users, and when required by law or public authorities.\n\n" +
                                "13. CONTACT INFORMATION \n" +
                                "\n" +
                                "If you have any questions or concerns related to this Privacy Policy, you may send a regular mail to: \n" +
                                "\n" +
                                "ATTN: Mass TV \n" +
                                "6020 Ocean Blvd \n" +
                                "Long Beach, CA \n" +
                                "\n" +
                                "United States \n" +
                                "\n" +
                                " ").setPositiveButton("Ok", new DialogInterface.OnClickListener() {
            @Override
            public void onClick(DialogInterface dialog, int which) {

            }
        }).show();
    }


    public void showTermConditionAlert(String title, String message) {
        // Create an alert builder
        AlertDialog.Builder builder
                = new AlertDialog.Builder(this);
        // set the custom layout
        final View customLayout
                = getLayoutInflater()
                .inflate(
                        R.layout.layout_alert_term,
                        null);
        builder.setView(customLayout);

        Button btn_ok = customLayout.findViewById(R.id.tv_btn_upload);
        TextView tv_title = customLayout.findViewById(R.id.tv_result);
        TextView tv_message = customLayout.findViewById(R.id.tv_message);
        Button btn_cancel = customLayout.findViewById(R.id.btn_cancel);
        CheckBox chk_agree = customLayout.findViewById(R.id.chk_agree);

        btn_ok.setText("Accept");

        tv_title.setText(title);
        tv_message.setText(message);

        tv_message.setVisibility(View.VISIBLE);

        btn_cancel.setVisibility(View.VISIBLE);

        setMessageWithClickableLink(tv_message);

        final AlertDialog dialog
                = builder.create();
        dialog.setCancelable(false);

        ColorDrawable back = new ColorDrawable(Color.TRANSPARENT);
        InsetDrawable inset = new InsetDrawable(back, 60, 0, 60, 0);
        if (dialog.getWindow() != null) {
            dialog.getWindow().setBackgroundDrawable(inset);

        }

        btn_ok.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {


                if (checkConnection()) {
                    if (isValidate()) {

                        // uploadFile(selectedVideoPath);
                        // new UploadFile().execute();
                        //sendFileToServer(selectedVideoPath,"https://masstv.app/api/uploadvideos/");
                        if(chk_agree.isChecked()){
                            dialog.dismiss();
                            uploadFilesTask = new uploadFilesEx();
                            uploadFilesTask.execute();
                        }else {
                            showMessageOK("Please Agree Terms Before Continue!", new DialogInterface.OnClickListener() {
                                @Override
                                public void onClick(DialogInterface dialog, int which) {

                                }
                            });
                        }

                    }
                } else {
                    dialog.dismiss();
                    showMessageOK("Please check your internet connection!", new DialogInterface.OnClickListener() {
                        @Override
                        public void onClick(DialogInterface dialog, int which) {

                        }
                    });
                }


            }
        });

        btn_cancel.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                dialog.dismiss();
            }
        });


        // create and show
        // the alert dialog

        dialog.show();
    }

    private boolean isValidate() {

        if (selectedVideoPath == null) {
            showMessageOK("Please Select a Video first!", new DialogInterface.OnClickListener() {
                @Override
                public void onClick(DialogInterface dialog, int which) {

                }
            });
            return false;
        } else if (et_title.getText().toString().isEmpty()) {
            showMessageOK("Field can't be left blank!", null);
            et_title.requestFocus();
            return false;
        } else if (et_description.getText().toString().isEmpty()) {
            showMessageOK("Field can't be left blank!", null);
            et_description.requestFocus();
            return false;
        } else if (categoryList == null || categoryList.size() == 0 || categoryList.get(sp_category.getSelectedItemPosition()).getName().equalsIgnoreCase("Select Category")) {
            showMessageOK("Please Select a category", null);
            sp_category.requestFocus();
            return false;
        } else if (time < 150000) {
            showMessageOK("Video Length must be at least 2 and 1/2 minute!", null);
            return false;

        }
        return true;
    }


    public void successFullUpload(String title, String message) {
        // Create an alert builder
        new SharedPreferenceHandler(UploadContentActivity.this).setPreferenceBoolean(Setting.VIDEO_UPLOADED, true);
        AlertDialog.Builder builder
                = new AlertDialog.Builder(this);
        // set the custom layout
        final View customLayout
                = getLayoutInflater()
                .inflate(
                        R.layout.layout_alert_loaded,
                        null);
        builder.setView(customLayout);

        Button btn_ok = customLayout.findViewById(R.id.tv_btn_upload);
        TextView tv_title = customLayout.findViewById(R.id.tv_result);
        TextView tv_message = customLayout.findViewById(R.id.tv_message);
        Button btn_cancel = customLayout.findViewById(R.id.btn_cancel);

        btn_cancel.setVisibility(View.GONE);

        tv_title.setText(title);
        tv_message.setText(message);

        final AlertDialog dialog
                = builder.create();
        dialog.setCancelable(false);

        ColorDrawable back = new ColorDrawable(Color.TRANSPARENT);
        InsetDrawable inset = new InsetDrawable(back, 60, 0, 60, 0);
        if (dialog.getWindow() != null) {
            dialog.getWindow().setBackgroundDrawable(inset);
        }

        btn_ok.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                dialog.dismiss();
                onBackPressed();
            }
        });

        btn_cancel.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                dialog.dismiss();
            }
        });


        // create and show
        // the alert dialog

        dialog.show();
    }

    @Override
    public void onClick(View v) {
        switch (v.getId()) {
            case R.id.iv_back:
                if (waitScreen.getVisibility() == View.VISIBLE) {
                    showCancelPopup();
                } else if (uploadFilesTask != null && !uploadFilesTask.isCancelled()) {
                    waitScreen.setVisibility(View.VISIBLE);
                    try {

                        uploadFilesTask.cancel(true);
                    } catch (Exception e) {

                    }
                } else {
                    onBackPressed();
                }
                break;

            case R.id.fl_take_image:
                if (isAllPermissionGranted()) {
                    selectFromGallery();

                } else {
                    requestAllPermission();
                }

                break;


            case R.id.tv_upload_new_thumbnail:
                if (isAllPermissionGranted()) {

                    if (selectedVideoImageFile != null && captureImageDialog != null) {
                        captureImageDialog.showSignatureDialog(selectedVideoImageFile, this);
                    } else {
                        new AlertDialog.Builder(UploadContentActivity.this, R.style.AlertDialogStyle).setCancelable(false)
                                .setMessage("Please Upload a video first!")
                                .setPositiveButton("Ok", new DialogInterface.OnClickListener() {
                                    @Override
                                    public void onClick(DialogInterface dialog, int which) {

                                    }
                                }).show();
                    }

                }

                break;

            case R.id.fl_add_content:
                if (isValidate()) {
                  /*  if(new SharedPreferenceHandler(UploadContentActivity.this).getPrefernceBoolean(Setting.VIDEO_UPLOADED)){
                        if(isValidate()){
                            uploadFile(selectedVideoPath);
                            //  new UploadFile().execute();
                        }

                    }else {
                                }*/
                    showTermConditionAlert("Accept Term", "" +
                            "By Uploading your Video, you are agreeing to Mass TV’s Terms and Agreements and Copyright Consents.");


                }

                break;
        }
    }

    public boolean isAllPermissionGranted() {
        boolean granted = false;
        if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.M) {
            if ((checkSelfPermission(Manifest.permission.READ_EXTERNAL_STORAGE) == PackageManager.PERMISSION_GRANTED)
                    && (checkSelfPermission(Manifest.permission.WRITE_EXTERNAL_STORAGE)
                    == PackageManager.PERMISSION_GRANTED)) {

                granted = true;
            }
        }

        return granted;
    }

    /**
     * Request permission
     */
    private void requestAllPermission() {

        if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.M) {

            requestPermissions(permission, PERMISSION_CODE);
        }
    }

    /**
     * @param permission
     * @return
     * @Rehan This method for checking specified permission
     */
    private boolean hasPermission(String permission) {

        if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.M) {
            return (checkSelfPermission(permission) == PackageManager.PERMISSION_GRANTED);
        }

        return true;
    }

    @Override
    public void onRequestPermissionsResult(
            int requestCode, final String[] permissions, int[] grantResults) {

        switch (requestCode) {
            case PERMISSION_CODE:
                ArrayList<String> permissionRejected = new ArrayList<>();
                for (String perms : permissions) {
                    if (hasPermission(perms)) {

                    } else {
                        permissionRejected.add(perms);
                    }
                }

                if (permissionRejected.size() > 0) {

                    if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.M) {
                        if (shouldShowRequestPermissionRationale(permissionRejected.get(0))) {
                            showMessageOKCancel(
                                    "These permissions are mandatory for the application. Please allow access.",
                                    new DialogInterface.OnClickListener() {
                                        @Override
                                        public void onClick(DialogInterface dialog, int which) {
                                            if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.M) {

                                                requestPermissions(permissions, PERMISSION_CODE);
                                            }
                                        }
                                    });
                            return;
                        }
                    }
                } else {
                    selectFromGallery();
                }

                break;
        }
    }

    private void showMessageOKCancel(String message, DialogInterface.OnClickListener okListener) {
        new AlertDialog.Builder(UploadContentActivity.this, R.style.AlertDialogStyle)
                .setMessage(message)
                .setCancelable(false)
                .setPositiveButton("OK", okListener)
                .setNegativeButton("Cancel", null)
                .create()
                .show();
    }

    private void showMessageOK(String message, DialogInterface.OnClickListener okListener) {
        new AlertDialog.Builder(UploadContentActivity.this, R.style.AlertDialogStyle)
                .setMessage(message)
                .setCancelable(false)
                .setPositiveButton("OK", okListener)
                .create()
                .show();
    }

    private void selectFromGallery() {
        Intent intent = new Intent();
        intent.setType("video/*");
        intent.setAction(Intent.ACTION_GET_CONTENT);
        startActivityForResult(Intent.createChooser(intent, "Select Video"), REQUEST_TAKE_GALLERY_VIDEO);
    }

    private void selectImageFromGallery() {
        Intent intent = new Intent();
        intent.setType("image/*");
        intent.setAction(Intent.ACTION_GET_CONTENT);
        startActivityForResult(Intent.createChooser(intent, "Select Video"), REQUEST_TAKE_GALLERY_IMAGE);

    }

    private String getRealPathFromURI(Uri contentURI) {
        String result;
        Cursor cursor = getContentResolver().query(contentURI, null, null, null, null);
        if (cursor == null) { // Source is Dropbox or other similar local file path
            result = contentURI.getPath();
        } else {
            cursor.moveToFirst();
            int idx = cursor.getColumnIndex(MediaStore.Images.ImageColumns.DATA);
            result = cursor.getString(idx);
            cursor.close();
        }
        return result;
    }

    @Override
    public void onActivityResult(int requestCode, int resultCode, Intent data) {
        super.onActivityResult(requestCode, resultCode, data);
        if (resultCode == RESULT_OK) {
            if (requestCode == REQUEST_TAKE_GALLERY_VIDEO) {
                Uri selectedImageUri = data.getData();

                try {
                    new onlyDeleteAsync().execute();
                } catch (Exception e) {

                }
                new prepareVideo(selectedImageUri).execute();

            } else if (requestCode == REQUEST_TAKE_GALLERY_IMAGE) {
                captureImageDialog.onActivityResult(requestCode, resultCode, data);
            }
        }
    }


    public class prepareVideo extends AsyncTask<String, String, String> {

        Uri selectedImageUri;
        CustomProgressDialog customProgressDialog;

        public prepareVideo(Uri uri) {
            this.selectedImageUri = uri;
            //waitScreen.setVisibility(View.VISIBLE);
            customProgressDialog = new CustomProgressDialog(UploadContentActivity.this);
            customProgressDialog.setCancelable(false);

        }

        @Override
        protected void onPreExecute() {
            super.onPreExecute();
            customProgressDialog.show();
        }

        @Override
        protected String doInBackground(String... strings) {

            selectedVideoPath = createCopyAndReturnRealPath(UploadContentActivity.this, selectedImageUri);

            return selectedVideoPath;
        }

        @Nullable
        public String createCopyAndReturnRealPath(
                @NonNull Context context, @NonNull Uri uri) {
            final ContentResolver contentResolver = context.getContentResolver();
            if (contentResolver == null)
                return null;
            File directory = new File(context.getCacheDir(), "media/");
            if (!directory.exists()) {
                try {
                    directory.mkdir();
                } catch (Exception e) {
                }
            }
            // Create file path inside app's data dir
            String name = System.currentTimeMillis() + ".mp4";
            //  newfile = new File(directory, filePath);
            File file = new File(directory, name);
            filePath = file.getAbsolutePath();
            try {
                InputStream inputStream = contentResolver.openInputStream(uri);
                if (inputStream == null)
                    return null;
                OutputStream outputStream = new FileOutputStream(file);
                byte[] buf = new byte[1024];
                int len;
                while ((len = inputStream.read(buf)) > 0)
                    outputStream.write(buf, 0, len);
                outputStream.close();
                inputStream.close();
            } catch (IOException ignore) {
                return null;
            }
            return file.getAbsolutePath();
        }

        @Override
        protected void onPostExecute(String s) {

            super.onPostExecute(s);
            waitScreen.setVisibility(View.GONE);


          /*  final BitmapFactory.Options options = new BitmapFactory.Options();
            options.inJustDecodeBounds = true;
            options.inSampleSize = 2;
            options.inJustDecodeBounds = false;
            options.inTempStorage = new byte[16 * 1024];

            Bitmap bmp = BitmapFactory.decodeFile(selectedVideoPath,options);
            bitmap = Bitmap.createScaledBitmap(bmp, 960, 730, false);*/

            try {
                bitmap = createVideoThumbNail(selectedVideoPath);
                iv_upload_video.setImageBitmap(bitmap);
                selectedVideoImageFile = bitmapToFile(UploadContentActivity.this, bitmap, "temporary.png");
                time = DateCalculation.getDurationFile(UploadContentActivity.this, new File(selectedVideoPath));
                videoDuration = DateCalculation.getDurationBreakdown(time);

            } catch (Exception e) {

                selectedVideoPath = null;

                new AlertDialog.Builder(UploadContentActivity.this, R.style.AlertDialogStyle).
                        setMessage("Unable to create thumbnail! \nPlease select file again").setPositiveButton("Ok", new DialogInterface.OnClickListener() {
                    @Override
                    public void onClick(DialogInterface dialog, int which) {

                    }
                }).show();
            }

            try {
                customProgressDialog.dismiss();
            } catch (Exception e) {

            }

        }
    }


    public Bitmap getResizedBitmap(Bitmap image, int maxSize) {
        int width = image.getWidth();
        int height = image.getHeight();

        float bitmapRatio = (float) width / (float) height;
        if (bitmapRatio > 1) {
            width = maxSize;
            height = (int) (width / bitmapRatio);
        } else {
            height = maxSize;
            width = (int) (height * bitmapRatio);
        }
        return Bitmap.createScaledBitmap(image, width, height, true);


    }


    public static File bitmapToFile(Context context, Bitmap bitmap, String fileNameToSave) {
        // File name like "image.png"
        //create a file to write bitmap data
        String filePath = context.getApplicationInfo().dataDir + File.separator
                + "temporary" + System.currentTimeMillis() + ".png";


        File file = null;
        try {
            file = new File(filePath);
            file.createNewFile();

//Convert bitmap to byte array
            ByteArrayOutputStream bos = new ByteArrayOutputStream();
            bitmap.compress(Bitmap.CompressFormat.PNG, 0, bos); // YOU can also save it in JPEG
            byte[] bitmapdata = bos.toByteArray();

//write the bytes in file
            FileOutputStream fos = new FileOutputStream(file);
            fos.write(bitmapdata);
            fos.flush();
            fos.close();
            return file;
        } catch (Exception e) {
            e.printStackTrace();
            return file; // it will return null
        }
    }


    @Nullable
    public String createCopyAndReturnRealPath(
            @NonNull Context context, @NonNull Uri uri) {
        final ContentResolver contentResolver = context.getContentResolver();
        if (contentResolver == null)
            return null;

        // Create file path inside app's data dir
        filePath = context.getApplicationInfo().dataDir + File.separator
                + System.currentTimeMillis() + ".mp4";

        File file = new File(filePath);
        try {
            InputStream inputStream = contentResolver.openInputStream(uri);
            if (inputStream == null)
                return null;

            OutputStream outputStream = new FileOutputStream(file);
            byte[] buf = new byte[1024];
            int len;
            while ((len = inputStream.read(buf)) > 0)
                outputStream.write(buf, 0, len);

            outputStream.close();
            inputStream.close();
        } catch (IOException ignore) {
            return null;
        }

        return file.getAbsolutePath();
    }

    // UPDATED!
    public String getPath(Uri uri) {
        String[] projection = {MediaStore.Video.Media.DATA};
        Cursor cursor = getContentResolver().query(uri, projection, null, null, null);
        if (cursor != null) {
            // HERE YOU WILL GET A NULLPOINTER IF CURSOR IS NULL
            // THIS CAN BE, IF YOU USED OI FILE MANAGER FOR PICKING THE MEDIA
            int column_index = cursor
                    .getColumnIndexOrThrow(MediaStore.Video.Media.DATA);
            cursor.moveToFirst();
            return cursor.getString(column_index);
        } else
            return null;
    }


    public Bitmap createVideoThumbNail(String path) {
        return ThumbnailUtils.createVideoThumbnail(path, MediaStore.Video.Thumbnails.FULL_SCREEN_KIND);

      /*  BitmapFactory.Options options = new BitmapFactory.Options();
        options.inPreferredConfig = Bitmap.Config.RGB_565;
        Bitmap bitmap = BitmapFactory.decodeStream(stream, null, options);*/


    }

    @Override
    public void onViewClicked(int position) {

    }

    private void uploadFile(String filePath) {

        tv_progress.setVisibility(View.VISIBLE);
        // Map is used to multipart the file using okhttp3.RequestBody
        fl_add_content.setEnabled(false);
        ProfileData profileData = new SharedPreferenceHandler(this).getProfileInfo();
        File file = new File(filePath);
        waitScreen.setVisibility(View.VISIBLE);
        fl_take_image.setEnabled(false);
        // Parsing any Media type file
        ProgressRequestBody fileBody = new ProgressRequestBody(file, "", this);

        //RequestBody fileBody = RequestBody.create(MediaType.parse("*/*"), file);
        //RequestBody requestBody = RequestBody.create(MediaType.parse("*image/*"), file);
        MultipartBody.Part fileToUpload = MultipartBody.Part.createFormData("source", file.getAbsolutePath(), fileBody);
        RequestBody user_id = RequestBody.create(MediaType.parse("text/plain"), "" + profileData.getId());
        RequestBody title = RequestBody.create(MediaType.parse("text/plain"), et_title.getText().toString());
        RequestBody duration = RequestBody.create(MediaType.parse("text/plain"), videoDuration);
        RequestBody subtitle = RequestBody.create(MediaType.parse("text/plain"), et_description.getText().toString());
        RequestBody tags = RequestBody.create(MediaType.parse("text/plain"), et_description.getText().toString());
        if (!et_tags.getText().toString().isEmpty()) {
            tags = RequestBody.create(MediaType.parse("text/plain"), et_tags.getText().toString());
        }
        RequestBody thumb = null;
        MultipartBody.Part ThumbToUpload = null;

        RequestBody category = RequestBody.create(MediaType.parse("text/plain"), categoryList.get(sp_category.getSelectedItemPosition()).getName());
        APIInterface apiInterface = APIUploadClient.getClient().create(APIInterface.class);

        callUpload = apiInterface.uploadFile(user_id,
                title, subtitle, ThumbToUpload, category, duration, tags, fileToUpload);
        callUpload.enqueue(new Callback<VideoUploadResponse>() {
            @Override
            public void onResponse(Call<VideoUploadResponse> call, Response<VideoUploadResponse> response) {
                waitScreen.setVisibility(View.GONE);
                if (response.code() == 200) {
                    successFullUpload("Uploaded", "Your content has been successfully uploaded! ");
                } else {
                    showMessageOK("SomeThing went wrong! please try again", new DialogInterface.OnClickListener() {
                        @Override
                        public void onClick(DialogInterface dialog, int which) {
                            fl_take_image.setEnabled(true);
                            iv_upload_video.setEnabled(true);
                            fl_add_content.setEnabled(true);
                        }
                    });
                }
            }

            @Override
            public void onFailure(Call<VideoUploadResponse> call, Throwable t) {
                waitScreen.setVisibility(View.GONE);
                showMessageOKCancel("" + t.getMessage(), new DialogInterface.OnClickListener() {
                    @Override
                    public void onClick(DialogInterface dialog, int which) {
                        dialog.dismiss();
                        waitScreen.setVisibility(View.GONE);
                        fl_take_image.setEnabled(true);
                        fl_add_content.setEnabled(true);
                        iv_upload_video.setEnabled(true);

                    }
                });

            }
        });

    }

    @Override
    protected void onPause() {
        super.onPause();
        try {
            MassTvApplication.getInstance().setUnRegister();
        } catch (Exception e) {

        }
    }

    @Override
    protected void onResume() {
        super.onResume();
        MassTvApplication.getInstance().setConnectivityListener(this);
        new CompleteProfile(UploadContentActivity.this, new CompleteProfile.profileAlertListener() {
            @Override
            public void isAlertShowing(boolean isShowing) {

            }
        });
    }

    @Override
    protected void onDestroy() {

        super.onDestroy();

    }

    @Override
    public void onBackPressed() {
        //super.onBackPressed();
        if (waitScreen.getVisibility() == View.VISIBLE) {
            showCancelPopup();
        } else {
            if(HomeDraweerStreamingActivity.isAliveActivity){
                finish();
            }else {
                startActivity(new Intent(this, HomeDraweerStreamingActivity.class).setFlags(Intent.FLAG_ACTIVITY_NO_HISTORY));
                finish();
            }

        }
    }

    private void showCancelPopup() {
        new AlertDialog.Builder(this, R.style.AlertDialogStyle).setCancelable(false).setMessage("Uploading will be cancelled! \nDo you want to exit?")
                .setPositiveButton("Ok", new DialogInterface.OnClickListener() {
                    @Override
                    public void onClick(DialogInterface dialog, int which) {
                        if (callUpload != null) {
                            callUpload.cancel();
                        }
                        startActivity(new Intent(UploadContentActivity.this, HomeDraweerStreamingActivity.class));
                        finish();
                    }
                }).setNegativeButton("Cancel", new DialogInterface.OnClickListener() {
            @Override
            public void onClick(DialogInterface dialog, int which) {
            }
        }).show();
    }

    @Override
    public void onProgressUpdate(final int percentage) {
        if (tv_progress != null) {
            if (startShowingPercentage(percentage)) {
                new Handler().postDelayed(new Runnable() {
                    @Override
                    public void run() {
                        tv_progress.setText("" + percentage + "%");
                    }
                }, 500);
            }
        }
    }

    private boolean startShowingPercentage(int percentage) {
        if (!secondTime && percentage > 50) {
            secondTime = true;
        }
        if (!isStartUploading && secondTime && percentage >= 0 && percentage < 50) {
            isStartUploading = true;
        }
        return isStartUploading;
    }

    @Override
    public void onError() {
    }

    @Override
    public void onFinish() {
    }


    public class uploadFilesEx extends AsyncTask<Integer, Integer, String> {
        NotificationManager mNotifyManager;
        NotificationCompat.Builder mBuilder;


        @Override
        protected String doInBackground(Integer... params) {

   //         return sendFileToServer(selectedVideoPath, "https://masstv.app/api/uploadvideos");
   //         return sendFileToServer(selectedVideoPath, "https://masstv.app/api/uploadvideos");
            return sendFileToServer(selectedVideoPath, "http://masstv.technodroidz.xyz/api/uploadvideos");
            // return response;
        }


        @Override
        protected void onPostExecute(String result) {
            // progressBar.setVisibility(View.GONE);
            waitScreen.setVisibility(View.GONE);

            if (isTaskCancel) {
                new deleteFileAsync().execute();

            } else {
                try {
                    if (result != null && result.equalsIgnoreCase("true")) {
                        successFullUpload("Uploaded", "Your content has been successfully uploaded! ");
                    } else {
//                        Log.d("Error",result);
                        showMessageOK("SomeThing went wrong! please try again", new DialogInterface.OnClickListener() {
                            @Override
                            public void onClick(DialogInterface dialog, int which) {
                                tv_progress.setText("0%");
                                fl_take_image.setEnabled(true);
                                iv_upload_video.setEnabled(true);
                                fl_add_content.setEnabled(true);
                                try {
                                    CaptureImageDialog.clearThumbnailFile();
                                } catch (Exception e) {

                                }
                                new deleteFileAsync().execute();

                            }
                        });
                    }
                } catch (Exception e) {

                }
            }


            // txt.setText(result);
          /*  mBuilder.setContentText("Upload complete");
            Toast.makeText(UploadContentActivity.this, "Upload Complete", Toast.LENGTH_SHORT).show();
            // Removes the progress bar
            mBuilder.setProgress(0, 0, false);
            mNotifyManager.notify(0, mBuilder.build());*/
        }


        @Override
        protected void onPreExecute() {
            waitScreen.setVisibility(View.VISIBLE);
            tv_progress.setVisibility(View.VISIBLE);
           /* mNotifyManager = (NotificationManager) getSystemService(Context.NOTIFICATION_SERVICE);
            mBuilder = new NotificationCompat.Builder(UploadContentActivity.this);
            mBuilder.setContentTitle("Uploading")
                    .setContentText("Upload in progress")
                    .setSmallIcon(R.drawable.ic_certificate_box);
            Toast.makeText(UploadContentActivity.this, "Uploading files... The upload progress is on notification bar.", Toast.LENGTH_LONG).show();
 */
        }

        @Override
        protected void onProgressUpdate(Integer... values) {
            //  txt.setText("Running..." + values[0]);


            if ((values[0]) % 25 == 0) {
                mBuilder.setProgress(100, values[0], false);
                // Displays the progress bar on notification
                mNotifyManager.notify(0, mBuilder.build());
            }
        }
    }

    public static String getFileExt(String fileName) {
        return fileName.substring(fileName.lastIndexOf(".") + 1, fileName.length());
    }

    public String sendFileToServer(String filename, String targetUrl) {
        String response = "error";

        HttpURLConnection connection = null;
        DataOutputStream outputStream = null;
        // DataInputStream inputStream = null;

        String pathToOurFile = selectedVideoPath;
        String pathToOurImageFile = selectedVideoImageFile.getAbsolutePath();
        String urlServer = targetUrl;
        String lineEnd = "\r\n";
        String twoHyphens = "--";
        String boundary = "*****";
        DateFormat df = new SimpleDateFormat("yyyy_MM_dd_HH:mm:ss");

        int bytesRead, bytesAvailable, bufferSize;
        byte[] buffer;
        int maxBufferSize = 1 * 1024;
        try {
            FileInputStream fileInputStream = new FileInputStream(new File(
                    pathToOurFile));

            FileInputStream fileImageInputStream = new FileInputStream(new File(
                    pathToOurImageFile));

            URL url = new URL(urlServer);
            connection = (HttpURLConnection) url.openConnection();

            // Allow Inputs & Outputs
            connection.setDoInput(true);
            connection.setDoOutput(true);
            connection.setUseCaches(false);
            connection.setConnectTimeout(300000);
            connection.setReadTimeout(300000);
            connection.setChunkedStreamingMode(1024);
            // Enable POST method
            connection.setRequestMethod("POST");

            connection.setRequestProperty("Connection", "Keep-Alive");
            connection.setRequestProperty("Content-Type",
                    "multipart/form-data;boundary=" + boundary);


            outputStream = new DataOutputStream(connection.getOutputStream());
            outputStream.writeBytes(twoHyphens + boundary + lineEnd);

            outputStream.writeBytes("Content-Disposition: form-data; name=\"user_id\"" + lineEnd);
            outputStream.writeBytes(lineEnd);
            outputStream.writeBytes("" + new SharedPreferenceHandler(UploadContentActivity.this).getProfileInfo().getId());
            outputStream.writeBytes(lineEnd);
            outputStream.writeBytes(twoHyphens + boundary + lineEnd);


            outputStream.writeBytes("Content-Disposition: form-data; name=\"category\"" + lineEnd);
            outputStream.writeBytes(lineEnd);
            outputStream.writeBytes(sp_category.getSelectedItem().toString());
            outputStream.writeBytes(lineEnd);
            outputStream.writeBytes(twoHyphens + boundary + lineEnd);


            outputStream.writeBytes("Content-Disposition: form-data; name=\"title\"" + lineEnd);
            outputStream.writeBytes(lineEnd);
            outputStream.writeBytes(et_title.getText().toString());
            outputStream.writeBytes(lineEnd);
            outputStream.writeBytes(twoHyphens + boundary + lineEnd);


            outputStream.writeBytes("Content-Disposition: form-data; name=\"subtitle\"" + lineEnd);
            outputStream.writeBytes(lineEnd);
            outputStream.writeBytes(et_description.getText().toString());
            outputStream.writeBytes(lineEnd);
            outputStream.writeBytes(twoHyphens + boundary + lineEnd);

            outputStream.writeBytes("Content-Disposition: form-data; name=\"duration\"" + lineEnd);
            outputStream.writeBytes(lineEnd);
            outputStream.writeBytes(videoDuration);
            outputStream.writeBytes(lineEnd);
            outputStream.writeBytes(twoHyphens + boundary + lineEnd);

            if (et_tags != null) {
                outputStream.writeBytes("Content-Disposition: form-data; name=\"tags\"" + lineEnd);
                outputStream.writeBytes(lineEnd);
                outputStream.writeBytes(et_tags.getText().toString());
                outputStream.writeBytes(lineEnd);
                outputStream.writeBytes(twoHyphens + boundary + lineEnd);
            }


            String connstrImage = null;
            connstrImage = "Content-Disposition: form-data; name=\"thumb\";filename=\""
                    + pathToOurImageFile + "\"" + lineEnd;
            Log.i("Connstr", connstrImage);

            outputStream.writeBytes(connstrImage);
            outputStream.writeBytes(lineEnd);

            bytesAvailable = fileImageInputStream.available();
            bufferSize = Math.min(bytesAvailable, maxBufferSize);
            buffer = new byte[bufferSize];

            // Read file
            bytesRead = fileImageInputStream.read(buffer, 0, bufferSize);

            try {
                while (bytesRead > 0) {
                    if (uploadFilesTask.isCancelled()) {
                        isTaskCancel = true;
                        try {
                            Thread.sleep(200);
                        } catch (Exception e) {

                        }

                        break;
                    }

                    try {
                        outputStream.write(buffer, 0, bufferSize);
                    } catch (OutOfMemoryError e) {
                        e.printStackTrace();
                        response = "outofmemoryerror";
                        return response;
                    }
                    bytesAvailable = fileImageInputStream.available();
                    bufferSize = Math.min(bytesAvailable, maxBufferSize);
                    bytesRead = fileImageInputStream.read(buffer, 0, bufferSize);
                }
            } catch (Exception e) {
                e.printStackTrace();
                response = "error";

            }
            outputStream.writeBytes(lineEnd);
            outputStream.writeBytes(twoHyphens + boundary + lineEnd);

            String connstr = null;
            connstr = "Content-Disposition: form-data; name=\"source\";filename=\""
                    + pathToOurFile + "\"" + lineEnd;
            Log.i("Connstr", connstr);

            outputStream.writeBytes(connstr);
            outputStream.writeBytes(lineEnd);

            bytesAvailable = fileInputStream.available();
            bufferSize = Math.min(bytesAvailable, maxBufferSize);
            buffer = new byte[bufferSize];

            // Read file
            long uploaded = 0;
            long fileLength = new File(selectedVideoPath).length();
            bytesRead = fileInputStream.read(buffer, 0, bufferSize);
            Log.e("Image length", bytesAvailable + "");
            try {
                Handler handler = new Handler(Looper.getMainLooper());
                while (bytesRead > 0) {
                    try {

                        if (uploadFilesTask.isCancelled()) {
                            isTaskCancel = true;
                            try {
                                Thread.sleep(200);
                            } catch (Exception e) {

                            }
                            break;
                        }

                        handler.post(new ProgressUpdater(uploaded, fileLength));

                        uploaded += bytesRead;

                        outputStream.write(buffer, 0, bufferSize);
                    } catch (OutOfMemoryError e) {
                        e.printStackTrace();
                        response = "outofmemoryerror";
                        return response;
                    }
                    bytesAvailable = fileInputStream.available();
                    bufferSize = Math.min(bytesAvailable, maxBufferSize);
                    bytesRead = fileInputStream.read(buffer, 0, bufferSize);
                }
            } catch (Exception e) {
                e.printStackTrace();
                response = "error";
                return response;
            }

            outputStream.writeBytes(lineEnd);
            outputStream.writeBytes(twoHyphens + boundary + twoHyphens
                    + lineEnd);

            // Responses from the server (code and message)
            int serverResponseCode = connection.getResponseCode();
            String serverResponseMessage = connection.getResponseMessage();
            Log.i("Server Response Code ", "" + serverResponseCode);
            Log.i("Server Response Message", serverResponseMessage);

            if (serverResponseCode == 200) {
                response = "true";
            }

            String CDate = null;
            Date serverTime = new Date(connection.getDate());
            try {
                CDate = df.format(serverTime);
            } catch (Exception e) {
                e.printStackTrace();

            }


            filename = CDate
                    + filename.substring(filename.lastIndexOf("."),
                    filename.length());


            fileInputStream.close();
            outputStream.flush();
            outputStream.close();
            outputStream = null;
        } catch (Exception ex) {
            // Exception handling
            response = "error";

            ex.printStackTrace();
        }
        return response;
    }

    private class ProgressUpdater implements Runnable {
        private long mUploaded;
        private long mTotal;

        public ProgressUpdater(long uploaded, long total) {
            mUploaded = uploaded;
            mTotal = total;
        }

        @Override
        public void run() {
            try {
                tv_progress.setText("" + (int) ((100 * mUploaded) / mTotal) + "%");
            } catch (Exception e) {

            }
        }
    }

    /**
     * Check internet connection Manually
     */
    private boolean checkConnection() {
        return ConnectivityReceiver.isConnected();
    }


    public class deleteFileAsync extends AsyncTask<String, String, Boolean> {

        @Override
        protected Boolean doInBackground(String... strings) {
            try {

                File directory = new File(getCacheDir(), "media/");
                if (directory.exists()) {
                    deleteDir(directory);
                    Toast.makeText(UploadContentActivity.this, "Deleted", Toast.LENGTH_SHORT).show();
                }
            } catch (Exception e) {
                e.printStackTrace();
            }
            return true;
        }

        public boolean deleteDir(File dir) {
            if (dir.isDirectory()) {
                String[] children = dir.list();
                for (int i = 0; i < children.length; i++) {
                    boolean success = deleteDir(new File(dir, children[i]));
                    if (!success) {
                        return false;
                    }
                }
            }

            // The directory is now empty so delete it
            return dir.delete();
        }

        @Override
        protected void onPreExecute() {
            super.onPreExecute();
            waitScreen.setVisibility(View.VISIBLE);
        }

        @Override
        protected void onPostExecute(Boolean aBoolean) {
            super.onPostExecute(aBoolean);
            waitScreen.setVisibility(View.GONE);
            onBackPressed();

        }
    }


    public class onlyDeleteAsync extends AsyncTask<String, String, Boolean> {

        @Override
        protected Boolean doInBackground(String... strings) {
            try {

                File directory = new File(getCacheDir(), "media/");
                if (directory.exists()) {
                    deleteDir(directory);
                    Toast.makeText(UploadContentActivity.this, "Deleted", Toast.LENGTH_SHORT).show();
                }
            } catch (Exception e) {
                e.printStackTrace();
            }
            return true;
        }

        public boolean deleteDir(File dir) {
            if (dir.isDirectory()) {
                String[] children = dir.list();
                for (int i = 0; i < children.length; i++) {
                    boolean success = deleteDir(new File(dir, children[i]));
                    if (!success) {
                        return false;
                    }
                }
            }

            // The directory is now empty so delete it
            return dir.delete();
        }

        @Override
        protected void onPreExecute() {
            super.onPreExecute();
            waitScreen.setVisibility(View.VISIBLE);
        }

        @Override
        protected void onPostExecute(Boolean aBoolean) {
            super.onPostExecute(aBoolean);
            waitScreen.setVisibility(View.GONE);

        }
    }


}