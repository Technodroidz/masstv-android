package com.example.livestreamingapp.utils;

import android.content.Context;
import androidx.annotation.NonNull;
import androidx.recyclerview.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.TextView;

import com.example.livestreamingapp.R;

import java.util.List;

public class AdapterSpinner extends RecyclerView.Adapter<AdapterSpinner.SpinnerHolder> {
    private  String lastSelected;
    private Context context;

    private List<String> list;

    private IItemActionListner listener;

    public AdapterSpinner(Context context, List<String> list,String lastSelected,  IItemActionListner listener){
        this.list = list;
        this.context = context;
        this.lastSelected = lastSelected;
        this.listener = listener;
    }

    @NonNull
    @Override
    public SpinnerHolder onCreateViewHolder(@NonNull ViewGroup viewGroup, int i) {
        View view = LayoutInflater.from(context).inflate(R.layout.item_spinner,viewGroup,false);
        return new SpinnerHolder(view);
    }

    @Override
    public void onBindViewHolder(@NonNull SpinnerHolder spinnerHolder, final int i) {

       // spinnerHolder.iv_check.setVisibility(View.GONE);
        spinnerHolder.tv_value.setText(list.get(i));
       /* if(!lastSelected.isEmpty() && list.get(i).equalsIgnoreCase(lastSelected)){
            spinnerHolder.iv_check.setVisibility(View.VISIBLE);
        }*/



    }

    @Override
    public int getItemCount() {
        return list.size();
    }

    public class SpinnerHolder extends RecyclerView.ViewHolder{
        TextView tv_value;
        LinearLayout ln_value;
        ImageView iv_check;
        public SpinnerHolder(@NonNull View itemView) {
            super(itemView);
            tv_value = itemView.findViewById(R.id.tv_value);
            ln_value = itemView.findViewById(R.id.ln_value);

            ln_value.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View view) {
                    listener.onItemClickListner(getLayoutPosition());
                }
            });

        }
    }
}
