package com.example.livestreamingapp.utils;

import android.app.Activity;
import android.content.Context;
import android.content.Intent;
import android.graphics.Color;
import android.graphics.PorterDuff;
import android.graphics.drawable.Drawable;
import android.os.Bundle;
import android.view.View;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.TextView;

import androidx.core.graphics.drawable.DrawableCompat;
import androidx.fragment.app.Fragment;
import androidx.fragment.app.FragmentManager;
import androidx.fragment.app.FragmentTransaction;

import com.example.livestreamingapp.MassTvActivity;
import com.example.livestreamingapp.R;
import com.example.livestreamingapp.home.ui.HomeDraweerStreamingActivity;
import com.example.livestreamingapp.model.TrueLiveVideoResponse;
import com.example.livestreamingapp.notification.NotificationActivity;
import com.example.livestreamingapp.ondemand.OnDemandCategoryActivity;
import com.example.livestreamingapp.profile.ProfileActivity;
import com.example.livestreamingapp.upload.UploadContentActivity;
import com.example.livestreamingapp.video.TureLiveVideoPlayer;

import java.util.ArrayList;
import java.util.Arrays;

public class BottomNavigationHelper implements View.OnClickListener {
    private View rootView;
    private  Context context;
    private TextView tv_home;
    private TextView tv_demand;
    private TextView tv_upload;
//    private TextView tv_notification;
    private TextView tv_streaming;
    private TextView tv_more;

    private ImageView iv_home;
    private ImageView iv_demand;
    private ImageView iv_upload;
//    private ImageView iv_notification;
    private ImageView iv_streaming;
    private ImageView iv_more;

    private LinearLayout lin_home;
    private LinearLayout lin_demand;
    private LinearLayout lin_upload;
    private LinearLayout lin_notification;
    private LinearLayout lin_more;
    private View notification_badge;
    private SharedPreferenceHandler sharedPreferenceHandler;

    public BottomNavigationHelper(Context context){
        this.context = context;
         rootView = ((Activity)context).getWindow().getDecorView().findViewById(android.R.id.content);
         sharedPreferenceHandler = new SharedPreferenceHandler(context);
         initViews(rootView);
    }

    private void initViews(View rootView) {
        tv_home = rootView.findViewById(R.id.tv_home);
        tv_demand = rootView.findViewById(R.id.tv_demand);
        tv_upload = rootView.findViewById(R.id.tv_upload);
//        tv_notification = rootView.findViewById(R.id.tv_notification);
        tv_streaming = rootView.findViewById(R.id.tv_streaming);
        tv_more = rootView.findViewById(R.id.tv_more);
        notification_badge = rootView.findViewById(R.id.notification_badge);
        notification_badge.setVisibility(View.GONE);

        int count = sharedPreferenceHandler.getUnReadCount();
        setNotification(count);

        iv_home = rootView.findViewById(R.id.iv_home);
        iv_demand = rootView.findViewById(R.id.iv_demand);
        iv_upload = rootView.findViewById(R.id.iv_upload);
//        iv_notification = rootView.findViewById(R.id.iv_notification);
        iv_streaming = rootView.findViewById(R.id.iv_streaming);
        iv_more = rootView.findViewById(R.id.iv_more);

        lin_home = rootView.findViewById(R.id.lin_home);
        lin_demand = rootView.findViewById(R.id.lin_demand);
        lin_upload = rootView.findViewById(R.id.lin_upload);
        lin_notification = rootView.findViewById(R.id.lin_notification);
        lin_more = rootView.findViewById(R.id.lin_more);
        setSelection();
        setListener();

    }

    private void setListener() {
        lin_home.setOnClickListener(this);
        lin_demand.setOnClickListener(this);
        lin_upload.setOnClickListener(this);
        lin_notification.setOnClickListener(this);
        lin_more.setOnClickListener(this);
    }

    private void setSelection() {
        if(context instanceof HomeDraweerStreamingActivity){
            setTextViewColor(tv_home,iv_home);
        }else if(context instanceof OnDemandCategoryActivity){
            setTextViewColor(tv_demand,iv_demand);
        }else if(context instanceof UploadContentActivity){
            setTextViewColor(tv_upload,iv_upload);
//        }else if(context instanceof NotificationActivity){
//            setTextViewColor(tv_notification,iv_notification);
        }else if(context instanceof TureLiveVideoPlayer){
            setTextViewColor(tv_streaming,iv_streaming);
        }else if(context instanceof ProfileActivity){
            setTextViewColor(tv_more,iv_more);
        }
    }

   public void setTextViewColor(TextView textView, ImageView imageView){
        tv_home.setTextColor(context.getResources().getColor(R.color.white_semi));
        tv_demand.setTextColor(context.getResources().getColor(R.color.white_semi));
        tv_upload.setTextColor(context.getResources().getColor(R.color.white_semi));
//        tv_notification.setTextColor(context.getResources().getColor(R.color.white_semi));
        tv_streaming.setTextColor(context.getResources().getColor(R.color.white_semi));
        tv_more.setTextColor(context.getResources().getColor(R.color.white_semi));

       iv_home.setColorFilter(context.getResources().getColor(R.color.white));
       iv_demand.setColorFilter(context.getResources().getColor(R.color.white));
       iv_upload.setColorFilter(context.getResources().getColor(R.color.white));
//       iv_notification.setColorFilter(context.getResources().getColor(R.color.white));
       iv_streaming.setColorFilter(context.getResources().getColor(R.color.white));
       iv_more.setColorFilter(context.getResources().getColor(R.color.white));

       imageView.setColorFilter(context.getResources().getColor(R.color.green));
       textView.setTextColor(context.getResources().getColor(R.color.green));


   }


    @Override
    public void onClick(View v) {
        switch (v.getId()){
            case R.id.lin_home:
                if(context instanceof HomeDraweerStreamingActivity){

                }else {
                 /*   Intent intent = new Intent(context,HomeDraweerStreamingActivity.class)
                            .setFlags(Intent.FLAG_ACTIVITY_NO_ANIMATION);
                    context.startActivity(intent);*/
                    ((Activity)context).finish();
                }

                break;

            case R.id.lin_demand:
                if(context instanceof OnDemandCategoryActivity){

                }else {
                    Intent intent = new Intent(context,OnDemandCategoryActivity.class)
                            .setFlags(Intent.FLAG_ACTIVITY_NO_ANIMATION);
                    context.startActivity(intent);

                   /* if(!(context instanceof HomeDraweerStreamingActivity)){
                        ((Activity)context).finish();
                    }*/

                }
                break;

            case R.id.lin_upload:
                if(context instanceof UploadContentActivity){

                }else {
                    Intent intent = new Intent(context,UploadContentActivity.class);
                    context.startActivity(intent);
                   /* if(!(context instanceof HomeDraweerStreamingActivity)){
                        ((Activity)context).finish();
                    }*/
                }
                break;

            case R.id.lin_notification:
//                if(context instanceof NotificationActivity){
                if(context instanceof TureLiveVideoPlayer){

                }else {
//                    Intent intent = new Intent(context,NotificationActivity.class).setFlags(Intent.FLAG_ACTIVITY_NO_ANIMATION);
                    Intent intent = new Intent(context,TureLiveVideoPlayer.class).setFlags(Intent.FLAG_ACTIVITY_NO_ANIMATION);
                    context.startActivity(intent);
                   /* if(!(context instanceof HomeDraweerStreamingActivity)){
                        ((Activity)context).finish();
                    }*/
                }
                break;

            case R.id.lin_more:
//                ArrayList<String> list = new ArrayList<>(Arrays.asList("True Live","Watchlist","Profile","Contact Us","FAQ","EXIT"));
                ArrayList<String> list = new ArrayList<>(Arrays.asList("Notification","Watchlist","Profile","Contact Us","FAQ","EXIT"));
                Fragment fragment = new BottomSpinnerFragment();
                FragmentManager fragmentManager = ((MassTvActivity)context).getSupportFragmentManager();
                FragmentTransaction transaction = fragmentManager.beginTransaction();
                transaction.setCustomAnimations(R.anim.slide_up, 0, R.anim.slide_down, R.anim.slide_down);
                transaction.add(R.id.ln_container, fragment);
                Bundle bundle = new Bundle();
                bundle.putStringArrayList("list", (ArrayList<String>) list);
                fragment.setArguments(bundle);
                transaction.addToBackStack(fragment.getClass().getName());
                transaction.commit();

                break;
        }
    }

    public void setNotification(int count) {
        if(count>0 && notification_badge!=null){
            notification_badge.setVisibility(View.VISIBLE);
        }else {
            notification_badge.setVisibility(View.GONE);
        }
    }
}
