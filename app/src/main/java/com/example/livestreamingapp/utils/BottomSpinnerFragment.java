package com.example.livestreamingapp.utils;

import android.app.Activity;
import android.content.Intent;
import android.os.Bundle;
import android.util.Log;
import android.view.DragEvent;
import android.view.LayoutInflater;
import android.view.MotionEvent;
import android.view.View;
import android.view.ViewGroup;
import android.widget.LinearLayout;
import android.widget.TextView;
import android.widget.Toast;

import androidx.annotation.NonNull;
import androidx.annotation.Nullable;
import androidx.appcompat.app.AppCompatActivity;
import androidx.fragment.app.Fragment;
import androidx.recyclerview.widget.LinearLayoutManager;
import androidx.recyclerview.widget.RecyclerView;


import com.example.livestreamingapp.MassTvActivity;
import com.example.livestreamingapp.R;
import com.example.livestreamingapp.contact.ContactUsActivity;
import com.example.livestreamingapp.contact.QueryPagerActivity;
import com.example.livestreamingapp.faq.ActivityTermCondition;
import com.example.livestreamingapp.home.ui.HomeDraweerStreamingActivity;
import com.example.livestreamingapp.login.MultipleLoginSelectionActivity;
import com.example.livestreamingapp.notification.NotificationActivity;
import com.example.livestreamingapp.ondemand.VideoListActivity;
import com.example.livestreamingapp.profile.ProfileActivity;
import com.example.livestreamingapp.video.TureLiveVideoPlayer;
import com.google.firebase.messaging.FirebaseMessaging;

import java.util.ArrayList;
import java.util.List;

import static android.view.MotionEvent.ACTION_DOWN;
import static android.view.MotionEvent.ACTION_MOVE;
import static android.view.MotionEvent.ACTION_UP;
import static com.example.livestreamingapp.utils.Constants.CATEGORY_TYPE;

public class BottomSpinnerFragment extends Fragment {


    LinearLayout ln_close;
    RecyclerView rv_spinner;
    TextView tvName;
    private LinearLayout ln_slide;

    AdapterSpinner adapterSpinner;
    List<String> spinnerList;
    private SharedPreferenceHandler sharedPreferenceHandler;

    @Override
    public void onDestroyView() {
        super.onDestroyView();
    }

    @Nullable
    @Override
    public View onCreateView(@NonNull LayoutInflater inflater, @Nullable ViewGroup container, @Nullable Bundle savedInstanceState) {
        View view = inflater.inflate(R.layout.layout_bottom_spinner, container, false);

        rv_spinner = view.findViewById(R.id.rv_spinner);
        ln_close = view.findViewById(R.id.ln_close);
        ln_slide = view.findViewById(R.id.ln_slide);
       /* ln_slide.setOnTouchListener(new View.OnTouchListener() {
            @Override
            public boolean onTouch(View v, MotionEvent event) {
                switch (event.getActionMasked()){
                    case ACTION_DOWN: //get initial state
                    case ACTION_MOVE: //do the sliding
                    case ACTION_UP: // slider release
                        event.getRawY() ;// this is the y-point where the slider has been releases --> upper 50% of display: show slider full-screen, lower 50% -> show map full-screen

                        break;
                }
                return false;
            }
        });*/
        sharedPreferenceHandler = new SharedPreferenceHandler(getContext());
        ln_close.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                close();
            }
        });

        spinnerList = new ArrayList<>();
        spinnerList = getArguments().getStringArrayList("list");

        if (spinnerList == null) {
            spinnerList = new ArrayList<>();
        }

        adapterSpinner = new AdapterSpinner(getContext(), spinnerList, "", new IItemActionListner() {
            @Override
            public void onItemClickListner(int pos) {
                switch (spinnerList.get(pos)) {
                    case "Profile":
                        close();
                        startActivity(new Intent(getContext(), ProfileActivity.class));
                        break;
                    case "Watchlist":
                        close();
                        startActivity(new Intent(getContext(), VideoListActivity.class).
                                putExtra("WatchLater", true).putExtra(CATEGORY_TYPE, "Watch Later"));

                        break;
                    case "Contact Us":
                        close();
                        startActivity(new Intent(getContext(), QueryPagerActivity.class));

                        break;
                    case "FAQ":
                        close();
                        startActivity(new Intent(getContext(), ActivityTermCondition.class));

                        break;
                    case "EXIT":
                        close();
                        sharedPreferenceHandler.saveFcmToken(null);
                        sharedPreferenceHandler.setProfileData(null);
                        sharedPreferenceHandler.setUnReadCount(0);
                        sharedPreferenceHandler.saveSubscription(null);
                        sharedPreferenceHandler.saveAllNotification(new ArrayList<>());
                        try {
                            FirebaseMessaging.getInstance().deleteToken();
                        }catch (Exception e){

                        }
                        ((MassTvActivity)getActivity()).signOut();
                        break;
                    case "Notification":
                       startActivity(new Intent(getContext(), NotificationActivity.class));
                        close();
//                        Toast.makeText(getContext(), "Coming Soon!", Toast.LENGTH_SHORT).show();
                        break;

//                    case "True Live":
//                       startActivity(new Intent(getContext(), TureLiveVideoPlayer.class));
//                        close();
//                        Toast.makeText(getContext(), "Coming Soon!", Toast.LENGTH_SHORT).show();
//                        break;
                }
            }
        });
        LinearLayoutManager linearLayoutManager = new LinearLayoutManager(getContext());
        rv_spinner.setLayoutManager(linearLayoutManager);
        rv_spinner.setAdapter(adapterSpinner);
        return view;
    }

    public void close() {
        ((MassTvActivity) getActivity()).removeSpinnerFragment();
    }

}
