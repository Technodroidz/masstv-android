package com.example.livestreamingapp.utils;

import android.app.Activity;
import android.app.Dialog;
import android.app.DialogFragment;
import android.content.Context;
import android.content.DialogInterface;
import android.graphics.Color;
import android.graphics.drawable.ColorDrawable;
import android.util.Log;
import android.view.View;
import android.view.Window;
import android.view.WindowManager;
import android.widget.Button;
import android.widget.EditText;
import android.widget.ImageView;

import androidx.appcompat.app.AlertDialog;

import com.example.livestreamingapp.R;
import com.example.livestreamingapp.model.LoginResponse;
import com.example.livestreamingapp.model.ProfileData;
import com.example.livestreamingapp.webservice.APIClient;
import com.example.livestreamingapp.webservice.APIInterface;
import com.google.gson.Gson;

import java.util.HashMap;
import java.util.Map;

import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;



public class CompleteProfile {
   public boolean isCompleteProfileOpen = true;
   private ProfileData profileInfo;
    Context context;
    private CustomProgressDialog customProgressDialog;
    private APIInterface apiInterface;
    private profileAlertListener profileAlertListener;

    public interface profileAlertListener{
        public void isAlertShowing(boolean isShowing);
    }

    public CompleteProfile(Context context,profileAlertListener profileAlertListener){
        this.context = context;
        profileInfo = new SharedPreferenceHandler(context).getProfileInfo();
        customProgressDialog = new CustomProgressDialog(context);
        apiInterface = APIClient.getClient().create(APIInterface.class);
        this.profileAlertListener = profileAlertListener;
        if(profileInfo.getPhoneNumber()==null||profileInfo.getPhoneNumber().isEmpty()){
            completeProfileAlert();
        }

    }

    public void completeProfileAlert() {

        profileAlertListener.isAlertShowing(true);

        View layout =((Activity)context). getLayoutInflater().inflate(R.layout.layout_register,
                null);
        final Dialog dialog = setDialog(context, layout);
        final EditText et_fname = (EditText) layout.findViewById(R.id.et_firstname);
        final EditText et_lname = (EditText) layout.findViewById(R.id.et_lastname);
        final EditText et_phone_number = (EditText) layout.findViewById(R.id.et_phone_number);
        final EditText et_email = (EditText) layout.findViewById(R.id.et_email);
        et_email.setText(""+profileInfo.getEmail());
        final ImageView iv_close = (ImageView) layout.findViewById(R.id.iv_close);
        Button btnOk = (Button) layout.findViewById(R.id.btnOk);
        iv_close.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                isCompleteProfileOpen = false;
                dialog.dismiss();
            }
        });
        dialog.setCancelable(false);
        dialog.getWindow().setLayout(WindowManager.LayoutParams.MATCH_PARENT, WindowManager.LayoutParams.WRAP_CONTENT);
        btnOk.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                boolean valid = true;
                if(et_fname.getText()!=null && et_fname.getText().toString().isEmpty()){
                    valid = false;
                    et_fname.requestFocus();
                    et_fname.setError("First Name is Mandatory!");
                }else if(et_phone_number.getText()!=null && et_phone_number.getText().toString().isEmpty()){
                    valid = false;
                    et_phone_number.setError("Field can't be left blank");
                }else if(et_phone_number.getText()!=null && !et_phone_number.getText().toString().isEmpty()){
                    if(et_phone_number.getText().toString().length()<10){
                        valid = false;
                        et_phone_number.setError("Please enter valid phone number!");
                    }
                }
                if(valid){
                    customProgressDialog.show();
                    final ProfileData data = new SharedPreferenceHandler(context).getProfileInfo();
                    Map<String, String> credential = new HashMap<>();
                    String name = et_fname.getText().toString();
                    if(et_lname.getText()!=null && !et_lname.getText().toString().isEmpty()){
                        name = name+" "+et_lname.getText().toString();
                    }
                    credential.put("name", name);
                    credential.put("phone_number",et_phone_number.getText().toString());
                    if(data.getCity()!=null){
                        credential.put("country", data.getCountry());
                        credential.put("state", data.getState());
                        credential.put("city", data.getCity());
                    }else {
                        credential.put("country", "");
                        credential.put("state", "");
                        credential.put("city", "");
                    }
                    credential.put("user_id",""+data.getId());
                    //   Log.e("check profile ","json "+new Gson().toJson(credential));
                    // credential.put("profile_picture", null);
                    Call<LoginResponse> editProfile = apiInterface.editProfile(credential);

                    // progressDialog.show();

                    editProfile.enqueue(
                            new Callback<LoginResponse>() {
                                @Override
                                public void onResponse(
                                        Call<LoginResponse> call, Response<LoginResponse> response) {
                                    // progressDialog.dismiss();

                                    Log.e("check response"," profile response "+new Gson().toJson(response));
                                    customProgressDialog.dismiss();
                                    profileAlertListener.isAlertShowing(false);
                                    if (response.code() == 200) {

                                        isCompleteProfileOpen = false;


                                        //  Log.e("Check ","res success "+new Gson().toJson(response.body()));
                                        dialog.dismiss();
                                        if (response.body().getStatus()) {
                                            // dialog.dismiss();

                                            data.setUserName(et_fname.getText().toString()+" "+et_lname.getText().toString());
                                            data.setPhoneNumber(et_phone_number.getText().toString());
                                            new SharedPreferenceHandler(context)
                                                    .setPreference(Constants.PROFILE_INFO,new Gson().toJson(data));
                                            dialog.dismiss();

                                        } else {
                                            new AlertDialog.Builder(context,R.style.AlertDialogStyle)
                                                    .setCancelable(false)
                                                    .setMessage(context.getResources().getString(R.string.something_went_wrong))
                                                    .setPositiveButton(
                                                            "Ok",
                                                            new DialogInterface.OnClickListener() {
                                                                @Override
                                                                public void onClick(
                                                                        DialogInterface dialog, int which) {}
                                                            })
                                                    .show();
                                        }


                                    }
                                }

                                @Override
                                public void onFailure(Call<LoginResponse> call, Throwable t) {
                                    customProgressDialog.dismiss();
                                    try {

                                        new AlertDialog.Builder(context,R.style.AlertDialogStyle)
                                                .setCancelable(false)
                                                .setMessage("" + t.getMessage())
                                                .setPositiveButton(
                                                        "Ok",
                                                        new DialogInterface.OnClickListener() {
                                                            @Override
                                                            public void onClick(
                                                                    DialogInterface dialog, int which) {}
                                                        })
                                                .show();
                                    }catch (Exception e){

                                    }

                                }
                            });
                }

            }
        });
        dialog.show();
    }

    public static Dialog setDialog(Context context, View view) {
        Dialog dialog_post_type = new Dialog(context);
        dialog_post_type.requestWindowFeature(Window.FEATURE_NO_TITLE);
        dialog_post_type.getWindow().setBackgroundDrawable(new ColorDrawable(Color.TRANSPARENT));
        dialog_post_type.setCancelable(true);
        dialog_post_type.setContentView(view);
        return dialog_post_type;
    }

}
