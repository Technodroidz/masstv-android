package com.example.livestreamingapp.utils;

import android.content.Context;

public class Constants {
    public static final String STREAMING = "Live Streaming" ;
    public static final String ON_DEMAND = "Video On Demand" ;
    public static final String TRENDING = "trending" ;
    public static final String CATEGORY_REALITY = "Reality Show" ;
    public static final String CATEGORY_ENTERTAINMENT = "Entertainment" ;
    public static final String CATEGORY_SHORT = "Short Movies" ;
    public static final String TOOLBAR_TITLE = "title" ;


    public static final String CATEGORY_TYPE = "category" ;
    public static final String EMAIL_ID = "email";
    public static final String PROFILE_INFO = "profile";
    public static final String PhoneNumber = "phone";


    public static final String SELECTED_ON_DEMAND_VIDEO =  "SelectedVideo";
    public static final String ACTIVITY_TO_OPEN = "activityToOpen";
    public static final String SELECTED_WEBSERIES = "webseries";
}
