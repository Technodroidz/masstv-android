package com.example.livestreamingapp.utils;

import java.util.Currency;

public class CurrencyUtil {

    public static String getCurrencySymbol(String curr){
        Currency currency = Currency.getInstance(curr);
        return  currency.getSymbol();
    }

}
