package com.example.livestreamingapp.utils;

import android.app.AlertDialog;
import android.content.Context;
import android.graphics.drawable.ColorDrawable;

import com.example.livestreamingapp.R;

public class CustomProgressDialog extends AlertDialog {
public CustomProgressDialog(Context context) {
    super(context);
    getWindow().setBackgroundDrawable(new ColorDrawable(android.graphics.Color.TRANSPARENT));
}

@Override
public void show() {
    super.show();
    setContentView(R.layout.wait_layout_trans);
 }
}