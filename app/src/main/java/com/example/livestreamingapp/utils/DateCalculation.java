package com.example.livestreamingapp.utils;

import android.content.Context;
import android.media.MediaMetadataRetriever;
import android.net.Uri;

import java.io.File;
import java.text.DateFormat;
import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.Calendar;
import java.util.Date;
import java.util.concurrent.TimeUnit;

public class DateCalculation {

    private static final long HOUR_IN_SECOND = 216000;
    private static final  long MINUTE_IN_SECOND = 3600;


    public static String getCurrentDateInDDMMYY() {
        DateFormat dateFormat = new SimpleDateFormat("yyyy/MM/dd");
        Date date = new Date();
        return dateFormat.format(date);
    }

    public static String getDateInDD_MM_YY(long time) {
        DateFormat dateFormat = new SimpleDateFormat("dd-MM-yyyy");
       Date date = new Date(time);
        return dateFormat.format(date);
    }

    public static String getTimeInHHmmss(long time) {
        DateFormat dateFormat = new SimpleDateFormat("HH:mm:ss");
        Date date = new Date(time);
        return dateFormat.format(date);
    }

    /**
     * Convert a millisecond duration to a string format
     *
     * @return A string of the form "X Days Y Hours Z Minutes A Seconds".
     */

    public static long getDurationFile(Context context, File file){
        MediaMetadataRetriever retriever = new MediaMetadataRetriever();
//use one of overloaded setDataSource() functions to set your data source
        retriever.setDataSource(context, Uri.fromFile(file));
        String time = retriever.extractMetadata(MediaMetadataRetriever.METADATA_KEY_DURATION);
        long timeInMillisec = Long.parseLong(time );

        retriever.release();
        return timeInMillisec;
    }

    public static String getDurationBreakdown(long millis) {
        if(millis < 0) {
            throw new IllegalArgumentException("Duration must be greater than zero!");
        }

        long hours = TimeUnit.MILLISECONDS.toHours(millis);
        millis -= TimeUnit.HOURS.toMillis(hours);
        long minutes = TimeUnit.MILLISECONDS.toMinutes(millis);
        millis -= TimeUnit.MINUTES.toMillis(minutes);
        long seconds = TimeUnit.MILLISECONDS.toSeconds(millis);

        String time;

        if(hours<10){
           time = "0"+hours+":";
        }else {
            time = ""+hours+":"+"";
        }
        if(minutes<10){
            time = time+"0"+minutes+":";
        }else {
            time = time+""+minutes+":"+"";
        }
        if(seconds<10){
            time = time+"0"+seconds;
        }else {
            time = time+""+seconds+"";
        }

        return time;
    }

    public static String getCurrentTimeInHours() {
        DateFormat dateFormat = new SimpleDateFormat("HH:mm");
        Date date = new Date();
        return dateFormat.format(date);
    }

    public static long getTotalDurationInMillis(String schedule_time, String duration, String date) {
        long totalMilis = -1;
        DateFormat dateFormat = new SimpleDateFormat("yyyy-MM-dd HH:mm:ss");
        DateFormat simpleDateFormat = new SimpleDateFormat("HH:mm:ss");
        try {
            Date endTime = dateFormat.parse(date+" "+duration);
            Date scheduleTime = dateFormat.parse(date+" "+schedule_time);
            Date currentTime = dateFormat.parse(dateFormat.format(new Date()));

            assert endTime != null;
            assert currentTime != null;
            if((endTime.getTime() > currentTime.getTime())) {
                assert scheduleTime != null;
                if (currentTime.getTime() > scheduleTime.getTime()) {

                    int hour = currentTime.getHours() - scheduleTime.getHours();
                    int minutes = currentTime.getMinutes() - scheduleTime.getMinutes();
                    int second = currentTime.getSeconds() - scheduleTime.getSeconds();

                    totalMilis = ((hour * 60 * 60 * 1000) + (minutes * 60 * 1000) + (second * 1000)) ;
                   /* if(totalMilis<8000 && totalMilis>5000){
                        totalMilis = ((hour * 60 * 60 * 1000) + (minutes * 60 * 1000) + (second * 1000));
                    }else {
                        totalMilis = ((hour * 60 * 60 * 1000) + (minutes * 60 * 1000) + (second * 1000));
                    }*/

                }
            }


        } catch (ParseException e) {
            e.printStackTrace();
        }

        return totalMilis;
    }

    public static boolean  isLiveTimeOver( String date,String schedule_time) {
        boolean timerOver = false;
        if(schedule_time.length()==5){
            schedule_time = schedule_time+":00";
        }
        DateFormat dateFormat = new SimpleDateFormat("yyyy-MM-dd HH:mm:ss");
        try {

            Date scheduleTime = dateFormat.parse(date+" "+schedule_time);
            Date currentTime = dateFormat.parse(dateFormat.format(new Date()));

            if (scheduleTime != null && currentTime != null && currentTime.getTime() >= (scheduleTime.getTime())) {

                timerOver = true;

            }

        } catch (ParseException e) {
            e.printStackTrace();
        }

        return timerOver;
    }

    public static boolean  isAdTimeOver( String date,String schedule_time,String duration) {
        boolean timerOver = false;
        DateFormat dateFormat = new SimpleDateFormat("yyyy-MM-dd HH:mm:ss");
        try {

            Date scheduleTime = dateFormat.parse(date+" "+schedule_time);
           // Date currentTime = dateFormat.parse(date+" "+duration);
            Date currentTime = dateFormat.parse(dateFormat.format(new Date()));

            if (currentTime.getTime() >= (scheduleTime.getTime())) {

                timerOver = true;

            }

        } catch (ParseException e) {
            e.printStackTrace();
        }

        return timerOver;
    }

    public static long getScheduleTimeInMillis(String sheduleTime) {
        Calendar calendar = null;
        DateFormat dateFormat = new SimpleDateFormat("HH:mm");
        try {
            Date date = dateFormat.parse(sheduleTime);

            calendar = Calendar.getInstance();
            calendar.setTime(new Date());
            calendar.add(Calendar.MINUTE, date.getMinutes());
            calendar.add(Calendar.HOUR, date.getHours());
        } catch (ParseException e) {
            e.printStackTrace();
        }
        return calendar.getTimeInMillis();
    }

    public static long getMillis(String duration) {
        Calendar calendar = null;
        DateFormat dateFormat = new SimpleDateFormat("mm:ss");
        try {
            Date date = dateFormat.parse(duration);

            calendar = Calendar.getInstance();
            calendar.setTime(date);
        } catch (ParseException e) {
            e.printStackTrace();
        }
        return calendar.getTimeInMillis();
    }


    public static String ConvertMillisToHHMMSS(long millis) {
        String hms;


        long hour = TimeUnit.MILLISECONDS.toHours(millis);

        if(hour>0){
              hms = String.format("%02d:%02d:%02d", hour,
                    TimeUnit.MILLISECONDS.toMinutes(millis) - TimeUnit.HOURS.toMinutes(TimeUnit.MILLISECONDS.toHours(millis)),
                    TimeUnit.MILLISECONDS.toSeconds(millis) - TimeUnit.MINUTES.toSeconds(TimeUnit.MILLISECONDS.toMinutes(millis)));
        }else {
             hms = String.format("%02d:%02d",
                    TimeUnit.MILLISECONDS.toMinutes(millis) - TimeUnit.HOURS.toMinutes(TimeUnit.MILLISECONDS.toHours(millis)),
                    TimeUnit.MILLISECONDS.toSeconds(millis) - TimeUnit.MINUTES.toSeconds(TimeUnit.MILLISECONDS.toMinutes(millis)));
        }

        System.out.println(hms);
        return hms;
    }

    public static String ConvertMillisToHHMMSSFull(long millis) {
        String hms;

        long hour = TimeUnit.MICROSECONDS.toHours(millis);

        hms = String.format("%02d:%02d:%02d", hour,
                TimeUnit.MILLISECONDS.toMinutes(millis) - TimeUnit.HOURS.toMinutes(TimeUnit.MILLISECONDS.toHours(millis)),
                TimeUnit.MILLISECONDS.toSeconds(millis) - TimeUnit.MINUTES.toSeconds(TimeUnit.MILLISECONDS.toMinutes(millis)));

        System.out.println(hms);
        return hms;
    }

    public static Object getTimeInHHMMSS(long duration) {
        Calendar calendar = null;
        DateFormat dateFormat = new SimpleDateFormat("HH:mm:ss");

        Date date = new Date(duration);

       /* try {
          //  Date date = dateFormat.parse(duration);

            calendar = Calendar.getInstance();
            calendar.setTime(date);
        } catch (ParseException e) {
            e.printStackTrace();
        }*/
        return calendar.getTimeInMillis();
    }

    public static long getTimeInMillis(String duration) {

        Calendar calendar = null;
        DateFormat dateFormat = new SimpleDateFormat("HH:mm:ss");
        try {
            Date date = dateFormat.parse(duration);
            calendar = Calendar.getInstance();
            calendar.setTime(date);
        } catch (ParseException e) {
            e.printStackTrace();
        }
        return calendar.getTimeInMillis();
    }

    public static String getTimeDiffinMinute(long startTime, long endTime,long duration) {

        try {
            long value  = getDateDiff(startTime,endTime,TimeUnit.SECONDS);
            long second = TimeUnit.SECONDS.convert(duration,TimeUnit.MILLISECONDS);

            if(second>0 && second<18000){
                 value = Math.min(value, second);
            }

            double val =value/60.0;


            return  String.format("%.1f", val);
        }catch (Exception e){
            return "1";
        }

     /*  if(value==0)
           return "1";
       }
       return ""+value;*/

    }

    public static long getDateDiff(long date1, long date2, TimeUnit timeUnit) {
        long diffInMillies = date2 - date1;
        return timeUnit.convert(diffInMillies,TimeUnit.MILLISECONDS);
    }


}
