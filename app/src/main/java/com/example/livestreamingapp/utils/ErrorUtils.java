package com.example.livestreamingapp.utils;

import com.example.livestreamingapp.model.APIError;
import com.example.livestreamingapp.webservice.APIClient;

import java.io.IOException;
import java.lang.annotation.Annotation;

import okhttp3.ResponseBody;
import retrofit2.Converter;
import retrofit2.Response;

public class ErrorUtils {

    public static APIError  parseError(Response<?> response) {
        Converter<ResponseBody, APIError> converter =
                APIClient.getClient()
                        .responseBodyConverter(APIError.class, new Annotation[0]);

        APIError error;

        try {
            error = converter.convert(response.errorBody());
        } catch (IOException e) {
            return new APIError();
        }

        return error;
    }


}