package com.example.livestreamingapp.utils;

import android.app.Activity;
import android.content.ContentResolver;
import android.content.Context;
import android.content.Intent;
import android.content.pm.PackageManager;
import android.content.pm.ResolveInfo;
import android.content.res.AssetManager;
import android.net.Uri;
import android.os.AsyncTask;
import android.os.Build;
import android.os.Environment;
import android.os.Handler;
import android.util.Base64;
import android.util.Log;

import androidx.annotation.NonNull;
import androidx.annotation.Nullable;
import androidx.annotation.RequiresApi;
import androidx.core.app.ShareCompat;
import androidx.core.content.FileProvider;

import com.example.livestreamingapp.BuildConfig;

import java.io.File;
import java.io.FileOutputStream;
import java.io.IOException;
import java.io.InputStream;
import java.io.OutputStream;
import java.util.List;

public class FileUtils {

    public static File copyFile1(Activity c, String filename) {
        AssetManager assetManager = c.getAssets();
        final ContentResolver contentResolver = c.getContentResolver();
        if (contentResolver == null)
            return null;
        File directory = new File(c.getCacheDir(), "media/thumbnail");
        if (!directory.exists()) {
            try {
                directory.mkdir();
            } catch (Exception e) {
            }
        }
        // Create file path inside app's data dir

        //  newfile = new File(directory, filePath);
        File file = new File(directory, filename);
        String filePath = file.getAbsolutePath();
        try {
            InputStream inputStream = assetManager.open(filename);
            if (inputStream == null)
                return null;
            OutputStream outputStream = new FileOutputStream(file);
            byte[] buf = new byte[1024];
            int len;
            while ((len = inputStream.read(buf)) > 0)
                outputStream.write(buf, 0, len);
            outputStream.close();
            inputStream.close();
        } catch (IOException ignore) {
            return null;
        }
        return file;

    }

    public void showPdf(Context context){
        new DownloadFile(context).execute();
    }

    @Nullable
    public static File copyFile(
            @NonNull Context context, String name) {
        AssetManager assetManager = context.getAssets();

        // Create file path inside app's data dir

        //  newfile = new File(directory, filePath);
        File file = null;
        try {
            file = createPdfFile(context);
        } catch (IOException e) {
            e.printStackTrace();
        }
        try {
            InputStream inputStream = assetManager.open("policy.pdf");
            if (inputStream == null)
                return null;
            OutputStream outputStream = new FileOutputStream(file);
            byte[] buf = new byte[1024];
            int len;
            while ((len = inputStream.read(buf)) > 0)
                outputStream.write(buf, 0, len);
            outputStream.close();
            inputStream.close();
            return file;
        } catch (IOException e) {
            e.printStackTrace();
            try {
                file = createPdfFile(context);
            } catch (IOException ioException) {
                ioException.printStackTrace();
            }
            return file;
        }
    }

    public class showPdfFileAsync extends AsyncTask<Void,Void,File>{
        CustomProgressDialog customProgressDialog;
        Context context;
        public showPdfFileAsync(Context context){
            customProgressDialog = new CustomProgressDialog(context);
            this.context = context;
        }

        @Override
        protected void onPreExecute() {
            super.onPreExecute();
            customProgressDialog.show();
        }

        @Override
        protected File doInBackground(Void... voids) {
            return null;
        }

        @Override
        protected void onPostExecute(File file) {
            super.onPostExecute(file);
        }
    }




    private static File createPdfFile(Context context) throws IOException {
        File storageDir =
                context.getExternalFilesDir(Environment.DIRECTORY_PICTURES);
        //  Create file path inside app's data dir
        String name = "policy" + ".pdf";
        File file = new File(storageDir, name);
        return file;

    }


    /**
     * @Rehan Download Preview
     */
    private class DownloadFile extends AsyncTask<String, Void, String> {

        public GetFilePathAndStatus getFileFromBase64AndSaveInSDCard(String base64, String filename, String extension) {
            GetFilePathAndStatus getFilePathAndStatus = new GetFilePathAndStatus();
            try {
                AssetManager assetManager = context.getAssets();
                InputStream inputStream = assetManager.open(filename+""+extension);
                if (inputStream == null)
                    return null;
                OutputStream outputStream = new FileOutputStream(createPdfFile(context).getAbsolutePath(), false);
                byte[] buf = new byte[1024];
                int len;
                while ((len = inputStream.read(buf)) > 0)
                    outputStream.write(buf, 0, len);
                outputStream.close();
                inputStream.close();
                getFilePathAndStatus.filStatus = true;
                getFilePathAndStatus.filePath = createPdfFile(context).getAbsolutePath();
                return getFilePathAndStatus;
            } catch (IOException e) {
                e.printStackTrace();
                getFilePathAndStatus.filStatus = false;
                try {
                    getFilePathAndStatus.filePath = createPdfFile(context).getAbsolutePath();
                } catch (IOException ioException) {
                    ioException.printStackTrace();
                }
                return getFilePathAndStatus;
            }
        }



        private  File createPdfFile(Context context) throws IOException {
            File storageDir =
                    context.getExternalFilesDir(Environment.DIRECTORY_PICTURES);
            //  Create file path inside app's data dir
            String name = "policy" + ".pdf";
            File file = new File(storageDir, name);
            return file;

        }

        public String getReportPath(String filename, String extension) {

            File directory = new File(context.getCacheDir(), "media/");

            File file = new File(directory, filename);
            if (!file.exists()) {
                file.mkdirs();
            }
            String uriSting = (file.getAbsolutePath() + "/" + filename + "." + extension);
            return uriSting;

        }

        public class GetFilePathAndStatus {
            public boolean filStatus;
            public String filePath;

        }

        String fileUrl;
        Context context;
        CustomProgressDialog customProgressDialog;

        public DownloadFile(Context context) {
            this.context = context;

        }


        @Override
        protected void onPreExecute() {
            super.onPreExecute();
            customProgressDialog = new CustomProgressDialog(context);
            customProgressDialog.show();

        }

        @Override
        protected String doInBackground(String... strings) {
            // -> http://maven.apache.org/maven-1.x/maven.pdf


            String filePath = getFileFromBase64AndSaveInSDCard(fileUrl, "policy", ".pdf").filePath;


            return filePath;
        }

        @RequiresApi(api = Build.VERSION_CODES.LOLLIPOP)
        @Override
        protected void onPostExecute(String filePath) {
            super.onPostExecute(filePath);


            try {
                Uri uri = FileProvider.getUriForFile(context, BuildConfig.APPLICATION_ID+".provider", new File(filePath));

                Activity activity = ((Activity)context);

                Intent intent = ShareCompat.IntentBuilder.from(activity)
                        .setStream(uri) // uri from FileProvider
                        .getIntent()
                        .setAction(Intent.ACTION_VIEW) //Change if needed
                        .setDataAndType(uri, "application/pdf")
                        .addFlags(Intent.FLAG_GRANT_READ_URI_PERMISSION);

                if (Build.VERSION.SDK_INT <= Build.VERSION_CODES.KITKAT) {
                    List<ResolveInfo> resInfoList =context. getPackageManager().queryIntentActivities(intent, PackageManager.MATCH_DEFAULT_ONLY);
                    for (ResolveInfo resolveInfo : resInfoList) {
                        String packageName = resolveInfo.activityInfo.packageName;
                        context.grantUriPermission(packageName, uri, Intent.FLAG_GRANT_READ_URI_PERMISSION);
                    }
                }

                if (new File(filePath).exists()) {

                    customProgressDialog.dismiss();

                    context.startActivity(intent);
                }


            } catch (Exception e) {
                e.printStackTrace();
            }


            new Handler().postDelayed(new Runnable() {
                @Override
                public void run() {
                    customProgressDialog.dismiss();
                }
            }, 500);


        }
    }
}
