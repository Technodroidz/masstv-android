package com.example.livestreamingapp.utils;

import android.content.Context;
import android.content.SharedPreferences;
import android.util.Log;

import com.example.livestreamingapp.model.DemoPlanModel;
import com.example.livestreamingapp.model.LiveShowData;
import com.example.livestreamingapp.model.MySubscription;
import com.example.livestreamingapp.model.ProfileData;
import com.example.livestreamingapp.model.SubscriptionPlan;
import com.example.livestreamingapp.model.webseries.Season;

import com.google.gson.Gson;
import com.google.gson.reflect.TypeToken;

import java.text.DateFormat;
import java.util.ArrayList;
import java.util.List;

import static android.content.Context.MODE_PRIVATE;
import static android.content.Context.NOTIFICATION_SERVICE;

public class SharedPreferenceHandler {
    public static final String SUBSCRIBED = "subscribed";
    public static final String DEFAULT_CARD = "default_payment_method";
    public static final String SELECTED_PLAN = "selected_plan";
    private static final String SUBSCRIPTION_INFO = "subscription";
    private static final String PROFILE_INFO = "profile";
    public static final String SAVED_CARDS = "saved_cards";
    private static final String NOTIFICATION_UNREAD_MSG = "unread_msg" ;
    private static final String NOTIFICATION_MSG = "all_msg" ;
    private static final String NOTIFICATION_UNREAD_COUNT =  "unread_count";
    private static final String BANNER_IMAGE = "banner_image";
    private static final String DEEPLINK = "deeplink";
    private static final String AD_VIDEO_POSITION = "ad_video";
    private static final String AD_VIDEO = "current_ad_video";
    SharedPreferences.Editor editor;
    String SHARED_DETAILS = "shared_details";
    Context context;
    public static final String SEASONS = "seasonsList";

    public SharedPreferenceHandler(Context context) {
this.context = context;
    }


    public void setPreference(String key, String value) {
        editor =context.getSharedPreferences(SHARED_DETAILS, MODE_PRIVATE).edit();
        editor.putString(key, value);
        editor.commit();
    }

    public void setPreferenceBoolean(String key, Boolean value) {
        editor =context.getSharedPreferences(SHARED_DETAILS, MODE_PRIVATE).edit();
        editor.putBoolean(key, value);
        editor.commit();
    }


    public void setPreferenceInteger(String key, int value) {
        editor =context.getSharedPreferences(SHARED_DETAILS, MODE_PRIVATE).edit();
        editor.putInt(key, value);
        editor.commit();
    }


    public String getPrefernceString(String key) {
        String values = null;
        try {
            SharedPreferences prefs =context. getSharedPreferences(SHARED_DETAILS, MODE_PRIVATE);
            values = prefs.getString(key, null);
        } catch (Exception e) {
            e.printStackTrace();
        }

        return values;

    }




    public Boolean getPrefernceBoolean(String key) {
        Boolean values =false ;
        try {
            SharedPreferences prefs =context. getSharedPreferences(SHARED_DETAILS, MODE_PRIVATE);
            values = prefs.getBoolean(key, false);
        } catch (Exception e) {
            e.printStackTrace();
        }

        return values;

    }


    public int getPrefernceInteger(String key) {
        int values = 0;
        try {
            SharedPreferences prefs =context. getSharedPreferences(SHARED_DETAILS, MODE_PRIVATE);
            values = prefs.getInt(key, 0);
        } catch (Exception e) {
            e.printStackTrace();
        }

        return values;

    }

    public void deleteAllData(Context context) {
        SharedPreferences prefs =context. getSharedPreferences(SHARED_DETAILS, MODE_PRIVATE);
        if (prefs != null) {
            prefs.edit().clear().apply();
        }
    }

    public ProfileData getProfileInfo() {
        String profileJson = new SharedPreferenceHandler(context).
                getPrefernceString(Constants.PROFILE_INFO);
        if(profileJson!=null){
            return new Gson().fromJson(profileJson,ProfileData.class);
        }
       return null;
    }

    public SubscriptionPlan getSelectedPlan() {
        String planJson = new SharedPreferenceHandler(context).
                getPrefernceString(SharedPreferenceHandler.SELECTED_PLAN);
        if(planJson!=null){
            return new Gson().fromJson(planJson, SubscriptionPlan.class);
        }
        return null;
    }

    public void setProfileData(ProfileData profileData) {
        String profileJson = new Gson().toJson(profileData);
        setPreference(PROFILE_INFO,profileJson);
    }

    public MySubscription getMySubscriptionDetail() {
        String subscriptionJson =  getPrefernceString(SUBSCRIPTION_INFO);
        MySubscription subscription;
        if(subscriptionJson!=null){
           subscription =  new Gson().fromJson(subscriptionJson,MySubscription.class);
          if(subscription!=null && subscription.getLastRefreshDate()!=null){
              return subscription;
          }

        }
        return null;
    }

    public void saveSubscription(MySubscription subscriptionInfo) {
        if(subscriptionInfo!=null && subscriptionInfo.getRemainingDays()!=null){
            subscriptionInfo.setLastRefreshDate(DateCalculation.getCurrentDateInDDMMYY());
            String subscriptionJson = new Gson().toJson(subscriptionInfo);
            if(subscriptionJson!=null){
                setPreference(SUBSCRIPTION_INFO,subscriptionJson);
            }
        }else {
            setPreference(SUBSCRIPTION_INFO,null);
        }

    }

    public void saveFcmToken(String token) {
        editor =context.getSharedPreferences(SHARED_DETAILS, MODE_PRIVATE).edit();
        editor.putString("token", token);
        editor.commit();
    }

    public String getFcmToken() {
        String values = null;
        try {
            SharedPreferences prefs =context. getSharedPreferences(SHARED_DETAILS, MODE_PRIVATE);
            values = prefs.getString("token", null);
        } catch (Exception e) {
            e.printStackTrace();
        }
        return values;
    }

    public int getUnReadMessageCount() {
       List<NotificationMessage> list = getAllMessage();
       int count=0;
       if(list!=null && list.size()>0){
          for(int i=0;i<list.size();i++){
              if(!list.get(i).isRead()){
                  count = count+1;
              }
          }
       }
       return count;
    }

    public List<NotificationMessage> getAllMessage() {
        String notificationJson =  getPrefernceString(NOTIFICATION_MSG);
        List<NotificationMessage> notificationMessages = null;
        if(notificationJson!=null){
            notificationMessages = new Gson().fromJson(notificationJson,
                    new TypeToken<List<NotificationMessage>>() {}.getType());
        }
        return notificationMessages;
    }

    public void setMessageRead(NotificationMessage notificationMessage){
        List<NotificationMessage> list = getAllMessage();
        for(int i=0;list.size()>i;i++){
            if(notificationMessage.getId()==list.get(i).getId()){
                saveAllNotification(list);
                list.set(i,notificationMessage);
                saveAllNotification(list);
                return;
            }
        }
    }

    public void saveAllNotification(List<NotificationMessage> list) {

       /* if(list!=null && list.size()>0 && list.get(list.size()-1).getTitle()==null){
            list.get(list.size()-1).setTitle("Testing");
        }
        if(list!=null && list.size()>0 && list.get(list.size()-1).getMessage()==null){
            list.get(list.size()-1).setMessage("Testing Message");
        }*/
        String notificationJson = new Gson().toJson(list);
        setPreference(NOTIFICATION_MSG,notificationJson);

     /*   if(list.size()==0){
            String notificationJsonEmpty = new Gson().toJson(list);
            setPreference(NOTIFICATION_MSG,notificationJsonEmpty);
        }*/
    }


    public void saveUnReadMessage(NotificationMessage notificationMessage) {
        List<NotificationMessage> list = getAllMessage();
        int id = 1;
        if(list==null){
            list = new ArrayList<>();
        }else {
            id = getId(list.get(list.size()-1));
        }
        notificationMessage.setId(id);
        list.add(0,notificationMessage);
        String notificationJson = new Gson().toJson(list);
        if(notificationJson!=null){
            setPreference(NOTIFICATION_MSG,notificationJson);
        }
    }
    public int getId(NotificationMessage notificationMessage){
        return notificationMessage.getId()+1;
    }

    public void setUnReadCount(int count) {

        setPreferenceInteger(NOTIFICATION_UNREAD_COUNT,count);

    }

    public int getUnReadCount() {
       return getPrefernceInteger(NOTIFICATION_UNREAD_COUNT);
    }

    public String getBannerImage() {
        return  getPrefernceString(SharedPreferenceHandler.BANNER_IMAGE);
    }


    public int getAdVideoPosition() {
        return  getPrefernceInteger(SharedPreferenceHandler.AD_VIDEO_POSITION);
    }


    public void saveSeason(List<Season> seasons) {
        String seasonJson = new Gson().toJson(seasons);
        Log.e("check seasonJson ",""+new Gson().toJson(seasonJson));
        if(seasonJson!=null){
            setPreference(SEASONS,seasonJson);
        }
    }

    public List<Season> getSeasonList() {
        String notificationJson =  getPrefernceString(SEASONS);
        List<Season> seasonsList = null;
        if(notificationJson!=null){
            seasonsList = new Gson().fromJson(notificationJson,
                    new TypeToken<List<Season>>() {}.getType());
        }
        return seasonsList;
    }

    public void saveDeepLink(String link) {
        setPreference(DEEPLINK,link);
    }

    public String getDeepLink(){
        return  getPrefernceString(DEEPLINK);
    }

    public void saveCurrentAdVideoPosition(int videoPosition) {
        setPreferenceInteger(AD_VIDEO_POSITION,videoPosition);
    }


    public LiveShowData getAdVideo() {
        String video =  getPrefernceString(AD_VIDEO);

        if(video!=null){
            return new Gson().fromJson(video, LiveShowData.class);
        }
        return null;
    }

    public void saveCurrentAdVideo(LiveShowData onDemandVideo) {
        String onDemand = new Gson().toJson(onDemandVideo);
        if(onDemand!=null){
            setPreference(AD_VIDEO,onDemand);
        }
    }
}
