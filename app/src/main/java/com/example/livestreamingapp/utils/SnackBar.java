package com.example.livestreamingapp.utils;

import android.app.Activity;
import android.content.Context;
import android.graphics.Color;
import android.view.View;
import android.widget.TextView;

import com.example.livestreamingapp.R;
import com.google.android.material.snackbar.Snackbar;

public class SnackBar {
    private Context  context;
    private View view;
    public SnackBar(Context context){
        this.context = context;
        view = ((Activity)context).getWindow().getDecorView ().getRootView();

    }
    // Showing the status in Snackbar for intenet connectivity
    public  void showPositiveSnack(String message) {
        int color = Color.GREEN;;

        Snackbar snackbar = Snackbar
                .make(view.findViewById(R.id.parent), message, Snackbar.LENGTH_LONG);
        View sbView = snackbar.getView();
        TextView textView = (TextView) sbView.findViewById(R.id.snackbar_text);
        textView.setTextColor(color);
        snackbar.show();

    }

    public  void showErrorSnack(String message) {

        int color = Color.RED;;

        Snackbar snackbar = Snackbar
                .make(view.findViewById(R.id.parent), message, Snackbar.LENGTH_LONG);
        View sbView = snackbar.getView();
        TextView textView = (TextView) sbView.findViewById(R.id.snackbar_text);
        textView.setTextColor(color);
        snackbar.show();

    }
}
