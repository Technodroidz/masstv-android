package com.example.livestreamingapp.utils;

import android.graphics.Rect;
import android.view.View;

import androidx.annotation.NonNull;
import androidx.recyclerview.widget.GridLayoutManager;
import androidx.recyclerview.widget.RecyclerView;

public class SpacesItemDecoration extends RecyclerView.ItemDecoration {
    private int space;
    private int space2 = 0;
    private int space3 = 0;
    private int space4 = 0;

    public SpacesItemDecoration(int space) {
        this.space = space;
    }

    public SpacesItemDecoration(int space, int space2) {
        this.space = space;
        this.space2 = space2;
    }

    public SpacesItemDecoration(int space, int space2, int space3) {
        this.space = space;
        this.space2 = space2;
        this.space3 = space3;
    }

    public SpacesItemDecoration(int space, int space2, int space3, int space4) {
        this.space = space;
        this.space2 = space2;
        this.space3 = space3;
        this.space4 = space4;
    }

    @Override
    public void getItemOffsets(Rect outRect, View view,
                               RecyclerView parent, RecyclerView.State state) {


        // Add top margin only for the first item to avoid double space between items

        if (space4 != 0) {
            outRect.bottom = space4;
            outRect.left = space4;
        } else if (space2 == 0) {
            outRect.bottom = space * 3;
            outRect.left = space * 2;
        } else {
            int position = parent.getChildAdapterPosition(view);
            outRect.bottom = space * 2;
            if (position % 2 == 0) {
                outRect.right = space * 2;

            }

        }

    }

}