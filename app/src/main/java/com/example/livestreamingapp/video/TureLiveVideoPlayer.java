package com.example.livestreamingapp.video;

import android.app.AlertDialog;
import android.content.DialogInterface;
import android.content.Intent;
import android.os.Build;
import android.os.Bundle;
import android.view.View;
import android.view.WindowManager;
import android.widget.FrameLayout;
import android.widget.ImageButton;
import android.widget.ProgressBar;
import android.widget.TextView;
import android.widget.Toast;

import androidx.appcompat.app.AppCompatActivity;

import com.example.livestreamingapp.R;
import com.example.livestreamingapp.home.ui.HomeDraweerStreamingActivity;
import com.example.livestreamingapp.model.OnDemandVideo;
import com.example.livestreamingapp.model.TrueLiveVideoResponse;
import com.example.livestreamingapp.profile.ProfileActivity;
import com.example.livestreamingapp.utils.CustomProgressDialog;
import com.example.livestreamingapp.webservice.APIClient;
import com.example.livestreamingapp.webservice.APIInterface;
import com.google.android.exoplayer2.DefaultRenderersFactory;
import com.google.android.exoplayer2.ExoPlayer;
import com.google.android.exoplayer2.MediaItem;
import com.google.android.exoplayer2.Player;
import com.google.android.exoplayer2.RenderersFactory;
import com.google.android.exoplayer2.SimpleExoPlayer;
import com.google.android.exoplayer2.source.hls.HlsMediaSource;
import com.google.android.exoplayer2.trackselection.AdaptiveTrackSelection;
import com.google.android.exoplayer2.trackselection.DefaultTrackSelector;
import com.google.android.exoplayer2.ui.PlayerControlView;
import com.google.android.exoplayer2.ui.PlayerView;
import com.google.android.exoplayer2.upstream.DefaultHttpDataSource;
import com.google.android.exoplayer2.upstream.DefaultHttpDataSourceFactory;
import com.google.android.exoplayer2.upstream.HttpDataSource;


import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;


/**
 * An example full-screen activity that shows and hides the system UI (i.e.
 * status bar and navigation/system bar) with user interaction.
 */
public class TureLiveVideoPlayer extends AppCompatActivity {

    private PlayerView exoPlayerView;

    private ImageButton btn_skip;

    private ExoPlayer player;
    ProgressBar buffering;
    private OnDemandVideo video;
    private View decorView;
    private TextView tv_title;
    private FrameLayout fl_toolbar;
    private APIInterface apiInterface;
    CustomProgressDialog customProgressDialog;


    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.layout_video_player);
        customProgressDialog = new CustomProgressDialog(this);
        customProgressDialog.setCancelable(false);
        customProgressDialog.setMessage("loading...");
         apiInterface = APIClient.getClient().create(APIInterface.class);
        decorView = getWindow().getDecorView();
        video = (OnDemandVideo) getIntent().getSerializableExtra("videos");
        video = new OnDemandVideo();
       /* video.setSource("https://bitdash-a.akamaihd.net/content/sintel/hls/playlist.m3u8");
        video.setTitle("test videos");*/
        hideSystemUi();
        initViews();
        getTruLiveVideo();

    }

  
    private void initViews() {
        exoPlayerView = findViewById(R.id.exoPlayerView);
        btn_skip = findViewById(R.id.btn_skip);
        buffering = findViewById(R.id.buffering);
        fl_toolbar = findViewById(R.id.fl_toolbar);
        tv_title = findViewById(R.id.tv_title);
       // tv_title.setText(""+video.getTitle());
        showHideBuffring(true);

        btn_skip.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
               onBackPressed();
            }
        });

    /*    exoPlayerView.setControllerVisibilityListener(new PlayerControlView.VisibilityListener() {
            @Override
            public void onVisibilityChange(int visibility) {
                if(visibility == View.VISIBLE){
                    fl_toolbar.setVisibility(View.VISIBLE);
                }else {
                    fl_toolbar.setVisibility(View.GONE);
                }
            }
        });*/

      //  playTrailerVideo(video);

    }

    public void playTrailerVideo(OnDemandVideo onDemandVideo) {
        //initializePlayer(onDemandVideo);
        initializeHlsPlayer(onDemandVideo);
    }

    private void initializeHlsPlayer(OnDemandVideo onDemandVideo) {
        DefaultTrackSelector trackSelector = new DefaultTrackSelector(this, new AdaptiveTrackSelection.Factory());
        DefaultTrackSelector.Parameters trackSelectorParameters = trackSelector.buildUponParameters().setMaxAudioChannelCount(0).build();
        trackSelector.setParameters(trackSelectorParameters);
        RenderersFactory renderersFactory = new DefaultRenderersFactory(this);
         player =  new SimpleExoPlayer.Builder(this, renderersFactory).setTrackSelector(trackSelector).build();
        HttpDataSource.Factory factory = new DefaultHttpDataSourceFactory("Online Audio Player", DefaultHttpDataSource.DEFAULT_CONNECT_TIMEOUT_MILLIS, DefaultHttpDataSource.DEFAULT_READ_TIMEOUT_MILLIS, true);
        exoPlayerView.setPlayer(player);
        MediaItem mediaItem = new MediaItem.Builder().setUri(onDemandVideo.getSource()).setTag("myObject").build();
       /* ProgressiveMediaSource hlsMediaSource = new ProgressiveMediaSource.Factory(factory).
                createMediaSource(MediaItem.fromUri(
                        onDemandVideo.getKey()));*/
        HlsMediaSource hlsMediaSource =  new HlsMediaSource.Factory(factory).createMediaSource(mediaItem);
        player.setMediaSource(hlsMediaSource);
        player.prepare(hlsMediaSource);
        player.setPlayWhenReady(true);

        player.addListener(new Player.EventListener() {
            @Override
            public void onIsLoadingChanged(boolean isLoading) {
                showHideBuffring(isLoading);
            }

            @Override
            public void onPlayerStateChanged(boolean playWhenReady, int playbackState) {
                if(playbackState == 4){
                    finish();
                }else if(playbackState == Player.STATE_READY){
                    showHideBuffring(false);
                }
            }
        });

    }

    private void showHideBuffring(boolean isLoading) {
        buffering.setVisibility(isLoading ? View.VISIBLE : View.GONE);
    }


    public void resumePlayer() {
        if (player != null && video!=null && video.getSource()!=null) {
            player.setPlayWhenReady(true);
        }
    }

    public void pauseVideoPlayer(){
        if (player != null && video!=null && video.getSource()!=null) {
            player.setPlayWhenReady(false);
        }
    }

    public void stopVideoPlayer(){
        try {
           player.stop();
           if(player!=null){
               player.release();
           }

        }catch (Exception e){
            e.printStackTrace();
        }
    }

    @Override
    protected void onPause() {
        super.onPause();
        pauseVideoPlayer();
    }

    @Override
    protected void onDestroy() {
        super.onDestroy();
        pauseVideoPlayer();

    }

    @Override
    protected void onResume() {
        super.onResume();
        resumePlayer();
    }

    private void hideSystemUi() {
        getWindow().clearFlags(WindowManager.LayoutParams.FLAG_FORCE_NOT_FULLSCREEN);
        getWindow().setFlags(WindowManager.LayoutParams.FLAG_FULLSCREEN,
                WindowManager.LayoutParams.FLAG_FULLSCREEN);
        decorView.setSystemUiVisibility(
                View.SYSTEM_UI_FLAG_IMMERSIVE
                        // Set the content to appear under the system bars so that the
                        // content doesn't resize when the system bars hide and show.
                        | View.SYSTEM_UI_FLAG_LAYOUT_STABLE
                        | View.SYSTEM_UI_FLAG_LAYOUT_HIDE_NAVIGATION
                        | View.SYSTEM_UI_FLAG_LAYOUT_FULLSCREEN
                        // Hide the nav bar and status bar
                        | View.SYSTEM_UI_FLAG_HIDE_NAVIGATION
                        | View.STATUS_BAR_HIDDEN
                        | View.SYSTEM_UI_FLAG_FULLSCREEN);

        //for higher api versions.

           /* int uiOptions = View.SYSTEM_UI_FLAG_HIDE_NAVIGATION | View.SYSTEM_UI_FLAG_IMMERSIVE_STICKY;
            decorView.setSystemUiVisibility(uiOptions);*/
    }

    @Override
    public void onBackPressed() {
      //  super.onBackPressed();
        try {
            pauseVideoPlayer();
            stopVideoPlayer();
        }catch (Exception e){

        }

      super.onBackPressed();

    }

    public void getTruLiveVideo(){

        customProgressDialog.show();
        Call<TrueLiveVideoResponse> trueLiveVideoResponseCall = apiInterface.getTruLiveVideo();
        trueLiveVideoResponseCall.enqueue(new Callback<TrueLiveVideoResponse>() {
            @Override
            public void onResponse(Call<TrueLiveVideoResponse> call, Response<TrueLiveVideoResponse> response) {

                customProgressDialog.dismiss();
                if(response.code()==200){
                    if (response.body().getData().get(0).getStatus().equals("1")){
                        video = new OnDemandVideo();
//                        video.setSource("https://bitdash-a.akamaihd.net/content/sintel/hls/playlist.m3u8");
                        video.setSource(response.body().getData().get(0).getUrl());
                        playTrailerVideo(video);

                    }else if (response.body().getData().get(0).getStatus().equals("0")){
                        AlertDialog.Builder alertDialog = new AlertDialog.Builder(TureLiveVideoPlayer.this);
                        final View customLayout = getLayoutInflater().inflate(R.layout.custom_alert, null);
                        alertDialog.setView(customLayout);
                        TextView tv_message;
                        tv_message = findViewById(R.id.tv_message);
                        tv_message.setText("Live Streaming is yet to be started !!");
                        alertDialog.setPositiveButton("OK", new DialogInterface.OnClickListener() {
                            @Override
                            public void onClick(DialogInterface dialog, int which) {
                                startActivity(new Intent(TureLiveVideoPlayer.this, HomeDraweerStreamingActivity.class));
                                finish();
                            }
                        });
                        AlertDialog alert = alertDialog.create();
                        alert.setCanceledOnTouchOutside(false);
                        alert.show();

                    }
                }else {
                    showHideBuffring(false);
                    hideCustomProgress();
                    new AlertDialog.Builder(TureLiveVideoPlayer.this,R.style.AlertDialogStyle).setCancelable(false)
                            .setMessage("Video not found! try again later")
                            .setPositiveButton("Ok", new DialogInterface.OnClickListener() {
                                @Override
                                public void onClick(DialogInterface dialogInterface, int i) {
                                    finish();
                                }
                            }).show();
                }
            }

            @Override
            public void onFailure(Call<TrueLiveVideoResponse> call, Throwable t) {
                hideCustomProgress();
                showHideBuffring(false);
                new AlertDialog.Builder(TureLiveVideoPlayer.this).setCancelable(false)
                        .setMessage("Please check your internet connection!")
                        .setPositiveButton("Ok", new DialogInterface.OnClickListener() {
                            @Override
                            public void onClick(DialogInterface dialogInterface, int i) {
                                finish();
                            }
                        }).show();
            }
        });
    }

    public void hideCustomProgress(){
        try {
            customProgressDialog.dismiss();
        }catch (Exception e){

        }
    }
}