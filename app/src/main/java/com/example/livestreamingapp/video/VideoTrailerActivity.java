package com.example.livestreamingapp.video;

import android.app.AlertDialog;
import android.content.DialogInterface;
import android.content.Intent;
import android.media.session.PlaybackState;
import android.net.Uri;
import android.os.AsyncTask;
import android.os.Build;
import android.os.Bundle;
import android.util.Log;
import android.view.View;
import android.view.WindowManager;
import android.widget.FrameLayout;
import android.widget.ImageButton;
import android.widget.ProgressBar;
import android.widget.TextView;

import com.example.livestreamingapp.MassTvActivity;
import com.example.livestreamingapp.R;
import com.example.livestreamingapp.home.ui.HomeDraweerStreamingActivity;
import com.example.livestreamingapp.login.MultipleLoginSelectionActivity;
import com.example.livestreamingapp.model.CountryResponse;
import com.example.livestreamingapp.model.OnDemandVideo;
import com.example.livestreamingapp.model.ProfileData;
import com.example.livestreamingapp.utils.ConnectivityReceiver;
import com.example.livestreamingapp.utils.Constants;
import com.example.livestreamingapp.utils.DateCalculation;
import com.example.livestreamingapp.utils.DefaultController;
import com.example.livestreamingapp.utils.SharedPreferenceHandler;
import com.example.livestreamingapp.utils.Utils;
import com.example.livestreamingapp.webservice.APIClient;
import com.example.livestreamingapp.webservice.APIInterface;
import com.google.android.exoplayer2.DefaultLoadControl;
import com.google.android.exoplayer2.ExoPlaybackException;
import com.google.android.exoplayer2.ExoPlayer;
import com.google.android.exoplayer2.ExoPlayerFactory;
import com.google.android.exoplayer2.LoadControl;
import com.google.android.exoplayer2.PlaybackParameters;
import com.google.android.exoplayer2.Timeline;
import com.google.android.exoplayer2.extractor.DefaultExtractorsFactory;
import com.google.android.exoplayer2.extractor.ExtractorsFactory;
import com.google.android.exoplayer2.source.ExtractorMediaSource;
import com.google.android.exoplayer2.source.MediaSource;
import com.google.android.exoplayer2.source.MergingMediaSource;
import com.google.android.exoplayer2.source.TrackGroupArray;
import com.google.android.exoplayer2.trackselection.DefaultTrackSelector;
import com.google.android.exoplayer2.trackselection.TrackSelectionArray;
import com.google.android.exoplayer2.ui.PlayerControlView;
import com.google.android.exoplayer2.ui.PlayerView;
import com.google.android.exoplayer2.upstream.BandwidthMeter;
import com.google.android.exoplayer2.upstream.DataSource;
import com.google.android.exoplayer2.upstream.DefaultAllocator;
import com.google.android.exoplayer2.upstream.DefaultBandwidthMeter;
import com.google.android.exoplayer2.upstream.DefaultDataSourceFactory;
import com.google.android.exoplayer2.util.Util;
import com.google.gson.Gson;

import java.io.BufferedReader;
import java.io.InputStreamReader;
import java.net.HttpURLConnection;
import java.net.URL;
import java.util.HashMap;
import java.util.Map;

import okhttp3.ResponseBody;
import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;

/**
 * An example full-screen activity that shows and hides the system UI (i.e.
 * status bar and navigation/system bar) with user interaction.
 */
public class VideoTrailerActivity extends MassTvActivity {

    private PlayerView exoPlayerView;

    private ImageButton btn_skip;
    private boolean isStartVideo;
    private ExoPlayer player;
    ProgressBar buffering;
    private OnDemandVideo onDemandVideo;
    private View decorView;
    private TextView tv_title;
    private FrameLayout fl_toolbar;
    private long startTime;
    private String IPaddress;
    private APIInterface apiInterface;


    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_video_trailer);
        new GetIpAsyncTask().execute();
        startTime = System.currentTimeMillis();
        onDemandVideo = (OnDemandVideo) getIntent().getSerializableExtra(Constants.SELECTED_ON_DEMAND_VIDEO);
        Log.e("check onDemand ", "jdslfs f" + new Gson().toJson(onDemandVideo));
        startTime = System.currentTimeMillis();
        apiInterface = APIClient.getClient().create(APIInterface.class);
        decorView = getWindow().getDecorView();
        hideSystemUi();
        initViews();


    }

    private void initViews() {
        exoPlayerView = findViewById(R.id.exoPlayerView);
        btn_skip = findViewById(R.id.btn_skip);
        buffering = findViewById(R.id.buffering);
        fl_toolbar = findViewById(R.id.fl_toolbar);
        tv_title = findViewById(R.id.tv_title);

        tv_title.setText("" + onDemandVideo.getTitle());

        showHideBuffring(true);
        playTrailerVideo(onDemandVideo);
        btn_skip.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                onBackPressed();
                // finish();
            }
        });

        exoPlayerView.setControllerVisibilityListener(new PlayerControlView.VisibilityListener() {
            @Override
            public void onVisibilityChange(int visibility) {
                if (visibility == View.VISIBLE) {
                    fl_toolbar.setVisibility(View.VISIBLE);
                } else {
                    fl_toolbar.setVisibility(View.GONE);
                }
            }
        });

    }


    public void playTrailerVideo(OnDemandVideo onDemandVideo) {
        initializePlayer(onDemandVideo);
    }

    private void initializePlayer(OnDemandVideo onDemandVideo) {
/// Create a default TrackSelector
        BandwidthMeter bandwidthMeter = new DefaultBandwidthMeter();
 /*       TrackSelection.Factory videoTrackSelectionFactory =
                new AdaptiveTrackSelection.Factory(bandwidthMeter);
        TrackSelector trackSelector =
                new DefaultTrackSelector(videoTrackSelectionFactory);

        DefaultLoadControl loadControl = new DefaultLoadControl(
                new DefaultAllocator(true, C.DEFAULT_BUFFER_SEGMENT_SIZE),
                DefaultController.DEFAULT_MIN_BUFFER_MS
                , DefaultController.DEFAULT_MAX_BUFFER_MS,
                DefaultController.DEFAULT_BUFFER_FOR_PLAYBACK_MS,
                DEFAULT_BUFFER_FOR_PLAYBACK_AFTER_REBUFFER_MS,
                DefaultLoadControl.DEFAULT_TARGET_BUFFER_BYTES,
                true
        );*/

        LoadControl loadControl = new DefaultLoadControl.Builder()
                .setAllocator(new DefaultAllocator(true, 16))
                .setBufferDurationsMs( DefaultController.DEFAULT_MIN_BUFFER_MS,
                        DefaultController.DEFAULT_MAX_BUFFER_MS,
                        DefaultController.DEFAULT_BUFFER_FOR_PLAYBACK_MS,
                        DefaultController.DEFAULT_BUFFER_FOR_PLAYBACK_AFTER_REBUFFER_MS)
                .setTargetBufferBytes(-1)
                .setPrioritizeTimeOverSizeThresholds(true).createDefaultLoadControl();

//Initialize the player
        player = ExoPlayerFactory.newSimpleInstance(this, new DefaultTrackSelector(), loadControl);


        exoPlayerView.setPlayer(player);

        exoPlayerView.setUseController(true);

// Produces DataSource instances through which media data is loaded.
        DataSource.Factory dataSourceFactory =
                new DefaultDataSourceFactory(this, Util.getUserAgent(this, getResources().getString(R.string.app_name)));

// Produces Extractor instances for parsing the media data.
        ExtractorsFactory extractorsFactory = new DefaultExtractorsFactory();

// This is the MediaSource representing the media to be played.
        Uri videoUri;
        MediaSource videoSource;
        videoUri = Uri.parse(Utils.fixedSpacesUrl(onDemandVideo.getSource()));
        videoSource = new ExtractorMediaSource(videoUri,
                dataSourceFactory,
                new DefaultExtractorsFactory(), null, null);


        //  showBuffering();

        MergingMediaSource mediaSource;

    /*    if (onDemandVideo.getSrt() != null && !onDemandVideo.getSrt().isEmpty() && onDemandVideo.getSrt().contains(".srt")) {
            Uri srtUri = Uri.parse(onDemandVideo.getSrt());


            Format textFormat = Format.createTextSampleFormat(null, MimeTypes.APPLICATION_SUBRIP, null,
                    Format.NO_VALUE, Format.NO_VALUE, "en", null, Format.OFFSET_SAMPLE_RELATIVE);

            MediaSource textMediaSource = new SingleSampleMediaSource.Factory(dataSourceFactory)
                    .createMediaSource((srtUri), textFormat, C.TIME_UNSET);

            exoPlayerView.getSubtitleView().setStyle(new CaptionStyleCompat(Color.WHITE,
                    Color.TRANSPARENT, Color.TRANSPARENT, CaptionStyleCompat.EDGE_TYPE_NONE, Color.WHITE,
                    Typeface.DEFAULT_BOLD));

            exoPlayerView.getSubtitleView().setStyle(new CaptionStyleCompat(Color.WHITE,
                    Color.TRANSPARENT, Color.TRANSPARENT, CaptionStyleCompat.EDGE_TYPE_NONE, Color.WHITE,
                    Typeface.DEFAULT_BOLD));

            mediaSource = new MergingMediaSource(videoSource, textMediaSource);
            player.prepare(mediaSource);
        } else {
            player.prepare(videoSource);
        }*/

        player.prepare(videoSource);

        //   player.prepare(videoSource);
        player.addListener(new ExoPlayer.EventListener() {
            @Override
            public void onTimelineChanged(Timeline timeline, Object manifest, int reason) {

            }

            @Override
            public void onTracksChanged(TrackGroupArray trackGroups, TrackSelectionArray trackSelections) {

            }

            @Override
            public void onLoadingChanged(boolean isLoading) {

                showHideBuffring(isLoading);


            }

            @Override
            public void onPlayerStateChanged(boolean playWhenReady, int playbackState) {

                if (playbackState == android.media.session.PlaybackState.STATE_PLAYING) {

                    showHideBuffring(false);

                }
                if (playbackState == PlaybackState.STATE_PAUSED) {


                } else if (playbackState == 4) {

                    showHideBuffring(false);
                    // pauseVideoPlayer();
                    stopVideoPlayer();
                    if (startTime != 0 && ConnectivityReceiver.isConnected()) {
                        sendAnaLyticsReport();
                        startTime = 0;
                    }

                    ProfileData profileData = new SharedPreferenceHandler(VideoTrailerActivity.this).getProfileInfo();
                    if (profileData != null && profileData.getId() != 0) {
                        startActivity(new Intent(VideoTrailerActivity.this, HomeDraweerStreamingActivity.class));
                        finish();
                    } else {

                        new AlertDialog.Builder(VideoTrailerActivity.this, R.style.AlertDialogStyle).setCancelable(false)
                                .setMessage("To Enjoy More Videos Like this! Please Login")
                                .setPositiveButton("Login", new DialogInterface.OnClickListener() {
                                    @Override
                                    public void onClick(DialogInterface dialog, int which) {

                                        ProfileData profileData = new SharedPreferenceHandler(VideoTrailerActivity.this).getProfileInfo();
                                        if (profileData != null && profileData.getUserName() != null) {
                                            startActivity(new Intent(VideoTrailerActivity.this, HomeDraweerStreamingActivity.class));
                                            finish();
                                        } else {
                                            startActivity(new Intent(VideoTrailerActivity.this, MultipleLoginSelectionActivity.class));
                                            finish();
                                        }

                                    }
                                }).setNegativeButton("Cancel", new DialogInterface.OnClickListener() {
                            @Override
                            public void onClick(DialogInterface dialog, int which) {
                                finish();
                            }
                        }).show();
                    }

                }

            }

            @Override
            public void onRepeatModeChanged(int repeatMode) {

            }

            @Override
            public void onShuffleModeEnabledChanged(boolean shuffleModeEnabled) {

            }

            @Override
            public void onPlayerError(ExoPlaybackException error) {

            }

            @Override
            public void onPositionDiscontinuity(int reason) {

            }

            @Override
            public void onPlaybackParametersChanged(PlaybackParameters playbackParameters) {

            }

            @Override
            public void onSeekProcessed() {

            }
        });

        player.setPlayWhenReady(true);

    }

    private void showHideBuffring(boolean isLoading) {
        buffering.setVisibility(isLoading ? View.VISIBLE : View.GONE);
    }

    public void resumePlayer() {
        player.setPlayWhenReady(true);
    }

    public void pauseVideoPlayer() {
        player.setPlayWhenReady(false);
    }

    public void stopVideoPlayer() {
        try {
            player.stop();
            if (player != null) {
                player.release();
            }

        } catch (Exception e) {
            e.printStackTrace();
        }
    }

    @Override
    protected void onPause() {
        super.onPause();
        pauseVideoPlayer();
    }

    @Override
    protected void onDestroy() {
        super.onDestroy();
        pauseVideoPlayer();

    }

    @Override
    protected void onResume() {
        super.onResume();
        resumePlayer();
    }

    private void hideSystemUi() {
        getWindow().clearFlags(WindowManager.LayoutParams.FLAG_FORCE_NOT_FULLSCREEN);
        getWindow().setFlags(WindowManager.LayoutParams.FLAG_FULLSCREEN,
                WindowManager.LayoutParams.FLAG_FULLSCREEN);
      /*  decorView.setSystemUiVisibility(
                View.SYSTEM_UI_FLAG_IMMERSIVE
                        // Set the content to appear under the system bars so that the
                        // content doesn't resize when the system bars hide and show.
                        | View.SYSTEM_UI_FLAG_LAYOUT_STABLE
                        | View.SYSTEM_UI_FLAG_LAYOUT_HIDE_NAVIGATION
                        | View.SYSTEM_UI_FLAG_LAYOUT_FULLSCREEN
                        // Hide the nav bar and status bar
                        | View.SYSTEM_UI_FLAG_HIDE_NAVIGATION
                        | View.STATUS_BAR_HIDDEN
                        | View.SYSTEM_UI_FLAG_FULLSCREEN);*/

        if (Build.VERSION.SDK_INT < 19) {

            decorView.setSystemUiVisibility(View.GONE);
        } else {
            //for higher api versions.

            int uiOptions = View.SYSTEM_UI_FLAG_HIDE_NAVIGATION | View.SYSTEM_UI_FLAG_IMMERSIVE_STICKY;
            decorView.setSystemUiVisibility(uiOptions);
        }
    }

    @Override
    public void onBackPressed() {
        //  super.onBackPressed();

        if (startTime != 0 && onDemandVideo != null && ConnectivityReceiver.isConnected()) {
            sendAnaLyticsReport();
        }
        pauseVideoPlayer();
        stopVideoPlayer();

        super.onBackPressed();

    }

    private void sendAnaLyticsReport() {

        ProfileData profileData = new SharedPreferenceHandler(VideoTrailerActivity.this).getProfileInfo();

       String  profileId = "0";
        if(profileData!=null){
            if(profileData.getUserName()!=null){
                profileId = ""+profileData.getId();
            }
        }

        long endTime = System.currentTimeMillis();
        Map<String, String> map = new HashMap<>();

        map.put("user_id", profileId);
        map.put("video_id", "" + onDemandVideo.getId());
        map.put("video_category", "" + onDemandVideo.getCategory());
        map.put("video_start_date", "" + DateCalculation.getDateInDD_MM_YY(startTime));
        map.put("video_start_time", "" + DateCalculation.getTimeInHHmmss(startTime));
        map.put("video_end_time", "" + DateCalculation.getTimeInHHmmss(endTime));
        map.put("total_watched_duration", "" + DateCalculation.getTimeDiffinMinute(startTime, endTime,player.getDuration()));
        map.put("from_country", IPaddress);
        Log.e("check object", "" + new Gson().toJson(map));

        Call<ResponseBody> call = apiInterface.sendUserAnalyticsData(map);
        call.enqueue(new Callback<ResponseBody>() {
            @Override
            public void onResponse(Call<ResponseBody> call, Response<ResponseBody> response) {

            }

            @Override
            public void onFailure(Call<ResponseBody> call, Throwable t) {

            }
        });
    }

    public class GetIpAsyncTask extends AsyncTask<String, String, String> {


        @Override
        protected String doInBackground(String... strings) {
            return findJSONFromUrl("http://ip-api.com/json");
        }



        @Override
        protected void onPreExecute() {
            super.onPreExecute();
        }

        @Override
        protected void onPostExecute(String s) {
            super.onPostExecute(s);

            if (s.equalsIgnoreCase("error")) {

            } else {
                try {
                    IPaddress = new Gson().fromJson(s, CountryResponse.class).getCountry();
                } catch (Exception e) {

                }


            }

        }
    }

    public String findJSONFromUrl(String url) {
        String result = "";
        try {
            URL urls = new URL(url);
            HttpURLConnection conn = (HttpURLConnection) urls.openConnection();
            conn.setReadTimeout(15000); //milliseconds
            conn.setConnectTimeout(15000); // milliseconds
            conn.setRequestMethod("GET");

            conn.connect();

            if (conn.getResponseCode() == HttpURLConnection.HTTP_OK) {

                BufferedReader reader = new BufferedReader(new InputStreamReader(
                        conn.getInputStream(), "iso-8859-1"), 8);
                StringBuilder sb = new StringBuilder();
                String line = null;
                while ((line = reader.readLine()) != null) {
                    sb.append(line + "\n");
                }
                result = sb.toString();
            } else {

                return "error";
            }


        } catch (Exception e) {
            // System.out.println("exception in jsonparser class ........");
            e.printStackTrace();
            return "error";
        }

        return result;
    } // method ends

}