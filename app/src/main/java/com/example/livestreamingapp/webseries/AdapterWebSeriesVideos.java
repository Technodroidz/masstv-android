package com.example.livestreamingapp.webseries;

import android.content.Context;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.ProgressBar;
import android.widget.TextView;

import androidx.annotation.NonNull;
import androidx.recyclerview.widget.RecyclerView;

import com.bumptech.glide.Glide;
import com.bumptech.glide.request.RequestOptions;
import com.example.livestreamingapp.R;

import com.example.livestreamingapp.model.webseries.Episode;
import com.example.livestreamingapp.utils.CustomProgressDialog;
import com.example.livestreamingapp.webservice.WebServiceConstant;

import java.util.ArrayList;
import java.util.List;

public class AdapterWebSeriesVideos extends RecyclerView.Adapter<AdapterWebSeriesVideos.VideoListHolder> {

    private Context context;
    private List<Episode> videosList;
    private EpisodeCLickListener listener;

    public void addData(List<Episode> episodes) {
        this.videosList.clear();
        this.videosList = new ArrayList<>();
        this.videosList = episodes;
    }


    public interface  EpisodeCLickListener{
        public void onClickEpisode(int position,Episode episodes);
    }
    private CustomProgressDialog pDialog;

    public AdapterWebSeriesVideos(Context context, List<Episode> videosList,
                                  EpisodeCLickListener listener){
        this.context = context;
        this.videosList = videosList;
        this.listener = listener;
    }

    protected void showProgrss() {
        if (pDialog == null) {
            // pDialog = new ProgressDialog(this);
            pDialog = new CustomProgressDialog(context);
            pDialog.setCancelable(false);
        }
        if (!pDialog.isShowing()) {
            // Show progressbar
            pDialog.show();
        }
    }

    protected void dismisProgrss() {
        if (pDialog != null && pDialog.isShowing()) {
            pDialog.dismiss();
        }
    }


    @NonNull
    @Override
    public VideoListHolder onCreateViewHolder(@NonNull ViewGroup parent, int viewType) {
        View view = LayoutInflater.from(context).inflate(R.layout._item_list_webseries,parent,false);
        return new VideoListHolder(view);
    }

    @Override
    public void onBindViewHolder(@NonNull VideoListHolder holder, int position) {


        holder.tv_title.setText("Ep."+(position+1) + ". "+ videosList.get(position).getEpisodeName());

        Episode video = videosList.get(position);


        String url = null;
        if(video.getThumb()!=null){
            if(!video.getThumb().contains("http")){
                url = WebServiceConstant.IMAGE_URL+video.getThumb();
            }else {
                url = video.getThumb();
            }
        }
        holder.progress_playback.setVisibility(View.GONE);

        RequestOptions requestOptions = new RequestOptions();
        requestOptions.placeholder(R.drawable.placeholder_black);
        requestOptions.error(R.drawable.placeholder_black);


        try {
            Glide.with(context).setDefaultRequestOptions(requestOptions)
                    .load(video.getThumb()).into(holder.iv_image);

        }catch (Exception e){

        }

        if(context instanceof WebSeriesVideoDetailActivity){
            holder.iv_now_playing.setVisibility(View.GONE);
        }


        if(videosList.get(position).getDuration()!=null && !video.getPlaybackTime().contains(":")){
            holder.progress_playback.setVisibility(View.VISIBLE);
            int progress = (int) getLastPlayPackPercentage(videosList.get(position).getDuration()
                    ,videosList.get(position).getPlaybackTime());
          //  Log.e("checck percentage ",""+progress);
            holder.progress_playback.setProgress(progress);

        }


    }

    /**
     * Add +1 extra size to show 'More Video' option
     * @return
     */
    @Override
    public int getItemCount() {
        return this.videosList.size();
    }

    public class VideoListHolder extends RecyclerView.ViewHolder {

        private ImageView iv_now_playing;
        private ImageView iv_image;
        private TextView tv_title;
        private ProgressBar progress_playback;
        public VideoListHolder(@NonNull View itemView) {
            super(itemView);
            iv_now_playing = (ImageView)itemView.findViewById(R.id.iv_now_playing);
            iv_image = (ImageView)itemView.findViewById(R.id.iv_image);
            tv_title = (TextView) itemView.findViewById(R.id.tv_title);
            progress_playback = (ProgressBar) itemView.findViewById(R.id.progress_playback);

            itemView.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View v) {
                    listener.onClickEpisode(getLayoutPosition(),videosList.get(getAdapterPosition()));
                }
            });



        }
    }

    public int getSize(){
        return videosList.size();
    }


    @Override
    public int getItemViewType(int position) {
        return super.getItemViewType(position);

    }

    public long getLastPlayPackPercentage(String durationInHHmmSS,String timeInMillis){
        if(!timeInMillis.contains(":")){
            try {
                long duration = getTimeInMillis(durationInHHmmSS);
                long playpackTime = Long.parseLong(timeInMillis);
                return ((playpackTime*100)/duration);
            }catch (Exception e){

            }

        }

       return 0;
    }

    public static long getTimeInMillis(String duration) {

        String h = duration.split(":")[0];
        String m = duration.split(":")[1];
        String s = duration.split(":")[2];

        return ((Integer.parseInt(h)*3600)+(Integer.parseInt(m)*60)+Integer.parseInt(s))*1000;
     /*
        Calendar calendar = null;
        DateFormat dateFormat = new SimpleDateFormat("HH:mm:ss");
        try {
            Date date = dateFormat.parse(duration);
            calendar = Calendar.getInstance();
            calendar.setTime(date);
        } catch (ParseException e) {
            e.printStackTrace();
        }
        return calendar.getTimeInMillis();*/
    }

}
