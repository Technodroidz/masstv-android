package com.example.livestreamingapp.webseries;

import android.app.Activity;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ArrayAdapter;
import android.widget.TextView;

import com.example.livestreamingapp.R;

import java.util.List;

public class CustomAdapter extends ArrayAdapter<String> {

    LayoutInflater flater;
    public CustomAdapter(Activity context, int resouceId, int textviewId, List<String> list){
    super(context,resouceId,textviewId, list);

    flater = context.getLayoutInflater();
}

    @Override
    public View getView(int position, View convertView, ViewGroup parent) {

        String rowItem = getItem(position);

        View rowview = flater.inflate(R.layout.listitems_layout,null,true);

        TextView txtTitle = (TextView) rowview.findViewById(R.id.title);
        txtTitle.setText(rowItem);


        return rowview;
    }
}