package com.example.livestreamingapp.webseries;

import android.content.Intent;
import android.graphics.Color;
import android.graphics.drawable.ColorDrawable;
import android.graphics.drawable.InsetDrawable;
import android.net.Uri;
import android.os.Bundle;
import android.os.Handler;
import android.util.Log;
import android.view.MotionEvent;
import android.view.View;
import android.widget.AdapterView;
import android.widget.ArrayAdapter;
import android.widget.Button;
import android.widget.ImageView;
import android.widget.RatingBar;
import android.widget.Spinner;
import android.widget.TextView;

import androidx.appcompat.app.AlertDialog;
import androidx.appcompat.app.AppCompatActivity;
import androidx.recyclerview.widget.LinearLayoutManager;
import androidx.recyclerview.widget.RecyclerView;
import androidx.viewpager.widget.ViewPager;

import com.bumptech.glide.Glide;
import com.example.livestreamingapp.MassTvActivity;
import com.example.livestreamingapp.R;
import com.example.livestreamingapp.home.ui.HomeDraweerStreamingActivity;
import com.example.livestreamingapp.home.ui.adapter.EndlessPagerAdapter;
import com.example.livestreamingapp.home.ui.adapter.ViewPagerAdapter;
import com.example.livestreamingapp.model.CategoryData;
import com.example.livestreamingapp.model.OnDemandCategory;
import com.example.livestreamingapp.model.ProfileData;
import com.example.livestreamingapp.model.WatchLaterResponse;
import com.example.livestreamingapp.model.homescreen.PromotionBanner;
import com.example.livestreamingapp.model.mastercategory.OnDemandMasterCategory;
import com.example.livestreamingapp.model.webseries.ContentDetail;
import com.example.livestreamingapp.model.webseries.ContentInfo;
import com.example.livestreamingapp.model.webseries.Episode;
import com.example.livestreamingapp.model.webseries.Season;
import com.example.livestreamingapp.model.webseries.WebSeriesResponse;
import com.example.livestreamingapp.model.webseries.WebseriesDetail;
import com.example.livestreamingapp.utils.BottomNavigationHelper;
import com.example.livestreamingapp.utils.Constants;
import com.example.livestreamingapp.utils.CustomProgressDialog;
import com.example.livestreamingapp.utils.SharedPreferenceHandler;
import com.example.livestreamingapp.utils.Utils;
import com.example.livestreamingapp.webservice.APIClient;
import com.example.livestreamingapp.webservice.APIInterface;
import com.example.livestreamingapp.webservice.GetVideoCategoryList;
import com.example.livestreamingapp.webservice.WatchLaterService;
import com.example.livestreamingapp.webservice.WebServiceConstant;
import com.google.gson.Gson;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import okhttp3.ResponseBody;
import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;

import static com.example.livestreamingapp.webseries.WebSeriesVideoDetailActivity.backupContentDetail;

public class WebSeriesDetailActivity extends MassTvActivity implements AdapterWebSeriesVideos.EpisodeCLickListener
        , View.OnClickListener {

    private ImageView iv_season_banner;
    private TextView tv_series_name;
    private TextView tv_season_rating;
    private TextView tv_season_description;

    private ImageView iv_rate_season;
    private ImageView iv_watchlater;

    private Spinner sp_season;

    private RecyclerView rv_episodes_list;
    private ViewPager viewPagerBanner;

    ContentDetail content_detail;
    APIInterface apiInterface;

    CustomProgressDialog customProgressDialog;
    private AdapterWebSeriesVideos adapterWebSeriesVideos;
    private List<Episode> episodes = new ArrayList<>();

    private List<String> seasonList = new ArrayList<>();

    private List<PromotionBanner> promotionBanners;
    private ViewPagerAdapter mViewPagerAdapter;
    private int bannerPosition;
    private EndlessPagerAdapter endlessPagerAdapter;
    private ArrayList<OnDemandCategory> categoryList;
    private Runnable bannerRunnable;
    private Handler bannerHandler;
    private boolean isRatingAlertShow;
    private ProfileData profileData;
    private static List<Season> seasons;
    private List<Episode> totalEpisode;

    private BottomNavigationHelper bottomNavigationHelper;
    private int selectedSeasonPosition = -1;
    private boolean isSecondTime;


    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_web_series_detail);
        seasons = new ArrayList<>();
        bottomNavigationHelper = new BottomNavigationHelper(this);
        content_detail = (ContentDetail) getIntent().getSerializableExtra(Constants.SELECTED_ON_DEMAND_VIDEO);
        Log.e("check is watchLater", " " + new Gson().toJson(content_detail));
        apiInterface = APIClient.getClient().create(APIInterface.class);
        profileData = new SharedPreferenceHandler(this).getProfileInfo();
        initViews();
        if (content_detail != null && content_detail.getContentInfo() != null) {
            setWebSeriesInfoData();
        }
        getWebSeriesData();
        setSeasonAdapter();


    }

    private void setWebSeriesInfoData() {

        try {
            Glide.with(WebSeriesDetailActivity.this).load(content_detail.getContentInfo()
                    .getThumb()).into(iv_season_banner);

        } catch (Exception e) {
            e.printStackTrace();
        }


        tv_season_description.setText("" + content_detail.getContentInfo().getSubtitle());
        tv_series_name.setText("" + content_detail.getContentInfo().getTitle());
        tv_season_rating.setText("" + Utils.getDecimalOnePoint(content_detail.getContentInfo().getAvgRate()));

        setWatchList();

    }

    /**
     * @Rehan Initialize All views here
     */
    private void initViews() {
        episodes = new ArrayList<>();
        adapterWebSeriesVideos = new AdapterWebSeriesVideos(this, episodes, this);
        customProgressDialog = new CustomProgressDialog(WebSeriesDetailActivity.this);
        customProgressDialog.setCancelable(false);
        iv_season_banner = findViewById(R.id.iv_season_banner);
        tv_series_name = findViewById(R.id.tv_series_name);
        tv_season_rating = findViewById(R.id.tv_season_rating);
        tv_season_description = findViewById(R.id.tv_season_description);
        iv_rate_season = findViewById(R.id.iv_rate_season);
        iv_watchlater = findViewById(R.id.iv_watchlater);
        sp_season = findViewById(R.id.sp_season);
        rv_episodes_list = findViewById(R.id.rv_episodes_list);
        viewPagerBanner = findViewById(R.id.viewPagerBanner);

        LinearLayoutManager layoutManager = new LinearLayoutManager(this);
        layoutManager.setOrientation(RecyclerView.HORIZONTAL);
        rv_episodes_list.setLayoutManager(layoutManager);
        rv_episodes_list.setAdapter(adapterWebSeriesVideos);

        bannerHandler = new Handler();
        promotionBanners = new ArrayList<>();

        mViewPagerAdapter = new ViewPagerAdapter(WebSeriesDetailActivity.this,
                promotionBanners, new ViewPagerAdapter.BannerListener() {
            @Override
            public void onBannerListener(PromotionBanner promotionBanner) {

                Intent intentBrowser = new Intent(Intent.ACTION_VIEW);
                Uri u = Uri.parse(promotionBanner.getWebsiteUrl());
                intentBrowser.setData(u);
                startActivity(intentBrowser);

            }
        });


        // Adding the Adapter to the ViewPager
        viewPagerBanner.setAdapter(mViewPagerAdapter);

        /*  viewPagerBanner.setCurrentItem(1);*/


        // banner change every 2 second
        categoryList = new ArrayList<>();
        bannerRunnable = new Runnable() {
            @Override
            public void run() {
                if (promotionBanners != null && promotionBanners.size() > 0) {
                    bannerPosition = viewPagerBanner.getCurrentItem() + 1;
                    viewPagerBanner.setCurrentItem(bannerPosition);
                    startBannerHandler();
                }
            }
        };
        iv_watchlater.setOnClickListener(this);
        iv_rate_season.setOnClickListener(this);
        fetchCategoryList();

    }


    /**
     * @Rehan Get Banner List from category
     * since Every category has one banner
     */
    private void fetchCategoryList() {
        new GetVideoCategoryList(this, new GetVideoCategoryList.onDemandCategoryListListener() {


            @Override
            public void onFetchedCategoryList(CategoryData categoryData) {

                try {
                    promotionBanners.clear();
                    promotionBanners = new ArrayList<>();
                    promotionBanners.addAll(categoryData.getPromotion_list());
                    if (promotionBanners.size() == 0) {

                        viewPagerBanner.setVisibility(View.GONE);
                        // fetchCategoryList();
                    } else {
                        viewPagerBanner.setVisibility(View.VISIBLE);
                        // iv_banner.setVisibility(View.VISIBLE);
                        promotionBanners.size();
                        mViewPagerAdapter.addBannerList(categoryData.getPromotion_list());
                        mViewPagerAdapter.notifyDataSetChanged();
                        endlessPagerAdapter = new EndlessPagerAdapter(mViewPagerAdapter, viewPagerBanner);
                        viewPagerBanner.setAdapter(endlessPagerAdapter);
                        startBannerHandler();

                    }
                } catch (Exception e) {
                    e.printStackTrace();
                }

            }

            @Override
            public void onFetchedMasterCategoryList(List<OnDemandMasterCategory> videoData) {

            }

            @Override
            public void onCategoryFetchingError(String error) {

            }
        }).GetCategoryList();
    }


    private void startBannerHandler() {
        stopBannerHandler();
        try {
            if (bannerHandler != null) {
                bannerHandler.postDelayed(bannerRunnable, 5000);
            }
        } catch (Exception e) {

        }
    }

    private void stopBannerHandler() {
        try {
            bannerHandler.removeCallbacks(bannerRunnable);
        } catch (Exception e) {

        }

    }


    /**
     * @Rehan
     */
    public void getWebSeriesData() {
        customProgressDialog.show();
        ProfileData profileData = new SharedPreferenceHandler(WebSeriesDetailActivity.this).getProfileInfo();
        Map<String, String> map = new HashMap<>();
        map.put("user_id", "" + profileData.getId());
        map.put("video_id", "" + content_detail.getContentInfo().getId());
        Call<WebSeriesResponse> webSeriesResponseCall = apiInterface.fetchWebSeriesData(map);
        webSeriesResponseCall.enqueue(new Callback<WebSeriesResponse>() {
            @Override
            public void onResponse(Call<WebSeriesResponse> call, Response<WebSeriesResponse> response) {
                customProgressDialog.dismiss();
                if (response != null && response.code() == 200 && response.body() != null && response.body().getSuccess()) {
                   /* Content_info contentInfoOld = new Content_info();
                    contentInfoOld = content_detail.getContent_info();
               */
                    content_detail = response.body().getData().getContentDetail();
                    backupContentDetail = response.body().getData().getContentDetail();
                    //content_detail.setContent_info(contentInfoOld);


                    if (content_detail != null && content_detail.getWebseriesDetail() != null && content_detail.getWebseriesDetail().getSeasons() != null
                            && content_detail.getWebseriesDetail().getSeasons().size() > 0) {
                        seasons = content_detail.getWebseriesDetail().getSeasons();
                        for (int i = 0; i < seasons.size(); i++) {
                            if (seasons.get(i).getId().equals(content_detail.getContentInfo().getId())) {
                                episodes = seasons.get(i).getEpisodes();
                                selectedSeasonPosition = i;
                            }
                        }

                        new SharedPreferenceHandler(WebSeriesDetailActivity.this).saveSeason(seasons);
                      /*  if (episodes.size() > 0) {
                            episodes = backupContentDetail.getWebseriesDetail().getSeasons().get(0).getEpisodes();
                        }*/

                        updateContentInfo(selectedSeasonPosition);

                        setSeasonAdapter();
                        setWebSeriesInfoData();
                        setEpisodeAdapter();

                        if(sp_season!=null && selectedSeasonPosition!=-1){
                            sp_season.setSelection(selectedSeasonPosition);
                            selectedSeasonPosition = -1;
                            isSecondTime = true;
                        }
                    }


                }


            }

            @Override
            public void onFailure(Call<WebSeriesResponse> call, Throwable t) {
                customProgressDialog.dismiss();
            }
        });
    }

    private void setSeasonAdapter() {

        seasonList.clear();
        seasonList = new ArrayList<>();
        if (content_detail.getWebseriesDetail() != null &&
                content_detail.getWebseriesDetail().getSeasons().size() > 0) {

            for (int i = 0; i < content_detail.getWebseriesDetail().getSeasons().size(); i++) {
                seasonList.add("Season " + (i + 1));
            }
            sp_season.setVisibility(View.VISIBLE);
            ArrayAdapter<String> dataAdapter = new ArrayAdapter<String>(this,
                    R.layout.items_layout, seasonList);
            dataAdapter.setDropDownViewResource(R.layout.listitems_layout);


            sp_season.setOnItemSelectedListener(new AdapterView.OnItemSelectedListener() {
                @Override
                public void onItemSelected(AdapterView<?> parent, View view, int position, long id) {

                   /* episodes = backupContentDetail.getWebseriesDetail().getSeasons().get(position).getEpisodes();
                    setEpisodeAdapter();*/

                    try {
                        if (content_detail.getWebseriesDetail().getSeasons().get(position).getId() !=
                                content_detail.getContentInfo().getId() && isSecondTime) {
                            seasons = new SharedPreferenceHandler(WebSeriesDetailActivity.this).getSeasonList();

                            episodes.clear();
                            episodes = new ArrayList<>();
                            episodes = seasons.get(position).getEpisodes();

                            updateContentInfo(position);

                            WebseriesDetail webseriesDetail = new WebseriesDetail();
                            webseriesDetail.setSeasons(seasons);

                            content_detail.setWebseriesDetail(webseriesDetail);

                            setWebSeriesInfoData();
                            setEpisodeAdapter();

                            Log.e("check ep after", "" + new Gson().toJson(backupContentDetail.getWebseriesDetail().getSeasons().get(position)));

                        }
                    } catch (Exception e) {
                        e.printStackTrace();
                    }

                }

                @Override
                public void onNothingSelected(AdapterView<?> parent) {

                }
            });


            sp_season.setAdapter(dataAdapter);



          /*  for (int i = 0; i < content_detail.getWebseriesDetail().getSeasons().size(); i++) {
                if (content_detail.getContentInfo().getId().equals(content_detail.getWebseriesDetail().getSeasons().get(i).getId())) {
                    sp_season.setSelection(i);
                }
            }
*/
        } else {
            sp_season.setVisibility(View.GONE);
        }
    }

    private void setEpisodeAdapter() {
        // episodes = backupContentDetail.getWebseriesDetail().getSeasons().get(0).getEpisodes();
        if (episodes != null && episodes.size() > 0) {
            adapterWebSeriesVideos.addData(episodes);
            adapterWebSeriesVideos.notifyDataSetChanged();
        }

    }

    @Override
    public void onClickEpisode(int position, Episode episodes) {
        Intent intent = new Intent(this, WebSeriesVideoDetailActivity.class);
        intent.putExtra(Constants.SELECTED_ON_DEMAND_VIDEO, episodes);
        intent.putExtra(Constants.SELECTED_WEBSERIES, content_detail);
        startActivity(intent);
        finish();
    }

    @Override
    public void onClick(View v) {
        switch (v.getId()) {

            case R.id.iv_rate_season:
                if (!isRatingAlertShow && !content_detail.getContentInfo().getIsRated()) {
                   // showRatingAlert();
                }
                break;
            case R.id.iv_watchlater:
                customProgressDialog.show();
                new WatchLaterService(this, new WatchLaterService.OnWatchLaterResponse() {
                    @Override
                    public void onResponse(WatchLaterResponse response) {
                        // set color
                        customProgressDialog.dismiss();
                        if (response.getSuccess()) {
                            content_detail.getContentInfo().setIsWatchLater(!content_detail.getContentInfo().getIsWatchLater());
                            setWatchList();
                        }

                    }

                    @Override
                    public void onError(String error) {
                        customProgressDialog.dismiss();
                    }
                }).addToWatchListVideo(content_detail.getContentInfo().getId(), content_detail.getContentInfo().getIsWatchLater() ? "remove" : "add",
                        content_detail.getContentInfo().getCategory());
                break;
        }
    }


    private void setWatchList() {

        if (isTablet()) {
            iv_watchlater.setImageResource(
                    (content_detail.getContentInfo().getIsWatchLater() ? R.drawable.ic_watch_list_36 : R.drawable.ic_watch_later_36));

        } else {
            iv_watchlater.setImageResource(
                    (content_detail.getContentInfo().getIsWatchLater() ? R.drawable.ic_watched_list : R.drawable.ic_watch_later));

        }

        // tv_btn_watchlater.setTextColor(getResources().getColor(onDemandVideo.isWatchList()?R.color.green:R.color.white));
    }

    /**
     * @return
     * @Rehan check if tablet or not
     */
    public boolean isTablet() {
       /* return (getResources().getConfiguration().screenLayout
                & Configuration.SCREENLAYOUT_SIZE_MASK)
                >= Configuration.SCREENLAYOUT_SIZE_LARGE;*/

        return getResources().getBoolean(R.bool.isTablet);


    }


    /**
     * @Rehan
     * Give Rating after video End only one time
     */

   /* public void showRatingAlert() {

        isRatingAlertShow = true;

        AlertDialog.Builder builder
                = new AlertDialog.Builder(this);
        // set the custom layout
        final View customLayout
                = getLayoutInflater()
                .inflate(
                        R.layout.layout_alert_rating,
                        null);
        builder.setView(customLayout);

        TextView tv_title = customLayout.findViewById(R.id.tv_title);
        Button btn_rate = customLayout.findViewById(R.id.btn_rate);
        Button btn_cancel = customLayout.findViewById(R.id.btn_cancel);

        tv_title.setText("Rate this Season");

        RatingBar ratingBar = customLayout.findViewById(R.id.rateBar);
        ratingBar.setRating(1);
        if (Float.parseFloat(content_detail.getContentInfo().getUserRating()) > 0) {
            ratingBar.setRating(Float.parseFloat(content_detail.getContentInfo().getUserRating()));
        }
        ratingBar.setOnRatingBarChangeListener(new RatingBar.OnRatingBarChangeListener() {
            private float rating;

            @Override
            public void onRatingChanged(RatingBar ratingBar, float rating, boolean fromUser) {
                this.rating = rating;
                if (rating >= 0 && rating <= 1) {
                    this.rating = 1;
                } else if (rating > 1 && rating <= 2) {
                    this.rating = 2;
                } else if (rating > 2 && rating <= 3) {
                    this.rating = 3;
                } else if (rating > 3 && rating <= 4) {
                    this.rating = 4;
                } else if (rating > 4 && rating <= 5) {
                    this.rating = 5;
                }

                ratingBar.setRating(this.rating);
            }
        });

        final AlertDialog dialog
                = builder.create();
        dialog.setCancelable(false);

        dialog.getWindow().setBackgroundDrawableResource(android.R.color.transparent);

        ColorDrawable back = new ColorDrawable(Color.TRANSPARENT);
        InsetDrawable inset = new InsetDrawable(back, 40, 0, 40, 0);
        if (dialog.getWindow() != null) {
            dialog.getWindow().setBackgroundDrawable(inset);
        }

//          int myRating = (int) ratingBar.getRating();
        int myRating = (int) 2;

        btn_rate.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                isRatingAlertShow = false;
                dialog.dismiss();
                customProgressDialog.show();
                if (ratingBar.getRating() < 1) {
                    ratingBar.setRating(1);
                }
                Map<String, Integer> ratingCall = new HashMap<>();
                ratingCall.put(WebServiceConstant.USER_ID, profileData.getId());
                ratingCall.put(WebServiceConstant.VIDEO_ID, content_detail.getContentInfo().getId());
                ratingCall.put(WebServiceConstant.RATING, (int) ratingBar.getRating());

                // todo
                Call<ResponseBody> call = apiInterface.updateRating(ratingCall);
                call.enqueue(new Callback<ResponseBody>() {
                    @Override
                    public void onResponse(Call<ResponseBody> call, Response<ResponseBody> response) {
                        customProgressDialog.dismiss();
                        HomeDraweerStreamingActivity.isResume = true;
                        getWebSeriesData();
                        successFullAlert("Rating Submitted Successfully", "Rating Submitted Successfully");
                    }

                    @Override
                    public void onFailure(Call<ResponseBody> call, Throwable t) {
                        customProgressDialog.dismiss();
                    }
                });

            }
        });

        btn_cancel.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                dialog.dismiss();
                isRatingAlertShow = false;
            }
        });

        // create and show
        // the alert dialog

        dialog.show();

    }*/

    /**
     * @Rehan Show successfull Alert
     */
    public void successFullAlert(String title, String message) {
        // Create an alert builder
        AlertDialog.Builder builder
                = new AlertDialog.Builder(this);
        // set the custom layout
        final View customLayout
                = getLayoutInflater()
                .inflate(
                        R.layout.layout_alert_loaded,
                        null);
        builder.setView(customLayout);

        Button btn_ok = customLayout.findViewById(R.id.tv_btn_upload);
        TextView tv_title = customLayout.findViewById(R.id.tv_result);
        TextView tv_message = customLayout.findViewById(R.id.tv_message);
        Button btn_cancel = customLayout.findViewById(R.id.btn_cancel);

        btn_cancel.setVisibility(View.GONE);

        tv_title.setText(title);
        tv_message.setText(message);

        final AlertDialog dialog
                = builder.create();
        dialog.setCancelable(false);

        ColorDrawable back = new ColorDrawable(Color.TRANSPARENT);
        InsetDrawable inset = new InsetDrawable(back, 60, 0, 60, 0);
        if (dialog.getWindow() != null) {
            dialog.getWindow().setBackgroundDrawable(inset);

        }

        btn_ok.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                dialog.dismiss();
            }
        });

        btn_cancel.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                dialog.dismiss();
            }
        });


        // create and show
        // the alert dialog

        dialog.show();
    }

    public void updateContentInfo(int position){
        Season season = seasons.get(position);
        ContentInfo content_info = new ContentInfo();
        content_info.setIsRated(season.getIsRated());
        content_info.setAvgRate(season.getAvgRate());
        content_info.setTitle(season.getTitle());
        content_info.setSubtitle(season.getSubtitle());
        content_info.setThumb(season.getThumb());
        content_info.setTotalUserRated(season.getTotalUserRated());
        content_info.setUserRating(String.valueOf((int) season.getUserRating()));
        content_info.setBannerImage(season.getBannerImage());
        content_info.setCategory(season.getCategory());
        content_info.setIsWatchLater(season.getIsWatchLater());
        content_info.setTags(season.getTags());
        content_info.setDuration(season.getDuration());
        content_info.setId(season.getId());
        content_detail.setContentInfo(content_info);

        WebseriesDetail webseriesDetail = new WebseriesDetail();
        webseriesDetail.setSeasons(seasons);

        content_detail.setWebseriesDetail(webseriesDetail);
    }


}