package com.example.livestreamingapp.webseries;

import android.content.Intent;
import android.content.pm.ActivityInfo;
import android.content.res.Configuration;
import android.graphics.Color;
import android.graphics.Point;
import android.graphics.Typeface;
import android.graphics.drawable.ColorDrawable;
import android.graphics.drawable.InsetDrawable;
import android.net.Uri;
import android.os.Build;
import android.os.Bundle;
import android.os.Handler;
import android.support.v4.media.session.PlaybackStateCompat;
import android.util.Log;
import android.view.Menu;
import android.view.MenuItem;
import android.view.MotionEvent;
import android.view.View;
import android.view.WindowManager;
import android.widget.Button;
import android.widget.FrameLayout;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.ProgressBar;
import android.widget.RatingBar;
import android.widget.SeekBar;
import android.widget.TextView;

import androidx.appcompat.app.AlertDialog;
import androidx.appcompat.widget.AppCompatSeekBar;
import androidx.appcompat.widget.Toolbar;
import androidx.recyclerview.widget.LinearLayoutManager;
import androidx.recyclerview.widget.RecyclerView;
import androidx.viewpager.widget.ViewPager;

import com.example.livestreamingapp.MassTvActivity;
import com.example.livestreamingapp.MassTvApplication;
import com.example.livestreamingapp.MediaItem;
import com.example.livestreamingapp.R;
import com.example.livestreamingapp.expandedcontrols.ExpandedControlsActivity;
import com.example.livestreamingapp.home.ui.HomeDraweerStreamingActivity;
import com.example.livestreamingapp.home.ui.adapter.AdapterVideoList;
import com.example.livestreamingapp.home.ui.adapter.EndlessPagerAdapter;
import com.example.livestreamingapp.home.ui.adapter.ViewPagerAdapter;
import com.example.livestreamingapp.model.CapturePlayBack;
import com.example.livestreamingapp.model.CategoryData;
import com.example.livestreamingapp.model.MySubscription;
import com.example.livestreamingapp.model.OnDemandCategory;
import com.example.livestreamingapp.model.OnDemandVideo;
import com.example.livestreamingapp.model.ProfileData;
import com.example.livestreamingapp.model.homescreen.PromotionBanner;
import com.example.livestreamingapp.model.mastercategory.OnDemandMasterCategory;
import com.example.livestreamingapp.model.webseries.ContentDetail;
import com.example.livestreamingapp.model.webseries.Episode;
import com.example.livestreamingapp.ondemand.AdapterViewClickListener;
import com.example.livestreamingapp.ondemand.VideoListActivity;
import com.example.livestreamingapp.subscription.SubscriptionSelectorActivity;
import com.example.livestreamingapp.utils.ConnectivityReceiver;
import com.example.livestreamingapp.utils.Constants;
import com.example.livestreamingapp.utils.CustomProgressDialog;
import com.example.livestreamingapp.utils.DateCalculation;
import com.example.livestreamingapp.utils.HorizontalItemDecoration;
import com.example.livestreamingapp.utils.SharedPreferenceHandler;
import com.example.livestreamingapp.utils.Utils;
import com.example.livestreamingapp.video.CacheDataSourceFactory;
import com.example.livestreamingapp.webservice.APIClient;
import com.example.livestreamingapp.webservice.APIInterface;
import com.example.livestreamingapp.webservice.GetOnDemandVideoList;
import com.example.livestreamingapp.webservice.GetVideoCategoryList;
import com.example.livestreamingapp.webservice.SubscriptionService;
import com.example.livestreamingapp.webservice.WebServiceConstant;
import com.facebook.shimmer.ShimmerFrameLayout;
import com.google.android.exoplayer2.C;
import com.google.android.exoplayer2.DefaultLoadControl;
import com.google.android.exoplayer2.ExoPlaybackException;
import com.google.android.exoplayer2.ExoPlayerFactory;
import com.google.android.exoplayer2.Format;
import com.google.android.exoplayer2.LoadControl;
import com.google.android.exoplayer2.PlaybackParameters;
import com.google.android.exoplayer2.SimpleExoPlayer;
import com.google.android.exoplayer2.Timeline;
import com.google.android.exoplayer2.extractor.DefaultExtractorsFactory;
import com.google.android.exoplayer2.extractor.ExtractorsFactory;
import com.google.android.exoplayer2.source.ExtractorMediaSource;
import com.google.android.exoplayer2.source.MediaSource;
import com.google.android.exoplayer2.source.MergingMediaSource;
import com.google.android.exoplayer2.source.SingleSampleMediaSource;
import com.google.android.exoplayer2.source.TrackGroupArray;
import com.google.android.exoplayer2.text.CaptionStyleCompat;
import com.google.android.exoplayer2.trackselection.DefaultTrackSelector;
import com.google.android.exoplayer2.trackselection.TrackSelectionArray;
import com.google.android.exoplayer2.ui.AspectRatioFrameLayout;
import com.google.android.exoplayer2.ui.PlayerView;
import com.google.android.exoplayer2.upstream.BandwidthMeter;
import com.google.android.exoplayer2.upstream.DataSource;
import com.google.android.exoplayer2.upstream.DefaultAllocator;
import com.google.android.exoplayer2.upstream.DefaultBandwidthMeter;
import com.google.android.exoplayer2.upstream.DefaultDataSourceFactory;
import com.google.android.exoplayer2.util.MimeTypes;
import com.google.android.exoplayer2.util.Util;
import com.google.android.gms.cast.MediaInfo;
import com.google.android.gms.cast.MediaLoadRequestData;
import com.google.android.gms.cast.MediaMetadata;
import com.google.android.gms.cast.framework.CastButtonFactory;
import com.google.android.gms.cast.framework.CastContext;
import com.google.android.gms.cast.framework.CastSession;
import com.google.android.gms.cast.framework.SessionManagerListener;
import com.google.android.gms.cast.framework.media.RemoteMediaClient;
import com.google.android.material.appbar.AppBarLayout;
import com.google.android.material.snackbar.Snackbar;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import java.util.Timer;
import java.util.TimerTask;

import okhttp3.ResponseBody;
import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;

public class WebSeriesVideoDetailActivity extends MassTvActivity implements
        View.OnClickListener, AdapterViewClickListener, SimpleExoPlayer.EventListener, AdapterWebSeriesVideos.EpisodeCLickListener,
        ConnectivityReceiver.ConnectivityReceiverListener {

    private int INDEX_SIZE = 0;
    private RecyclerView rv_on_demand;
    private RecyclerView rv_trending;

    private RecyclerView rv_live_streaming;
    private TextView tv_label_streaming;

    private static final String TAG = WebSeriesVideoDetailActivity.class.getName();
    private SimpleExoPlayer player;

    private ImageView iv_play_pause;
    FrameLayout fl_controller_portrait;
    FrameLayout fl_controller_landscape;
    private int playbackPosition = 0;
    AppCompatSeekBar mSeekbar;

    private Runnable runnable;
    boolean isVideoPlaying;
    private PlaybackState mPlaybackState;
    private Timer mSeekbarTimer;
    private Timer mControllersTimer;
    private PlaybackLocation mLocation;
    private Handler mHandler;
    private CastSession mCastSession;
    private SessionManagerListener<CastSession> mSessionManagerListener;
    private MediaItem mSelectedMedia;
    private CastContext mCastContext;

    private ImageView iv_full_screen_toggle;
    private int requestedOrientation;
    private final float mAspectRatio = 72f / 128;
    private AppBarLayout appBarLayout;
    private LinearLayout lin_content;
    private ImageView iv_full_screen_toggle_landscape;
    private ImageView iv_play_pause_landscape;
    private SeekBar mSeekbar_landscape;

    private PlayerView simpleExoPlayerView_portrait;
    private PlayerView simpleExoPlayerView_landscape;

    public static final int DEFAULT_MIN_BUFFER_MS = 60000;
    public static final int DEFAULT_MAX_BUFFER_MS = 60000;
    public static final int DEFAULT_BUFFER_FOR_PLAYBACK_MS = 10000;
    public static final int DEFAULT_BUFFER_FOR_PLAYBACK_AFTER_REBUFFER_MS = 5000;

    Episode onDemandVideo;
    private ProgressBar buffering_portrait;
    private ProgressBar buffering_landscape;

    Handler userInteractionHandler;
    Runnable userInteractionRunnable;

    private TextView tv_category_heading;

    private TextView tv_title_landscape;
    private TextView tv_video_title;
    private TextView tv_video_subtitle;
    private TextView tv_toolbar_title;
    private ImageView tv_btn_watchlater;

    private ShimmerFrameLayout shimmer_view_container;

    private MySubscription mySubscription;
    private SharedPreferenceHandler sharedPreferenceHandler;

    private ImageView iv_next_portrait;
    private ImageView iv_previous_portrait;
    private ImageView iv_next_landscape;
    private ImageView iv_previous_landscape;
    private ArrayList<Episode> videosList;

    public boolean isPlayingAd;
    public boolean isStartVideo = false;
    private MenuItem mediaRouteMenuItem;
    private AdapterWebSeriesVideos similarVideoAdapter;
    private ProfileData profileData;
    private CustomProgressDialog pDialog;
    private APIInterface apiInterface;
    private boolean isRatingAlertShow;
    private ImageView iv_btn_rate;
    private TextView tv_rating;

    private TextView tv_title;

    private ImageView iv_caption_portrait;
    private ImageView iv_caption_landscape;

    private TextView tv_time_portrait;
    private TextView tv_time_lanscape;
    private String totalVideoTime;
    private boolean isFirstTime;
    private boolean isRated;
    private boolean isRatingEnable;

    private ImageView iv_banner;
    private Handler bannerHandler;
    private Runnable bannerRunnable;
    public List<OnDemandCategory> categoryList;

    private List<PromotionBanner> promotionBanners;
    private ViewPagerAdapter mViewPagerAdapter;
    private int bannerPosition;
    private ViewPager viewPagerBanner;
    private EndlessPagerAdapter endlessPagerAdapter;
    ContentDetail content_detail;
    private CustomProgressDialog customProgressDialog;

    public static ContentDetail backupContentDetail;
    private View decorView;

    @Override
    public boolean onCreateOptionsMenu(Menu menu) {
        // Inflate the menu; this adds items to the action bar if it is present.
        getMenuInflater().inflate(R.menu.menu_cast, menu);
        mediaRouteMenuItem = CastButtonFactory.setUpMediaRouteButton(getApplicationContext(), menu,
                R.id.media_route_menu_item);
        return true;
    }

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_weseries_detail);

        Toolbar toolbar = (Toolbar) findViewById(R.id.toolbar);
        setSupportActionBar(toolbar);

        decorView = getWindow().getDecorView();


        customProgressDialog = new CustomProgressDialog(this);
        onDemandVideo = (Episode) getIntent().getSerializableExtra(Constants.SELECTED_ON_DEMAND_VIDEO);
        content_detail = (ContentDetail) getIntent().getSerializableExtra(Constants.SELECTED_WEBSERIES);

       /* if (onDemandVideo.getSource() != null) {
            if (!onDemandVideo.getSource().contains("http")) {
                onDemandVideo.setSource(WebServiceConstant.VIDEO_URL + onDemandVideo.getSource());
            }
        }*/
        videosList = new ArrayList<>();
        initViews();
        setWatchList();
        apiInterface = APIClient.getClient().create(APIInterface.class);

        tv_category_heading.setText("" + onDemandVideo.getCategory());
        mCastContext = CastContext.getSharedInstance(this);
        mCastSession = mCastContext.getSessionManager().getCurrentCastSession();
        setupCastListener();
        isPlayingAd = false;
        showBuffering();
        updatePlayerWithNewVideo(onDemandVideo);

        isStartVideo = false;

        // hide control after 3 second  df
        userInteractionHandler = new Handler();
        bannerHandler = new Handler();
        userInteractionRunnable = new Runnable() {
            @Override
            public void run() {
                if (mPlaybackState == PlaybackState.PLAYING && isStartVideo) {
                    fl_controller_portrait.setVisibility(View.GONE);
                    fl_controller_landscape.setVisibility(View.GONE);

                    if(checkLandscapeOrientation()){
                        // getWindow().getDecorView().setSystemUiVisibility(View.SYSTEM_UI_FLAG_HIDE_NAVIGATION);
                        hideSystemUi();

                    }
                }

            }
        };

        getSimilarVideoList(content_detail.getContentInfo().getId());

        fetchCategoryList();

    }

    private void getSimilarVideoList(int id) {
        // show list of same category video

        videosList = new ArrayList<>();
        hideShimmerEffect();
        for (int i = 0; i < content_detail.getWebseriesDetail().getSeasons().size(); i++) {
            if (id == content_detail.getWebseriesDetail().getSeasons().get(i).getId()) {
                videosList.addAll(content_detail.getWebseriesDetail().getSeasons().get(i).getEpisodes());
                tv_category_heading.setText("Season " + (i + 1));
            }
        }

        similarVideoAdapter =
                new AdapterWebSeriesVideos(WebSeriesVideoDetailActivity.this,
                        videosList,
                        WebSeriesVideoDetailActivity.this);
        rv_trending.setAdapter(similarVideoAdapter);

    }

    private void updateVideoList(List<OnDemandVideo> videoData, OnDemandVideo onDemandVideo) {
        videosList.clear();
        videosList = new ArrayList<>();
        for (int i = 0; i < videoData.size(); i++) {

            // updateRating(videoData.get(i));
            onDemandVideo = videoData.get(i);
            if (isRated) {
                onDemandVideo.setRated(true);
                isRated = false;
            }

            /*if (!videoData.get(i).getId().equals(onDemandVideo.getId())) {
                videosList.add(videoData.get(i));
                tv_category_heading.setVisibility(View.VISIBLE);
                // tv_category_heading.setText(videosList.get(i).getCategory());
            } else {
                updateRating(videoData.get(i));
                onDemandVideo = videoData.get(i);
                if (isRated) {
                    onDemandVideo.setRated(true);
                    isRated = false;
                }


            }*/
        }

      /*  similarVideoAdapter =
                new AdapterVideoList(WebSeriesVideoDetailActivity.this,
                        videosList, WebSeriesVideoDetailActivity.this);
        rv_trending.setAdapter(similarVideoAdapter);*/
    }

    private void hideShimmerEffect() {
        rv_trending.setVisibility(View.VISIBLE);
        shimmer_view_container.hideShimmer();
        shimmer_view_container.setVisibility(View.GONE);
        if (videosList == null || videosList.size() == 0) {
            tv_category_heading.setVisibility(View.VISIBLE);
        }
    }

    private void updatePlayerWithNewVideo(Episode onDemandVideo) {
        isStartVideo = false;
        isFirstTime = true;
        if (player != null) {
            player.setPlayWhenReady(false);
        }

        relasePlayer();
        showBuffering();
        this.onDemandVideo = onDemandVideo;

        //(onDemandVideo);
        tv_title.setText(onDemandVideo.getEpisode() + ". " + onDemandVideo.getEpisodeName());

        tv_video_title.setText(onDemandVideo.getEpisode() + ". " + onDemandVideo.getEpisodeName());
        tv_video_subtitle.setText("" + onDemandVideo.getEpisodeDescription());
        tv_title_landscape.setText(onDemandVideo.getEpisode() + ". " + onDemandVideo.getEpisodeName());

        mSelectedMedia = new MediaItem();
        isPlayingAd = false;
        if (isPlayingAd) {
            mSelectedMedia.setUrl("http://commondatastorage.googleapis.com/gtv-videos-bucket/sample/BigBuckBunny.mp4");
            mSelectedMedia.setTitle("Playing Ad");
            mSelectedMedia.setSubTitle("");
        } else {
            mSelectedMedia.setUrl("" + onDemandVideo.getSource());
            //mSelectedMedia.setUrl("http://commondatastorage.googleapis.com/gtv-videos-bucket/sample/BigBuckBunny.mp4");
            mSelectedMedia.setTitle("" + onDemandVideo.getTitle());
            mSelectedMedia.setSubTitle("" + onDemandVideo.getSubtitle());
        }
        playVideo();
        //  resumePlayer();

    }

    private void updateRating(OnDemandVideo onDemandVideo) {
        tv_rating.setText("" + Utils.getDecimalOnePoint(onDemandVideo.getAvg_rate()) + "/5");
        //  this.onDemandVideo.setMy_rating(onDemandVideo.getMy_rating());

    }

    private void playVideo() {
        showBuffering();
        initializePlayer();
        mPlaybackState = PlaybackState.PLAYING;
        updatePlayButton(mPlaybackState);
        if (mCastSession != null && mCastSession.isConnected()) {
            updatePlaybackLocation(PlaybackLocation.REMOTE);
        } else {
            showBuffering();
            updatePlaybackLocation(PlaybackLocation.LOCAL);

            if (!onDemandVideo.getPlaybackTime().contains(":")) {
                isFirstTime = false;
                play(Integer.parseInt(onDemandVideo.getPlaybackTime()));

            } else {
                play(mSeekbar.getProgress() * 1000);
            }

        }
        setCaptionVisibility();
        //  resumeLastTime();
    }

    private void resumeLastTime() {
        if (!onDemandVideo.getPlaybackTime().equalsIgnoreCase("00:00:00")) {
            player.seekTo(DateCalculation.getTimeInMillis(onDemandVideo.getPlaybackTime()));
        }
    }

    private void setCaptionVisibility() {
        if (HomeDraweerStreamingActivity.isCaptionOff) {
            simpleExoPlayerView_portrait.getSubtitleView().setVisibility(View.GONE);
            simpleExoPlayerView_landscape.getSubtitleView().setVisibility(View.GONE);
            iv_caption_landscape.setImageResource(R.drawable.ic_caption_off);
            iv_caption_portrait.setImageResource(R.drawable.ic_caption_off);
        } else {
            simpleExoPlayerView_portrait.getSubtitleView().setVisibility(View.VISIBLE);
            simpleExoPlayerView_landscape.getSubtitleView().setVisibility(View.VISIBLE);
            iv_caption_landscape.setImageResource(R.drawable.ic_caption_on);
            iv_caption_portrait.setImageResource(R.drawable.ic_caption_on);
        }
    }

    public void initViews() {

        iv_caption_landscape = findViewById(R.id.iv_caption_landscape);
        iv_caption_portrait = findViewById(R.id.iv_caption_portrait);

        tv_time_portrait = findViewById(R.id.tv_time_portrait);
        tv_time_lanscape = findViewById(R.id.tv_time_landscape);

        iv_banner = findViewById(R.id.iv_banner);
        viewPagerBanner = findViewById(R.id.viewPagerBanner);
        // set banner image
        String bannerUrl = new SharedPreferenceHandler(this).getBannerImage();
        promotionBanners = new ArrayList<>();

        mViewPagerAdapter = new ViewPagerAdapter(WebSeriesVideoDetailActivity.this, promotionBanners, new ViewPagerAdapter.BannerListener() {
            @Override
            public void onBannerListener(PromotionBanner promotionBanner) {

                Intent intentBrowser = new Intent(Intent.ACTION_VIEW);
                Uri u = Uri.parse(promotionBanner.getWebsiteUrl());
                intentBrowser.setData(u);
                startActivity(intentBrowser);

            }
        });


        // Adding the Adapter to the ViewPager
        viewPagerBanner.setAdapter(mViewPagerAdapter);

        /*  viewPagerBanner.setCurrentItem(1);*/


        // banner change every 2 second
        categoryList = new ArrayList<>();
        bannerRunnable = new Runnable() {
            @Override
            public void run() {
                if (promotionBanners != null && promotionBanners.size() > 0) {
                    bannerPosition = viewPagerBanner.getCurrentItem() + 1;
                    viewPagerBanner.setCurrentItem(bannerPosition);
                    startBannerHandler();
                }
            }
        };


       /* if (bannerUrl != null) {
            Glide.with(this).load(bannerUrl).into(iv_banner);
        } else {
            iv_banner.setVisibility(View.GONE);
        }*/


        pDialog = new CustomProgressDialog(this);
        profileData = new SharedPreferenceHandler(this).getProfileInfo();
        appBarLayout = findViewById(R.id.appBar);
        lin_content = findViewById(R.id.lin_content);
        rv_trending = (RecyclerView) findViewById(R.id.rv_trending);
        tv_btn_watchlater = (ImageView) findViewById(R.id.tv_btn_watchlater);
        iv_btn_rate = (ImageView) findViewById(R.id.iv_btn_rate);
        tv_rating = (TextView) findViewById(R.id.tv_rating);

        tv_title = findViewById(R.id.tv_title);

        shimmer_view_container = (ShimmerFrameLayout) findViewById(R.id.shimmer_view_container);

        tv_toolbar_title = findViewById(R.id.tv_toolbar_title);
        tv_toolbar_title.setText("" + onDemandVideo.getCategory());

        tv_category_heading = findViewById(R.id.tv_category_heading);
        tv_title_landscape = findViewById(R.id.tv_title_landscape);
        tv_video_subtitle = findViewById(R.id.tv_video_subtitle);
        tv_video_title = findViewById(R.id.tv_video_title);

        fl_controller_portrait = (FrameLayout) findViewById(R.id.fl_controller_portrait);
        fl_controller_landscape = (FrameLayout) findViewById(R.id.controllers2);

        iv_play_pause = (ImageView) findViewById(R.id.iv_play_pause);
        iv_play_pause_landscape = (ImageView) findViewById(R.id.iv_play_pause_landscape);

        buffering_landscape = (ProgressBar) findViewById(R.id.buffering_landscape);
        buffering_portrait = (ProgressBar) findViewById(R.id.buffering_portrait);

        iv_full_screen_toggle = (ImageView) findViewById(R.id.iv_full_screen_toggle);
        iv_full_screen_toggle_landscape = (ImageView) findViewById(R.id.iv_full_screen_toggle_landscape);

        iv_next_portrait = (ImageView) findViewById(R.id.iv_next_portrait);
        iv_previous_portrait = (ImageView) findViewById(R.id.iv_previous_portrait);
        iv_next_landscape = (ImageView) findViewById(R.id.iv_next_landscape);
        iv_previous_landscape = (ImageView) findViewById(R.id.iv_previous_landscape);


        simpleExoPlayerView_portrait = findViewById(R.id.exoplayerView_portrait);
        simpleExoPlayerView_landscape = findViewById(R.id.exoplayerView_landscape);

        mSeekbar = findViewById(R.id.seekbar);
        mSeekbar_landscape = findViewById(R.id.seekbar_landscape);
        mHandler = new Handler();

        iv_play_pause.setVisibility(View.VISIBLE);
        iv_play_pause.setOnClickListener(this);
        iv_play_pause_landscape.setOnClickListener(this);

        iv_full_screen_toggle.setOnClickListener(this);
        iv_full_screen_toggle_landscape.setOnClickListener(this);

        fl_controller_portrait.setOnClickListener(this);
        fl_controller_landscape.setOnClickListener(this);


        simpleExoPlayerView_portrait.setOnTouchListener(new View.OnTouchListener() {
            @Override
            public boolean onTouch(View v, MotionEvent event) {
                fl_controller_portrait.setVisibility(View.VISIBLE);
                startUserInteractionHandler();

                if (mPlaybackState == PlaybackState.PLAYING) {
                    if (checkLandscapeOrientation()) {
                        hideSystemUi();
                    }
                }

                return false;
            }
        });

        simpleExoPlayerView_landscape.setOnTouchListener(new View.OnTouchListener() {
            @Override
            public boolean onTouch(View v, MotionEvent event) {
                fl_controller_landscape.setVisibility(View.VISIBLE);
                startUserInteractionHandler();

                if (mPlaybackState == PlaybackState.PLAYING) {
                    if (checkLandscapeOrientation()) {
                        hideSystemUi();
                    }
                }

                return false;
            }
        });


        mSeekbar_landscape.setOnSeekBarChangeListener(new SeekBar.OnSeekBarChangeListener() {


            @Override
            public void onStopTrackingTouch(SeekBar seekBar) {

                if (!isPlayingAd) {
                    showBuffering();
                    mPlaybackState = PlaybackState.PLAYING;
                    mSeekbar_landscape.setProgress(seekBar.getProgress());
                    if (mPlaybackState == WebSeriesVideoDetailActivity.PlaybackState.PLAYING) {
                        play(mSeekbar_landscape.getProgress() * 1000);
                    } else if (mPlaybackState != WebSeriesVideoDetailActivity.PlaybackState.IDLE) {
                        player.seekTo(mSeekbar_landscape.getProgress() * 1000);
                    }
                    startControllersTimer();
                }


            }

            @Override
            public void onStartTrackingTouch(SeekBar seekBar) {
                if (!isPlayingAd) {
                    stopTrickplayTimer();
                    player.setPlayWhenReady(false);
                    mPlaybackState = WebSeriesVideoDetailActivity.PlaybackState.PAUSED;
                    stopControllersTimer();
                }

            }

            @Override
            public void onProgressChanged(SeekBar seekBar, int progress,
                                          boolean fromUser) {
                if (!isPlayingAd) {
                    mSeekbar.setProgress(progress);
                    mSeekbar_landscape.setProgress(progress);



                }

                //  mStartText.setText(Utils.formatMillis(progress));
            }
        });


        mSeekbar.setOnSeekBarChangeListener(new SeekBar.OnSeekBarChangeListener() {

            @Override
            public void onStopTrackingTouch(SeekBar seekBar) {

                if (!isPlayingAd) {
                    mPlaybackState = PlaybackState.PLAYING;
                    mSeekbar_landscape.setProgress(seekBar.getProgress());
                    if (mPlaybackState == WebSeriesVideoDetailActivity.PlaybackState.PLAYING) {
                        play(seekBar.getProgress() * 1000);
                    } else if (mPlaybackState != WebSeriesVideoDetailActivity.PlaybackState.IDLE) {
                        player.seekTo(seekBar.getProgress() * 1000);
                    }

                    startControllersTimer();
                }

            }

            @Override
            public void onStartTrackingTouch(SeekBar seekBar) {
                if (!isPlayingAd) {
                    stopTrickplayTimer();
                    player.setPlayWhenReady(false);
                    mPlaybackState = PlaybackState.PAUSED;
                    stopControllersTimer();
                }

            }

            @Override
            public void onProgressChanged(SeekBar seekBar, int progress,
                                          boolean fromUser) {
                mSeekbar.setProgress(progress);
                mSeekbar_landscape.setProgress(progress);
                //  mStartText.setText(Utils.formatMillis(progress));
            }
        });

        LinearLayoutManager layoutManagerTrending = new LinearLayoutManager(this);
        layoutManagerTrending.setOrientation(LinearLayoutManager.HORIZONTAL);
        // set Trending Adapter for recyclerView
        AdapterVideoList trendingAdapter = new AdapterVideoList(this, new ArrayList<OnDemandVideo>(), this);
        rv_trending.setLayoutManager(layoutManagerTrending);
        rv_trending.addItemDecoration(new HorizontalItemDecoration(16));
        rv_trending.setAdapter(trendingAdapter);

        sharedPreferenceHandler = new SharedPreferenceHandler(this);

        /**
         * this below logic for getting user subsription detail
         *  uncomment this logic when subscription is required
         */
        // mySubscription = sharedPreferenceHandler.getMySubscriptionDetail();

        setUpClickListener();


    }

    private void startBannerHandler() {
        stopBannerHandler();
        try {
            if (bannerHandler != null) {
                bannerHandler.postDelayed(bannerRunnable, 5000);
            }
        } catch (Exception e) {

        }
    }

    private void stopBannerHandler() {
        try {
            bannerHandler.removeCallbacks(bannerRunnable);
        } catch (Exception e) {

        }

    }

    private void setUpClickListener() {
        iv_next_landscape.setOnClickListener(this);
        iv_previous_landscape.setOnClickListener(this);
        iv_next_portrait.setOnClickListener(this);
        iv_previous_portrait.setOnClickListener(this);
        tv_btn_watchlater.setOnClickListener(this);
        iv_btn_rate.setOnClickListener(this);
        iv_caption_portrait.setOnClickListener(this);
        iv_caption_landscape.setOnClickListener(this);
    }

    @Override
    public void onClickEpisode(int position, Episode episodes) {

        if (player != null && player.getCurrentPosition() > 10000 && player.getCurrentPosition()
                < (player.getDuration() - 10000)) {
           // this.onDemandVideo.setPlayback_time("" + player.getCurrentPosition());
            capturePlayBackTime(onDemandVideo.getId(), "" +
                    player.getCurrentPosition());
         /*   this.videosList.get(position).setPlayback_time( ""+player.getCurrentPosition());
           // this.videosList.get(position).setDuration( ""+player.getDuration());
            similarVideoAdapter.addData(videosList);
            similarVideoAdapter.notifyItemChanged(position);*/

        }

        this.onDemandVideo = episodes;
        // setWatchList();

        isPlayingAd = false;
        pauseMediaPlayer();
        relasePlayer();
        updatePlayerWithNewVideo(episodes);
    }

    /**
     * indicates whether we are doing a local or a remote playback
     */
    public enum PlaybackLocation {
        LOCAL,
        REMOTE
    }

    /**
     * List of various states that we can be in
     */
    public enum PlaybackState {
        PLAYING, PAUSED, BUFFERING, IDLE, END
    }

    private void play(int position) {


        startControllersTimer();
        switch (mLocation) {
            case LOCAL:
                player.seekTo(position);
                try {
                    // int currentPos = (int) player.getCurrentPosition() / 1000;
                    // updateSeekbar(currentPos, (int) player.getDuration() / 1000);
                } catch (Exception e) {

                }

                playMedia();
               /* if(checkLandscapeOrientation()){
                    mSeekbar.setProgress(position);
                }else {
                    mSeekbar_landscape.setProgress(position);
                }*/
                break;
            case REMOTE:
                mPlaybackState = PlaybackState.BUFFERING;
                updatePlayButton(mPlaybackState);
                mCastSession.getRemoteMediaClient().seek(position);
                //  mSeekbar.setVisibility(View.GONE);
                fl_controller_landscape.setVisibility(View.GONE);
                fl_controller_portrait.setVisibility(View.GONE);
                break;
            default:
                break;
        }
        restartTrickplayTimer();
    }


    public void resumePlayer() {
        mPlaybackState = PlaybackState.PLAYING;
        startControllersTimer();
        switch (mLocation) {
            case LOCAL:
                //   player.seekTo(position);
                playMedia();
                break;
            case REMOTE:
                mPlaybackState = PlaybackState.BUFFERING;
                updatePlayButton(mPlaybackState);
                mCastSession.getRemoteMediaClient().seek(mSeekbar.getProgress());
                //  mSeekbar.setVisibility(View.GONE);
                fl_controller_landscape.setVisibility(View.GONE);
                fl_controller_portrait.setVisibility(View.GONE);
                break;
            default:
                break;
        }
        restartTrickplayTimer();
    }

    private void updatePlayButton(PlaybackState state) {
        Log.d(TAG, "Controls: PlayBackState: " + state);
        boolean isConnected = (mCastSession != null)
                && (mCastSession.isConnected() || mCastSession.isConnecting());

        if (!isConnected) {
            switch (state) {
                case PLAYING:
                    // hideBuffering();
                   /* fl_controller_portrait.setVisibility(View.INVISIBLE);
                    fl_controller_landscape.setVisibility(View.INVISIBLE);*/
                    iv_play_pause.setImageDrawable(getResources().getDrawable(R.drawable.ic_pause_video_24));
                    iv_play_pause_landscape.setImageDrawable(getResources().getDrawable(R.drawable.ic_pause_video_36));
                    // playMedia();
                    startUserInteractionHandler();
                    break;
                case IDLE:
                    // pauseMediaPlayer();
                    iv_play_pause.setImageDrawable(getResources().getDrawable(R.drawable.ic_play_video));
                    iv_play_pause_landscape.setImageDrawable(getResources().getDrawable(R.drawable.ic_play_video_36));

                    /*if (checkLandscapeOrientation()) {

                        fl_controller_landscape.setVisibility(View.VISIBLE);
                        fl_controller_portrait.setVisibility(View.GONE);
                    } else {
                        fl_controller_portrait.setVisibility(View.VISIBLE);
                        fl_controller_landscape.setVisibility(View.GONE);
                    }*/
                    break;
                case PAUSED:
                    if (checkLandscapeOrientation()) {
                        fl_controller_landscape.setVisibility(View.VISIBLE);
                        fl_controller_portrait.setVisibility(View.GONE);
                    } else {
                        fl_controller_portrait.setVisibility(View.VISIBLE);
                        fl_controller_landscape.setVisibility(View.GONE);
                    }
                    iv_play_pause.setImageDrawable(getResources().getDrawable(R.drawable.ic_play_video));
                    iv_play_pause_landscape.setImageDrawable(getResources().getDrawable(R.drawable.ic_play_video_36));
                    break;
                case BUFFERING:
                    showBuffering();
                    break;
                default:
                    break;
            }
        }
    }


    private void stopTrickplayTimer() {
        Log.d(TAG, "Stopped TrickPlay Timer");
        if (mSeekbarTimer != null) {
            mSeekbarTimer.cancel();
        }
    }

    private void restartTrickplayTimer() {
        stopTrickplayTimer();
        mSeekbarTimer = new Timer();
        mSeekbarTimer.scheduleAtFixedRate(new UpdateSeekbarTask(), 100, 1000);
        Log.d(TAG, "Restarted TrickPlay Timer");

    }

    private class UpdateSeekbarTask extends TimerTask {

        @Override
        public void run() {
            mHandler.post(new Runnable() {

                @Override
                public void run() {
                    if (mLocation == PlaybackLocation.LOCAL) {
                        // int currentPos = (int) player.getCurrentPosition() / 1000;
                        int position = (int) player.getCurrentPosition();
                        int currentPos = position / 1000;

                        if (currentPos > 1) {
                            isStartVideo = true;
                        }

                        /*if(mSeekbar.getProgress()>101){
                            pauseMediaPlayer();
                            mPlaybackState = PlaybackState.END;
                            updatePlayButton(mPlaybackState);
                            capturePlayBackTime(onDemandVideo.getId(), "00:00:00");
                            relasePlayer();
                            playNextVideo();
                        }*/

                        updateSeekbar(currentPos, (int) player.getDuration() / 1000);
                    }
                }
            });
        }
    }


    private void updateSeekbar(int position, int duration) {


        mSeekbar.setProgress(position);
        mSeekbar.setMax(duration);
        mSeekbar_landscape.setProgress(position);
        mSeekbar_landscape.setMax(duration);
        if (totalVideoTime == null) {
            totalVideoTime = "00:00";
        }
        if (player != null) {
            long percentage = player.getBufferedPosition() / 1000;
            Log.e("check postion ", "" + percentage);
            mSeekbar_landscape.setSecondaryProgress((int) percentage);
            mSeekbar.setSecondaryProgress((int) percentage);

            if(duration>5){

                if(position >= duration){
                    pauseMediaPlayer();
                    mPlaybackState = PlaybackState.END;
                    updatePlayButton(mPlaybackState);
                    capturePlayBackTime(onDemandVideo.getId(), "00:00:00");
                    relasePlayer();
                    playNextVideo();
                }

            }

        }



        String time = DateCalculation.ConvertMillisToHHMMSS(player.getCurrentPosition()) + " | " + totalVideoTime;
        tv_time_lanscape.setText("" + time);
        tv_time_portrait.setText("" + time);







        //  hideBuffering();
    }

    private void stopControllersTimer() {
        if (mControllersTimer != null) {
            mControllersTimer.cancel();
        }
    }

    private void startControllersTimer() {
        if (mControllersTimer != null) {
            mControllersTimer.cancel();
        }
        if (mLocation == PlaybackLocation.REMOTE) {
            return;
        }
        mControllersTimer = new Timer();
        mControllersTimer.schedule(new HideControllersTask(), 5000);
    }

    private class HideControllersTask extends TimerTask {

        @Override
        public void run() {
            mHandler.post(new Runnable() {
                @Override
                public void run() {
              /*      updateControllersVisibility(false);
                    mControllersVisible = false;*/
                }
            });

        }
    }

    @Override
    protected void onPause() {
        super.onPause();
        try {
            MassTvApplication.getInstance().setUnRegister();
        } catch (Exception e) {

        }
        Log.d(TAG, "onPause() was called");
        if (mLocation == PlaybackLocation.LOCAL) {

            if (mSeekbarTimer != null) {
                mSeekbarTimer.cancel();
                mSeekbarTimer = null;
            }
            if (mControllersTimer != null) {
                mControllersTimer.cancel();
            }
            // since we are playing locally, we need to stop the playback of
            // video (if user is not watching, pause it!)
            pauseMediaPlayer();
            mPlaybackState = PlaybackState.PAUSED;
            updatePlayButton(PlaybackState.PAUSED);
        }

        if (player != null) {
            player.setPlayWhenReady(false);
            //relasePlayer();
        }


        releaseAllHandler();
        mCastContext.getSessionManager().removeSessionManagerListener(
                mSessionManagerListener, CastSession.class);
        if (player != null && player.getCurrentPosition() > 10000 && player.getCurrentPosition() < (player.getDuration() - 10000)) {
            capturePlayBackTime(onDemandVideo.getId(), "" + player.getCurrentPosition());
        }

    }


    @Override
    protected void onStop() {
        super.onStop();

    }


    @Override
    protected void onResume() {
        Log.d(TAG, "onResume() was called");
        super.onResume();
        try {
            MassTvApplication.getInstance().setConnectivityListener(this);

            mCastContext.getSessionManager().addSessionManagerListener(
                    mSessionManagerListener, CastSession.class);
            if (mCastSession != null && mCastSession.isConnected()) {
                updatePlaybackLocation(PlaybackLocation.REMOTE);
            } else {
                //  resumePlayer();
                updatePlaybackLocation(PlaybackLocation.LOCAL);
                if (player != null) {
                    //  playMedia();
                } else if (onDemandVideo != null) {
                    //  updatePlayerWithNewVideo(onDemandVideo);
                }

            }
        } catch (Exception e) {

        }

        startBannerHandler();

    }


    /**
     * Pause media player and change icon to indicate it is pause
     * Handle both landscape and portrait mode
     * mControllers handle seek bar. in this  case we stop seek bar
     */
    private void pauseMediaPlayer() {
        mPlaybackState = PlaybackState.PAUSED;
        if (player != null) {
            if (mControllersTimer != null) {
                mControllersTimer.cancel();
            }
            if (checkLandscapeOrientation()) {

                fl_controller_landscape.setVisibility(View.VISIBLE);
                fl_controller_portrait.setVisibility(View.GONE);
            } else {
                fl_controller_portrait.setVisibility(View.VISIBLE);
                fl_controller_landscape.setVisibility(View.GONE);

            }
            iv_play_pause.setImageDrawable(getResources().getDrawable(R.drawable.ic_play_video));
            iv_play_pause_landscape.setImageDrawable(getResources().getDrawable(R.drawable.ic_play_video_36));

            player.setPlayWhenReady(false);
        }
    }

    private void initializePlayer() {
/// Create a default TrackSelector
        BandwidthMeter bandwidthMeter = new DefaultBandwidthMeter();
       /* TrackSelection.Factory videoTrackSelectionFactory =
                new AdaptiveTrackSelection.Factory(bandwidthMeter);
        TrackSelector trackSelector =
                new DefaultTrackSelector(videoTrackSelectionFactory);

        DefaultLoadControl loadControl = new DefaultLoadControl(
                new DefaultAllocator(true, C.DEFAULT_BUFFER_SEGMENT_SIZE),
                DefaultController.DEFAULT_MIN_BUFFER_MS
                , DefaultController.DEFAULT_MAX_BUFFER_MS,
                DefaultController.DEFAULT_BUFFER_FOR_PLAYBACK_MS,
                DEFAULT_BUFFER_FOR_PLAYBACK_AFTER_REBUFFER_MS,
                DefaultLoadControl.DEFAULT_TARGET_BUFFER_BYTES,
                true
        );*/

        LoadControl loadControl = new DefaultLoadControl.Builder()
                .setAllocator(new DefaultAllocator(true, 16))
                .setBufferDurationsMs( DEFAULT_MIN_BUFFER_MS,
                        DEFAULT_MAX_BUFFER_MS,
                        DEFAULT_BUFFER_FOR_PLAYBACK_MS,
                        DEFAULT_BUFFER_FOR_PLAYBACK_AFTER_REBUFFER_MS)
                .setTargetBufferBytes(-1)
                .setPrioritizeTimeOverSizeThresholds(true).createDefaultLoadControl();

//Initialize the player
        player = ExoPlayerFactory.newSimpleInstance(this, new DefaultTrackSelector(), loadControl);


//Initialize simpleExoPlayerView
        if (checkLandscapeOrientation()) {
            simpleExoPlayerView_landscape.setPlayer(player);
        } else {
            simpleExoPlayerView_portrait.setPlayer(player);
        }

        simpleExoPlayerView_landscape.setUseController(false);
        simpleExoPlayerView_portrait.setUseController(false);

// Produces DataSource instances through which media data is loaded.
        DataSource.Factory dataSourceFactory =
                new DefaultDataSourceFactory(this, Util.getUserAgent(this, getResources().getString(R.string.app_name)));

// Produces Extractor instances for parsing the media data.
        ExtractorsFactory extractorsFactory = new DefaultExtractorsFactory();

// This is the MediaSource representing the media to be played.
        Uri videoUri;
        MediaSource videoSource;
        videoUri = Uri.parse(onDemandVideo.getSource());
        videoSource = new ExtractorMediaSource(videoUri,
                dataSourceFactory,
                new DefaultExtractorsFactory(), null, null);

        showBuffering();

        MergingMediaSource mediaSource;

        if (onDemandVideo.getSrt() != null && !onDemandVideo.getSrt().isEmpty() && onDemandVideo.getSrt().contains(".srt")) {
            Uri srtUri = Uri.parse(onDemandVideo.getSrt());

            iv_caption_portrait.setVisibility(View.VISIBLE);
            iv_caption_landscape.setVisibility(View.VISIBLE);

          /*  Format textFormat = Format.createTextSampleFormat(null, MimeTypes.APPLICATION_SUBRIP, null,
                    Format.NO_VALUE, Format.NO_VALUE, "en", null, Format.OFFSET_SAMPLE_RELATIVE);*/

            Format textFormat = Format.createTextSampleFormat(null,MimeTypes.APPLICATION_SUBRIP,Format.NO_VALUE,"en",Format.NO_VALUE,Format.OFFSET_SAMPLE_RELATIVE, null);


            MediaSource textMediaSource = new SingleSampleMediaSource.Factory(dataSourceFactory)
                    .createMediaSource((srtUri), textFormat, C.TIME_UNSET);

            simpleExoPlayerView_portrait.getSubtitleView().setStyle(new CaptionStyleCompat(Color.WHITE,
                    Color.TRANSPARENT, Color.TRANSPARENT, CaptionStyleCompat.EDGE_TYPE_NONE, Color.WHITE,
                    Typeface.DEFAULT_BOLD));

            simpleExoPlayerView_landscape.getSubtitleView().setStyle(new CaptionStyleCompat(Color.WHITE,
                    Color.TRANSPARENT, Color.TRANSPARENT, CaptionStyleCompat.EDGE_TYPE_NONE, Color.WHITE,
                    Typeface.DEFAULT_BOLD));

            mediaSource = new MergingMediaSource(videoSource, textMediaSource);
            player.prepare(mediaSource);
        } else {
            iv_caption_portrait.setVisibility(View.GONE);
            iv_caption_landscape.setVisibility(View.GONE);
            player.prepare(videoSource);
        }

        //   player.prepare(videoSource);
        player.addListener(this);

        //  simpleExoPlayerView_portrait.setResizeMode(AspectRatioFrameLayout.RESIZE_MODE_FILL);
        //  simpleExoPlayerView_landscape.setResizeMode(AspectRatioFrameLayout.RESIZE_MODE_FILL);
        //  player.setVideoScalingMode(C.VIDEO_SCALING_MODE_SCALE_TO_FIT_WITH_CROPPING);


      /*  simpleExoPlayerView_portrait.setResizeMode(AspectRatioFrameLayout.RESIZE_MODE_FILL);
        player.setVideoScalingMode(C.VIDEO_SCALING_MODE_SCALE_TO_FIT_WITH_CROPPING);
*/


    }

    private void playAds() {
       /* iv_previous_portrait.setEnabled(true);
        iv_previous_portrait.setEnabled(true);*/

/// Create a default TrackSelector
        BandwidthMeter bandwidthMeter = new DefaultBandwidthMeter();
       /* TrackSelection.Factory videoTrackSelectionFactory =
                new AdaptiveTrackSelection.Factory(bandwidthMeter);
        TrackSelector trackSelector =
                new DefaultTrackSelector(videoTrackSelectionFactory);

        DefaultLoadControl loadControl = new DefaultLoadControl(
                new DefaultAllocator(true, C.DEFAULT_BUFFER_SEGMENT_SIZE),
                DEFAULT_MIN_BUFFER_MS
                , DEFAULT_MAX_BUFFER_MS,
                DEFAULT_BUFFER_FOR_PLAYBACK_MS,
                DefaultLoadControl.DEFAULT_BUFFER_FOR_PLAYBACK_AFTER_REBUFFER_MS,
                DEFAULT_MAX_BUFFER_MS,
                true
        );*/

        LoadControl loadControl = new DefaultLoadControl.Builder()
                .setAllocator(new DefaultAllocator(true, 16))
                .setBufferDurationsMs( DEFAULT_MIN_BUFFER_MS,
                        DEFAULT_MAX_BUFFER_MS,
                        DEFAULT_BUFFER_FOR_PLAYBACK_MS,
                        DEFAULT_BUFFER_FOR_PLAYBACK_AFTER_REBUFFER_MS)
                .setTargetBufferBytes(-1)
                .setPrioritizeTimeOverSizeThresholds(true).createDefaultLoadControl();

//Initialize the player
        player = ExoPlayerFactory.newSimpleInstance(this, new DefaultTrackSelector(), loadControl);


//Initialize simpleExoPlayerView
        if (checkLandscapeOrientation()) {
            simpleExoPlayerView_landscape.setPlayer(player);
        } else {
            simpleExoPlayerView_portrait.setPlayer(player);
        }

        simpleExoPlayerView_landscape.setUseController(false);
        simpleExoPlayerView_portrait.setUseController(false);

// Produces DataSource instances through which media data is loaded.
        DataSource.Factory dataSourceFactory =
                new DefaultDataSourceFactory(this, Util.getUserAgent(this, "Mass Tv"));

// Produces Extractor instances for parsing the media data.
        ExtractorsFactory extractorsFactory = new DefaultExtractorsFactory();

// This is the MediaSource representing the media to be played.
        Uri videoUri = Uri.parse(onDemandVideo.getSource());

        MediaSource videoSource = new ExtractorMediaSource(videoUri,
                new CacheDataSourceFactory(this, 100 * 1024 * 1024, 100 * 1024 * 1024),
                new DefaultExtractorsFactory(), null, null);


      /*  MediaSource videoSource = new ExtractorMediaSource(videoUri,
                dataSourceFactory, extractorsFactory, null, null);
*/

// Prepare the player with the source.
        player.prepare(videoSource);

        player.addListener(this);

        simpleExoPlayerView_portrait.setResizeMode(AspectRatioFrameLayout.RESIZE_MODE_FILL);
        simpleExoPlayerView_landscape.setResizeMode(AspectRatioFrameLayout.RESIZE_MODE_FILL);
        //  player.setVideoScalingMode(C.VIDEO_SCALING_MODE_SCALE_TO_FIT_WITH_CROPPING);


      /*  simpleExoPlayerView_portrait.setResizeMode(AspectRatioFrameLayout.RESIZE_MODE_FILL);
        player.setVideoScalingMode(C.VIDEO_SCALING_MODE_SCALE_TO_FIT_WITH_CROPPING);
*/

    }


    @Override
    public void onTimelineChanged(Timeline timeline, Object manifest, int reason) {

    }

    @Override
    public void onTracksChanged(TrackGroupArray trackGroups, TrackSelectionArray trackSelections) {

    }

    @Override
    public void onLoadingChanged(boolean isLoading) {
        Log.e("check loading ", " is Loading " + isLoading);
        runOnUiThread(new Runnable() {
            @Override
            public void run() {
                if (isLoading) {
                    showBuffering();
                } else {
                    hideBuffering();
                }
            }
        });

    }

    @Override
    public void onPlayerStateChanged(boolean playWhenReady, int playbackState) {


        if (playbackState == android.media.session.PlaybackState.STATE_PLAYING) {
            isStartVideo = false;
            totalVideoTime = DateCalculation.ConvertMillisToHHMMSS(player.getDuration());
            hideBuffering();


        } else if (playbackState == android.media.session.PlaybackState.STATE_BUFFERING) {
            mPlaybackState = PlaybackState.BUFFERING;
            showBuffering();
        } else if (playbackState == PlaybackStateCompat.STATE_PAUSED) {
            // todo playbacktime
            //mPlaybackState = PlaybackState.PAUSED;
        } else if (playbackState == 4) {
            // relasePlayer();
            if (isPlayingAd) {
                isPlayingAd = false;
                updatePlayerWithNewVideo(onDemandVideo);
            } else {
                pauseMediaPlayer();
                mPlaybackState = PlaybackState.END;
                updatePlayButton(mPlaybackState);
                capturePlayBackTime(onDemandVideo.getId(), "00:00:00");
                relasePlayer();
                playNextVideo();
              /*  if ((!onDemandVideo.isRated()) && !isRatingAlertShow) {
                    showRatingAlert();
                } else {
                    if (videosList.size() == 0 && onDemandVideo.isRated()) {
                        finish();
                    }
                }*/

            }
        }
    }

    private void playNextVideo() {
        int currentPosition = 0;
        for(int i=0;i<videosList.size();i++){
            String id = ""+videosList.get(i).getId();
            if(id.equalsIgnoreCase(""+onDemandVideo.getId())){
                currentPosition = i;
            }
        }

        if(currentPosition+1<videosList.size()){

            updatePlayerWithNewVideo(videosList.get(currentPosition+1));
        } else  {

            if ((!content_detail.getContentInfo().getIsRated()) && !isRatingAlertShow) {
                showRatingAlert();
            } else {
               onBackPressed();
            }

        }
    }

    private void capturePlayBackTime(int id, String time) {
        new GetOnDemandVideoList(WebSeriesVideoDetailActivity.this, new GetOnDemandVideoList.onSuccessCaptureListener() {
            @Override
            public void onSuccess(CapturePlayBack data) {

            }

            @Override
            public void onError(String error) {

            }
        }).capturePlayBackTime(id, time);

    }

    @Override
    public void onRepeatModeChanged(int repeatMode) {

    }

    @Override
    public void onShuffleModeEnabledChanged(boolean shuffleModeEnabled) {

    }

    @Override
    public void onPlayerError(ExoPlaybackException error) {

        Utils.showErrorDialog(this, error.getMessage());
        mPlaybackState = PlaybackState.IDLE;
        pauseMediaPlayer();

    }

    @Override
    public void onPositionDiscontinuity(int reason) {

    }

    @Override
    public void onBackPressed() {


        if (checkLandscapeOrientation()) {
            changeOrientationToLandscape(false);
        } else {
            Intent intent = new Intent(this, WebSeriesDetailActivity.class);
            intent.putExtra(Constants.SELECTED_ON_DEMAND_VIDEO, content_detail);
            startActivity(intent);
            finish();
        }

    }

    @Override
    public void onPlaybackParametersChanged(PlaybackParameters playbackParameters) {

    }

    @Override
    public void onSeekProcessed() {
        //   progressBar.setProgress((int)player.getCurrentPosition()/1000);
    }


    /**
     * Play media player and change icon to indicate it is play
     * Handle both landscape and portrait mode
     */
    private void playMedia() {
        //  hideBuffering();
        iv_play_pause.setImageDrawable(getResources().getDrawable(R.drawable.ic_pause_video_24));
        iv_play_pause_landscape.setImageDrawable(getResources().getDrawable(R.drawable.ic_pause_video_36));

        if (player != null) {
            getWindow().addFlags(WindowManager.LayoutParams.FLAG_KEEP_SCREEN_ON);
            mPlaybackState = PlaybackState.PLAYING;
            player.setPlayWhenReady(true);
            startUserInteractionHandler();
        }

    }

    // listener for casting
    private void setupCastListener() {
        mSessionManagerListener = new SessionManagerListener<CastSession>() {

            @Override
            public void onSessionEnded(CastSession session, int error) {
                onApplicationDisconnected();
            }

            @Override
            public void onSessionResumed(CastSession session, boolean wasSuspended) {
                onApplicationConnected(session);
            }

            @Override
            public void onSessionResumeFailed(CastSession session, int error) {
                onApplicationDisconnected();
            }

            @Override
            public void onSessionStarted(CastSession session, String sessionId) {
                onApplicationConnected(session);
            }

            @Override
            public void onSessionStartFailed(CastSession session, int error) {
                onApplicationDisconnected();
            }

            @Override
            public void onSessionStarting(CastSession session) {
            }

            @Override
            public void onSessionEnding(CastSession session) {
            }

            @Override
            public void onSessionResuming(CastSession session, String sessionId) {
            }

            @Override
            public void onSessionSuspended(CastSession session, int reason) {
            }

            private void onApplicationConnected(CastSession castSession) {
                mCastSession = castSession;
                mLocation = PlaybackLocation.REMOTE;
                if (null != mSelectedMedia) {
                    mPlaybackState = PlaybackState.PLAYING;
                    if (isPlayingAd) {
                        playMedia();
                        if (mSeekbar != null) {
                            loadRemoteMedia(mSeekbar.getProgress(), true);
                        } else {
                            loadRemoteMedia(0, true);

                        }
                    } else {
                        playMedia();
                        loadRemoteMedia(mSeekbar.getProgress(), true);
                    }
                    return;
                }
                updatePlayButton(mPlaybackState);
                invalidateOptionsMenu();


            /*    if (null != mSelectedMedia) {

                    if (mPlaybackState == VideoDetailActivity.PlaybackState.PLAYING) {
                        playMedia();
                        loadRemoteMedia(mSeekbar.getProgress(), true);
                        return;
                    } else {
                        mPlaybackState = VideoDetailActivity.PlaybackState.IDLE;
                        updatePlaybackLocation(VideoDetailActivity.PlaybackLocation.REMOTE);
                    }
                }
                updatePlayButton(mPlaybackState);
                invalidateOptionsMenu();*/
            }

            private void onApplicationDisconnected() {
                updatePlaybackLocation(WebSeriesVideoDetailActivity.PlaybackLocation.LOCAL);
                mPlaybackState = WebSeriesVideoDetailActivity.PlaybackState.IDLE;
                mLocation = WebSeriesVideoDetailActivity.PlaybackLocation.LOCAL;
                updatePlayButton(mPlaybackState);
                invalidateOptionsMenu();
            }
        };
    }

    private void updatePlaybackLocation(PlaybackLocation location) {
        mLocation = location;
        if (location == WebSeriesVideoDetailActivity.PlaybackLocation.LOCAL) {
            if (mPlaybackState == WebSeriesVideoDetailActivity.PlaybackState.PLAYING
                    || mPlaybackState == WebSeriesVideoDetailActivity.PlaybackState.BUFFERING) {
                // setCoverArtStatus(null);
                startControllersTimer();
            } else {
                stopControllersTimer();
                //  setCoverArtStatus(mSelectedMedia.getImage(0));
            }
        } else {
            stopControllersTimer();
            // setCoverArtStatus(mSelectedMedia.getImage(0));
            fl_controller_landscape.setVisibility(View.GONE);
            fl_controller_portrait.setVisibility(View.GONE);

        }
    }

    private MediaInfo buildMediaInfo() {
        MediaMetadata movieMetadata = new MediaMetadata(MediaMetadata.MEDIA_TYPE_MOVIE);

        movieMetadata.putString(MediaMetadata.KEY_SUBTITLE, mSelectedMedia.getSubTitle());
        movieMetadata.putString(MediaMetadata.KEY_TITLE, mSelectedMedia.getTitle());
       /* movieMetadata.addImage(new WebImage(Uri.parse(mSelectedMedia.getImage(0))));
        movieMetadata.addImage(new WebImage(Uri.parse(mSelectedMedia.getImage(1))));
*/
        return new MediaInfo.Builder(mSelectedMedia.getUrl())
                .setStreamType(MediaInfo.STREAM_TYPE_BUFFERED)
                .setContentType("videos/mp4")
                .setMetadata(movieMetadata)
                .setStreamDuration(mSelectedMedia.getDuration() * 1000)
                .build();
    }

    private void loadRemoteMedia(int position, boolean autoPlay) {
        if (mCastSession == null) {
            return;
        }
        final RemoteMediaClient remoteMediaClient = mCastSession.getRemoteMediaClient();
        if (remoteMediaClient == null) {
            return;
        }
        remoteMediaClient.registerCallback(new RemoteMediaClient.Callback() {
            @Override
            public void onStatusUpdated() {
                Intent intent = new Intent(WebSeriesVideoDetailActivity.this, ExpandedControlsActivity.class);
                startActivity(intent);
                remoteMediaClient.unregisterCallback(this);
            }
        });
        remoteMediaClient.load(new MediaLoadRequestData.Builder()
                .setMediaInfo(buildMediaInfo())
                .setAutoplay(autoPlay)
                .setCurrentTime(position).build());
    }


    protected void showProgrss() {
        if (pDialog == null) {
            // pDialog = new ProgressDialog(this);
            pDialog = new CustomProgressDialog(WebSeriesVideoDetailActivity.this);
            pDialog.setCancelable(false);
        }
        if (!pDialog.isShowing()) {
            // Show progressbar
            pDialog.show();
        }
    }

    protected void dismisProgrss() {
        if (pDialog != null && pDialog.isShowing()) {
            pDialog.dismiss();
        }
    }

    @Override
    public void onClick(View v) {
        switch (v.getId()) {
            case R.id.iv_back:
                onBackPressed();
                break;

            case R.id.iv_caption_landscape:
                //todo load caption
                startUserInteractionHandler();
                HomeDraweerStreamingActivity.isCaptionOff = !HomeDraweerStreamingActivity.isCaptionOff;
                setCaptionVisibility();
                break;

            case R.id.iv_caption_portrait:
                //todo load caption
                startUserInteractionHandler();
                HomeDraweerStreamingActivity.isCaptionOff = !HomeDraweerStreamingActivity.isCaptionOff;
                setCaptionVisibility();
                /*simpleExoPlayerView_portrait.getSubtitleView().setStyle(new CaptionStyleCompat(Color.TRANSPARENT,
                        Color.TRANSPARENT, Color.TRANSPARENT, CaptionStyleCompat.EDGE_TYPE_NONE, Color.TRANSPARENT,
                        Typeface.DEFAULT_BOLD));*/

                break;

            case R.id.tv_btn_watchlater:
                showProgrss();
                /*new WatchLaterService(this, new WatchLaterService.OnWatchLaterResponse() {
                    @Override
                    public void onResponse(WatchLaterResponse response) {
                        // set color
                        dismisProgrss();
                        if (response.getSuccess()) {
                            onDemandVideo.setWatchList(!onDemandVideo.isWatchList());
                            setWatchList();
                            for (int i = 0; i < videosList.size(); i++) {
                                if (videosList.get(i).getId().equals(onDemandVideo.getId())) {
                                    videosList.get(i).setWatchList(onDemandVideo.isWatchList());
                                    similarVideoAdapter.changeVideoList(videosList);
                                    similarVideoAdapter.notifyItemChanged(i);
                                }
                            }
                        }

                    }

                    @Override
                    public void onError(String error) {
                        dismisProgrss();
                    }
                }).addToWatchListVideo(onDemandVideo.getId(), onDemandVideo.isWatchList() ? "remove" : "add");
*/
                break;


            case R.id.iv_next_portrait:
                if (!isPlayingAd) {
                    skipToNext();
                }

                break;

            case R.id.iv_previous_portrait:
                if (!isPlayingAd) {
                    skipToPrevious();
                }

                break;


            case R.id.iv_next_landscape:
                if (!isPlayingAd) {
                    skipToNext();
                }

                break;

            case R.id.iv_previous_landscape:
                if (!isPlayingAd) {
                    skipToPrevious();
                }

                break;

            case R.id.iv_btn_rate:
                /*if (!onDemandVideo.isRated()) {
                    // showRatingAlert();
                }*/

                break;


            case R.id.iv_play_pause:
                if (mPlaybackState != PlaybackState.PLAYING) {
                    if (mPlaybackState == PlaybackState.END) {
                        player.seekTo(0);
                    }
                    resumePlayer();

                } else {
                    pauseMediaPlayer();
                }
                break;

            case R.id.iv_play_pause_landscape:
                if (mPlaybackState != PlaybackState.PLAYING) {
                    if (mPlaybackState == PlaybackState.END) {
                        player.seekTo(0);
                    }
                    resumePlayer();
                } else {
                    pauseMediaPlayer();
                }
                break;

            case R.id.fl_controller_portrait:

                if (mPlaybackState == PlaybackState.PLAYING) {
                    fl_controller_portrait.setVisibility(View.INVISIBLE);
                   /* if(checkLandscapeOrientation()){
                        hideSystemUi();
                    }*/
                } else {
                    fl_controller_portrait.setVisibility(View.VISIBLE);
                }
                break;

            case R.id.controllers2:

                if (mPlaybackState == PlaybackState.PLAYING) {
                    fl_controller_landscape.setVisibility(View.INVISIBLE);
                   /* if(checkLandscapeOrientation()){
                        hideSystemUi();
                    }*/
                } else {
                    fl_controller_landscape.setVisibility(View.VISIBLE);
                }
                break;


            case R.id.iv_full_screen_toggle:
                if (checkLandscapeOrientation()) {
                    changeOrientationToLandscape(false);

                } else {
                    changeOrientationToLandscape(true);

                }

            case R.id.iv_full_screen_toggle_landscape:
                if (checkLandscapeOrientation()) {
                    changeOrientationToLandscape(false);
                } else {
                    changeOrientationToLandscape(true);

                }

        }
    }

    private void setWatchList() {

       /* if (isTablet()) {
            tv_btn_watchlater.setImageResource(
                    (onDemandVideo.isWatchList() ? R.drawable.ic_watch_list_36 : R.drawable.ic_watch_later_36));

        } else {
            tv_btn_watchlater.setImageResource(
                    (onDemandVideo.isWatchList() ? R.drawable.ic_watched_list : R.drawable.ic_watch_later));

        }*/

        // tv_btn_watchlater.setTextColor(getResources().getColor(onDemandVideo.isWatchList()?R.color.green:R.color.white));
    }

    public boolean isTablet() {
       /* return (getResources().getConfiguration().screenLayout
                & Configuration.SCREENLAYOUT_SIZE_MASK)
                >= Configuration.SCREENLAYOUT_SIZE_LARGE;*/

        return getResources().getBoolean(R.bool.isTablet);


    }

    private void skipToNext() {
        mPlaybackState = WebSeriesVideoDetailActivity.PlaybackState.PAUSED;
        pauseMediaPlayer();

        mPlaybackState = PlaybackState.PLAYING;

        long totalDuration = player.getDuration();
        long nextDuration = mSeekbar.getProgress() * 1000 + 10 * 1000;

        if (nextDuration < totalDuration) {
            int progress = (int) (nextDuration / 1000);
            mSeekbar_landscape.setProgress(progress);
            mSeekbar.setProgress(progress);
            if (mPlaybackState == WebSeriesVideoDetailActivity.PlaybackState.PLAYING) {
                play(mSeekbar_landscape.getProgress() * 1000);
            } else if (mPlaybackState != WebSeriesVideoDetailActivity.PlaybackState.IDLE) {
                player.seekTo(mSeekbar_landscape.getProgress() * 1000);
            }
        } else {
            resumePlayer();
        }

    }

    private void skipToPrevious() {
        mPlaybackState = WebSeriesVideoDetailActivity.PlaybackState.PAUSED;
        pauseMediaPlayer();

        mPlaybackState = PlaybackState.PLAYING;

        long totalDuration = 10 * 1000;
        long previousDuation = (mSeekbar.getProgress() * 1000) - 10 * 1000;

        if (previousDuation > totalDuration) {
            int progress = (int) (previousDuation / 1000);
            mSeekbar_landscape.setProgress(progress);
            mSeekbar.setProgress(progress);
            if (mPlaybackState == WebSeriesVideoDetailActivity.PlaybackState.PLAYING) {
                play(mSeekbar_landscape.getProgress() * 1000);
            } else if (mPlaybackState != WebSeriesVideoDetailActivity.PlaybackState.IDLE) {
                player.seekTo(mSeekbar_landscape.getProgress() * 1000);
            }
        } else {
            resumePlayer();
        }
    }

    /**
     * @Rehan Get Banner List from category
     * since Every category has one banner
     */
    private void fetchCategoryList() {
        new GetVideoCategoryList(this, new GetVideoCategoryList.onDemandCategoryListListener() {


            @Override
            public void onFetchedCategoryList(CategoryData categoryData) {

                try {
                    promotionBanners.clear();
                    promotionBanners = new ArrayList<>();
                    promotionBanners.addAll(categoryData.getPromotion_list());
                    if (promotionBanners.size() == 0) {

                        viewPagerBanner.setVisibility(View.GONE);
                        //  fetchCategoryList();
                    } else {
                        // iv_banner.setVisibility(View.VISIBLE);
                        if (promotionBanners.size() > 0) {
                            mViewPagerAdapter.addBannerList(categoryData.getPromotion_list());
                            mViewPagerAdapter.notifyDataSetChanged();
                            endlessPagerAdapter = new EndlessPagerAdapter(mViewPagerAdapter, viewPagerBanner);
                            viewPagerBanner.setAdapter(endlessPagerAdapter);
                            startBannerHandler();

                        }
                    }
                } catch (Exception e) {
                    e.printStackTrace();
                }

            }

            @Override
            public void onFetchedMasterCategoryList(List<OnDemandMasterCategory> videoData) {

            }

            @Override
            public void onCategoryFetchingError(String error) {

            }
        }).GetCategoryList();
    }

    /**
     * Change device orientation manually
     *
     * @param shouldLandscape
     */
    public void changeOrientationToLandscape(Boolean shouldLandscape) {
        if (shouldLandscape) {
            setRequestedOrientation(ActivityInfo.SCREEN_ORIENTATION_USER_LANDSCAPE);
            //   setRequestedOrientation(ActivityInfo.SCREEN_ORIENTATION_FULL_SENSOR);
            setRequestedOrientation(ActivityInfo.SCREEN_ORIENTATION_SENSOR_LANDSCAPE);

        } else {
            setRequestedOrientation(ActivityInfo.SCREEN_ORIENTATION_USER_PORTRAIT);
           // setRequestedOrientation(ActivityInfo.SCREEN_ORIENTATION_SENSOR);

        }

    }

    /**
     * Checks the Orientation
     * And returns true if Landscape else false
     */
    public boolean checkLandscapeOrientation() {
        int orientation = getResources().getConfiguration().orientation;
        return orientation == Configuration.ORIENTATION_LANDSCAPE;
    }

    @Override
    public void onConfigurationChanged(Configuration newConfig) {
        super.onConfigurationChanged(newConfig);
        getSupportActionBar().show();
        if (newConfig.orientation == Configuration.ORIENTATION_LANDSCAPE) {
           /* getWindow().clearFlags(WindowManager.LayoutParams.FLAG_FORCE_NOT_FULLSCREEN);
            getWindow().setFlags(WindowManager.LayoutParams.FLAG_FULLSCREEN,
                    WindowManager.LayoutParams.FLAG_FULLSCREEN);
            getWindow().getDecorView().setSystemUiVisibility(View.SYSTEM_UI_FLAG_LOW_PROFILE);
            updateMetadata(false);*/
            hideSystemUi();
            updateMetadata(false);
            //   mContainer.setBackgroundColor(getResources().getColor(R.color.black));

        } else {
            getWindow().setFlags(WindowManager.LayoutParams.FLAG_FORCE_NOT_FULLSCREEN,
                    WindowManager.LayoutParams.FLAG_FORCE_NOT_FULLSCREEN);
            getWindow().clearFlags(
                    WindowManager.LayoutParams.FLAG_FULLSCREEN);
            getWindow().getDecorView().setSystemUiVisibility(View.SYSTEM_UI_FLAG_VISIBLE);

            updateMetadata(true);
        }
    }

    private void updateMetadata(boolean landscape) {
        Point displaySize;
        if (!landscape) {
            fl_controller_landscape.setVisibility(View.VISIBLE);
            fl_controller_portrait.setVisibility(View.GONE);
            appBarLayout.setVisibility(View.GONE);
            lin_content.setVisibility(View.GONE);
            buffering_portrait.setVisibility(View.GONE);

            switchPlayerView(player, simpleExoPlayerView_portrait, simpleExoPlayerView_landscape);


            //  play(mSeekbar.getProgress()*1000);
        } else {

            fl_controller_landscape.setVisibility(View.GONE);
            fl_controller_portrait.setVisibility(View.VISIBLE);
            appBarLayout.setVisibility(View.VISIBLE);
            lin_content.setVisibility(View.VISIBLE);
            fl_controller_portrait.setVisibility(View.VISIBLE);
            buffering_landscape.setVisibility(View.GONE);
            showBottomNavigation();
            switchPlayerView(player, simpleExoPlayerView_landscape, simpleExoPlayerView_portrait);


            //  play(mSeekbar.getProgress()*1000);
        }
    }

    /**
     * Hide bottom navigation
     */
    private void hideBottomNavigation() {


    }

    /**
     * SHow Bottom navigation
     */
    private void showBottomNavigation(){

    }

    @Override
    protected void onDestroy() {
        Log.d(TAG, "onDestroy() is called");

        try {
            stopBannerHandler();
        } catch (Exception e) {

        }

        try {
            relasePlayer();

        } catch (Exception e) {

        }

        try {

            stopControllersTimer();
            stopTrickplayTimer();
            releaseAllHandler();
        } catch (Exception e) {

        }

        super.onDestroy();
    }


    private void showBuffering() {

        // isStartVideo = false;
        //   pauseMediaPlayer();

        if (checkLandscapeOrientation()) {
            buffering_landscape.setVisibility(View.VISIBLE);
            buffering_portrait.setVisibility(View.GONE);
        } else {
            buffering_landscape.setVisibility(View.GONE);
            buffering_portrait.setVisibility(View.VISIBLE);
        }

       /* if (checkLandscapeOrientation()) {
            fl_controller_landscape.setVisibility(View.VISIBLE);
            fl_controller_portrait.setVisibility(View.GONE);
        } else {
            fl_controller_portrait.setVisibility(View.VISIBLE);
            fl_controller_landscape.setVisibility(View.GONE);
        }*/
        startUserInteractionHandler();

    }

    private void hideBuffering() {
        buffering_landscape.setVisibility(View.GONE);
        buffering_portrait.setVisibility(View.GONE);
        startUserInteractionHandler();
    }

    /**
     * start the handler so that if we play any media and donot do any interaction
     * then i will hide after 3 second we can set any amount of time
     */
    public void startUserInteractionHandler() {
        try {
            userInteractionHandler.removeCallbacks(userInteractionRunnable);
        } catch (Exception e) {

        }
        if (userInteractionHandler != null) {
            userInteractionHandler.postDelayed(userInteractionRunnable, 5 * 1000);

        }

    }

    public void releaseAllHandler() {
        try {
            userInteractionHandler.removeCallbacks(userInteractionRunnable);
        } catch (Exception e) {

        }
    }

    /**
     * This method is responsible for showing video in landscape and portrait
     *
     * @param player
     * @param oldPlayerView
     * @param newPlayerView
     */
    private void switchPlayerView(SimpleExoPlayer player, PlayerView oldPlayerView, PlayerView newPlayerView) {
        oldPlayerView.setVisibility(View.GONE);
        newPlayerView.setVisibility(View.VISIBLE);
        PlayerView.switchTargetView(player, oldPlayerView, newPlayerView);
        playMedia();

    }


    private void relasePlayer() {
        try {
            player.setPlayWhenReady(false);
            player.stop();
            player.release();
            mSeekbar_landscape.setProgress(0);
            mSeekbar.setProgress(0);
            try {
                stopTrickplayTimer();
            } catch (Exception e) {
                e.printStackTrace();
            }
        } catch (Exception e) {

        }

    }


    @Override
    public void onVideoSelected(int position, OnDemandVideo selectedVideo) {
        if (checkConnection()) {
            if (position == -1) {
                startActivity(new Intent(WebSeriesVideoDetailActivity.this,
                        VideoListActivity.class).putExtra(Constants.CATEGORY_TYPE, "" + onDemandVideo.getCategory()));
            } else {

                /**
                 * this below logic for checking user has subscription or not
                 *  uncomment this logic when subscription is required
                 */

              /*    if (mySubscription != null && Integer.parseInt(mySubscription.getRemainingDays()) != 0) {
                if (!mySubscription.getLastRefreshDate().equalsIgnoreCase(DateCalculation.getCurrentDateInDDMMYY())) {
                    if (sharedPreferenceHandler.getPrefernceBoolean(SharedPreferenceHandler.SUBSCRIBED)) {
                        getMySubscriptionDetail(selectedVideo);
                    }
                } else {
                    isPlayingAd = onDemandVideo.getSource_ad() != null && !onDemandVideo.getSource_ad().isEmpty();
                    pauseMediaPlayer();
                    relasePlayer();
                    updatePlayerWithNewVideo(selectedVideo);
                }

            } else {
                showSubscriptionAlert();
            }*/
               /* this.onDemandVideo = selectedVideo;
                setWatchList();

                isPlayingAd = onDemandVideo.getSource_ad() != null && !onDemandVideo.getSource_ad().isEmpty();
                pauseMediaPlayer();
                relasePlayer();
                updatePlayerWithNewVideo(selectedVideo);
              */  // getSimilarVideoList(0);

            }
        } else {
            showSnack(checkConnection());
        }

    }

    @Override
    public void onCategorySelected(int position, OnDemandCategory onDemandCategory) {

    }

    @Override
    public void onWatchLater(int i, OnDemandVideo onDemandVideo) {
      /*  videosList.clear();;
        videosList = new ArrayList<>();
        videosList.addAll(trendingAdapter.getVideoList());*/
        similarVideoAdapter.notifyDataSetChanged();
    }


    public void showSubscriptionAlert() {
        // Create an alert builder
        AlertDialog.Builder builder
                = new AlertDialog.Builder(this);
        // set the custom layout
        final View customLayout
                = getLayoutInflater()
                .inflate(
                        R.layout.layout_alert_subscribe,
                        null);
        builder.setView(customLayout);

        Button btn_subscribe = customLayout.findViewById(R.id.btn_subscribe);

        final AlertDialog dialog
                = builder.create();

        dialog.getWindow().setBackgroundDrawableResource(android.R.color.transparent);


        ColorDrawable back = new ColorDrawable(Color.TRANSPARENT);
        InsetDrawable inset = new InsetDrawable(back, 40, 0, 40, 0);
        if (dialog.getWindow() != null) {
            dialog.getWindow().setBackgroundDrawable(inset);

        }

        btn_subscribe.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                dialog.dismiss();
                startActivity(new Intent(WebSeriesVideoDetailActivity.this, SubscriptionSelectorActivity.class));
            }
        });

        // create and show
        // the alert dialog

        dialog.show();
    }


    /**
     * Update Subscription detail
     *
     * @param videoToOpen
     */
    private void getMySubscriptionDetail(final OnDemandVideo videoToOpen) {
        new SubscriptionService(WebSeriesVideoDetailActivity.this).
                getMySubscriptionInfo(new SubscriptionService.onSubscriptionInfoListener() {
                    @Override
                    public void onSubscriptionInfo(MySubscription subscriptionInfo) {
                        if (subscriptionInfo != null) {
                            sharedPreferenceHandler.saveSubscription(subscriptionInfo);
                            mySubscription = sharedPreferenceHandler.getMySubscriptionDetail();

                            if (videoToOpen != null && !subscriptionInfo.getRemainingDays().equalsIgnoreCase("0")) {
                                pauseMediaPlayer();
                                relasePlayer();
                                //updatePlayerWithNewVideo(videoToOpen);
                            }
                        }
                    }

                    @Override
                    public void onFailure(String error) {

                    }
                });
    }


    @Override
    public void onNetworkConnectionChanged(boolean isConnected) {
        if ((videosList == null || videosList.size() == 0) && isConnected) {
            getSimilarVideoList(content_detail.getContentInfo().getId());
        } else {
            showSnack(isConnected);
        }
    }


    /**
     * Check internet connection Manually
     */
    private boolean checkConnection() {
        return ConnectivityReceiver.isConnected();
    }

    // Showing the status in Snackbar for intenet connectivity
    private void showSnack(boolean isConnected) {
        String message;
        int color;
        if (isConnected) {
            message = "Connection back Online!";
            color = Color.WHITE;
        } else {
            message = "Offline! Not connected to internet";
            color = Color.RED;


        }
        Snackbar snackbar = Snackbar
                .make(findViewById(R.id.parent), message, Snackbar.LENGTH_LONG);
        View sbView = snackbar.getView();
        TextView textView = (TextView) sbView.findViewById(R.id.snackbar_text);
        textView.setTextColor(color);
        snackbar.show();

    }

    /**
     * @Rehan Give Rating after video End only one time
     */
    public void showRatingAlert() {

        isRatingAlertShow = true;

        AlertDialog.Builder builder
                = new AlertDialog.Builder(this);
        // set the custom layout
        final View customLayout
                = getLayoutInflater()
                .inflate(
                        R.layout.layout_alert_rating,
                        null);
        builder.setView(customLayout);

        TextView tv_title = customLayout.findViewById(R.id.tv_title);
        Button btn_rate = customLayout.findViewById(R.id.btn_rate);
        Button btn_cancel = customLayout.findViewById(R.id.btn_cancel);

        tv_title.setText("Rate this Season");

//        RatingBar ratingBar = customLayout.findViewById(R.id.rateBar);
//        ratingBar.setRating(1);
        if (Float.valueOf(content_detail.getContentInfo().getUserRating()) > 0) {
//            ratingBar.setRating(Float.valueOf(content_detail.getContentInfo().getUserRating()));
        }
//        ratingBar.setOnRatingBarChangeListener(new RatingBar.OnRatingBarChangeListener() {
//            private float rating;

//            @Override
//            public void onRatingChanged(RatingBar ratingBar, float rating, boolean fromUser) {
//                this.rating = rating;
//                if (rating >= 0 && rating <= 1) {
//                    this.rating = 1;
//                } else if (rating > 1 && rating <= 2) {
//                    this.rating = 2;
//                } else if (rating > 2 && rating <= 3) {
//                    this.rating = 3;
//                } else if (rating > 3 && rating <= 4) {
//                    this.rating = 4;
//                } else if (rating > 4 && rating <= 5) {
//                    this.rating = 5;
//                }
//
//                ratingBar.setRating(this.rating);
//            }
//        });
        final AlertDialog dialog
                = builder.create();
        dialog.setCancelable(false);

        dialog.getWindow().setBackgroundDrawableResource(android.R.color.transparent);

        ColorDrawable back = new ColorDrawable(Color.TRANSPARENT);
        InsetDrawable inset = new InsetDrawable(back, 40, 0, 40, 0);
        if (dialog.getWindow() != null) {
            dialog.getWindow().setBackgroundDrawable(inset);
        }

        //  int myRating = (int) ratingBar.getRating();
        int myRating = (int) 2;

        btn_rate.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                isRatingAlertShow = false;
                dialog.dismiss();
                customProgressDialog.show();
//                if (ratingBar.getRating() < 1) {
//                    ratingBar.setRating(1);
//                }
                Map<String, Integer> ratingCall = new HashMap<>();
                ratingCall.put(WebServiceConstant.USER_ID, profileData.getId());
                ratingCall.put(WebServiceConstant.VIDEO_ID, content_detail.getContentInfo().getId());
//                ratingCall.put(WebServiceConstant.RATING, (int) ratingBar.getRating());

                // todo
                Call<ResponseBody> call = apiInterface.updateRating(ratingCall);
                call.enqueue(new Callback<ResponseBody>() {
                    @Override
                    public void onResponse(Call<ResponseBody> call, Response<ResponseBody> response) {
                        customProgressDialog.dismiss();
                        HomeDraweerStreamingActivity.isResume = true;
                       /* content_detail.getContentInfo().setUserRating(String.valueOf(myRating));
                        content_detail.getContentInfo().setIsRated(true);
                        int totalRating = content_detail.getContentInfo().getTotalUserRated();
                        double avgRating = content_detail.getContentInfo().getAvgRate();
                        double newAvgRaing;
                        if (totalRating > 0) {
                            newAvgRaing = (totalRating * avgRating) / (totalRating + 1);
                        } else {
                            newAvgRaing = myRating;
                        }

                        content_detail.getContentInfo().setTotalUserRated((totalRating + 1));
                        content_detail.getContentInfo().setAvgRate((int) newAvgRaing);*/
                        successFullAlert("Rating Submitted Successfully", "Rating Submitted Successfully");
                    }

                    @Override
                    public void onFailure(Call<ResponseBody> call, Throwable t) {
                        customProgressDialog.dismiss();
                    }
                });

            }
        });

        btn_cancel.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                dialog.dismiss();
                isRatingAlertShow = false;
                Intent intent = new Intent(WebSeriesVideoDetailActivity.this, WebSeriesDetailActivity.class);
                intent.putExtra(Constants.SELECTED_ON_DEMAND_VIDEO, content_detail);
                startActivity(intent);
                finish();
            }
        });

        // create and show
        // the alert dialog

        dialog.show();

    }

    /**
     * @Rehan Show successfull Alert
     */
    public void successFullAlert(String title, String message) {
        // Create an alert builder
        AlertDialog.Builder builder
                = new AlertDialog.Builder(this);
        // set the custom layout
        final View customLayout
                = getLayoutInflater()
                .inflate(
                        R.layout.layout_alert_loaded,
                        null);
        builder.setView(customLayout);

        Button btn_ok = customLayout.findViewById(R.id.tv_btn_upload);
        TextView tv_title = customLayout.findViewById(R.id.tv_result);
        TextView tv_message = customLayout.findViewById(R.id.tv_message);
        Button btn_cancel = customLayout.findViewById(R.id.btn_cancel);

        btn_cancel.setVisibility(View.GONE);

        tv_title.setText(title);
        tv_message.setText(message);

        final AlertDialog dialog
                = builder.create();
        dialog.setCancelable(false);

        ColorDrawable back = new ColorDrawable(Color.TRANSPARENT);
        InsetDrawable inset = new InsetDrawable(back, 60, 0, 60, 0);
        if (dialog.getWindow() != null) {
            dialog.getWindow().setBackgroundDrawable(inset);

        }

        btn_ok.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                dialog.dismiss();
                onBackPressed();
            }
        });

        btn_cancel.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                dialog.dismiss();
                onBackPressed();
            }
        });


        // create and show
        // the alert dialog

        dialog.show();
    }

    //Hide navigation and everything in full screen mode
    private void hideSystemUi() {
        getWindow().clearFlags(WindowManager.LayoutParams.FLAG_FORCE_NOT_FULLSCREEN);
        getWindow().setFlags(WindowManager.LayoutParams.FLAG_FULLSCREEN,
                WindowManager.LayoutParams.FLAG_FULLSCREEN);
       /* decorView.setSystemUiVisibility(
                View.SYSTEM_UI_FLAG_IMMERSIVE
                        // Set the content to appear under the system bars so that the
                        // content doesn't resize when the system bars hide and show.
                        | View.SYSTEM_UI_FLAG_LAYOUT_STABLE
                        | View.SYSTEM_UI_FLAG_LAYOUT_HIDE_NAVIGATION
                        | View.SYSTEM_UI_FLAG_LAYOUT_FULLSCREEN
                        // Hide the nav bar and status bar
                        | View.SYSTEM_UI_FLAG_HIDE_NAVIGATION
                        |View.STATUS_BAR_HIDDEN
                        | View.SYSTEM_UI_FLAG_FULLSCREEN);*/

        if(Build.VERSION.SDK_INT < 19){

            decorView.setSystemUiVisibility(View.GONE);
        } else {
            //for higher api versions.

            int uiOptions = View.SYSTEM_UI_FLAG_HIDE_NAVIGATION | View.SYSTEM_UI_FLAG_IMMERSIVE_STICKY;
            decorView.setSystemUiVisibility(uiOptions);
        }
    }

}