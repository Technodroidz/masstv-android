package com.example.livestreamingapp.webservice;

import com.example.livestreamingapp.model.AddPhoneResponse;
import com.example.livestreamingapp.model.BankAccountModel;
import com.example.livestreamingapp.model.CapturePlayBackTimeResponse;
import com.example.livestreamingapp.model.CategoryListResponse;
import com.example.livestreamingapp.model.CustomerModelStripe;
import com.example.livestreamingapp.model.FcmUploadModel;
import com.example.livestreamingapp.model.LiveShow;
import com.example.livestreamingapp.model.MyVideoDetailResponse;
import com.example.livestreamingapp.model.NotificationResponse;
import com.example.livestreamingapp.model.PaymentIntentModel;
import com.example.livestreamingapp.model.Phone;
import com.example.livestreamingapp.model.LoginResponse;
import com.example.livestreamingapp.model.SavedCardResponse;
import com.example.livestreamingapp.model.SubscriptionPlanResponse;
import com.example.livestreamingapp.model.SubscriptionResponse;
import com.example.livestreamingapp.model.TrueLiveVideoResponse;
import com.example.livestreamingapp.model.UserUploadVideoDetails;
import com.example.livestreamingapp.model.VideoByIdResponse;
import com.example.livestreamingapp.model.VideoListResponse;
import com.example.livestreamingapp.model.VideoUploadResponse;
import com.example.livestreamingapp.model.WatchLaterResponse;
import com.example.livestreamingapp.model.homescreen.HomeScreenResponse;
import com.example.livestreamingapp.model.mastercategory.CategoryMasterListResponse;
import com.example.livestreamingapp.model.query.QueryResponseModel;
import com.example.livestreamingapp.model.query.QuerySubmitResponseModel;
import com.example.livestreamingapp.model.webseries.WebSeriesResponse;
import com.example.livestreamingapp.utils.NotificationMessage;

import java.util.Map;

import okhttp3.MultipartBody;
import okhttp3.RequestBody;
import okhttp3.ResponseBody;
import retrofit2.Call;
import retrofit2.http.FieldMap;
import retrofit2.http.FormUrlEncoded;
import retrofit2.http.GET;
import retrofit2.http.Headers;
import retrofit2.http.Multipart;
import retrofit2.http.POST;
import retrofit2.http.Part;

public interface APIInterface {

    @POST("login")
    @FormUrlEncoded
    Call<LoginResponse> login(@FieldMap Map<String, String> params);

    @POST("googlelogin")
    @FormUrlEncoded
    Call<LoginResponse> loginGoogle(@FieldMap Map<String, String> params);


    @POST("verifyOTP")
    @FormUrlEncoded
    Call<LoginResponse> verifyOtp(@FieldMap Map<String, String> params);

    @POST("addphone")
    @FormUrlEncoded
    Call<AddPhoneResponse> addPhoneNumber(@FieldMap Map<String, String> credential);


    @POST("verifyphone")
    @FormUrlEncoded
    Call<LoginResponse> verifyOtpPhone(@FieldMap Map<String, String> credential);

    @GET("livevideo")
    Call<LiveShow> getLiveVideo();

    @GET("streamingurl")
    Call<TrueLiveVideoResponse> getTruLiveVideo();

    @GET("checklivevideo")
    Call<LiveShow> checkLiveVideo();

    @POST("videolist")
    @FormUrlEncoded
    Call<VideoListResponse> getOnDemandVideoList(@FieldMap Map<String, String> category);

    @POST("watchlist")
    @FormUrlEncoded
    Call<WatchLaterResponse> addToWatchList(@FieldMap Map<String, String> category);


    @POST("fetchwatchlist")
    @FormUrlEncoded
    Call<VideoListResponse> getWatchListVideos(@FieldMap Map<String, String> category);

    @POST("captureplaybacktime")
    @FormUrlEncoded
    Call<CapturePlayBackTimeResponse> capturePlayBackTime(@FieldMap Map<String, String> category);

    @POST("videolist")
    @FormUrlEncoded
    Call<MyVideoDetailResponse> getVideoList(@FieldMap Map<String, String> category);

    @POST("videobyid")
    @FormUrlEncoded
    Call<VideoByIdResponse> getVideoById(@FieldMap Map<String, String> category);

    @POST("fetchuservideodetails")
    @FormUrlEncoded
    Call<MyVideoDetailResponse> getMyVideoList(@FieldMap Map<String, String> category);


    @POST("searchresult")
    @FormUrlEncoded
    Call<VideoListResponse> getOnDemandSearchVideoList(@FieldMap Map<String, String> category);


    @Headers({"Accept: application/json"})
    @Multipart
    @POST("uploadvideos")
    Call<VideoUploadResponse> uploadFile(
            @Part("user_id") RequestBody user_id,
            @Part("title") RequestBody title,
            @Part("subtitle") RequestBody subtitle,
            @Part MultipartBody.Part thumb,
            @Part("category") RequestBody category,
            @Part("duration") RequestBody duration,
            @Part("tags") RequestBody tags,
            @Part MultipartBody.Part file);



    @GET("fetchcategories")
    Call<CategoryListResponse> fetchOnlyCategory();


    @POST("fetchhomescreendata")
    @FormUrlEncoded
    Call<HomeScreenResponse> fetchAllVideoData(@FieldMap Map<String, String> userId);



    @POST("contentdetail")
    @FormUrlEncoded
    Call<WebSeriesResponse> fetchWebSeriesData(@FieldMap Map<String, String> userId);


    @GET("fetchmastercategories")
    Call<CategoryListResponse> getCategoryList();

    @GET("fetchmastercategories")
    Call<CategoryMasterListResponse> getMasterCategoryList();

    @POST("searchhint")
    @FormUrlEncoded
    Call<CategoryMasterListResponse> getHintList(@FieldMap Map<String, String> userId);


    @POST("")
    @FormUrlEncoded
    Call<SubscriptionResponse> get(@FieldMap Map<String, String> userId);

    @GET("fetchsubscriptiondetails")
    Call<SubscriptionPlanResponse> getSubscriptionPlan();


    @POST("editprofile")
    @FormUrlEncoded
    Call<LoginResponse> editProfile(@FieldMap Map<String, String> profileInfo);

    @POST("fetchuseruploadedvideodetails")
    @FormUrlEncoded
    Call<UserUploadVideoDetails> getUserVideoDetails(@FieldMap Map<String, String> profileInfo);


    @POST("captureuserquery")
    @FormUrlEncoded
    Call<QuerySubmitResponseModel> submitQuery(@FieldMap Map<String, String> profileInfo);

    @POST("getuserquerydetails")
    @FormUrlEncoded
    Call<QueryResponseModel> getQuery(@FieldMap Map<String, String> profileInfo);


    @POST("paymentprocess")
    @FormUrlEncoded
    Call<PaymentIntentModel> getPaymentIntent(@FieldMap Map<String, String> amount);

    @POST("getpaymentmethods")
    @FormUrlEncoded
    Call<SavedCardResponse> getMySavedCardList(@FieldMap Map<String, String> subscription);

    @POST("getcustomerid")
    @FormUrlEncoded
    Call<CustomerModelStripe> getStripeCustomerId(@FieldMap Map<String, String> customer);

    @POST("verifysubscription")
    @FormUrlEncoded
    Call<SubscriptionResponse> verifyPayment(@FieldMap Map<String, String> userId);

    @POST("userbankdetails")
    @FormUrlEncoded
    Call<LoginResponse> updateAccountDetails(@FieldMap Map<String, String> bank);

    @POST("fetchuseraccount")
    @FormUrlEncoded
    Call<BankAccountModel> fetchUserAccount(@FieldMap Map<String, String> bank);

    @POST("userfcmtoken")
    @FormUrlEncoded
    Call<FcmUploadModel> uploadFcmToken(@FieldMap Map<String, String> uploadToken);

    @POST("applypromocode")
    @FormUrlEncoded
    Call<SubscriptionPlanResponse> applySubscription(@FieldMap Map<String, String> uploadToken);

    @POST("ratevideos")
    @FormUrlEncoded
    Call<ResponseBody> updateRating(@FieldMap Map<String, Integer> rateVideo);

    @POST("notification")
    @FormUrlEncoded
    Call<NotificationResponse> getNotification(@FieldMap Map<String, String> notification);

    @POST("deletenotification")
    @FormUrlEncoded
    Call<ResponseBody> deleteNotificaiton(@FieldMap Map<String, String> notification);

    @POST("useranalyticsdata")
    @FormUrlEncoded
    Call<ResponseBody> sendUserAnalyticsData(@FieldMap Map<String, String> notification);
}
