package com.example.livestreamingapp.webservice;

import android.content.Context;
import android.util.Log;

import com.example.livestreamingapp.model.CategoryListResponse;
import com.example.livestreamingapp.model.FcmUploadModel;
import com.example.livestreamingapp.utils.SharedPreferenceHandler;
import com.google.gson.Gson;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.Map;

import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;

public class FcmService {

    private final Context context;
    private APIInterface apiInterface;

    public FcmService(Context context){
        this.context = context;
        apiInterface = APIClient.getClient().create(APIInterface.class);
    }

    public void uploadFcmToken(String userId,String token) {
        Map<String,String> categoryMap = new HashMap<>();
        categoryMap.put(WebServiceConstant.USER_ID,userId);
        categoryMap.put(WebServiceConstant.FCM_TOKEN,token);
        Call<FcmUploadModel> uploadFcmToek = apiInterface.uploadFcmToken(categoryMap);
        uploadFcmToek.enqueue(new Callback<FcmUploadModel>() {
            @Override
            public void onResponse(Call<FcmUploadModel> call, Response<FcmUploadModel> response) {

                if(response.body() != null && response.body().getSuccess()!=null) {

                    new SharedPreferenceHandler(context).saveFcmToken(null);
                }
            }

            @Override
            public void onFailure(Call<FcmUploadModel> call, Throwable t) {
   // categoryListListener.onCategoryFetchingError(""+t.getMessage());
            }
        });

    }
}

