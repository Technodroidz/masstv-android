package com.example.livestreamingapp.webservice;

import android.content.Context;

import com.example.livestreamingapp.model.ProfileData;
import com.example.livestreamingapp.model.homescreen.CategoryModel;
import com.example.livestreamingapp.model.homescreen.HomeScreenData;
import com.example.livestreamingapp.model.homescreen.HomeScreenResponse;
import com.example.livestreamingapp.utils.SharedPreferenceHandler;

import java.io.IOException;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;

public class GenericResponseClass {
    private final APIInterface apiInterface;
    private final Context context;

    private GenericApiListener genericApiListener;

    public interface GenericApiListener {
        void onSuccess(HomeScreenData response);

        void onError(String error);
    }

    public GenericResponseClass(Context context, GenericApiListener genericApiListener) {
        apiInterface = APIClient.getClient().create(APIInterface.class);
        this.genericApiListener = genericApiListener;
        this.context = context;
    }

    public void FetchAllVideoData() {
        ProfileData profileData = new SharedPreferenceHandler(context).getProfileInfo();
        Map<String, String> map = new HashMap<>();
        map.put("user_id", "" + profileData.getId());
        final Call<HomeScreenResponse> videoListCall = apiInterface.fetchAllVideoData(map);
        videoListCall.enqueue(new Callback<HomeScreenResponse>() {
            @Override
            public void onResponse(Call<HomeScreenResponse> call, Response<HomeScreenResponse> response) {

                if (response.code() == 200) {

                    HomeScreenData homeScreenData = response.body().getData();
                    if(homeScreenData!=null && homeScreenData.getCategoryData()!=null){
                        homeScreenData.getCategoryData().setName("New Releases");
                        homeScreenData.getCategory_list().add(0,homeScreenData.getCategoryData());
                    }

                    genericApiListener.onSuccess(homeScreenData);
                } else if (response.code() == 404) {
                    genericApiListener.onSuccess(null);
                } else if (response.code() == 500) {
                    genericApiListener.onError("Maintenance break !Please try again after some time!");
                } else {

                    try {
                        FetchAllVideoData();
                    } catch (Exception e) {
                        e.printStackTrace();
                    }
                }

            }

            @Override
            public void onFailure(Call<HomeScreenResponse> call, Throwable t) {
                genericApiListener.onError("" + t.getMessage());
            }
        });

    }
}
