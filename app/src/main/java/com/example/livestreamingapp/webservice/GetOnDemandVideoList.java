package com.example.livestreamingapp.webservice;

import android.content.Context;
import android.util.Log;

import com.example.livestreamingapp.model.CapturePlayBack;
import com.example.livestreamingapp.model.CapturePlayBackTimeResponse;
import com.example.livestreamingapp.model.MyVideoDetailResponse;
import com.example.livestreamingapp.model.OnDemandVideo;
import com.example.livestreamingapp.model.ProfileData;
import com.example.livestreamingapp.model.VideoListResponse;
import com.example.livestreamingapp.utils.SharedPreferenceHandler;
import com.google.gson.Gson;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;

public class GetOnDemandVideoList {

    public interface onDemandVideoListListener{
        public void onDemandFetchedVideoList(List<OnDemandVideo> videoData);
        public void onDemandFetchingError(String error);
    }

    public interface videoListListener{
        public void onDemandFetchedVideoList(List<MyVideoDetailResponse.MyVideoDetails> videoData);
        public void onDemandFetchingError(String error);
    }

    public interface onSuccessCaptureListener{
        public void onSuccess(CapturePlayBack data);
        public void onError(String error);
    }
    private onSuccessCaptureListener successCaptureListener ;
    private APIInterface apiInterface;
    private onDemandVideoListListener onDemandVideoListListener;
    private videoListListener videoListListener ;

    private Context context;

    public  GetOnDemandVideoList(Context context,
                                 onSuccessCaptureListener successCaptureListener){
        apiInterface = APIClient.getClient().create(APIInterface.class);
        this.successCaptureListener = successCaptureListener;
        this.context = context;
    }

    public  GetOnDemandVideoList(Context context,
                                 onDemandVideoListListener onDemandVideoListListener){
        apiInterface = APIClient.getClient().create(APIInterface.class);
        this.onDemandVideoListListener = onDemandVideoListListener;
        this.context = context;
    }

    public  GetOnDemandVideoList(Context context,
                                 videoListListener videoListListener){
        apiInterface = APIClient.getClient().create(APIInterface.class);
        this.videoListListener = videoListListener;
        this.context = context;
    }



    public  void GetVideoList(String category,int index,int userId) {
        Map<String,String> categoryMap = new HashMap<>();
        categoryMap.put(WebServiceConstant.CATEGORY_NAME,category);
        categoryMap.put(WebServiceConstant.INDEX,""+index);
        categoryMap.put(WebServiceConstant.OFFSET,"15");
        categoryMap.put(WebServiceConstant.USER_ID,""+userId);
        final Call<VideoListResponse> videoListCall = apiInterface.getOnDemandVideoList(categoryMap);
        videoListCall.enqueue(new Callback<VideoListResponse>() {
            @Override
            public void onResponse(Call<VideoListResponse> call, Response<VideoListResponse> response) {

                if(response!=null && response.body()!=null && response.body().getSuccess()){
                  String jsonString = new Gson().toJson(response.body());
                    VideoListResponse videoListResponse = (VideoListResponse) new Gson().fromJson(jsonString,
                            VideoListResponse.class);

                    onDemandVideoListListener.onDemandFetchedVideoList(videoListResponse.getData());
                }else {
                    if(response!=null && response.body()!=null && response.body().getMessage()!=null){
                        onDemandVideoListListener.onDemandFetchingError(response.body().getMessage());

                    }else {
                        onDemandVideoListListener.onDemandFetchingError("SomeThing went wrong! Please try again!");
                    }
                }

            }

            @Override
            public void onFailure(Call<VideoListResponse> call, Throwable t) {

                onDemandVideoListListener.onDemandFetchingError(""+t.getMessage());
            }
        });

    }

    public  void GetSearchVideoList(String category,int index,int userId) {
        Map<String,String> categoryMap = new HashMap<>();
        categoryMap.put(WebServiceConstant.SEARCH_NAME,category);
        categoryMap.put(WebServiceConstant.INDEX,""+index);
        categoryMap.put(WebServiceConstant.OFFSET,"15");
        categoryMap.put(WebServiceConstant.USER_ID,""+userId);
        final Call<VideoListResponse> videoListCall = apiInterface.getOnDemandSearchVideoList(categoryMap);
        videoListCall.enqueue(new Callback<VideoListResponse>() {
            @Override
            public void onResponse(Call<VideoListResponse> call, Response<VideoListResponse> response) {

                if(response!=null && response.body()!=null && response.body().getMessage()!=null){
                    if(response.body().getSuccess()){
                        String jsonString = new Gson().toJson(response.body());
                        VideoListResponse videoListResponse = (VideoListResponse) new Gson().fromJson(jsonString,
                                VideoListResponse.class);

                        onDemandVideoListListener.onDemandFetchedVideoList(videoListResponse.getData());
                    }else {
                        onDemandVideoListListener.onDemandFetchingError(response.body().getMessage());
                    }
                }else if(response!=null && response.code()==404){
                    onDemandVideoListListener.onDemandFetchedVideoList(new ArrayList<>());

                }else {
                    onDemandVideoListListener.onDemandFetchedVideoList(new ArrayList<>());

                }


            }

            @Override
            public void onFailure(Call<VideoListResponse> call, Throwable t) {

                onDemandVideoListListener.onDemandFetchingError(""+t.getMessage());
            }
        });

    }

    public  void GetWatchList() {
        ProfileData profileData = new SharedPreferenceHandler(context).getProfileInfo();
        Map<String,String> categoryMap = new HashMap<>();
        categoryMap.put(WebServiceConstant.USER_ID,""+profileData.getId());
        final Call<VideoListResponse> videoListCall = apiInterface.getWatchListVideos(categoryMap);
        videoListCall.enqueue(new Callback<VideoListResponse>() {
            @Override
            public void onResponse(Call<VideoListResponse> call, Response<VideoListResponse> response) {
                if(response!=null && response.body()!=null && response.body().getSuccess()){
                    String jsonString = new Gson().toJson(response.body());
                    VideoListResponse videoListResponse = (VideoListResponse) new Gson().fromJson(jsonString,
                            VideoListResponse.class);

                    onDemandVideoListListener.onDemandFetchedVideoList(videoListResponse.getData());
                }else {
                    if(response!=null && response.body()!=null && response.body().getMessage()!=null){
                        onDemandVideoListListener.onDemandFetchingError(response.body().getMessage());
                    }else {
                        onDemandVideoListListener.onDemandFetchingError("SomeThing went wrong! Please try again!");

                    }
                }

            }

            @Override
            public void onFailure(Call<VideoListResponse> call, Throwable t) {

                onDemandVideoListListener.onDemandFetchingError(""+t.getMessage());
            }
        });

    }

    public  void getMyVideoList() {
        ProfileData profileData = new SharedPreferenceHandler(context).getProfileInfo();
        Map<String,String> categoryMap = new HashMap<>();
        categoryMap.put(WebServiceConstant.USER_ID,""+profileData.getId());
        final Call<MyVideoDetailResponse> videoListCall = apiInterface.getMyVideoList(categoryMap);
        videoListCall.enqueue(new Callback<MyVideoDetailResponse>() {
            @Override
            public void onResponse(Call<MyVideoDetailResponse> call, Response<MyVideoDetailResponse> response) {
                if(response!=null && response.body()!=null && response.body().getSuccess()){
                    String jsonString = new Gson().toJson(response.body());
                    MyVideoDetailResponse videoListResponse = (MyVideoDetailResponse) new Gson().fromJson(jsonString,
                            MyVideoDetailResponse.class);

                    videoListListener.onDemandFetchedVideoList(videoListResponse.getData());
                }else {
                    if(response!=null && response.body()!=null && response.body().getMessage()!=null){
                        videoListListener.onDemandFetchingError(response.body().getMessage());
                    }else {
                        videoListListener.onDemandFetchedVideoList(new ArrayList<>());

                    }
                }

            }

            @Override
            public void onFailure(Call<MyVideoDetailResponse> call, Throwable t) {

                videoListListener.onDemandFetchingError(""+t.getMessage());
            }
        });

    }

    public  void capturePlayBackTime(int video_id,String time) {
        ProfileData profileData = new SharedPreferenceHandler(context).getProfileInfo();
        Map<String,String> categoryMap = new HashMap<>();
        categoryMap.put(WebServiceConstant.USER_ID,""+profileData.getId());
        categoryMap.put(WebServiceConstant.VIDEO_ID,""+video_id);
        categoryMap.put(WebServiceConstant.PLAYBACK_TIME,""+time);
        final Call<CapturePlayBackTimeResponse> videoListCall = apiInterface.capturePlayBackTime(categoryMap);
        videoListCall.enqueue(new Callback<CapturePlayBackTimeResponse>() {
            @Override
            public void onResponse(Call<CapturePlayBackTimeResponse> call, Response<CapturePlayBackTimeResponse> response) {
                if(response!=null && response.body()!=null && response.body().getSuccess()){
                    String jsonString = new Gson().toJson(response.body());
                    CapturePlayBackTimeResponse videoListResponse = (CapturePlayBackTimeResponse) new Gson().fromJson(jsonString,
                            CapturePlayBackTimeResponse.class);

                    successCaptureListener.onSuccess(videoListResponse.getData());
                }else {
                    if(response!=null && response.body()!=null && response.body().getMessage()!=null){
                        successCaptureListener.onError(response.body().getMessage());
                    }else {
                        successCaptureListener.onError("SomeThing went wrong! Please try again!");

                    }
                }

            }

            @Override
            public void onFailure(Call<CapturePlayBackTimeResponse> call, Throwable t) {

                successCaptureListener.onError(""+t.getMessage());
            }
        });

    }


}
