package com.example.livestreamingapp.webservice;

import android.content.Context;
import android.graphics.Shader;
import android.util.Log;

import com.example.livestreamingapp.model.CategoryData;
import com.example.livestreamingapp.model.CategoryListResponse;
import com.example.livestreamingapp.model.OnDemandCategory;
import com.example.livestreamingapp.model.OnDemandVideo;
import com.example.livestreamingapp.model.ProfileData;
import com.example.livestreamingapp.model.homescreen.HomeScreenData;
import com.example.livestreamingapp.model.homescreen.HomeScreenResponse;
import com.example.livestreamingapp.model.mastercategory.CategoryMasterListResponse;
import com.example.livestreamingapp.model.mastercategory.OnDemandMasterCategory;
import com.example.livestreamingapp.utils.SharedPreferenceHandler;
import com.google.gson.Gson;

import java.io.IOException;
import java.util.ArrayList;
import java.util.Collections;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;

public class GetVideoCategoryList {

    Context context;

    public interface onDemandCategoryListListener{
        public void onFetchedCategoryList(CategoryData videoData);
        public void onFetchedMasterCategoryList(List<OnDemandMasterCategory> categoryList);
        public void onCategoryFetchingError(String error);

    }



    private APIInterface apiInterface;
    private onDemandCategoryListListener categoryListListener;


    public GetVideoCategoryList(Context context,
                                onDemandCategoryListListener categoryListListener){
        apiInterface = APIClient.getClient().create(APIInterface.class);
        this.categoryListListener = categoryListListener;
        this.context = context;
    }





    public void GetCategoryList() {

        final Call<CategoryListResponse> videoListCall = apiInterface.fetchOnlyCategory();
        videoListCall.enqueue(new Callback<CategoryListResponse>() {
            @Override
            public void onResponse(Call<CategoryListResponse> call, Response<CategoryListResponse> response) {

                if(response.code()==200){

                    categoryListListener.onFetchedCategoryList(response.body().getData());
                }else if(response.code()==404){
                    categoryListListener.onFetchedCategoryList(null);
                }else if(response.code()==500){
                    categoryListListener.onCategoryFetchingError("Maintenance break !Please try again after some time!");
                }else {

                   /* try {
                        videoListCall.clone().execute();
                    } catch (IOException e) {
                        e.printStackTrace();
                    }*/
                }

            }

            @Override
            public void onFailure(Call<CategoryListResponse> call, Throwable t) {
                try {
                    categoryListListener.onCategoryFetchingError(""+t.getMessage());

                }catch (Exception e){

                }
            }
        });

    }

    public void GetMasterCategoryList() {

        final Call<CategoryMasterListResponse> videoListCall = apiInterface.getMasterCategoryList();
        videoListCall.enqueue(new Callback<CategoryMasterListResponse>() {
            @Override
            public void onResponse(Call<CategoryMasterListResponse> call, Response<CategoryMasterListResponse> response) {


                if(response.code()==200){
                  /*  if(response.body().getSuccess()){
                        CategoryListResponse categoryListResponse =
                                new Gson().fromJson(new Gson().toJson(response.body()),CategoryListResponse.class);
                        categoryListListener.onFetchedCategoryList(categoryListResponse.getData());
                    }else {
                        categoryListListener.onCategoryFetchingError(response.body().getMessage());
                    }*/
                    CategoryMasterListResponse categoryListResponse =
                            new Gson().fromJson(new Gson().toJson(response.body()),CategoryMasterListResponse.class);
                    categoryListListener.onFetchedMasterCategoryList(categoryListResponse.getData());
                }else if(response.code()==404){
                    categoryListListener.onCategoryFetchingError("No Categories found!");
                }else if(response.code()==500){
                    categoryListListener.onCategoryFetchingError("Maintenance break !Please try again after some time!");
                }else {

                   /* try {
                        videoListCall.clone().execute();
                    } catch (IOException e) {
                        e.printStackTrace();
                    }*/
                }



            }

            @Override
            public void onFailure(Call<CategoryMasterListResponse> call, Throwable t) {

                categoryListListener.onCategoryFetchingError(""+t.getMessage());
            }
        });

    }

    public void GetSearchHint(String search,int index) {
        Map<String,String> categoryMap = new HashMap<>();
        categoryMap.put(WebServiceConstant.SEARCH_NAME,search);


        final Call<CategoryMasterListResponse> videoListCall = apiInterface.getHintList(categoryMap);
        videoListCall.enqueue(new Callback<CategoryMasterListResponse>() {
            @Override
            public void onResponse(Call<CategoryMasterListResponse> call, Response<CategoryMasterListResponse> response) {

                if(response.body() != null && response.body().getSuccess()!=null){
                    if(response.body().getSuccess() && response.code()==200){
                            categoryListListener.onFetchedMasterCategoryList(response.body().getData());
                    }else {
                        categoryListListener.onFetchedCategoryList(null);
                    }
                }else {
                    categoryListListener.onCategoryFetchingError("Not found!");
                }

            }

            @Override
            public void onFailure(Call<CategoryMasterListResponse> call, Throwable t) {

                categoryListListener.onCategoryFetchingError(""+t.getMessage());
            }
        });

    }

}
