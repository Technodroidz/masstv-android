package com.example.livestreamingapp.webservice;

import android.content.Context;
import android.util.Log;
import android.widget.TextView;

import com.example.livestreamingapp.model.MySubscription;
import com.example.livestreamingapp.model.OnDemandVideo;
import com.example.livestreamingapp.model.ProfileData;
import com.example.livestreamingapp.model.SubscriptionPlan;
import com.example.livestreamingapp.model.SubscriptionPlanResponse;
import com.example.livestreamingapp.model.SubscriptionResponse;
import com.example.livestreamingapp.model.VideoListResponse;
import com.example.livestreamingapp.utils.SharedPreferenceHandler;
import com.google.gson.Gson;

import java.util.HashMap;
import java.util.List;
import java.util.Map;

import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;

public class SubscriptionService {

    private final Context context;
    private onGetSubscriptionPlanListener onSubscriptionListener;

    public interface onGetSubscriptionPlanListener{
        public void onGetSubscriptionPlan(List<SubscriptionPlan> subscriptionPlan, String error);
    }

    public interface onSubscriptionBuyListener{
        public void onSubscriptionSuccessful(List<OnDemandVideo> videoData);
        public void onSubscriptionFailure(String error);
    }

    public interface onSubscriptionInfoListener{
        public void onSubscriptionInfo(MySubscription subscriptionInfo);
        public void onFailure(String error);
    }

    private APIInterface apiInterface;
    private onSubscriptionBuyListener onSubscriptionBuyListener;
    private onSubscriptionInfoListener onSubscriptionInfoListener;

    public SubscriptionService(Context context
                              ){
        this.context = context;
        apiInterface = APIClient.getClient().create(APIInterface.class);
        this.onSubscriptionInfoListener = onSubscriptionInfoListener;
    }

    public  void getMySubscriptionInfo(onSubscriptionInfoListener onSubscriptionListener) {
       /* this.onSubscriptionInfoListener = onSubscriptionListener;
        ProfileData profileData = new SharedPreferenceHandler(context).getProfileInfo();
        final Map<String,String> subscription = new HashMap<>();
        subscription.put(WebServiceConstant.USER_ID,""+profileData.getId());
        final Call<SubscriptionResponse> videoListCall = apiInterface.getMySubscription(subscription);
        videoListCall.enqueue(new Callback<SubscriptionResponse>() {
            @Override
            public void onResponse(Call<SubscriptionResponse> call, Response<SubscriptionResponse> response) {

                if(response.code()==200){
                    if(response.body().getSuccess()){
                        String jsonString = new Gson().toJson(response.body());
                        SubscriptionResponse subscriptionResponse = (SubscriptionResponse) new Gson().fromJson(jsonString,
                                SubscriptionResponse.class);
                        onSubscriptionInfoListener.onSubscriptionInfo(subscriptionResponse.getSubscription());
                    }else {
                        onSubscriptionInfoListener.onSubscriptionInfo(null);
                    }
                }else {
                    onSubscriptionInfoListener.onSubscriptionInfo(null);
                }



            }

            @Override
            public void onFailure(Call<SubscriptionResponse> call, Throwable t) {

                onSubscriptionInfoListener.onFailure(""+t.getMessage());
            }
        });*/

    }

    public void verifyPayment(String userId,
                              String subscription_id,
                            String payment_id,
                              String clientKey,
                              String method_id,
                              String status,
                              onSubscriptionInfoListener onSubscriptionListener) {
        this.onSubscriptionInfoListener = onSubscriptionListener;
        ProfileData profileData = new SharedPreferenceHandler(context).getProfileInfo();
        final Map<String,String> subscription = new HashMap<>();
        subscription.put(WebServiceConstant.USER_ID,""+userId);
        subscription.put(WebServiceConstant.SUBSCRIPTION_ID,""+subscription_id);
        subscription.put(WebServiceConstant.CLIENT_SECRET_KEY,""+clientKey);
        subscription.put(WebServiceConstant.PAYMENT_ID,""+payment_id);
        subscription.put(WebServiceConstant.PAYMENT_METHOD_STATUS,status);
        subscription.put(WebServiceConstant.PAYMENT_METHOD_ID,method_id);
        final Call<SubscriptionResponse> videoListCall = apiInterface.verifyPayment(subscription);
        videoListCall.enqueue(new Callback<SubscriptionResponse>() {
            @Override
            public void onResponse(Call<SubscriptionResponse> call, Response<SubscriptionResponse> response) {

                if(response.code()==200){
                    if(response.body().getSuccess()){
                        String jsonString = new Gson().toJson(response.body());
                        SubscriptionResponse subscriptionResponse = (SubscriptionResponse) new Gson().fromJson(jsonString,
                                SubscriptionResponse.class);
                        onSubscriptionInfoListener.onSubscriptionInfo(subscriptionResponse.getSubscription());
                    }else {
                        onSubscriptionInfoListener.onFailure(response.body().getMessage());
                    }
                }else {
                    onSubscriptionInfoListener.onFailure("SomeThing went wrong!\nPlease try again later");
                }



            }

            @Override
            public void onFailure(Call<SubscriptionResponse> call, Throwable t) {

                onSubscriptionInfoListener.onFailure(""+t.getMessage());
            }
        });

    }

    public  void getSubscriptionPlan(onGetSubscriptionPlanListener onSubscriptionListener) {
        this.onSubscriptionListener = onSubscriptionListener;

        final Call<SubscriptionPlanResponse> videoListCall = apiInterface.getSubscriptionPlan();
        videoListCall.enqueue(new Callback<SubscriptionPlanResponse>() {
            @Override
            public void onResponse(Call<SubscriptionPlanResponse> call, Response<SubscriptionPlanResponse> response) {

                if(response.code()==200){
                    if(response.body().getSuccess()){
                        String jsonString = new Gson().toJson(response.body());
                        SubscriptionPlanResponse subscriptionResponse = (SubscriptionPlanResponse) new Gson().fromJson(jsonString,
                                SubscriptionPlanResponse.class);
                        onSubscriptionListener.onGetSubscriptionPlan(subscriptionResponse.getData(),"");
                    }else {
                        onSubscriptionListener.onGetSubscriptionPlan(null,response.body().getMessage());
                    }
                }else {
                    onSubscriptionListener.onGetSubscriptionPlan(null,"SomeThing went wrong!\nPlease try again later");
                }



            }

            @Override
            public void onFailure(Call<SubscriptionPlanResponse> call, Throwable t) {

                onSubscriptionListener.onGetSubscriptionPlan(null,"SomeThing went wrong!\nPlease try again later");
            }
        });

    }

}
