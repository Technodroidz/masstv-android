package com.example.livestreamingapp.webservice;

import android.content.Context;

import com.example.livestreamingapp.model.ProfileData;
import com.example.livestreamingapp.model.WatchLaterResponse;
import com.example.livestreamingapp.utils.SharedPreferenceHandler;

import java.util.HashMap;
import java.util.Map;

import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;

public class WatchLaterService {

    public interface OnWatchLaterResponse{
        public void onResponse(WatchLaterResponse response);
        public void onError(String error);
    }

    private APIInterface apiInterface;
    private OnWatchLaterResponse onWatchLaterResponse;
    private Context context;
    public WatchLaterService(Context context,
                             OnWatchLaterResponse onWatchLaterResponse){
        apiInterface = APIClient.getClient().create(APIInterface.class);
        this.onWatchLaterResponse = onWatchLaterResponse;
        this.context = context;
    }

    public String getTableType(String type){
        if(type.equalsIgnoreCase("what's your mass?")){
            return "whatsyourmass";
        }else if(type.equalsIgnoreCase("web series")){
            return "webseries";
        }else {
            return "video";
        }
    }

    public  void addToWatchListVideo(int index,String type,String category) {
        Map<String,String> categoryMap = new HashMap<>();
        ProfileData profileData = new SharedPreferenceHandler(context).getProfileInfo();
        categoryMap.put(WebServiceConstant.USER_ID,""+profileData.getId());
        categoryMap.put(WebServiceConstant.VIDEO_ID,""+index);
        categoryMap.put(WebServiceConstant.VALUE,type);
        categoryMap.put(WebServiceConstant.VIDEO_TYPE,getTableType(category));
        final Call<WatchLaterResponse> videoListCall = apiInterface.addToWatchList(categoryMap);
        videoListCall.enqueue(new Callback<WatchLaterResponse>() {
            @Override
            public void onResponse(Call<WatchLaterResponse> call, Response<WatchLaterResponse> response) {

                if(response!=null && response.body()!=null && response.body().getSuccess()){

                    onWatchLaterResponse.onResponse(response.body());
                }else {
                    if(response!=null && response.body()!=null && response.body().getMessage()!=null){
                        onWatchLaterResponse.onResponse(response.body());
                    }else {
                        onWatchLaterResponse.onError("SomeThing went wrong! Please try again!");
                    }
                }

            }

            @Override
            public void onFailure(Call<WatchLaterResponse> call, Throwable t) {

                onWatchLaterResponse.onError(""+t.getMessage());
            }
        });

    }

}
